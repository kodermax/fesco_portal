<?
$aMenuLinks = Array(
	Array(
		"Официальные новости", 
		"/about/index.php", 
		Array("/about/official.php"), 
		Array(), 
		"" 
	),
	Array(
		"О компании", 
		"/about/company/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Совет Директоров", 
		"/about/company/governance/",
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Правление", 
		"/about/company/tops.php", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Наша жизнь", 
		"/about/life/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Миссия и стратегия", 
		"/about/company/mission.php", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Политика СМК", 
		"/about/politika-smk/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"История группы", 
		"/about/company/history.php", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Корпоративная культура", 
		"/about/company/style.php", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Контакты", 
		"/about/company/contacts.php", 
		Array(), 
		Array(), 
		"false" 
	),
	Array(
		"Реквизиты", 
		"/about/company/bank_info.php", 
		Array(), 
		Array(), 
		"false" 
	),
	Array(
		"Фотогалерея", 
		"/about/gallery/", 
		Array(), 
		Array(), 
		"CBXFeatures::IsFeatureEnabled('CompanyPhoto')" 
	),
	Array(
		"Видеогалерея", 
		"/about/media.php", 
		Array(), 
		Array(), 
		"CBXFeatures::IsFeatureEnabled('CompanyVideo')" 
	),
	Array(
		"Карьера, вакансии", 
		"/about/career.php", 
		Array("/about/resume.php"), 
		Array(), 
		"CBXFeatures::IsFeatureEnabled('CompanyCareer')" 
	)
);
?>