<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Совет директоров");
?>
<style type="text/css">
    /* раскрывающийся блок */
    div.center-col #popdown-block ul {margin: 0; padding: 0}
    div.center-col #popdown-block ul li {margin: 0; padding: 0; text-indent: 0}
    div.center-col #popdown-block ul li:before {content: ''}
    div.center-col #popdown-block h3 {font-size: 109.9%; text-transform: uppercase; margin: 2.5em 0 .5em; padding:0; padding-left: 15px; }
    div.center-col #popdown-block .popdown-record-opened {background: #f9f9f9}
    div.center-col #popdown-block .popdown-record,
    div.center-col #popdown-block .popdown-record-opened {margin: 0 0 0 0; padding: .75em 1em .75em 15px}
    div.center-col #popdown-block .popdown-record .popdown-toggle,
    div.center-col #popdown-block .popdown-record-opened .popdown-toggle {}
    div.center-col #popdown-block .popdown-record h4 {text-decoration:underline}
    div.center-col #popdown-block .popdown-record-opened h4 {text-decoration:none}
    div.center-col #popdown-block .popdown-record h4,
    div.center-col #popdown-block .popdown-record-opened h4 {font-weight: normal; cursor: pointer; position: relative; margin: 0 0 .25em -9px; padding-left: 9px}
    div.center-col #popdown-block .popdown-record h4 {background: url('/assets/images/plus02.gif') no-repeat 0 50%}
    div.center-col #popdown-block .popdown-record-opened h4 {background: url('/assets/images/minus02.gif') no-repeat 0 50%}
    div.center-col #popdown-block .popdown-record .popdown-lead-text,{font-size: 89.9%; line-height:120%; display:block; position:relative}
    div.center-col #popdown-block .popdown-record-opened .popdown-lead-text {font-size: 89.9%; line-height:120%; display:block; position:relative}
    div.center-col #popdown-block .popdown-record-opened .popdown-main-photo {display: block; float: left; margin-right: 15px}
    #popdown-block.with-photos .popdown-record-opened .popdown-main-text {padding-right: 10px}

    /* вакансии */
    div.center-col #popdown-block .popdown-record table {display: none}
    div.center-col #popdown-block .popdown-record-opened table {margin-top: 1em}
    div.center-col #popdown-block .popdown-record-opened td {vertical-align: top}
    div.center-col #popdown-block .popdown-record-opened li {margin-bottom: .5em}
    div.center-col #popdown-block .popdown-record-opened td.label {font-size: 89.9%; font-weight: bold; white-space: nowrap; padding-right: 3em; color: #58595b;}
    div.center-col #popdown-block .popdown-record-opened tr.candidate-requirements td.label,
    div.center-col #popdown-block .popdown-record-opened tr.candidate-requirements td.value {padding-top: 1em}
    div.center-col #popdown-block .popdown-record-opened tr.rank-staff td.label,
    div.center-col #popdown-block .popdown-record-opened tr.rank-staff td.value {padding-top: 1em}
    div.center-col #popdown-block .popdown-record-opened tr.vacancies-contacts td {font-size: 89.9%}

    /* списки */
    div.center-col #popdown-block .popdown-record .popdown-main-text {display: none}
    div.center-col #popdown-block .popdown-record-opened .popdown-main-text {display: block; margin-top: 1em; position: relative}
</style>
<div class="center-col">
    <div id="popdown-block" class="with-photos">

        <div class="popdown-record">
            <h4>Боуд Клайв Денис</h4>

            <div class="popdown-main-text"> <img class="popdown-main-photo" alt="photo" src="/assets/images/tops/clive.jpg">
                <p>Клайв Дэнис Боуд является партнером TPG. До прихода в TPG в 2006 году, г-н Боуд работал старшим советником Bass family, Fort Worth, штат Техас, до этого являлся партнером юридической фирмы Vinson &amp; Elkins, LLP (Хьюстон, штат Техас).</p>

                <p>Г-н Боуд имеет степень доктора права, окончил с отличием юридический университет Мичигана и университет Окленда. Он также входит в состав Совета директоров компании Petro Harvester Oil.</p>
            </div>

            <div class="clear"></div>
        </div>

        <div class="popdown-record">
            <h4>Грапенгейссер Ханс Густав Якоб</h4>

            <div class="popdown-lead-text"></div>

            <div class="popdown-main-text"><img class="popdown-main-photo" alt="photo" src="/assets/images/tops/grapengiesser.jpg">
                <p>Якоб Грапенгиссер — партнер и глава московского офиса, Ист Кэпитал.</p>

                <p>Якоб является партнером компании «Ист Кэпитал», главой московского офиса и управляет инвестициями в Россию, Турцию и Балканы. До «Ист Кэпитал» Якоб работал аналитиком в компании «Брансвик Имерджинг Маркетс» (Brunswick Emerging Markets) и в Московском представительстве Ханты-мансийской нефтяной корпорации. Он входит в состав Совета директоров сербского медиа-холдинга B92. Якоб окончил Стокгольмскую школу экономики (Stockholm School of Economics), Королевский Технологический Институт (Royal Institute of Technology) и получил квалификацию Магистра делового администрирования (MBA) в Корнельском университете (США) (Cornell University).</p>
            </div>

            <div class="clear"></div>
        </div>


        <div class="popdown-record">
            <h4>Изосимова Наталья Вадимовна</h4>

            <div class="popdown-main-text"><img class="popdown-main-photo" alt="photo" src="/assets/images/tops/izosimova.jpg">
                <p>С&nbsp;января 2014 года Наталья занимается коучингом и&nbsp;консультативной работой, помогая своим клиентам разрабатывать и&nbsp;внедрять системы корпоративного управления, изменения в&nbsp;организационных структурах, ключевые управленческие процессы, стратегии в&nbsp;области HR&nbsp;и&nbsp;коммуникаций.</p>

                <p>С&nbsp;2010 по&nbsp;2013 годы Наталья входила в&nbsp;состав Наблюдательного Совета компании «ДТЭК», возглавляя Комитет по&nbsp;кадрам и&nbsp;вознаграждениям. С&nbsp;2007&nbsp;г. по&nbsp;2013&nbsp;г. госпожа Изосимова возглавляла Фонд «Эффективное управление».</p>

                <p>В&nbsp;1994–2004 годах Наталья работала в&nbsp;McKinsey &amp; Company на&nbsp;должности директора по&nbsp;профессиональному развитию в&nbsp;Восточной Европе.</p>

                <p>Наталья закончила Московский государственный педагогический университет, факультет английского языка, а&nbsp;также OCM (the Oxford School for Coaching and Mentoring), где получила Advanced Diploma. Наталья является членом EMCC (European Mentoring and Coaching Council).</p>
            </div>

            <div class="clear"></div>
        </div>

        <div class="popdown-record">
            <h4>Калинин Дмитрий Вячеславович</h4>

            <div class="popdown-lead-text"></div>

            <div class="popdown-main-text"><img class="popdown-main-photo" alt="photo" src="/assets/images/tops/kalinin.jpg">
                <p>Дмитрий окончил Московский государственный университет по&nbsp;специальности экономист, а&nbsp;также обучался в&nbsp;Хиросаки, Япония.</p>

                <p>Являлся заместителем генерального директора по&nbsp;финансам в&nbsp;международном аэропорте Шереметьево в&nbsp;течение 6 лет. До&nbsp;этого Дмитрий занимал пост финансового директора Транстелеком (разработка <nobr>волоконно-оптических</nobr> линий для российских железных дорог).</p>

                <p>Дмитрий также имеет международный опыт: работал в&nbsp;сфере инвестиционного банкинга и&nbsp;консалтинга в&nbsp;Москве и&nbsp;<nobr>Нью-Йорке</nobr>.</p>
            </div>

            <div class="clear"></div>
        </div>

        <div class="popdown-record">
            <h4>Каяшев Владимир Анатольевич</h4>

            <div class="clear"></div>
        </div>

            <div class="popdown-record">
            <h4>Шайдаев Марат Магомедович</h4>

            <div class="popdown-main-text"><img class="popdown-main-photo" alt="photo" src="/assets/images/tops/shaydaev.jpg">
                <p>Входит в&nbsp;Совет директоров с&nbsp;2013 года.</p>
                <p>Марат Шайдаев начал работу в&nbsp;компаниях группы «Сумма» в&nbsp;2004 году, в&nbsp;сферу его ответственности входила
                    оптимизация бизнеса и&nbsp;развитие перспективных направлений деятельности. В&nbsp;период с&nbsp;2005 по&nbsp;2009 год возглавлял <nobr>ЗАО «Транс-Флот»</nobr>, управляющую компанию порта Приморск. Впоследствии работал в&nbsp;головной компании группы «Сумма», обеспечивая оперативное управление активами.</p>

                <p>С&nbsp;2012 года Марат Шайдаев входит в&nbsp;Совет директоров <nobr>ОАО «Новороссийский морской торговый порт»</nobr>, с&nbsp;2014 года в&nbsp;Совет директоров <nobr>ОАО «Объединенная зерновая компания»</nobr></p>

                <p>Марат Шайдаев окончил Российскую академию государственной службы при Президенте РФ&nbsp;по&nbsp;специальности «государственное и&nbsp;муниципальное управление».</p>
            </div>
            <div class="clear"></div>
        </div>
        <div class="popdown-record">
            <h4>Швец Дмитрий Владимирович</h4>
            <div class="popdown-main-text"><img class="popdown-main-photo" alt="photo" src="/assets/images/tops/shvets.jpg">
                <p>Дмитрий Швец является главой московского филиала инвестиционного фонда TPG Capital и отвечает за деятельность фонда в России и СНГ. Дмитрий является членом Совета директоров и возглавляет комитеты Совета директоров по инвестициям и персоналу розничной компании «Лента». С 2004 по 2008 г.</p>
                <p>Дмитрий Швец возглавлял в ГМК «Норильский никель» Дирекцию по операционной эффективности и отвечал за оптимизацию деятельности существующих активов компании и интеграцию новых покупаемых активов. С 1998 по 2004 г. Дмитрий работал вMcKinsey &amp; Company, где руководил проектами в таких отраслях, как потребительские товары, транспорт, металлургия и горнодобывающая промышленность, нефтедобыча. Он также работал в компании Coca-Cola на различных позициях в области маркетинга. Дмитрий получил степень MBA в университете Эмори в США, а также с отличием закончил Московский государственный институт международных отношений.</p>
            </div>
            <div class="clear"></div>
        </div>
        <div class="popdown-record">
            <h4>Шохин Дмитрий Александрович</h4>
            <div class="popdown-main-text"><img class="popdown-main-photo" alt="photo" src="/assets/images/tops/shokhin.jpg"></div>
            <div class="clear"></div>
        </div>
        <div class="popdown-record">
            <h4>Успенский Андрей Маркович</h4>
            <div class="clear"></div>
        </div>
        <div style="width: 300px; border-top-width: 1px; border-top-style: dashed; border-top-color: rgb(204, 204, 204); line-height: 0.0001em; margin: 1em 0px;"></div>
        <div class="popdown-record">
            <h4 style="color: rgb(153, 153, 153);">Ревизионная комиссия</h4>
            <div class="popdown-main-text">
                <ul>
                    <li><strong>Вид Татьяна Вильгельмовна</strong></li>
                    <li><strong>Лаврик Елена Петровна</strong></li>
                    <li><strong>Рогов Игорь Иванович</strong></li>
                    <li><strong>Тимофеева Ирина Анатольевна</strong></li>
                    <li><strong>Шамкуть Артур Викторович</strong></li>
                </ul>
            </div>
        </div>
<script type="text/javascript">

    var av = document.getElementById('popdown-block').getElementsByTagName('div');
    var aa = document.getElementById('popdown-block').getElementsByTagName('a');

    function initPopdown(id) {
        var box = document.getElementById(id);
    }


    for (var i=0;a = aa[i];i++) {
        if (a.className == 'popdown-toggle')
            a.onclick = function() {
                if (this.parentNode.className == 'popdown-record-opened') {this.parentNode.className = 'popdown-record'; return false}
                for (var j=0;v = av[j];j++) if (v.className == 'popdown-record-opened') v.className = 'popdown-record';
                this.parentNode.parentNode.className = this.parentNode.parentNode.className == 'popdown-record' ? 'popdown-record-opened' : 'popdown-record';
                return false;
            }
    }
    var ah4 = document.getElementById('popdown-block').getElementsByTagName('h4');
    for (var i=0;a = ah4[i];i++) {
        a.onclick = function() {
            if (this.parentNode.className == 'popdown-record-opened') {this.parentNode.className = 'popdown-record'; return false}
            for (var j=0;v = av[j];j++) if (v.className == 'popdown-record-opened') v.className = 'popdown-record';
            this.parentNode.className = this.parentNode.className == 'popdown-record' ? 'popdown-record-opened' : 'popdown-record';
            return false;
        }
    }
</script>
</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>