<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Руководство");
?>
<style>
 .full-size-column {
        width: 750px !important;
    }
    .center-col h1:first-of-type {
        margin-top: 0;
    }
    .center-col h1 {
        font-family: 'istok_webregular', sans-serif;
        font-size: 18px;
        color: #414042;
        line-height: 20px;
    }
    div.contacts-box {
        overflow: hidden;
    }
    div.contacts-box .contact {
        float: left;
        width: 220px;
        height: 300px;
        margin-right: 20px;
    }
    div.contacts-box .contact .contact-name {
        color: #333;
        font-weight: bold;
        line-height: 1em;
        padding-top: .2em;
    }
    div.contacts-box .contact .contact-position {
        display: table-cell;
        height: 35px;
        border-bottom: 1px solid #ccc;
        vertical-align: bottom;
        font-size: 89.9%;
        padding: .4em 0;
    }
</style>
    <div class="center-col full-size-column">
        <h1>Правление ОАО «ДВМП»</h1>
        <br/>
        <div class="contacts-box">
            <div class="contact">
                <div class="contact-photo"><img alt="photo" src="/assets/images/tops/sokolov.jpg"></div>

                <div class="contact-name">Соколов
                    <br>
                    <span class="nowrap">Константин Анатольевич</span></div>

                <div class="contact-position">Президент и Председатель Правления</div>
            </div>

            <div class="contact">
                <div class="contact-photo"><img alt="photo" src="/assets/images/tops/korchanov.jpg"></div>

                <div class="contact-name">Корчанов
                    <br>
                    <span class="nowrap">Владимир Никодимович</span></div>

                <div class="contact-position">Член Правления, Первый вице-президент</div>
            </div>

            <div class="contact">
                <div class="contact-photo"><img alt="photo" src="/assets/images/tops/vbelyakov.jpg"></div>

                <div class="contact-name">Беляков
                    <br>
                    <span class="nowrap">Виктор Николаевич</span></div>

                <div class="contact-position">Член Правления, Вице-президент по финансам</div>
            </div>

            <div class="contact">
                <div class="contact-photo"><img alt="photo" src="/assets/images/tops/isurin.jpg"></div>

                <div class="contact-name">Исурин
                    <br>
                    <span class="nowrap">Александр Иосифович</span></div>

                <div class="contact-position">Член Правления, Вице-президент по линейно-логистическому дивизиону</div>
            </div>


            <div class="contact">
                <div class="contact-photo"><img alt="photo" src="/assets/images/tops/kuzovkov.jpg"></div>

                <div class="contact-name">Кузовков
                    <br>
                    <span class="nowrap">Константин Валентинович</span></div>

                <div class="contact-position">Член Правления, Вице-президент по&nbsp;инвестициям и&nbsp;развитию</div>
            </div>

            <div class="contact">
                <div class="contact-photo"><img alt="photo" src="/assets/images/tops/melyakin.jpg"></div>

                <div class="contact-name">Мелякин
                    <br>
                    <span class="nowrap">Юрий Иванович</span></div>

                <div class="contact-position">Член Правления, Вице-президент по&nbsp;безопасности</div>
            </div>

            <div class="contact">
                <div class="contact-photo"><img alt="photo" src="/assets/images/tops/vmestulov.jpg"></div>

                <div class="contact-name">Местулов
                    <br>
                    <span class="nowrap">Валерий Евгеньевич</span></div>

                <div class="contact-position">Член Правления, Вице-президент по портовому дивизиону</div>
            </div>


            <div class="contact">
                <div class="contact-photo"><img alt="photo" src="/assets/images/tops/rezvov.jpg"></div>

                <div class="contact-name">Резвов
                    <br>
                    <span class="nowrap">Николай Андреевич</span></div>

                <div class="contact-position">Член Правления, Вице-президент по железнодорожному дивизиону</div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>