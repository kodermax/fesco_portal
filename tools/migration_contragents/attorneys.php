<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 1/26/15
 * Time: 12:04 PM
 */

$_SERVER["DOCUMENT_ROOT"] = '/home/bitrix/www';
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define("BX_CRONTAB", true);
define('BX_NO_ACCELERATOR_RESET', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
@set_time_limit(0);
@ignore_user_abort(true);
\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('highloadblock');

$list = CIBlockElement::GetList(array(),array('IBLOCK_ID' => 46),false,false,array('ID','IBLOCK_ID','PROPERTY_GUID'));
while($row = $list->GetNext())
{
    $arContragents[$row['ID']] = $row['PROPERTY_GUID_VALUE'];
}
//заявки
$list = CIBlockElement::GetList(array(),array('IBLOCK_ID' => 71),false,false,array('ID'));
while($row = $list->GetNext())
{
    $arOldContragents = array();
    $res = CIBlockElement::GetProperty(71, $row['ID'], "sort", "asc", array("CODE" => "COMPANY_ID"));
    if ($ob = $res->GetNext())
    {
        CIBlockElement::SetPropertyValuesEx($row['ID'],71,array('CONTRAGENT_ID' => $arContragents[$ob['VALUE']]));
        //if($ob['VALUE'] > 0)
//            $arOldContragents[] = array('VALUE' => $arContragents[$ob['VALUE']]);
    }

}
//Доверенности
$list = CIBlockElement::GetList(array(),array('IBLOCK_ID' => 72),false,false,array('ID'));
while($row = $list->GetNext())
{
    $arOldContragents = array();
    $res = CIBlockElement::GetProperty(72, $row['ID'], "sort", "asc", array("CODE" => "COMPANY_ID"));
    if ($ob = $res->GetNext())
    {
        CIBlockElement::SetPropertyValuesEx($row['ID'],72,array('CONTRAGENT_ID' => $arContragents[$ob['VALUE']]));
    }
}
print "done!";