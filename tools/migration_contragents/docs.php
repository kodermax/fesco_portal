<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 1/26/15
 * Time: 12:04 PM
 */

$_SERVER["DOCUMENT_ROOT"] = '/home/bitrix/www';
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define("BX_CRONTAB", true);
define('BX_NO_ACCELERATOR_RESET', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
@set_time_limit(0);
@ignore_user_abort(true);
\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('highloadblock');

$list = CIBlockElement::GetList(array(),array('IBLOCK_ID' => 46),false,false,array('ID','IBLOCK_ID','PROPERTY_GUID'));
while($row = $list->GetNext())
{
    $arContragents[$row['ID']] = $row['PROPERTY_GUID_VALUE'];
}
$list = CIBlockElement::GetList(array(),array('IBLOCK_ID' => 66),false,false,array('ID'));
while($row = $list->GetNext())
{
    $arOldContragents = array();
    $res = CIBlockElement::GetProperty(66, $row['ID'], "sort", "asc", array("CODE" => "CONTRACTORS"));
    while ($ob = $res->GetNext())
    {

        if($ob['VALUE'] > 0)
            $arOldContragents[] = array('VALUE' => $arContragents[$ob['VALUE']]);
    }
    CIBlockElement::SetPropertyValuesEx($row['ID'],66, array('CONTRAGENTS' => $arOldContragents));

}

$list = CIBlockElement::GetList(array(),array('IBLOCK_ID' => 67,'SHOW_NEW' => 'Y'),false,false,array('ID'));
while($row = $list->GetNext())
{
    $arOldContragents = array();
    $res = CIBlockElement::GetProperty(67, $row['ID'], "sort", "asc", array("CODE" => "CONTRACTORS"));
    while ($ob = $res->GetNext())
    {
        if($ob['VALUE'])
            $arOldContragents[] = array('VALUE' => $arContragents[$ob['VALUE']]);
    }
            CIBlockElement::SetPropertyValuesEx($row['ID'],67, array('CONTRAGENTS' => $arOldContragents));

}
print "done!";