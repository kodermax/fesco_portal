<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 1/26/15
 * Time: 12:04 PM
 */

$_SERVER["DOCUMENT_ROOT"] = '/home/bitrix/www';
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define("BX_CRONTAB", true);
define('BX_NO_ACCELERATOR_RESET', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
@set_time_limit(0);
@ignore_user_abort(true);
\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('highloadblock');

$arOrgs = array();
$rsOrg = CUserFieldEnum::GetList(array(), array(
    "USER_FIELD_NAME" => 'UF_ORG_FORM'
));
while($arOrg = $rsOrg->GetNext())
{
    $arOrgs[$arOrg['XML_ID']] = $arOrg['ID'];
}

$arResult = array();
$list = CIBlockElement::GetList(array(),array('IBLOCK_ID' => 46),false,false,array('ID','IBLOCK_ID','NAME','PROPERTY_*'));
while($row = $list->GetNextElement())
{
    $arFields = $row->GetFields();
    $arProps = $row->GetProperties();
    $legalAddress = '';
    if(strlen($arProps['REG_COUNTRY']['VALUE']) > 0)
        $legalAddress = $arProps['REG_COUNTRY']['VALUE'].", ";
    if(strlen($arProps['REG_POST_INDEX']['VALUE']) > 0)
        $legalAddress .= $arProps['REG_POST_INDEX']['VALUE'].", ";
    if(strlen($arProps['REG_CITY_NAME']['VALUE']) > 0)
        $legalAddress .= $arProps['REG_CITY_NAME']['VALUE'].", ";
    if(strlen($arProps['REG_STREET_NAME']['VALUE']) > 0)
        $legalAddress .= $arProps['REG_STREET_NAME']['VALUE'].', ';
    if(strlen($arProps['REG_HOUSE_NUMBER']['VALUE']) > 0)
        $legalAddress .= $arProps['REG_HOUSE_NUMBER']['VALUE'];

    $actualAddress = '';
    if(strlen($arProps['FACT_COUNTRY']['VALUE']) > 0)
        $actualAddress = $arProps['FACT_COUNTRY']['VALUE'].", ";
    if(strlen($arProps['FACT_POST_INDEX']['VALUE']) > 0)
        $actualAddress .= $arProps['FACT_POST_INDEX']['VALUE'].", ";
    if(strlen($arProps['FACT_CITY_NAME']['VALUE']) > 0)
        $actualAddress .= $arProps['FACT_CITY_NAME']['VALUE'].", ";
    if(strlen($arProps['FACT_STREET_NAME']['VALUE']) > 0)
        $actualAddress .= $arProps['FACT_STREET_NAME']['VALUE'].', ';
    if(strlen($arProps['FACT_HOUSE_NUBMER']['VALUE']) > 0)
        $actualAddress .= $arProps['FACT_HOUSE_NUBMER']['VALUE'];

    $arResult[] = array(
        'UF_NAME' => $arFields['~NAME'],
        'UF_NAME_EN' => $arProps['EN_NAME']['~VALUE'],
        'UF_FULL_NAME' => $arProps['FULL_NAME']['~VALUE'],
        'UF_SHORT_NAME' => $arProps['SHORT_NAME']['~VALUE'],
        'UF_INN' => $arProps['INN']['VALUE'],
        'UF_KPP' => $arProps['KPP']['VALUE'],
        'UF_OGRN' => $arProps['OGRN']['VALUE'],
        'UF_PHONE' => $arProps['PHONE']['VALUE'],
        'UF_FAX' => $arProps['FAX']['VALUE'],
        'UF_WWW' => $arProps['SITE']['VALUE'],
        'UF_LEGAL_COUNTRY' => strpos($arProps['REG_COUNTRY']['VALUE'],'РФ')!== false || strpos($arProps['REG_COUNTRY']['VALUE'], 'Россия') !== false || strpos($arProps['REG_COUNTRY']['VALUE'],'Российская Федерация') !== false ? 160 : "",
        'UF_ACTUAL_COUNTRY' => strpos($arProps['FACT_COUNTRY']['VALUE'],'РФ')!== false || strpos($arProps['FACT_COUNTRY']['VALUE'], 'Россия') !== false || strpos($arProps['FACT_COUNTRY']['VALUE'],'Российская Федерация') !== false ? 160 : "",
        'UF_RESIDENT' => trim($arProps['ORG_TYPE']['VALUE']) == 'Резидент' ? 1: 0,
        'UF_FESCO' => $arProps['AFFILIATION']['VALUE'],
        'UF_LEGAL_ADDRESS' => $legalAddress,
        'UF_ACTUAL_ADDRESS' => $actualAddress,
        'UF_XML_ID' => $arProps['GUID']['VALUE'],
        'UF_ORG_FORM' => $arOrgs[$arProps['ORG_FORM']['VALUE_XML_ID']]
    );
}
$hlblock = \Bitrix\HighLoadBlock\HighloadBlockTable::getById(3)->fetch();
$entity = \Bitrix\HighLoadBlock\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();
foreach($arResult as $data) {
    $result = $entity_data_class::add($data);
    if (!$result->isSuccess()) {
        print "bad";
    }
}
print "Done!";
?>