<?

$_SERVER["DOCUMENT_ROOT"] = '/home/bitrix/www';
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define("BX_CRONTAB", true);
define('BX_NO_ACCELERATOR_RESET', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
\Bitrix\Main\Loader::includeModule('iblock');

$list = CIBlockElement::GetList(array(),array('IBLOCK_ID' => 46),false,false,array('ID'));
while($row = $list->GetNext())
{
    CIBlockElement::SetPropertyValuesEx($row['ID'], 46, array('GUID' => CFESCOTools::GUID()));
}
print "done!";