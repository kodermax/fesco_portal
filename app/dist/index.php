<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?><?
    $keyToken = base64_encode(json_encode(array('login' =>$_SESSION['SESS_AUTH']['LOGIN'],'password' => $_SESSION['SESS_AUTH']['PASSWORD_HASH']))); $env = getenv('APPLICATION_ENV'); if($env == 'development'){ $serverName = 'https://msk-api01.fesco.com'; } else if($env == 'production'){ $serverName = 'https://app15.fesco.com'; } ?><script type="text/javascript">
    window.sessionStorage.setItem('serverName','<?=$serverName?>');
    window.sessionStorage.setItem('userToken','<?=$keyToken?>');
    window.sessionStorage.setItem('userName','<?=$_SESSION['SESS_AUTH']['LOGIN']?>');
</script><link rel="stylesheet" href="styles/vendor-c8543f55.css"><link rel="stylesheet" href="styles/app-90d30a2b.css"><script src="scripts/modernizr-c1ff1650.js"></script><div class="fesco-app" ng-app="fesco"><div class="loader-bar" route-loader=""></div><div class="loader-bar" http-loader=""></div><div data-position="top" class="pgn-wrapper"><div class="pgn pgn-bar"><toaster-container toaster-options="{'time-out': 5000,'position-class': 'toast-top-right'}"></toaster-container></div></div><div style="height:100%;" ui-view=""></div></div><script src="scripts/vendor-c49ea6a2.js"></script><script src="scripts/app-d1b9453c.js"></script><script src="../bower_components/angular-i18n/angular-locale_ru-ru.js"></script><script>
        $(document).ready(function(){
            $('#workarea').attr('id','');
            $('.pagetitle-wrap').html('<h1 class="pagetitle"></h1>');
            $('.bx-layout-inner-inner-table').addClass('page-one-column flexible-layout')
        });
    </script><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>