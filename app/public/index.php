<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 2/20/15
 * Time: 12:59 PM
 */
$DOCUMENT_ROOT = realpath(dirname(__FILE__)."/../..");
if ($_SERVER['REQUEST_METHOD'] !==  'OPTIONS') {
    define("NO_KEEP_STATISTIC", true);
    define("NOT_CHECK_PERMISSIONS", true);
    define('BX_NO_ACCELERATOR_RESET', true);
    define('STOP_STATISTICS', true);
    define('BX_SECURITY_SHOW_MESSAGE', false);
    define('NO_AGENT_STATISTIC', 'Y');
    define('NO_AGENT_CHECK', true);
    define('DisableEventsCheck', true);
    define("STOP_WEBDAV", true);
    require($DOCUMENT_ROOT."/bitrix/modules/main/include/prolog_before.php");
}
else {


    die();
}

require_once 'vendor/autoload.php';
require_once 'lib/auth.php';

$app = new \Slim\Slim();
//Check token
$app->add(new FESCO\Middleware\AuthToken());
//// API group

$app->hook("slim.before.router",function() use ($app){

    \Bitrix\Main\loader::includeModule('fesco');

    if (strpos($app->request()->getPathInfo(), "/attorneys") > 0) {
        require_once('routes/attorneys.php');
    } elseif (strpos($app->request()->getPathInfo(), "/auto") > 0) {
        require_once('routes/auto.php');
    } elseif (strpos($app->request()->getPathInfo(), "/countries") > 0) {
        require_once('routes/countries.php');
    } elseif (strpos($app->request()->getPathInfo(), "/contragents") > 0) {
        require_once('routes/contragents.php');
    } elseif (strpos($app->request()->getPathInfo(), "/payplan") > 0) {
        require_once('routes/payplan.php');
    } elseif (strpos($app->request()->getPathInfo(), "/tco") > 0) {
        require_once('routes/tco.php');
    } elseif (strpos($app->request()->getPathInfo(), "/trips") > 0) {
        require_once('routes/trips.php');
    } elseif (strpos($app->request()->getPathInfo(), "/users") > 0) {
        require_once('routes/users.php');
    }
});

$app->run();
?>