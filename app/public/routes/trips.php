<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 3/18/15
 * Time: 4:16 PM
 */
$app->group('/api', function () use ($app) {
    $app->group('/trips', function () use ($app) {
        $app->get('/requests', array('FescoTrips','getRequests'));
        $app->get('/requests/my', array('FescoTrips','getMyRequests'));
        $app->get('/requests/:id', array('FescoTrips','getRequest'));
        $app->post('/requests', array('FescoTrips','postRequests'));
        $app->post('/requests/:Id/addFile', array('FescoTrips','addFile'));
        $app->post('/requests/:Id', array('FescoTrips','postRequest'))->conditions(array('id' => '[0-9]{1,}'));
        $app->post('/requests/:Id/done', array('FescoTrips', 'doneRequest'));
    });
});