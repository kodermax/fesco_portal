<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 3/16/15
 * Time: 3:17 PM
 */
$app->group('/api', function () use ($app) {
    $app->group('/attorneys', function() use ($app){
          $app->get('/items', array('FescoAttorneys','getAttorneys'));
          $app->get('/items/my', array('FescoAttorneys','getMyAttorneys'));
          $app->get('/items/:id',array('FescoAttorneys','getAttorney'));
          $app->post('/items',array('FescoAttorneys', 'postAttorney'));
          $app->post('/items/:id', array('FescoAttorneys','putAttorney'));
          $app->post('/items/:id/addFile',array('FescoAttorneys', 'uploadFileToAttorney'));
          $app->post('/items/:id/addDocFile',array('FescoAttorneys', 'uploadDocFileToAttorney'));
          $app->post('/items/:id/done',array('FescoAttorneys', 'doneAttorney'));
          $app->post('/items/:id/revoke',array('FescoAttorneys', 'revokeAttorney'));
          $app->get('/access', array('FescoAttorneys','getAccess'));
          $app->get('/requests', array('FescoAttorneys','getRequests'));
          $app->get('/requests/my', array('FescoAttorneys','getMyRequests'));
          $app->get('/requests/:id', array('FescoAttorneys','getRequest'));
          $app->post('/requests', array('FescoAttorneys','postRequest'));
          $app->put('/requests/:id/cancel', array('FescoAttorneys','cancelRequest'));
    });
});

