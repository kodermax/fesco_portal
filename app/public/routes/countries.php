<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 3/12/15
 * Time: 2:26 PM
 */

$app->group('/api', function () use ($app) {
    $app->get('/countries', array('FescoCountries', 'getCountries'));
});