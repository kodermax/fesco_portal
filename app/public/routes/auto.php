<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 3/18/15
 * Time: 9:31 AM
 */
$app->group('/api', function () use ($app) {
    $app->group('/auto', function () use ($app) {
        $app->get('/access', array('FescoAuto','getAccess'));
        $app->get('/cars', array('FescoAuto', 'getCars'));
        $app->get('/requests',array('FescoAuto','getRequests'));
        $app->get('/requests/my', array('FescoAuto', 'getMyRequests'));
        $app->get('/requests/:id', array('FescoAuto', 'getRequest'));
        $app->post('/requests', array('FescoAuto', 'postRequests'));
        $app->post('/requests/:Id', array('FescoAuto', 'postRequest'));
        $app->post('/requests/:id/cancel', array('FescoAuto', 'cancelEvent'));
        $app->get('/events', array('FescoAuto','getEvents'));
        $app->post('/events', array('FescoAuto', 'postEvents'));
        $app->post('/events/:id/cancel', array('FescoAuto', 'cancelEvent'));
    });
});
?>