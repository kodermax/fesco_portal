<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 3/16/15
 * Time: 12:22 PM
 */
$app->group('/api', function () use ($app) {
    $app->get('/users/my', array('FescoUsers', 'getMyUser'));
    $app->get('/users', array('FescoUsers', 'getUsers'));
});