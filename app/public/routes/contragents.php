<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 3/12/15
 * Time: 9:53 AM
 */

$app->group('/api', function () use ($app) {
    $app->get('/contragents/my',array('FescoContragents','getMyContragents'));
    $app->get('/contragents',array('FescoContragents','getContragents'));
    $app->get('/contragents/:id', array('FescoContragents','getContragent'));
    $app->post('/contragents', array('FescoContragents','postContragents'));
    $app->post('/contragents/:id', array('FescoContragents','postContragent'));
    $app->post('/contragents/:id/addFile', array('FescoContragents', 'addFile'));
});