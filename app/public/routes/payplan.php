<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 3/3/15
 * Time: 10:39 AM
 */

$app->group('/api', function () use ($app) {
    $app->group('/payplan', function () use ($app) {
        $app->get('/access',array('FescoPayPlan', 'getAccess')); // Проверка на доступ к админ. панели
        $app->get('/cfo/my', array('FescoPayPlan','getMyCFO')); // Вывод ЦФО текущего пользователя
        $app->get('/cfo', array('FescoPayPlan','getListCFO')); // Вывод всего списка ЦФО
        $app->post('/cfo/:Id/changeActive', array('FescoPayPlan','changeActiveCFO')); //Открыть или закрыть период конкретного ЦФО
        $app->post('/cfo/activateAll', array('FescoPayPlan', 'activateAllCFO')); //Открытие периодов всех ЦФО
        $app->post('/cfo/deActivateAll', array('FescoPayPlan', 'deActivateAllCFO')); //Закрытие периодов всех ЦФО
        $app->get('/payments/wsdl', array('FescoPayPlan','getPaymentsWSDL')); //WSDL веб-сервиса для получения данных платежей Hyperion
        $app->post('/payments/xml',array('FescoPayPlan','getPaymentsXML')); //Получение данных платежей системой Hyperion
        $app->get('/payments/:depId', array('FescoPayPlan','getPayments')); //Получение данных для построения таблицы по ЦФО
        $app->post('/payments/:id/save', array('FescoPayPlan','postPayment')); //Запись платёжки
        $app->delete('/payments/:id', array('FescoPayPlan','removePayment')); //Удаление платёжки
        $app->get('/helps/wsdl', array('FescoPayPlan', 'updateHelpsWsdl')); //WSDL веб-сервиса для обновления ЦФО и статей
        $app->post('/helps',array('FescoPayPlan','postHelps')); //Обновление ЦФО и статей системой Hyperion
    });
});



