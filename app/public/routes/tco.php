<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 2/20/15
 * Time: 1:21 PM
 */

$app->group('/api', function () use ($app) {
   $app->group('/tco', function () use ($app) {
        $app->get('/access',array('FescoTCO','getAccess'));
        $app->get('/library', array('FescoTCO', 'getLibrary'));
        $app->get('/library/my', array('FescoTCO', 'getMyLibrary'));
        $app->get('/docs', array('FescoTCO', 'getDocsLibrary'));
        $app->get('/docs/:id', array('FescoTCO', 'getDoc'));
        $app->get('/docs/:id/wordFile/:section', array('FescoTCO', 'getDocWordFile'));
        $app->post('/docs', array('FescoTCO', 'postDocs'));
        $app->post('/docs/:id',array('FescoTCO', 'postDoc'));
        $app->post('/docs/:id/addFile',array('FescoTCO', 'addFileToDoc'));
        $app->post('/docs/:id/changeStatusTraffic', array('FescoTCO', 'changeStatusTraffic'));
        $app->put('/docs/:id/removeFile/:fileId',array('FescoTCO','removeFileFromDoc'));
        $app->put('/docs/:id/startApproval',array('FescoTCO','startDocApproval'));
        $app->put('/docs/:id/approve',array('FescoTCO','approveDoc'));
        $app->put('/docs/:id/revision',array('FescoTCO','revisionDoc'));
        $app->get('/helps', array('FescoTCO', 'getHelps'));
        $app->get('/methods', array('FescoTCO', 'getMethodsLibrary'));
        $app->get('/methods/select', array('FescoTCO', 'getMethodsForSelect'));
        $app->get('/methods/:id/whereUsed',array('FescoTCO','getDocsFromMethod'));
        $app->get('/methods/:id', array('FescoTCO', 'getMethod'));
        $app->post('/methods/addFile', array('FescoTCO','addFileToMethod'));
        $app->post('/methods/:id',array('FescoTCO', 'postMethod'));
        $app->post('/methods', array('FescoTCO', 'postMethods'));
        $app->put('/methods/:id/startApproval',array('FescoTCO','startMethodApproval'));
        $app->put('/methods/:id/approve',array('FescoTCO','approveMethod'));
        $app->put('/methods/:id/revision',array('FescoTCO','revisionMethod'));
    });
});

?>