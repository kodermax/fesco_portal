<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 3/2/15
 * Time: 3:55 PM
 */
namespace FESCO\Middleware;
use Bitrix\Main\Entity;
use Bitrix\Main\UserTable;

class AuthToken extends \Slim\Middleware
{

    /**
     * Call
     *
     * Perform actions specific to this middleware and optionally
     * call the next downstream middleware.
     */
    public function call()
    {
        global $DB;
        global $USER;
        if (!$USER->IsAuthorized()) {
            $token = $this->app->request()->headers('token');
            if(strlen($token) <= 0) {
                $token = $this->app->request()->get('token');
            }
            $arToken = json_decode(base64_decode($token), true);
            $q = new Entity\Query(UserTable::getEntity());
            $q->setSelect(array('ID'));
            $q->setFilter(array(
                '=LOGIN' => $arToken['login'],
                //'=PASSWORD' => $arToken['password']
            ));
            $sql = $q->getQuery();
            $result = $q->exec();
            if($row = $result->fetch()){
                $USER->Authorize($row['ID'],true,true);
            }
        }
        $this->next->call();
    }
}
