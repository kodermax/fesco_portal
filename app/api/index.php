<?php
global $APPLICATION;
$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('BX_NO_ACCELERATOR_RESET', true);
define('STOP_STATISTICS', true);
define('BX_SECURITY_SHOW_MESSAGE', false);
define('NO_AGENT_STATISTIC','Y');
define('NO_AGENT_CHECK', true);
define('DisableEventsCheck', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/local/classes/Rest/Rest.php");
// Define path to application directory
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__)));
// Define application environment
defined('APPLICATION_ENV')  || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));
// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH),
    get_include_path(),
)));
// Define path to data directory
//defined('APPLICATION_DATA') || define('APPLICATION_DATA', realpath(dirname(__FILE__) . '/../../data/logs'));
include_once('Controllers/TripsRequests.php');
include_once('Controllers/TripsHelper.php');
include_once('Controllers/Countries.php');
include_once('Controllers/Contragents.php');
include_once('Controllers/Attorneys.php');
include_once('Controllers/AttorneysAccess.php');
include_once('Controllers/AttorneysRequests.php');
include_once('Controllers/Employees.php');
include_once('Controllers/AutoRequests.php');
include_once('Controllers/AutoCars.php');
include_once('Controllers/AutoEvents.php');
include_once('Controllers/AutoAccess.php');
include_once('Controllers/ADAccount.php');
include_once('Controllers/TCOLibrary.php');
include_once('Controllers/TCOHelper.php');
include_once('Controllers/TCOMethods.php');
include_once('Controllers/TCODocs.php');
$rest = new Rest();
$rest->process();
