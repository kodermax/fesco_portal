<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 1/22/15
 * Time: 12:37 PM
 */

class Controllers_AutoRequests extends RestController
{
    private $_helper = null;
    public function __construct($request)
    {
        parent::__construct($request);                    // Init parent constructor
        \Bitrix\Main\loader::includeModule('iblock');
        $this->_helper = new CFESCOIBlockAuto();
    }

    public function get()
    {
        $request = $this->request['params'];
        $arResult = array();
        $userId = $GLOBALS['USER']->GetID();
        $arFilter = array('IBLOCK_ID' => $this->_helper->GetRequestsIBlockId());
        if ($request['filter'] == 'my') {
            $arFilter[] = array(
                'LOGIC' => 'OR',
                'PROPERTY_USER_ID' => $userId,
                'CREATED_BY' => $userId
            );
        }
        if ($request['id'] > 0)
            $arFilter['ID'] = $request['id'];
        $arFilter['PROPERTY_CONTRAGENT_ID'] = $this->_helper->GetCompanyIdByUser($userId);

        $arNavParams = array('nPageSize' => 10);
        if ($request['page'] > 0)
            $arNavParams['iNumPage'] = $request['page'];
        $arSelect = array('ID', 'ACTIVE_TO', 'ACTIVE_FROM', 'NAME', 'PREVIEW_TEXT',
            'PROPERTY_USER_ID',
            'PROPERTY_SHIPPING_ADDRESS',
            'PROPERTY_DESTINATION',
            'PROPERTY_TARGET',
            'PROPERTY_STATUS',
            'PROPERTY_CONTRAGENT_ID',
            'PROPERTY_AUTO_ID'
        );
        $arOrder = array('ACTIVE_TO' => 'DESC');
        if ($request['sortBy'] && $request['sortOrder'])
        {
            switch ($request['sortBy'])
            {
                case 'date':
                    $sortId = 'ACTIVE_TO';
                    break;
                case 'status':
                    $sortId = 'PROPERTY_STATUS';
                    break;
                default:
                    $sortId = 'ID';
                    break;
            }
            $arOrder = array($sortId => strtoupper($request['sortOrder']));

        }
        $list = CIBlockElement::Getlist($arOrder, $arFilter, false, $arNavParams, $arSelect);
        $total = $list->NavRecordCount;
        $NavFirstRecordShow = ($list->NavPageNomer-1)*$list->NavPageSize+1;

        if ($list->NavPageNomer != $list->NavPageCount)
            $NavLastRecordShow = $list->NavPageNomer * $list->NavPageSize;
        else
            $NavLastRecordShow = $list->NavRecordCount;

        while ($row = $list->GetNext()) {
            $arUser = $this->_helper->GetUserInfoArray($row['PROPERTY_USER_ID_VALUE']);
            $arCar = $this->_helper->GetCarInfoArray($row['PROPERTY_AUTO_ID_VALUE']);
            $arContragent = $this->_helper->GetCompanyInfoArray($row['PROPERTY_CONTRAGENT_ID_VALUE']);
            switch ($this->_helper->GetStatusXmlIdById($row['PROPERTY_STATUS_ENUM_ID'])) {
                case 'draft':
                    $cssClass = 'label-warning';
                    break;
                case 'approve':
                    $cssClass = 'label-info';
                    break;
                case 'cancel':
                    $cssClass = 'label-important';
                    break;
                case 'stop':
                    $cssClass = 'label-inverse';
                    break;
                default:
                    $cssClass = '';
                    break;
            }

            $arResult[] = array(
                'Id' => $row['ID'],
                "Contragent" => $arContragent,
                'User' => $arUser,
                'Comment' => $row['~PREVIEW_TEXT'],
                'Car' => $arCar,
                'Title' => $row['NAME'],
                'ShippingAddress' => $row['~PROPERTY_SHIPPING_ADDRESS_VALUE'],
                'Destination' => $row['~PROPERTY_DESTINATION_VALUE'],
                'Target' => $row['~PROPERTY_TARGET_VALUE'],
                'Status' => array('Title' => $row['PROPERTY_STATUS_VALUE'], 'CssClass' => $cssClass),
                'StartDate' => ConvertDateTime($row['ACTIVE_FROM'], "DD.MM.YYYY HH:MI"),
                'FinishDate' => ConvertDateTime($row['ACTIVE_TO'], "DD.MM.YYYY HH:MI")
            );
        }
        $this->responseStatus = 200;
        if (count($arResult) == 1 && $request['type'] != 'list') {
            $arResult = $arResult[0];
            $this->response = json_encode($arResult);
        } else {
            $arData = array();
            $arData['data'] = array(
                'Items' => $arResult,
                'Count' => (int)$total,
                'NavFirstRecordShow' => (int)$NavFirstRecordShow,
                'NavLastRecordShow' => (int)$NavLastRecordShow
            );
            $this->response = json_encode($arData);
        }
    }
    public function post()
    {
        $request = $this->request['params'];
        $startDate = new DateTime($request['StartDate']);
        $startDate = $startDate->format('d.m.Y H:i:00');
        $endDate = new DateTime($request['FinishDate']);
        $endDate = $endDate->format('d.m.Y H:i:00');
        $this->request['content-type'] = 'json';
        $userId = intval($request['User']['Id']) > 0 ? $request['User']['Id'] : $GLOBALS['USER']->GetID();
        if (strlen($request['Contragent']['Id']) > 0)
            $contragentId = $request['Contragent']['Id'];
        else
            $contragentId = $this->_helper->GetCompanyIdByUser($GLOBALS['USER']->GetID());
        $arProp = array(
            'USER_ID' => $userId,
            'TARGET' => $request['Target'],
            'SHIPPING_ADDRESS' => $request['ShippingAddress'],
            'DESTINATION' => $request['Destination'],
            'CONTRAGENT_ID' => $contragentId,
            'AUTO_ID' => $request['Car']['Id']
        );
        $arProp['STATUS'] = $this->_helper->isAdmin() ? $this->_helper->GetStatusEnumIdByXmlId('approve') : $this->_helper->GetStatusEnumIdByXmlId('draft');
        $userName = $this->_helper->GetUserShortName($userId);
        $arFields = array(
            'NAME' => 'Заявка от '.$userName,
            'IBLOCK_ID' => $this->_helper->GetRequestsIBlockId(),
            'ACTIVE_FROM' => $startDate,
            'ACTIVE_TO' => $endDate,
            'PROPERTY_VALUES' => $arProp,
            'PREVIEW_TEXT' => $request['Comment']
        );
        $el = new CIBlockElement();
        $ID = $el->Add($arFields,false,false,false);
        $this->responseStatus = 200;
        if ($ID > 0)
        {
            $this->response = json_encode(array('success'=>1));
            //Уведомление администратора о новой заявке
            $templateUrl = $this->_helper->GetTemplateUrl($request['Id']);
            $arAdmins = $this->_helper->GetAdminByCompanyId($request['Contragent']['id']);
            $arEmails = array();
            foreach($arAdmins as $item)
            {
                $arEmails[] = $item['EMAIL'];
            }
            $strEmails = implode(";",$arEmails);
            $arEventFields = array(
                'EMAIL_TO' => $strEmails,
                'MESSAGE' => 'У вас есть новая заявка на автотранспорт
                        <br>
                        <a href="' . $templateUrl['requests_manage.list'] . '">Перейти к списку заявок</a>',
                'SUBJECT' => 'Новая заявка на автотранспорт',
            );
            CEvent::Send("MAIL_TEMPLATE", "s1", $arEventFields);
            return;
        }
        $this->response = json_encode(array('success'=>0));

    }
    public function put()
    {
        $request = $this->request['params'];
        $this->response = json_encode(array());
        $this->responseStatus = 200;
    }
    public function delete()
    {
        $this->response = array('TestResponse' => 'I am DELETE response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }
}
?>