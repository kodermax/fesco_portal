<?
class Controllers_AttorneysRequests extends RestController
{
    private $_iblockId = 0;
    private $_arStatus = array();
    private $_arStatusIds = array();
    private  $_transference = array();

    function __construct($request)
    {
        parent::__construct($request);	    			// Init parent constructor
        \Bitrix\Main\Loader::includeModule('iblock');
        $res = CIBlock::GetList(
            Array(),
            Array(
                'ACTIVE'=>'Y',
                "CODE"=>'req_attorneys'
            ), true
        );
        if($ar_res = $res->Fetch())
        {
            $this->_iblockId = $ar_res['ID'];
        }
        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$this->_iblockId, "CODE"=>"STATUS"));
        while($enum_fields = $property_enums->GetNext())
        {
            $this->_arStatus[$enum_fields['XML_ID']] = $enum_fields['ID'];
            $this->_arStatusIds[$enum_fields['ID']] = $enum_fields['XML_ID'];
        }
        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$this->_iblockId, "CODE"=>"TRANSFERENCE"));
        while($enum_fields = $property_enums->GetNext())
        {
            $this->_transference[$enum_fields['XML_ID']] = $enum_fields['ID'];
        }

    }

    /**
     *
     */
    private function GetTemplateId($docId)
    {
        \Bitrix\Main\Loader::includeModule('bizproc');
        $dbWorkflowTemplate = CBPWorkflowTemplateLoader::GetList(
            array(),
            array("DOCUMENT_TYPE" => array("iblock", "CIBlockDocument", "iblock_".$this->_iblockId), "ACTIVE"=>"Y"),
            false,
            false,
            array("ID", "NAME", "DESCRIPTION", "MODIFIED", "USER_ID", "AUTO_EXECUTE", "USER_NAME", "USER_LAST_NAME", "USER_LOGIN", "USER_SECOND_NAME")
        );
        while ($arWorkflowTemplate = $dbWorkflowTemplate->GetNext())
        {
            return $arWorkflowTemplate['ID'];
        }
        return 0;
    }
    public function get()
    {
        $arResult = array();
        $userId = $GLOBALS['USER']->GetID();
        $request = $this->request['params'];
        $status = $request['status'];
        $arFilter = array(
            'IBLOCK_ID' => $this->_iblockId
        );
        if ($status == 'approval')
        {
            $arFilter['PROPERTY_STATUS'] = $this->_arStatus['approve'];
        }
        if($request['filter'] == 'my')
        {
            $arFilter[]= array(
                'LOGIC' => 'OR',
                'PROPERTY_USER_ID' => $userId,
                'CREATED_BY' => $userId
            );
        }
        if ($request['id'] > 0)
            $arFilter['ID'] = $request['id'];
        $arSelect = array('ID','IBLOCK_ID','NAME','PREVIEW_TEXT',
            'PROPERTY_*'

        );
        $arNavParams = array('nPageSize' => 10);
        if ($request['page'] > 0)
            $arNavParams['iNumPage'] = $request['page'];
        $arSort = array('ID' => "DESC");
        $list = CIBlockElement::Getlist($arSort,$arFilter,false,$arNavParams, $arSelect);
        $total = $list->NavRecordCount;
        $NavFirstRecordShow = ($list->NavPageNomer-1)*$list->NavPageSize+1;

        if ($list->NavPageNomer != $list->NavPageCount)
            $NavLastRecordShow = $list->NavPageNomer * $list->NavPageSize;
        else
            $NavLastRecordShow = $list->NavRecordCount;

        while($oRow = $list->GetNextElement())
        {
            $arFields = $oRow->GetFields();
            $arProps = $oRow->GetProperties();
            switch($arProps['STATUS']['VALUE_XML_ID'])
            {
                case 'draft':
                case 'approving':
                case 'approve':
                case 'wait':
                    $cssClass = "warning";
                    break;
                case 'done':
                    $cssClass = "positive";
                    break;
                case 'cancel':
                case 'not_approve':
                    $cssClass = "negative";
                    break;
                default:
                    $cssClass = "";
                    break;
            }
            $arEmployee = array();
            if($arProps['USER_ID']['VALUE'] > 0) {
                $rsUser = CUser::GetByID($arProps['USER_ID']['VALUE']);
                $arUser = $rsUser->Fetch();
                $arEmployee = array('Id' => $arUser['ID'],'Title' => $arUser['NAME']. ' '.$arUser['LAST_NAME']);
            }
            $arApprover = array();
            if($arProps['APPROVER']['VALUE'] > 0) {
                $rsUser = CUser::GetByID($arProps['APPROVER']['VALUE']);
                $arUser = $rsUser->Fetch();
                $arApprover = array('Id' => $arUser['ID'],'Title' => $arUser['NAME']. ' '.$arUser['LAST_NAME']);
            }
            $arResult[] = array(
                'Id' => $arFields['ID'],
                'SeqId' => $arProps['SEQ_ID']['VALUE'] > 0 ? $arProps['SEQ_ID']['VALUE'] : $arProps['ID']['VALUE'],
                'Title' => $arFields['NAME'],
                'Contragent' => array('Id' => $arProps['CONTRAGENT_ID']["VALUE"]),
                'Description' => $arFields['~PREVIEW_TEXT'],
                'StartDate' => $arProps['START_DATE']['VALUE'],
                'FinishDate' => $arProps['FINISH_DATE']['VALUE'],
                'Passport' => array(
                    'Id' => $arProps['PASSPORT_ID']['VALUE'],
                    'Date' => $arProps['PASSPORT_DATE']['VALUE'],
                    'Whom' => $arProps['PASSPORT_WHOM']['~VALUE'],
                    'Dep' => $arProps['PASSPORT_DEP']['~VALUE']
                ),
                'Comment' => $arProps['BOSS_COMMENT']['~VALUE'],
                'Status' => array(
                    'Title' => $arProps['STATUS']['VALUE'],
                    'CssClass' => $cssClass,
                    'Code' => $arProps['STATUS']['VALUE_XML_ID']
                ),
                'User' => $arEmployee,
                'Approver' => $arApprover,
                'Transference' => $arProps['TRANSFERENCE']['VALUE'],
                'TransferenceText' => $arProps['TRANSFERENCE']['VALUE'] === "Y" ? "Да" : "Нет"
            );
        }
        $client = new Bitrix\Main\Web\HttpClient();
        foreach($arResult as &$arItem) {
            if (strlen($arItem['Contragent']['Id']) > 0) {
                $response = $client->get("https://".$_SERVER['SERVER_NAME']."/app/api/Contragents?guid=".$arItem['Contragent']['Id']);
                $response = json_decode($response, true);
                $arItem['Contragent']['Title'] = $response['Title'];
            }
        }
        $this->responseStatus = 200;
        if (count($arResult) == 1 && $request['type'] != 'list') {
            $this->response = json_encode($arResult[0]);
            return;
        }
        $arData = array();
        $arData['data'] = array(
            'Items' => $arResult,
            'Count' => (int)$total,
            'NavFirstRecordShow' => (int)$NavFirstRecordShow,
            'NavLastRecordShow' => (int)$NavLastRecordShow
        );
        $this->response = json_encode($arData);

    }
    public function post()
    {
        $request = $this->request['params'];
        $this->request['content-type'] = 'json';
        $rsProp = CIBlockElement::GetProperty($this->_iblockId,$request['Id'],'sort','asc',array('CODE' => 'SEQ_ID'));
        if($arProp = $rsProp->GetNext())
        {
            $PROPERTY_ID = $arProp['ID'];
            $seq = new CIBlockSequence($this->_iblockId, $PROPERTY_ID);
            $nextSeq = $seq->GetNext();
        }
        $arProp = array(
            'CONTRAGENT_ID' => $request['Contragent']["Id"],
            'USER_ID' => $request['User']['Id'],
            'PASSPORT_ID' => $request['Passport']["Id"],
            'PASSPORT_DATE' => date('d.m.Y',strtotime($request['Passport']["Date"])),
            'PASSPORT_WHOM' => $request['Passport']["Whom"],
            'PASSPORT_DEP' => $request['Passport']["Dep"],
            'START_DATE' => $request['Date']['StartDate'],
            'FINISH_DATE' => $request['Date']['EndDate'],
            'TRANSFERENCE' => $this->_transference[$request['Transference']],
            'STATUS' => $this->_arStatus['draft'],
            'APPROVER' => $request['Approver']['Id'],
            "SEQ_ID" => $nextSeq
        );
        //Согласование президентом
        if ($request['Approver']['Id'] == 8081)
        {
            $arProp['STATUS'] = $this->_arStatus['approve'];
            $arProp['BOSS_COMMENT'] = 'Шаг электронного согласования не применим*';
        }
        $rsUser = CUser::GetByID($request['User']['Id']);
        $arUser = $rsUser->Fetch();
        $arFields = array(
            "NAME" => "Доверенность для ".$arUser['NAME']." ".$arUser['LAST_NAME'],
            'PROPERTY_VALUES' => $arProp,
            "IBLOCK_SECTION_ID" => false,
            'IBLOCK_ID' => $this->_iblockId,
            'PREVIEW_TEXT' => $request['Description']
        );
        $el = new CIBlockElement;
        if($ID = $el->Add($arFields,false,false))
        {
            if ($request['Approver']['Id'] != 8081) {
                $templateId = $this->GetTemplateId($ID);
                $wfId = CBPDocument::StartWorkflow(
                    $templateId,
                    array("iblock", "CIBlockDocument", $ID),
                    array("Approvers" => array("user_" . $request['Approver']['Id'])), $arErrorsTmp);
                if (count($arErrorsTmp) <= 0) {
                    CIBlockElement::SetPropertyValuesEx($ID, $this->_iblockId, array('STATUS' => $this->_arStatus['approving']));
                }
            }
            $this->status = 200;
            $this->responseStatus = 200;
            $this->response = json_encode(array('Id' => $ID,'success'=> 1,'errors'=>$arErrorsTmp  ));
        }
        else {
            $this->status = 200;
            $this->responseStatus = 200;
            $this->response = json_encode(array('success'=>0,'error' => $el->LAST_ERROR));
        }

    }

    public function put()
    {
        $request = $this->request['params'];
        $id = $request['Id'];
        $this->responseStatus = 200;
        if ($request['action'] == 'cancel')
        {
            CIBlockElement::SetPropertyValuesEx($id, false, array('STATUS' => $this->_arStatus['cancel']));
            $this->response = json_encode(array('success' => 1));
            //Отправить уведомление
            $userId = $request['User']['Id'];
            $rsUser = CUser::GetByID($userId);
            $arUser = $rsUser->Fetch();
            $rsSites = CSite::GetByID("s1");
            $arSite = $rsSites->Fetch();
            $url = "http://".$arSite['SERVER_NAME']."/app/attorneys/#/auto/requests/".$request['Id'];
            if (strlen($arUser['EMAIL']) > 0) {
                $arEventFields = array(
                    "EMAIL_TO" => $arUser['EMAIL'],
                    "SUBJECT" => "Ваша заявка на доверенность отклонена",
                    "MESSAGE" => "
                            Пользователь ".$GLOBALS['USER']->GetFullName()." отклонил Вашу заявку на доверенность.
                            <br>
                            <a href='".$url."'>Перейти к заявке</a>
                            <br><br>"
                );
                CEvent::Send("MAIL_TEMPLATE", "s1", $arEventFields);
            }
            return;
        }
        $this->response = json_encode(array('success' => 1))  ;
    }
    public function delete()
    {
        $this->response = array('TestResponse' => 'I am DELETE response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }

}
