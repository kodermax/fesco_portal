<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 2/3/15
 * Time: 12:11 PM
 */
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

class Controllers_TCOMethods extends RestController {

    private $_entity = null;

    function __construct($request) {
        parent::__construct($request);
        \Bitrix\Main\loader::includeModule('highloadblock');
        \Bitrix\Main\loader::includeModule('fesco');
        $filter = array(
            'select' => array('ID', 'NAME', 'TABLE_NAME', 'FIELDS_COUNT'),
            'filter' => array('=TABLE_NAME' => 'fesco_tcolibrary')
        );
        $hlblock = HL\HighloadBlockTable::getList($filter)->fetch();
        $this->_entity = HL\HighloadBlockTable::compileEntity($hlblock);
    }
    public function get()
    {
        $arResult = array();
        $request = $this->request['params'];
        $this->responseStatus = 200;
        $arHelps = \FESCO\TCO::getHelpers();
        $hl = $this->_entity->getDataClass();
        $filter = array();
        if (strlen($request['q']) > 0) {
            $filter[] = array(
                'LOGIC' => 'OR',
                array(
                    '%=UF_NAME' => '%' . $request['q'] . '%'
                ),
                array(
                    '%=UF_SHORT_NAME' => '%' . $request['q'] . '%'
                ),
                array(
                    '%=UF_INN' => '%' . $request['q'] . '%'
                ),
                array(
                    '%=UF_KPP' => '%' . $request['q'] . '%'
                ),
                array(
                    '%=UF_OGRN' => '%' . $request['q'] . '%'
                ),
            );
        } else {
            if ($request['Id'] > 0) {
                $filter = array('ID' => $request['Id']);
            }
        }
        $list = $hl::getList(
            array(
                'select' => array('*'),
                'filter' => $filter
            ));
        while ($row = $list->fetch()) {
            if ($request['mode'] == 'select'){
                $arResult[] = array(
                    'id' => $row['ID'],
                    'Title' => $row['UF_NAME'],
                    'text' => $row['UF_NAME']
                );
            }
            else {
                $arResult[] = array(
                    'Id' => $row['ID'],
                    'id' => $row['ID'],
                    'Contragent' => FESCO\Contragents::getContragentInfo($row['UF_CONTRAGENT']),
                    'DealType' => $arHelps['DealTypes'][$row['UF_DEAL_TYPE']],
                    'Division' => $arHelps['Divisions'][$row['UF_DIVISION']],
                    'DocFile' => array('Url' => CFile::GetPath($row['UF_DOC_FILE'])),
                    'Name' => $row['UF_NAME'],
                    'Title' => $row['UF_NAME'],
                    'text' => $row['UF_NAME'],
                    'IncomeSide' => $row['UF_INCOME_SIDE'],
                    'InfoSource' => $arHelps['InfoSources'][$row['UF_INFO_SOURCE']],
                    'TaxPayer' => FESCO\Contragents::getContragentInfo($row['UF_TAXPAYER']),
                    'TestContragent' => FESCO\Contragents::getContragentInfo($row['UF_TEST_CONTRAGENT']),
                    'TCOMethod' => $arHelps['TCOMethods'][$row['UF_METHOD_TCO']]
                );
            }
        }
        if (count($arResult) == 1 && $request['type'] != 'list') {
            $arResult = $arResult[0];
            $this->response = json_encode($arResult);
        }
        else {
            $arData['data'] = array(
                'Items' => $arResult
            );
            $this->response = json_encode($arData);
        }
    }

    public function post()
    {
        $request = $this->request['params'];
        $this->responseStatus = 200;
        $hl = $this->_entity->getDataClass();
        if($request['mode'] == 'add_file'){
            $result = $hl::update($request['Id'], array('UF_DOC_FILE' => $_FILES['file']));
            if ($result->isSuccess())
                $this->response = json_encode(array('success' => 1, 'Id' => $request['Id']));
            else
                $this->response = json_encode(array('success' => 0));
            return;
        }
        $arHelps = \FESCO\TCO::getHelpers();
        $arData = array(
            'UF_NAME' => $request['Name'],
            'UF_TAXPAYER' => $request['TaxPayer']['id'],
            'UF_CONTRAGENT' => $request['Contragent']['id'],
            'UF_DEAL_TYPE' => $request['DealType']['Id'],
            'UF_CREATED_BY' => $GLOBALS['USER']->GetID(),
            'UF_CREATE_DATE' => date('d.m.Y h:i:s'),
            'UF_CHANGE_DATE' => date('d.m.Y h:i:s'),
            'UF_INFO_SOURCE' => $request['InfoSource']['Id'],
            'UF_METHOD_TCO' => $request['TCOMethod']['Id'],
            'UF_TEST_CONTRAGENT' => $request['TestContragent']['id'],
            'UF_INCOME_SIDE' => $request['IncomeSide'],
            'UF_DIVISION' => $request['Division']['Id'],
            'UF_TYPE' => 'METHOD'

        );
        $oCounter = null;
        if ($request['Id'] == 0) {
            $oCounter = new FESCO\Counters();
            $cnt = $oCounter->getCounter('tco_methods');
            $number = sprintf('%1$04d_', $cnt + 1);
            $number .= 'М1_';
            if ($request['IncomeSide'])
                $number .= 'Д3_';
            else
                $number .= 'Р3_';
            $number .= $arHelps['Divisions'][$request['Division']['Id']]['Title'];
            $arData['UF_NUMBER'] = $number;

        }

        if ($request['Id'] == 0) {
            $result = $hl::add($arData);
        }
        else {
            $result = $hl::update($request['Id'], $arData);
        }
        if ($result->isSuccess()) {
            if ($request['Id'] == 0)
                $oCounter->incCounter('tco_methods');
            $this->response = json_encode(array('success' => 1,'Id' => $result->getId()));
        }
        else {
            $this->response = json_encode(array('success' => 0, 'errors' => $result->getErrors()));
        }
    }

    public function put()
    {
        // TODO: Implement put() method.
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }
}