<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 2/3/15
 * Time: 10:16 AM
 */
class Controllers_TCOHelper extends RestController
{
    private $arDealTypes = array();
    function __construct($request){
        parent::__construct($request);


    }
    public function get()
    {
        $this->responseStatus = 200;
        $request = $this->request['params'];
        if ($request['mode'] == 'helps'){
            $arResult = array();
            $rs = CUserFieldEnum::GetList(array(), array(
                "USER_FIELD_NAME" => 'UF_DEAL_TYPE',
            ));
            while($ar = $rs->GetNext()) {
                $arResult['DealTypes'][$ar['ID']] = array('Id' => $ar['ID'], 'Title' => $ar['VALUE']);
            }
            $rs = CUserFieldEnum::GetList(array(), array(
                "USER_FIELD_NAME" => 'UF_INFO_SOURCE',
            ));
            while($ar = $rs->GetNext()) {
                $arResult['InfoSources'][$ar['ID']] = array('Id' => $ar['ID'], 'Title' => $ar['VALUE']);
            }
            $rs = CUserFieldEnum::GetList(array(), array(
                "USER_FIELD_NAME" => 'UF_METHOD_TCO',
            ));
            while($ar = $rs->GetNext()) {
                $arResult['TCOMethods'][$ar['ID']] = array('Id' => $ar['ID'], 'Title' => $ar['VALUE']);
            }
            $rs = CUserFieldEnum::GetList(array(), array(
                "USER_FIELD_NAME" => 'UF_DIVISION',
            ));
            while($ar = $rs->GetNext()) {
                $arResult['Divisions'][$ar['ID']] = array('Id' => $ar['ID'], 'Title' => $ar['VALUE']);
            }
            $rs = CUserFieldEnum::GetList(array(), array(
                "USER_FIELD_NAME" => 'UF_MATERIALITY',
            ));
            while($ar = $rs->GetNext()) {
                $arResult['Materiality'][$ar['ID']] = array('Id' => $ar['ID'], 'Title' => $ar['VALUE']);
            }

            $this->response = json_encode($arResult);
            return;
        }

    }

    public function post()
    {
        // TODO: Implement post() method.
    }

    public function put()
    {
        // TODO: Implement put() method.
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }
}
?>