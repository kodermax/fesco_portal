<?
class Controllers_TripsRequests extends RestController
{
    private $_helper = null;
    public function __construct($request)
    {
        parent::__construct($request);                    // Init parent constructor
        $this->_helper = new CFESCOIBlockTrips();
    }

    public function get()
    {
        $request = $this->request['params'];
        $arResult = array();
        $userId = $GLOBALS['USER']->GetID();
        $arFilter = array('IBLOCK_ID' => $this->_helper->iblockId);
        if ($this->_helper->isAdmin()) {
            $arFilter['CHECK_PERMISSIONS'] = 'N';
        }
        else if ($request['filter'] == 'my') {
            $arFilter[] = array(
                'LOGIC' => 'OR',
                'PROPERTY_USER_ID' => $userId,
                'CREATED_BY' => $userId
            );
        }

        if ($request['id'] > 0)
            $arFilter['ID'] = $request['id'];
        $arNavParams = array('nPageSize' => 10);
        if ($request['page'] > 0)
            $arNavParams['iNumPage'] = $request['page'];
        $arSelect = array('ID','IBLOCK_ID', 'ACTIVE_TO', 'ACTIVE_FROM', 'NAME','DETAIL_TEXT', 'PREVIEW_TEXT','PROPERTY_*');
        if ($request['sortBy'] && $request['sortOrder'])
        {
            switch ($request['sortBy'])
            {
                case 'num':
                    $sortId = 'ID';
                    break;
                case 'date':
                    $sortId = 'ACTIVE_FROM';
                    break;
                case 'transport':
                    $sortId = 'PROPERTY_TRANSPORT';
                    break;
                case 'status':
                    $sortId = 'PROPERTY_STATUS';
                    break;
                case 'company':
                    $sortId = 'PROPERTY_COMPANY';
                    break;
                case 'user':
                    $sortId = 'PROPERTY_USER_NAME';
                    break;
                default:
                    $sortId = 'ID';
                    break;
            }
            $arOrder = array($sortId => strtoupper($request['sortOrder']));

        }
        $list = CIBlockElement::Getlist($arOrder, $arFilter, false, $arNavParams, $arSelect);
        $total = $list->NavRecordCount;
        $NavFirstRecordShow = ($list->NavPageNomer-1)*$list->NavPageSize+1;
        if ($list->NavPageNomer != $list->NavPageCount)
            $NavLastRecordShow = $list->NavPageNomer * $list->NavPageSize;
        else
            $NavLastRecordShow = $list->NavRecordCount;

        while ($row = $list->GetNextElement()) {
            $arFields = $row->GetFields();
            $arProp = $row->GetProperties();
            $arUser = array('id' =>$arProp['USER_ID']['VALUE'],'text' => $this->_helper->GetUserShortName($arProp['USER_ID']['VALUE']));
            $arStatuses = $this->_helper->GetStatuses();
            $arResult[] = array(
                'Id' => $arFields['ID'],
                'Title' => $arFields['NAME'],
                'Company' => $arProp['COMPANY']['~VALUE'],
                'Transport' => array('Id' => $arProp['TRANSPORT']['VALUE_ENUM_ID'],'Title'=> $arProp['TRANSPORT']['VALUE'], 'Code' => $arProp['TRANSPORT']['VALUE_XML_ID']),
                'Transfer' => strlen($arProp['TRANSFER']['VALUE']) > 0 ? true : false,
                'Visa' => strlen($arProp['VISA']['VALUE']) > 0 ? true : false,
                'Hotel' => strlen($arProp['HOTEL']['VALUE']) > 0 ? true : false,
                'Target' => $arFields['~DETAIL_TEXT'],
                'Date' => array('StartDate' =>ConvertDateTime($arFields['ACTIVE_FROM'], "DD.MM.YYYY"),
                    'EndDate' =>  ConvertDateTime($arFields['ACTIVE_TO'], "DD.MM.YYYY"), 'Text' => ConvertDateTime($arFields['ACTIVE_FROM'], "DD.MM.YYYY") . " - " .ConvertDateTime($arFields['ACTIVE_TO'], "DD.MM.YYYY")),
                'DateText' => 'max',
                'TicketComment' => $arProp['TICKET_COMMENT']['~VALUE'],
                'ApproverComments' => $arProp['APPROVER_COMMENTS']['~VALUE']['TEXT'],
                'Status' => array('Title' => $arProp['STATUS']['VALUE'], 'Code' => $arProp['STATUS']['VALUE_XML_ID']),
                'Comment' => $arFields['~PREVIEW_TEXT'],
                'File' => array('Url' => CFile::GetPath($arProp['FILE']['VALUE'])),
                'User' => $arUser,
                'UserId' => $arUser['Id'],
                'Places' => $arProp['PLACES']['~VALUE']
            );
        }
        if (count($arResult) == 1 && $request['type'] != 'list') {
            $arResult = $arResult[0];
            $this->response = json_encode($arResult);
        } else {
            $arData = array();
            $arData['data'] = array(
                'Items' => $arResult,
                'Count' => $total,
                'NavFirstRecordShow' => $NavFirstRecordShow,
                'NavLastRecordShow' => $NavLastRecordShow
            );
            $this->response = json_encode($arData);
        }
   }
    public function post()
    {
        $request = $this->request['params'];
        $this->responseStatus = 200;
        switch ($request['mode']) {
            case 'add_files':
                CIBlockElement::SetPropertyValuesEx($request['Id'], $this->_helper->iblockId, array('FILE' => $_FILES['file']));
                $this->response = json_encode(array('success'=>1, 'Id'=> $request['Id']));
                break;
            default:
                $this->request['content-type'] = 'json';
                $userId = $request['User']['id'] > 0 ? $request['User']['id'] : $GLOBALS['USER']->GetID();
                $userName = $this->_helper->GetUserShortName($userId);
                $arProp = array(
                    'USER_NAME' => $userName,
                    'USER_ID' => $userId,
                    'PLACES' => $request['Places'],
                    'COMPANY' => $request['Company'],
                    'TICKET_COMMENT' => $request['TicketComment'],
                    'VISA' => $request['Visa'] === 'true' ? $this->_helper->GetVisaId() : "",
                    'HOTEL' => $request['Hotel'] === 'true' ? $this->_helper->GetHotelId(): "",
                    'TRANSFER' => $request['Transfer'] === 'true' ? $this->_helper->GetTransferId(): "",
                    'TRANSPORT' => $this->_helper->GetTransportId($request['Transport']['Code'])
                );
                $el = new CIBlockElement();
                if ($request['Id'] > 0) {
                    $arFields = array(
                        'ACTIVE_FROM' => $request['Date']['StartDate'],
                        'ACTIVE_TO' => $request['Date']['EndDate'],
                        'PREVIEW_TEXT' => $request['Comment'],
                        'DETAIL_TEXT' => $request['Target']
                    );
                    $result = $el->Update($request['Id'],$arFields,false,false,false);
                    CIBlockElement::SetPropertyValuesEx($request['Id'],$this->_helper->iblockId,$arProp);
                    if($result) $ID = $request['Id'];
                }
                else {
                    $arFields = array(
                        'NAME' => 'Заявка от '.$userName,
                        'IBLOCK_ID' => $this->_helper->iblockId,
                        'ACTIVE_FROM' => $request['Date']['StartDate'],
                        'ACTIVE_TO' => $request['Date']['EndDate'],
                        'PROPERTY_VALUES' => $arProp,
                        'PREVIEW_TEXT' => $request['Comment'],
                        'DETAIL_TEXT' => $request['Target']
                    );
                    $ID = $el->Add($arFields, false, false, false);
                    $arFile = CFile::MakeFileArray($_FILES['file']['tmp_name']);
                    $arFile['name'] = $_FILES['file']['name'];
                    $arFile["MODULE_ID"] = "iblock";
                    CIBlockElement::SetPropertyValuesEx($ID, false, array('FILE' => $arFile));
                    $templateId = $this->_helper->GetTemplateId($ID);
                    $wfId = CBPDocument::StartWorkflow(
                        $templateId,
                        array("iblock", "CIBlockDocument", $ID),
                        array(), $arErrorsTmp);
                    if (count($arErrorsTmp) <= 0) {
                        $arStatuses = $this->_helper->GetStatuses();
                        CIBlockElement::SetPropertyValuesEx($ID, $this->_helper->iblockId, array('STATUS' => $arStatuses['approve']));
                    }
                }

                if ($ID > 0)
                {
                    $this->response = json_encode(array('success'=>1, 'Id'=> $ID));
                    return;
                }
                $this->response = json_encode(array('success'=>0));
                break;

        }


    }
    public function put()
    {
        $request = $this->request['params'];
        if ($request['Id'] > 0)
        {
            if ($request['mode'] == 'done')
            {
                $arStatuses = $this->_helper->GetStatuses();
                CIBlockElement::SetPropertyValuesEx($request['Id'], $this->_helper->iblockId,array('STATUS' => $arStatuses['done']));
            }
        }
        $this->response = json_encode(array('success' => 1));
        $this->responseStatus = 200;
    }
    public function delete()
    {
        $this->response = array('TestResponse' => 'I am DELETE response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }
}
