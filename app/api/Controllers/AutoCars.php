<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 1/22/15
 * Time: 12:38 PM
 */
class Controllers_AutoCars extends RestController
{

    private $_helper = null;
    public function __construct($request)
    {
        parent::__construct($request);	    			// Init parent constructor
        \Bitrix\Main\Loader::includeModule('iblock');
        $this->_helper = new CFESCOIBlockAuto();
    }
    public function get()
    {
        $arResult = array();
        $userId = $GLOBALS['USER']->GetID();
        $carTaxi = 0;
        $request = $this->request['params'];
        $companyId = $this->_helper->GetCompanyIdByUser($userId);
        $arFilter = array(
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => $this->_helper->GetCarsIBlockId(),
            'PROPERTY_CONTRAGENT_ID' => $companyId,
            'CHECK_PERMISSIONS' => 'N'
        );
        $list = CIBlockElement::GetList(array("SORT"=>"ASC"),$arFilter, false,false,array('ID','IBLOCK_ID','NAME','PROPERTY_IS_TAXI','PREVIEW_PICTURE'));
        while($row = $list->GetNext())
        {
            if ($row['PROPERTY_IS_TAXI_VALUE'] == 'Y')
                $carTaxi = $row['ID'];
            $arResult[] = array(
                'key' => $row['ID'],
                'label' => $row['NAME'],
                'link' => CFile::GetPath($row['PREVIEW_PICTURE'])
            );
        }
        $arResult[] = array(
            'key' => 'requests',
            'label' => 'Не обработано',
            'link' => '/images/requests/question.jpg'
        );
        $this->responseStatus = 200;
        $arData = array();
        $arData['data']['Items'] = $arResult;
        $arData['data']['Taxi'] = $carTaxi;
        $this->response = json_encode($arData);

    }
    public function post()
    {
        $request = $this->request['params'];
        $this->request['content-type'] = 'json';

        $this->responseStatus = 200;
        $this->response = json_encode(array('success'=>1));


    }
    public function put()
    {
        $request = $this->request['params'];
        $this->response = json_encode(array())  ;
        $this->responseStatus = 200;
    }
    public function delete()
    {
        $this->response = array('TestResponse' => 'I am DELETE response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }

}
?>