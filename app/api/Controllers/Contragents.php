<?php
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main\Entity\ExpressionField;

class Controllers_Contragents extends RestController
{
    private $_hliblock = null;
    public function __construct($request)
    {
        parent::__construct($request);                    // Init parent constructor
        \Bitrix\Main\loader::includeModule('highloadblock');
        $this->_hlblock = HL\HighloadBlockTable::getById(3)->fetch();
    }
    private function getMyContragent(){
        $userId = $GLOBALS['USER']->GetID();
        $rsUsers =CUser::GetList($by, $order, array('ID' => $userId),array('SELECT'=>array('UF_CONTRAGENT')));
        if($arUser = $rsUsers->GetNext()) {
            if (strlen($arUser['UF_CONTRAGENT']) > 0) {
                $entity = HL\HighloadBlockTable::compileEntity($this->_hlblock);
                $entity_data_class = $entity->getDataClass();
                $main_query = new Entity\Query($entity);
                $main_query->setFilter(array('ID' => $arUser['UF_CONTRAGENT']));
                $main_query->setSelect(array('*'));
                $result = $main_query->exec();
                $result = new CDBResult($result);
                if ($row = $result->Fetch()) {
                    return array('Id' => $row['UF_XML_ID'],'id' => $row['ID'],'Title' => $row['UF_NAME']);
                }
            }
        }
            return array();
    }
    public function get()
    {
        $request = $this->request['params'];
        $this->responseStatus = 200;
        if ($request['filter'] == 'my'){
            $arResult = $this->getMyContragent();
            $this->response = json_encode($arResult);
            return;
        }
        //Страны
        $ctrl = new Controllers_Countries(array());
        $ctrl->get();
        $arCountries = array();
        $arTemp = json_decode($ctrl->response, true);
        foreach($arTemp as $arItem)
        {
            $arCountries[$arItem['Id']] = $arItem;
        }

        $arResult = array();

        $entity = HL\HighloadBlockTable::compileEntity($this->_hlblock);
        $entity_data_class = $entity->getDataClass();
        $filter = array();
        if (strlen($request['q']) > 0) {
            $filter[] = array(
                'LOGIC' => 'OR',
                array(
                    '%=UF_NAME' => '%'.$request['q'].'%'
                ),
                array(
                    '%=UF_SHORT_NAME' => '%'.$request['q'].'%'
                ),
                array(
                    '%=UF_INN' => '%'.$request['q'].'%'
                ),
                array(
                    '%=UF_KPP' => '%'.$request['q'].'%'
                ),
                array(
                    '%=UF_OGRN' => '%'.$request['q'].'%'
                ),
            );
        }
        if (strlen($request['guid']) > 0)
            $filter['=UF_XML_ID'] = $request['guid'];
        if ($request['id'] > 0)
            $filter['=ID'] = $request['id'];
         if ($request['fesco'] == 1)
            $filter[] = array(
                'LOGIC' => 'AND',
                '=UF_FESCO' => 1
            );
        $main_query = new Entity\Query($entity);
        $main_query->setFilter(array($filter));
        $main_query->setSelect(array('*'));
        $main_query->setOrder(array('UF_NAME' => 'ASC'));
        $limit = array(
            'nPageSize' => 10,
            'iNumPage' => $request['page'] ? $request['page'] : 1,
            'bShowAll' => false
        );
        $main_query->setLimit($limit['nPageSize']);
        $main_query->setOffset(($limit['iNumPage']-1) * $limit['nPageSize']);
        $result = $main_query->exec();
        $result = new CDBResult($result);
        while ($row = $result->Fetch())
        {
            $arFiles = array();
            foreach($row['UF_FILES'] as $file)
            {
                $rsFile = CFile::GetById($file);
                $arFile = $rsFile->Fetch();
                $arFiles[] = array('Name' => $arFile['FILE_NAME'],'Path' => CFile::GetPath($file),'Size' => $arFile['FILE_SIZE']);
            }
            $arResult[] = array(
                'Id' => $row["UF_XML_ID"],
                'id' => $row["ID"],
                'Title' => $row['UF_NAME'],
                'Name' => $row['UF_NAME'],
                'Prefix' => $row['UF_PREFIX'],
                'Fesco' => $row['UF_FESCO'] == 1 ? true: false,
                'FullName' => $row['UF_FULL_NAME'],
                'NameEn' => $row['UF_NAME_EN'],
                'OrgForm' => $row['UF_ORG_FORM'],
                'Offshore' => $row['UF_LEGAL_COUNTRY'] > 0 ? $arCountries[$row['UF_LEGAL_COUNTRY']]['Offshore'] : 0,
                'LegalCountry' => $row['UF_LEGAL_COUNTRY'],
                'LegalAddress' => $row['UF_LEGAL_ADDRESS'],
                'FullLegalAddress' => $row['UF_LEGAL_COUNTRY'] > 0 ? $arCountries[$row['UF_LEGAL_COUNTRY']]['Title'].', '.$row['UF_LEGAL_ADDRESS'] : $row['UF_LEGAL_ADDRESS'],
                'ActualCountry' => $row['UF_ACTUAL_COUNTRY'],
                'ActualAddress' => $row['UF_ACTUAL_ADDRESS'],
                'FullActualAddress' => $row['UF_ACTUAL_COUNTRY'] > 0 ? $arCountries[$row['UF_ACTUAL_COUNTRY']]['Title'].', '.$row['UF_ACTUAL_ADDRESS'] : $row['UF_ACTUAL_ADDRESS'],
                'Resident' => $row['UF_RESIDENT'],
                'Agcy' => $row['UF_AGCY'],
                'Phone' => $row['UF_PHONE'],
                'Fax' => $row['UF_FAX'],
                'WWW' => $row['UF_WWW'],
                'OGRN' => $row['UF_OGRN'],
                'INN' => $row['UF_INN'],
                'KPP' => $row['UF_KPP'],
                'Files' => $arFiles
            );
        }

        if (count($arResult) == 1 && $request['type'] != 'list') {
            $arResult = $arResult[0];
            $this->response = json_encode($arResult);
        } else {
            $arData = array();
            $countQuery = new Entity\Query($entity_data_class::getEntity());
            $countQuery->addSelect(new ExpressionField('CNT', 'COUNT(1)'));
            $countQuery->setFilter($filter);
            $totalCount = $countQuery->setLimit(null)->setOffset(null)->exec()->fetch();
            $totalCount = (int)$totalCount['CNT'];
            $totalPages = ceil($totalCount/$limit['nPageSize']);
            $NavFirstRecordShow = ($limit['iNumPage']-1)*$limit['nPageSize']+1;
            if ($limit['iNumPage'] != $totalPages)
                $NavLastRecordShow = $limit['iNumPage'] * $limit['nPageSize'];
            else
                $NavLastRecordShow = $totalCount;
            unset($countQuery);
            $arData['data'] = array(
                'Items' => $arResult,
                'Count' => $totalCount,
                'NavFirstRecordShow' => $NavFirstRecordShow,
                'NavLastRecordShow' => $NavLastRecordShow
            );
            $this->response = json_encode($arData);
        }
    }
    public function post()
    {
        $request = $this->request['params'];
        // get entity
        $entity = HL\HighloadBlockTable::compileEntity($this->_hlblock);
        /** @var HL\DataManager $entity_data_class */
        $entity_data_class = $entity->getDataClass();
        if ($request['Id'] <= 0) {
            $data = array(
                'UF_FULL_NAME' => $request['FullName'],
                'UF_NAME' => $request['Name'],
                'UF_NAME_EN' => $request['NameEn'],
                'UF_LEGAL_COUNTRY' => $request['LegalCountry'] ,
                'UF_LEGAL_ADDRESS' => $request['LegalAddress'],
                'UF_ACTUAL_COUNTRY' => $request['ActualCountry'],
                'UF_ACTUAL_ADDRESS' => $request['ActualAddress'],
                'UF_PHONE' => $request['Phone'],
                'UF_FAX' => $request['Fax'],
                'UF_WWW' => $request['WWW'],
                'UF_OGRN' => $request['OGRN'],
                'UF_INN' => $request['INN'],
                'UF_KPP' => $request['KPP'],
                'UF_RESIDENT' => $request['Resident'],
                'UF_AGCY' => strpos($request['INN'],'9909') !== false ? 1 : 0,
                'UF_FESCO' => $request['Fesco'],
                'UF_XML_ID' => CFESCOTools::GUID()
            );
            $result = $entity_data_class::add($data);
            if($result->isSuccess())
            {
                $this->response = json_encode(array('success' => 1,'Id' => $result->getId()));
            }
        }
        else {
            if ($request['mode'] == 'add_files')
            {
                $data = array();
                $list = $entity_data_class::getList(array(
                    'filter' => array('ID' => $request['Id']),
                    'select' => array('UF_FILES')
                ));
                $arFiles = array();
                if($row = $list->fetch())
                {
                    foreach ($row['UF_FILES'] as $file)
                    {
                        $arFile = array('name' => '','type' => '','tmp_name'=>'','error'=>4,'size'=>0,'del'=>false,'old_id' => $file);
                        $arFiles[] = $arFile;
                    }
                }
                $i = 0;
                $arFiles['n'.$i++] = $_FILES['file'];
                $result = $entity_data_class::update($request['Id'], array('UF_FILES' => $arFiles));
                if ($result->isSuccess())
                    $this->response = json_encode(array('success' => 1));
                else
                    $this->response = json_encode(array('success' => 0));
            }
            else {
                $data = array(
                    'UF_FULL_NAME' => $request['FullName'],
                    'UF_NAME' => $request['Name'],
                    'UF_NAME_EN' => $request['NameEn'],
                    'UF_LEGAL_COUNTRY' => $request['LegalCountry'] ,
                    'UF_LEGAL_ADDRESS' => $request['LegalAddress'],
                    'UF_ACTUAL_COUNTRY' => $request['ActualCountry'],
                    'UF_ACTUAL_ADDRESS' => $request['ActualAddress'],
                    'UF_PHONE' => $request['Phone'],
                    'UF_FAX' => $request['Fax'],
                    'UF_WWW' => $request['WWW'],
                    'UF_OGRN' => $request['OGRN'],
                    'UF_INN' => $request['INN'],
                    'UF_KPP' => $request['KPP']
                );
                $result = $entity_data_class::update($request['Id'],$data);
                if($result->isSuccess())
                    $this->response = json_encode(array('success' => 1, 'Id' => $request['Id']));
                else
                    $this->response = json_encode(array('success' => 0));
            }

        }
        $this->request['content-type'] = 'json';

    }
    public function put()
    {
        $request = $this->request['params'];
        $this->response = json_encode(array());
        $this->responseStatus = 200;
    }
    public function delete()
    {
        $this->response = array('TestResponse' => 'I am DELETE response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }
}