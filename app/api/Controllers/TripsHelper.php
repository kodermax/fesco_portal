<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 26.11.2014
 * Time: 12:36
 */
class Controllers_TripsHelper extends RestController
{
    private $_helper = null;
    public function __construct($request)
    {
        parent::__construct($request);                    // Init parent constructor
        $this->_helper = new CFESCOIBlockTrips();
    }
    public function get()
    {
        $request = $this->request['params'];
        $arResult = array();
        $this->responseStatus = 200;
        $arResult = $this->_helper->GetTransportType();
        $num = 1;
        $userId = intval($GLOBALS['USER']->GetID());
        if ($userId > 0) {
            CModule::IncludeModule("intranet");
            $dbUser = CUser::GetList($by, $order, array("ID_EQUAL_EXACT" => $userId), array("SELECT" => array("UF_*")));
            $arUser = $dbUser->GetNext();
            $i = 0;
            while ($i < $num) {
                $i++;
                $arManagers = CIntranetUtils::GetDepartmentManager($arUser["UF_DEPARTMENT"], $arUser["ID"], true);
                foreach ($arManagers as $key => $value) {
                    $arUser = $value;
                    break;
                }
            }
        }
        $this->response =  json_encode(array(
            'Approver' => array('Id' => $arUser['ID'],'Title'=>$arUser['NAME']. ' '.$arUser['LAST_NAME']),
            'TransportItems' => $arResult
            )
        );

    }
    public function post()
    {
        $request = $this->request['params'];
        $this->request['content-type'] = 'json';

        $this->response = json_encode(array('success'=>1));

    }
    public function put()
    {
        $request = $this->request['params'];
        $this->response = json_encode(array());
        $this->responseStatus = 200;
    }
    public function delete()
    {
        $this->response = array('TestResponse' => 'I am DELETE response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }
}