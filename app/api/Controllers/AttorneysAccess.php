<?
class Controllers_AttorneysAccess extends RestController
{

    public function __construct($request)
    {
        parent::__construct($request);	    			// Init parent constructor
        \Bitrix\Main\Loader::includeModule('iblock');

    }
    public function get()
    {
        $arResult = array();
        $userId = $GLOBALS['USER']->GetID();
        $request = $this->request['params'];
        $this->responseStatus = 200;

        if ($request['entity'] == 'menu') {
            $rsGroups = CGroup::GetList($by = "c_sort", $order = "asc", array("STRING_ID" => 'admin_attorneys'));
            if ($arGroups = $rsGroups->Fetch()) {
                $groupId = $arGroups['ID'];
                if (in_array($groupId, $GLOBALS['USER']->GetUserGroupArray())) {
                    $this->response = json_encode(array('admin' => 1));
                    return;
                }
            }
        }
        $this->response = json_encode(array('admin' => 0));
    }
    public function post()
    {
        $request = $this->request['params'];
        $this->request['content-type'] = 'json';
        $rsProp = CIBlockElement::GetProperty(array(),array('IBLOCK_ID'=>$this->_iblockId,'CODE' => 'SEQ_ID'));
        if($arProp = $rsProp->GetNext())
        {
            $PROPERTY_ID = $arProp['ID'];
        }

        $seq = new CIBlockSequence($this->_iblockId, $PROPERTY_ID);
        $arProp = array(
            'COMPANY_ID' => $request['Company']["Id"],
            'USER_ID' => $request['User']['Id'],
            'PASSPORT_ID' => $request['Passport']["Id"],
            'PASSPORT_DATE' => date('d.m.Y',strtotime($request['Passport']["Date"])),
            'PASSPORT_WHOM' => $request['Passport']["Whom"],
            'PASSPORT_DEP' => $request['Passport']["Dep"],
            'START_DATE' => date('d.m.Y',strtotime($request['StartDate'])),
            'FINISH_DATE' => date('d.m.Y',strtotime($request['FinishDate'])),
            'TRANSFERENCE' => $this->_transference[$request['Transference']],
            'STATUS' => $this->_arStatus['draft'],
            'APPROVER' => $request['Approver']['Id'],
            $PROPERTY_ID => $seq->GetNext()

        );
        $rsUser = CUser::GetByID($request['User']['Id']);
        $arUser = $rsUser->Fetch();
        $arFields = array(
            "NAME" => "Доверенность для ".$arUser['NAME']." ".$arUser['LAST_NAME'],
            'PROPERTY_VALUES' => $arProp,
            "IBLOCK_SECTION_ID" => false,
            'IBLOCK_ID' => $this->_iblockId,
            'PREVIEW_TEXT' => $request['Description']
        );
        $el = new CIBlockElement;
        if($ID = $el->Add($arFields,false,false))
        {
            $templateId = $this->GetTemplateId($ID);
            $wfId  = CBPDocument::StartWorkflow(
                $templateId,
                array("iblock","CIBlockDocument",$ID),
                array("Approvers"=>array("user_".$request['Approver']['Id'])),$arErrorsTmp);
            if (count($arErrorsTmp) <= 0)
            {
                CIBlockElement::SetPropertyValuesEx($ID,$this->_iblockId,array('STATUS' => $this->_arStatus['approving']));
            }
            $this->status = 200;
            $this->responseStatus = 200;
            $this->response = json_encode(array('ID' => $ID,'SUCCESS'=> 1,'ERRORS'=>$arErrorsTmp  ));
        }
        else {
            $this->status = 200;
            $this->responseStatus = 200;
            $this->response = json_encode(array('SUCCESS'=>0,'ERROR' => $el->LAST_ERROR));
        }

    }
    private function GetTemplateId($docId)
    {
        \Bitrix\Main\Loader::includeModule('bizproc');
        $dbWorkflowTemplate = CBPWorkflowTemplateLoader::GetList(
            array(),
            array("DOCUMENT_TYPE" => array("iblock", "CIBlockDocument", "iblock_".$this->_iblockId), "ACTIVE"=>"Y"),
            false,
            false,
            array("ID", "NAME", "DESCRIPTION", "MODIFIED", "USER_ID", "AUTO_EXECUTE", "USER_NAME", "USER_LAST_NAME", "USER_LOGIN", "USER_SECOND_NAME")
        );
        while ($arWorkflowTemplate = $dbWorkflowTemplate->GetNext())
        {
            return $arWorkflowTemplate['ID'];
        }
        return 0;
    }
    public function put()
    {
        $request = $this->request['params'];
        $id = $request['Id'];
        if ($request['action'] == 'cancel')
        {
            CIBlockElement::SetPropertyValuesEx($id, false, array('STATUS' => $this->_arStatus['cancel']));
            $this->response = json_encode(array('SUCCESS' => '1'));
            $this->responseStatus = 200;
            //Отправить уведомление
            $userId = $request['User']['Id'];
            $rsUser = CUser::GetByID($userId);
            $arUser = $rsUser->Fetch();
            $rsSites = CSite::GetByID("s1");
            $arSite = $rsSites->Fetch();
            $url = "http://".$arSite['SERVER_NAME']."/services/requests/attorneys/#/requests/".$request['Id'];
            if (strlen($arUser['EMAIL']) > 0) {
                $arEventFields = array(
                    "EMAIL_TO" => $arUser['EMAIL'],
                    "SUBJECT" => "Ваша заявка на доверенность отклонена",
                    "MESSAGE" => "
                            Пользователь ".$GLOBALS['USER']->GetFullName()." отклонил Вашу заявку на доверенность.
                            <br>
                            <a href='".$url."'>Перейти к заявке</a>
                            <br><br>"
                );
                CEvent::Send("MAIL_TEMPLATE", "s1", $arEventFields);
            }
            return;
        }
        $this->response = json_encode(array('SUCCESS' => '0'))  ;
        $this->responseStatus = 200;
    }
    public function delete()
    {
        $this->response = array('TestResponse' => 'I am DELETE response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }

}
?>