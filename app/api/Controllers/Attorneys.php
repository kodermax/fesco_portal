<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 07.11.2014
 * Time: 14:57
 */
class Controllers_Attorneys extends RestController
{
    private $_iblockId = 0;
    private $_arStatus = array();
    private $_arStatusIds = array();
    private $_propertySeqId = 0;
    private $_iblockRequestsId = 0;
    private $_arRequestsStatus = array();

    public function __construct($request)
    {
        parent::__construct($request);                // Init parent constructor
        \Bitrix\Main\Loader::includeModule('iblock');
        $res = CIBlock::GetList(
            Array(),
            Array(
                "CODE" => 'done_attorneys'
            ), true
        );
        if ($ar_res = $res->Fetch()) {
            $this->_iblockId = $ar_res['ID'];
        }
        $res = CIBlock::GetList(
            Array(),
            Array(
                "CODE" => 'req_attorneys'
            ), true
        );
        if ($ar_res = $res->Fetch()) {
            $this->_iblockRequestsId = $ar_res['ID'];
        }
        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => $this->_iblockId, "CODE" => "STATUS"));
        while ($enum_fields = $property_enums->GetNext()) {
            $this->_arStatus[$enum_fields['XML_ID']] = $enum_fields['ID'];
            $this->_arStatusIds[$enum_fields['ID']] = $enum_fields['XML_ID'];
        }
        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => $this->_iblockRequestsId, "CODE" => "STATUS"));
        while ($enum_fields = $property_enums->GetNext()) {
            $this->_arRequestsStatus[$enum_fields['XML_ID']] = $enum_fields['ID'];
        }
        $properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$this->_iblockId,'CODE'=>'SEQ'));
        if ($prop_fields = $properties->GetNext())
        {
            $this->_propertySeqId = $prop_fields["ID"];
        }

    }
    public function get()
    {
        $request = $this->request['params'];
        $userId =  $GLOBALS['USER']->GetID();
        $arResult = array();
        $arFilter = array('IBLOCK_ID' => $this->_iblockId);
        if ($request['id'] > 0)
            $arFilter['ID'] = $request['id'];
        if ($request['filter'] == 'my') {
            $arFilter['PROPERTY_USER_ID'] = $userId;
        }
        $arSort = array('ID'=>'DESC');
        $arNavParams = array('nPageSize' => 10);
        if ($request['page'] > 0)
            $arNavParams['iNumPage'] = $request['page'];
        $list = CIBlockElement::GetList($arSort, $arFilter,false,$arNavParams,
            array('ID','DATE_CREATE','NAME','PROPERTY_CONTRAGENT_ID','PROPERTY_ATTORNEY_ID','PROPERTY_STATUS',
                'PROPERTY_SIGNATORY_ID','PROPERTY_REQUEST_ID',
                'PROPERTY_DOC_FILE',
                'PROPERTY_USER_NAME',
                'PROPERTY_SCAN','PROPERTY_USER_ID','ACTIVE_TO','ACTIVE_FROM','PREVIEW_TEXT'));
        $total = $list->NavRecordCount;
        $NavFirstRecordShow = ($list->NavPageNomer-1)*$list->NavPageSize+1;

        if ($list->NavPageNomer != $list->NavPageCount)
            $NavLastRecordShow = $list->NavPageNomer * $list->NavPageSize;
        else
            $NavLastRecordShow = $list->NavRecordCount;


        while($row = $list->GetNext()) {
            $ar = array();
            if ($row['PROPERTY_USER_ID_VALUE'] > 0) {
                $rsUser = CUser::GetByID($row['PROPERTY_USER_ID_VALUE']);
                $arUser = $rsUser->Fetch();
                $ar = array('Id' => $arUser['ID'], 'Title' => $arUser['NAME'] . ' ' . $arUser['LAST_NAME']);
            }
            else {
                $ar = array('Id' => 0, 'Title' => $row['PROPERTY_USER_NAME_VALUE']);
            }
            $arSignatory = array('id' => '', 'text' => '');
            if ($row['PROPERTY_SIGNATORY_ID_VALUE'] > 0) {
                $rsUser = CUser::GetByID($row['PROPERTY_SIGNATORY_ID_VALUE']);
                $arUser = $rsUser->Fetch();
                $arSignatory = array('Id' => $arUser['ID'], 'Title' => $arUser['NAME'] . ' ' . $arUser['LAST_NAME'],'id'=>$arUser['ID'],'text'=>$arUser['NAME'] . ' ' . $arUser['LAST_NAME']);
            }
            $arRequest = array();
            $rsProp = CIBlockElement::GetProperty($this->_iblockRequestsId, $row['PROPERTY_REQUEST_ID_VALUE'], "sort", "asc", array('CODE' => 'SEQ_ID'));
            if ($arProp = $rsProp->GetNext()) {
                $arRequest = array('Id' => $row['PROPERTY_REQUEST_ID_VALUE'], 'SeqId' => $arProp['VALUE']);
            }
            if ($arRequest['SeqId'] <= 0) {
                $rsProp = CIBlockElement::GetProperty($this->_iblockRequestsId, $row['PROPERTY_REQUEST_ID_VALUE'], "sort", "asc", array('CODE' => 'ID'));
                if ($arProp = $rsProp->GetNext()) {
                    $arRequest['SeqId'] = $arProp['VALUE'];
                }
            }

            switch ($this->_arStatusIds[$row['PROPERTY_STATUS_ENUM_ID']]) {
                case 'draft':
                    $cssClass = "label-warning";
                    break;
                case 'active':
                    $cssClass = "label-info";
                    break;
                case 'deactive':
                case 'expired':
                case 'revoked':
                    $cssClass = "label-important";
                    break;
                default:
                    $cssClass = "";
                    break;
            }
            $arResult[] = array(
                'Id' => $row['ID'],
                'CreateDate' => $row['DATE_CREATE'],
                'Num' => $row['PROPERTY_ATTORNEY_ID_VALUE'],
                'Title' => $row['NAME'],
                'Contragent' => array('Id' => $row['PROPERTY_CONTRAGENT_ID_VALUE']),
                'Description' => $row['~PREVIEW_TEXT'],
                'StartDate' => $row['ACTIVE_FROM'],
                'FinishDate' => $row['ACTIVE_TO'],
                'Scan' => array('Path' => CFile::GetPath($row['PROPERTY_SCAN_VALUE'])),
                'WordFile' => array('Path' => CFile::GetPath($row['PROPERTY_DOC_FILE_VALUE'])),
                'User' => $ar,
                'Signatory' => $arSignatory,
                'Request' => $arRequest,
                'Status' => array(
                    'Code' => $this->_arStatusIds[$row['PROPERTY_STATUS_ENUM_ID']],
                    'Title' => $row['PROPERTY_STATUS_VALUE'],
                    'CssClass' => $cssClass
                ),
            );
        }
        $client = new Bitrix\Main\Web\HttpClient();
        foreach($arResult as &$arItem) {
            if (strlen($arItem['Contragent']['Id']) > 0) {
                $response = $client->get("https://".$_SERVER['SERVER_NAME']."/app/api/Contragents?guid=".$arItem['Contragent']['Id']);
                $response = json_decode($response, true);
                $arItem['Contragent']['Title'] = $response['Title'];
            }
        }
        $this->responseStatus = 200;
        if (count($arResult) == 1 && $request['type'] != 'list') {
            $this->response = json_encode($arResult[0]);
            return;
        }
        $arData['data'] = array(
            'Items' => $arResult,
            'Count' => (int)$total,
            'NavFirstRecordShow' => (int)$NavFirstRecordShow,
            'NavLastRecordShow' => (int)$NavLastRecordShow
        );
        $this->response = json_encode($arData);

    }
    public function post()
    {
        $request = $this->request['params'];
        $this->responseStatus = 200;
        if ($request['mode'] == 'UploadFile')
        {
            CIBlockElement::SetPropertyValuesEx($request['Id'], false, array('SCAN' => $_FILES['file']));
            $this->response = json_encode(array('success' => '1', 'Id' => $request['Id']));
            return;
        }
        else if($request['mode'] == 'UploadDocFile')
        {
            CIBlockElement::SetPropertyValuesEx($request['Id'], false, array('DOC_FILE' => $_FILES['file']));
            $this->response = json_encode(array('success' => '1', 'Id' => $request['Id']));
            return;
        }
        $requestId = $request['requestId'];
        //Массив заявки
        $list = CIBlockElement::GetList(array(),array('ID' => $requestId),false,false,array('ID','PREVIEW_TEXT','IBLOCK_ID','NAME','PROPERTY_VALUES'));
        if ($row = $list->GetNextElement()){
            $arFields = $row->GetFields();
            $arProp = $row->GetProperties();
            //Получить префикс компании
            $prefix = 'ДВМП';
            $oContragents = new Controllers_Contragents();
            if (strlen($arProp['CONTRAGENT_ID']['VALUE']) > 0) {
                $oContragents->request['params']['xmlId'] = $arProp['CONTRAGENT_ID']['VALUE'];
                $oContragents->get();
                $row = json_decode($oContragents->response, true);
                $prefix = $row['Prefix'];
            }
            $seq = new CIBlockSequence($this->_iblockId, $this->_propertySeqId);
            $seqValue = $seq->GetNext();
            $arNewFields = array(
                'IBLOCK_ID' => $this->_iblockId,
                'NAME' => $arFields['NAME'],
                'ACTIVE_FROM' => $arProp['START_DATE']['VALUE'],
                'ACTIVE_TO' => $arProp['FINISH_DATE']['VALUE'],
                'PREVIEW_TEXT' => $arFields['~PREVIEW_TEXT'],
                'PROPERTY_VALUES' => array(
                    'CONTRAGENT_ID' => $arProp['CONTRAGENT_ID']['VALUE'],
                    'USER_ID' => $arProp['USER_ID']['VALUE'],
                    'STATUS' => $this->_arStatus['draft'],
                    'REQUEST_ID' => $requestId,
                    'ATTORNEY_ID' => $prefix."-".$seqValue."-".date('y'),
                    'SEQ' => $seqValue

                )
            );
            $el = new CIBlockElement();
            if($ID = $el->Add($arNewFields,false,false,false))
            {
                $this->response = json_encode(array('success' => 1, 'Id' => $ID));
                $this->responseStatus = 200;
                //Меняем статус заявки
                CIBlockElement::SetPropertyValuesEx($requestId, false, array('STATUS' => $this->_arRequestsStatus['wait']));
            }
            else {
                $error = $el->LAST_ERROR;
                $this->response = json_encode(array('success' => 0));
                $this->responseStatus = 200;
            }
            return;

        }
        $this->response = json_encode(array('success' => 0));
        $this->responseStatus = 200;
    }

    public function put()
    {
        $request = $this->request['params'];
        $this->responseStatus = 200;
        if ($request['action'] == 'revoke')
        {
            CIBlockElement::SetPropertyValuesEx($request['Id'],$this->_iblockId,array('STATUS' => $this->_arStatus['revoked']));
            $this->response = json_encode(array('success' => 1));
            $this->responseStatus = 200;
            return;
        }
        if ($request['action'] == 'done')
        {
            CIBlockElement::SetPropertyValuesEx($request['Id'],$this->_iblockId,array('STATUS' => $this->_arStatus['active']));
            //ID заявки
            $list = CIBlockElement::GetList(array(),array('ID'=> $request['Id'],'IBLOCK_ID'=>$this->_iblockId),false,false,array('PROPERTY_REQUEST_ID'));
            if($row = $list->GetNext())
            {
                $linkId = $row['PROPERTY_REQUEST_ID_VALUE'];
                if($linkId > 0)
                    CIBlockElement::SetPropertyValuesEx($linkId,$this->_iblockRequestsId,array('STATUS' => $this->_arRequestsStatus['done']));
            }
            $this->response = json_encode(array('success' => 1));
            //Отправить уведомление
            $userId = $request['User']['Id'];
            $rsUser = CUser::GetByID($userId);
            $arUser = $rsUser->Fetch();
            $rsSites = CSite::GetByID("s1");
            $arSite = $rsSites->Fetch();
            $url = "http://".$arSite['SERVER_NAME']."/services/requests/attorneys/#/attorneys/".$request['Id'];
            if (strlen($arUser['EMAIL']) > 0) {
                $arEventFields = array(
                    "EMAIL_TO" => $arUser['EMAIL'],
                    "SUBJECT" => "Доверенность готова",
                    "MESSAGE" => "
                Ваша доверенность готова.<br>
                <a href='" . $url . "'>Перейти к доверенности</a>
                "
                );
                CEvent::Send("MAIL_TEMPLATE", "s1", $arEventFields);
            }
        }
        $arFields = array(
            'ACTIVE_FROM' => date('d.m.Y',strtotime($request['StartDate'])),
            'ACTIVE_TO' => date('d.m.Y',strtotime($request['FinishDate'])),
            'PREVIEW_TEXT' => $request['Description']
        );
        $el = new CIBlockElement();
        if($ID = $el->Update($request['Id'], $arFields, false, false, false))
        {
            if ($request['Signatory']['Id'] > 0)
                CIBlockElement::SetPropertyValuesEx($request['Id'], $this->_iblockId,array('SIGNATORY_ID' => $request['Signatory']['Id']));
            $this->response = json_encode(array('success' => 1, 'Id' => $request['Id']));
            $this->responseStatus = 200;
            return;
        }
        $this->response = json_encode(array('success' => 0, 'Id' => $request['Id']));
        $this->responseStatus = 200;
    }
    public function delete()
    {
        $this->response = array('TestResponse' => 'I am DELETE response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }
}
?>
