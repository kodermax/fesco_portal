<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 2/5/15
 * Time: 4:26 PM
 */


use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

class Controllers_TCODocs extends RestController {

    private $_entity = null;

    function __construct($request) {
        parent::__construct($request);
        \Bitrix\Main\loader::includeModule('highloadblock');
        \Bitrix\Main\loader::includeModule('fesco');
        $filter = array(
            'select' => array('ID', 'NAME', 'TABLE_NAME', 'FIELDS_COUNT'),
            'filter' => array('=TABLE_NAME' => 'fesco_tcolibrary')
        );
        $hlblock = HL\HighloadBlockTable::getList($filter)->fetch();
        $this->_entity = HL\HighloadBlockTable::compileEntity($hlblock);
    }
    public function get()
    {
        $request = $this->request['params'];
        $this->responseStatus = 200;
        $arHelps = \FESCO\TCO::getHelpers();
        $hl = $this->_entity->getDataClass();
        $row = $hl::getList(
            array(
                'select' => array('*'),
                'filter' => array('ID' => $request['Id'])
            ))->fetch();
        if (!empty($row)){
            $contractDate = new DateTime($row['UF_CONTRACT_DATE']);
            $arData = array(
                'Contragent' => FESCO\Contragents::getContragentInfo($row['UF_CONTRAGENT']),
                'ContractDate' => $contractDate->format('d.m.Y'),
                'ContractNumber' => $row['UF_CONTRACT_NUMBER'],
                'DealType' => $arHelps['DealTypes'][$row['UF_DEAL_TYPE']],
                'Division' => $arHelps['Divisions'][$row['UF_DIVISION']],
                'Id' => $row['ID'],
                'IncomeSide' => $row['UF_INCOME_SIDE'],
                'InfoSource' => $arHelps['InfoSources'][$row['UF_INFO_SOURCE']],
                'Name' => $row['UF_NAME'],
                'Number' => $row['UF_NUMBER'],
                'Materiality' => $arHelps['Materiality'][$row['UF_MATERIALITY']],
                'Method' => FESCO\TCO::getMethod($row['UF_METHOD_ID']),
                'Sections' => json_decode($row['UF_SECTIONS'], true),
                'TaxPayer' => FESCO\Contragents::getContragentInfo($row['UF_TAXPAYER']),
                'TestContragent' => FESCO\Contragents::getContragentInfo($row['UF_TEST_CONTRAGENT']),
                'TCOMethod' => $arHelps['TCOMethods'][$row['UF_METHOD_TCO']]
            );

            $this->response = json_encode($arData);
        }
        else{
            $this->response = json_encode(array());
        }
    }

    public function post()
    {
        $request = $this->request['params'];
        $this->responseStatus = 200;
        $arHelps = \FESCO\TCO::getHelpers();
        $arData = array(
            'UF_NAME' => $request['Name'],
            'UF_TAXPAYER' => $request['TaxPayer']['id'],
            'UF_CONTRAGENT' => $request['Contragent']['id'],
            'UF_CONTRACT_DATE' => $request['ContractDate'],
            'UF_CONTRACT_NUMBER' => $request['ContractNumber'],
            'UF_DEAL_TYPE' => $request['DealType']['Id'],
            'UF_CREATED_BY' => $GLOBALS['USER']->GetID(),
            'UF_CREATE_DATE' => date('d.m.Y h:i:s'),
            'UF_CHANGE_DATE' => date('d.m.Y h:i:s'),
            'UF_INFO_SOURCE' => $request['InfoSource']['Id'],
            'UF_MATERIALITY' => $request['Materiality']['Id'],
            'UF_METHOD_ID' => $request['Method']['id'],
            'UF_TEST_CONTRAGENT' => $request['TestContragent']['id'],
            'UF_INCOME_SIDE' => $request['IncomeSide'],
            'UF_DIVISION' => $request['Division']['Id'],
            'UF_TYPE' => 'DOC'

        );
        $oCounter = null;
        if ($request['Id'] == 0) {
            $oCounter = new FESCO\Counters();
            $cnt = $oCounter->getCounter('tco_docs');
            $number = sprintf('%1$04d_', $cnt + 1);
            $number .= 'Д1_';
            if ($request['IncomeSide'])
                $number .= 'Д3_';
            else
                $number .= 'Р3_';
            $number .= $arHelps['Divisions'][$request['Division']['Id']]['Title'];
            $arData['UF_NUMBER'] = $number;

        }
        $hl = $this->_entity->getDataClass();
        if ($request['Id'] == 0) {
            $result = $hl::add($arData);
        }
        else {
            $result = $hl::update($request['Id'], $arData);
        }
        if ($result->isSuccess()) {
            if ($request['Id'] == 0)
                $oCounter->incCounter('tco_methods');
            $this->response = json_encode(array('success' => 1,'Id' => $result->getId()));
        }
        else {
            $this->response = json_encode(array('success' => 0, 'errors' => $result->getErrors()));
        }
    }

    public function put()
    {
        $request = $this->request['params'];
        $this->responseStatus = 200;
        $hl = $this->_entity->getDataClass();
        $row = $hl::getList(
            array(
                'select' => array('UF_SECTIONS'),
                'filter' => array('ID' => $request['Id'])
            ))->fetch();
        if (!empty($row)){
            $arSections = json_decode($row['UF_SECTIONS'], true);
        }
        $arSections[$request['Name']]['Status'] = intval($request['Value']);
        $arData = array('UF_SECTIONS' => json_encode($arSections));
        $result = $hl::update($request['Id'], $arData);
        $this->response = json_encode(array('success' => 1));
    }

    public function delete()
    {
        // TODO: Implement delete() method.

    }
}