<?

class Controllers_Employees extends RestController
{
    public function __construct($request)
    {
        parent::__construct($request);				// Init parent constructor
    }

    public function get()
    {
        $arResult = array();
        $this->responseStatus = 200;
        $request = $this->request['params'];
        $arParams["NAME_TEMPLATE"] = CSite::GetNameFormat();
        $arFilter['!UF_DEPARTMENT'] = false;
        $arFilter['ACTIVE'] = 'Y';
        $q = trim($request['q']);
        if ($request['Id'] > 0)
            $arFilter['ID'] = $request['Id'];
        if (strlen($q) > 0)
            $arFilter['NAME'] = "%".$q."%";
        if ($request['filter'] == 'my')
        {
            $arFilter['ID'] = $GLOBALS['USER']->GetID();
        }
        $dbRes = CUser::GetList($by = 'last_name', $order = 'asc', $arFilter, array(
            'SELECT' => array('UF_DEPARTMENT'),
            'NAV_PARAMS'=>array("nPageSize"=>"20")));
        while ($arRes = $dbRes->GetNext())
        {
            $arResult[] = array(
                'id' => $arRes['ID'],
                'Id' => $arRes['ID'],
                'text' => CUser::FormatName($arParams["NAME_TEMPLATE"], $arRes, true, false),
                'WorkPosition' => $arRes['WORK_POSITION'] ? $arRes['WORK_POSITION'] : $arRes['PERSONAL_PROFESSION'],
                'PHOTO' => (string)CIntranetUtils::createAvatar($arRes, array(), $arParams['SITE_ID']),
                'HEAD' => false
            );
        }
        if ($request['filter'] == 'my' && count($arResult) == 1)
            $this->response = json_encode($arResult[0]);
        else
            $this->response = json_encode($arResult);
    }

    public function post()
    {
        $request = $this->request['params'];

    }

    public function put()
    {
        $this->response = array('TestResponse' => 'I am PUT response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }
    public function delete()
    {
        $this->response = array('TestResponse' => 'I am DELETE response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }

}
