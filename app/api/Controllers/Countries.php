<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 26.11.2014
 * Time: 12:36
 */
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
class Controllers_Countries extends RestController
{

    public function __construct($request)
    {
        parent::__construct($request);                    // Init parent constructor
        \Bitrix\Main\loader::includeModule('highloadblock');
    }
    public function get()
    {
        $request = $this->request['params'];
        $arResult = array();
        $hlblock = HL\HighloadBlockTable::getById(2)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $main_query = new Entity\Query($entity);
        $main_query->setSelect(array('*'));
        $main_query->setOrder(array('UF_NAME' => 'ASC'));
        $result = $main_query->exec();
        $result = new CDBResult($result);
        while ($row = $result->Fetch())
        {
            $arResult[] = array(
                'Id' => $row["UF_XML_ID"],
                'Title' => $row['UF_NAME'],
                'Offshore' => $row['UF_OFFSHORE']
            );
        }
        $this->responseStatus = 200;
        $this->response =  json_encode($arResult);

    }
    public function post()
    {
        $request = $this->request['params'];
        $this->request['content-type'] = 'json';

        $this->response = json_encode(array('success'=>1));

    }
    public function put()
    {
        $request = $this->request['params'];
        $this->response = json_encode(array());
        $this->responseStatus = 200;
    }
    public function delete()
    {
        $this->response = array('TestResponse' => 'I am DELETE response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }
}