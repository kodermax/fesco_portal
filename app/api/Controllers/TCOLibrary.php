<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 1/30/15
 * Time: 11:47 AM
 */
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main\Entity\ExpressionField;


class Controllers_TCOLibrary extends RestController
{
    private $_entity = null;
    private $arDealTypes = array();

    function __construct($request){
        parent::__construct($request);
        \Bitrix\Main\loader::includeModule('highloadblock');
        \Bitrix\Main\loader::includeModule('fesco');
        $hlblock = HL\HighloadBlockTable::getById(5)->fetch();
        $this->_entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $rsTypes = CUserFieldEnum::GetList(array(), array(
            "USER_FIELD_NAME" => 'UF_DEAL_TYPE',
        ));
        while($arType = $rsTypes->GetNext()) {
            $this->arDealTypes[$arType['ID']] = $arType['VALUE'];
        }
    }
    public function get()
    {
        $arResult = array();
        $filter = array();
        $request = $this->request['params'];
        $this->responseStatus = 200;
        $limit = array(
            'nPageSize' => 10,
            'iNumPage' => $request['page'] ? $request['page'] : 1,
            'bShowAll' => false
        );

        $main_query = new Entity\Query($this->_entity);
        $main_query->setSelect(array('*'));
        $main_query->setOrder(array('UF_NAME' => 'ASC'));
        $main_query->setFilter(array($filter));
        $main_query->setLimit($limit['nPageSize']);
        $main_query->setOffset(($limit['iNumPage']-1) * $limit['nPageSize']);
        $result = $main_query->exec();
        $result = new CDBResult($result);
        while ($row = $result->Fetch())
        {
            $createdDate = new DateTime($row['UF_CREATE_DATE']);
            $changedDate = new DateTime($row['UF_CHANGE_DATE']);
            $arResult[] = array(
                'Id' => $row['ID'],
                'Number' => $row['UF_NUMBER'],
                'Contragent' => FESCO\Contragents::getContragentInfo($row['UF_CONTRAGENT']),
                'TaxPayer' => FESCO\Contragents::getContragentInfo($row['UF_TAXPAYER']),
                'GUID' => $row["UF_XML_ID"],
                'Title' => $row['UF_NAME'],
                'Type' => $row['UF_TYPE'],
                'DealType' => array('Title' => $this->arDealTypes[$row['UF_DEAL_TYPE']]),
                'Author' => FESCO\Users::getById($row['UF_CREATED_BY']),
                'Date' => array('Change' => $changedDate->format('d.m.Y'),'Create' => $createdDate->format('d.mm.Y')),


            );

        }
        if (count($arResult) == 1 && $request['type'] != 'list') {
            $arResult = $arResult[0];
            $this->response = json_encode($arResult);
        } else {
            $arData = array();
            $countQuery = new Entity\Query($this->_entity);
            $countQuery->addSelect(new ExpressionField('CNT', 'COUNT(1)'));
            $countQuery->setFilter($filter);
            $totalCount = $countQuery->setLimit(null)->setOffset(null)->exec()->fetch();
            $totalCount = (int)$totalCount['CNT'];
            $totalPages = ceil($totalCount/$limit['nPageSize']);
            $NavFirstRecordShow = ($limit['iNumPage']-1)*$limit['nPageSize']+1;
            if ($limit['iNumPage'] != $totalPages)
                $NavLastRecordShow = $limit['iNumPage'] * $limit['nPageSize'];
            else
                $NavLastRecordShow = $totalCount;
            unset($countQuery);
            $arData['data'] = array(
                'Items' => $arResult,
                'Count' => $totalCount,
                'NavFirstRecordShow' => $NavFirstRecordShow,
                'NavLastRecordShow' => $NavLastRecordShow
            );
            $this->response = json_encode($arData);
        }
    }

    public function post()
    {
        // TODO: Implement post() method.
    }

    public function put()
    {
        // TODO: Implement put() method.
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }
}