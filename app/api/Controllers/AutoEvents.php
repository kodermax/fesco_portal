<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 1/22/15
 * Time: 12:39 PM
 */
class Controllers_AutoEvents extends RestController
{
    private $_helper = null;
    public function __construct($request)
    {
        parent::__construct($request);	    			// Init parent constructor
        \Bitrix\Main\loader::includeModule('iblock');
        $this->_helper = new CFESCOIBlockAuto();
    }
    public function get()
    {
        $arResult = array();
        $userId = $GLOBALS['USER']->GetID();
        $request = $this->request['params'];
        $arFilter = array('IBLOCK_ID' => $this->_helper->GetRequestsIBlockId(),'ACTIVE'=>'Y');
        $arFilter['PROPERTY_CONTRAGENT_ID'] = $this->_helper->GetCompanyIdByUser($userId);

        $arSelect = array('ID','IBLOCK_ID','ACTIVE_TO','ACTIVE_FROM','NAME','PREVIEW_TEXT',
            'PROPERTY_USER_ID',
            'PROPERTY_SHIPPING_ADDRESS',
            'PROPERTY_DESTINATION',
            'PROPERTY_TARGET',
            'PROPERTY_STATUS',
            'PROPERTY_AUTO_ID'
        );
        $list = CIBlockElement::Getlist(array('ID' => "DESC"),$arFilter,false,false, $arSelect);
        while($row = $list->GetNext())
        {
            $arUser = $this->_helper->GetUserInfoArray($row['PROPERTY_USER_ID_VALUE']);
            $arResult[] = array(
                'id' => $row['ID'],
                'text' => $row['PROPERTY_TARGET_VALUE'],
                'author' => $arUser['Title'],
                'start_date' => date('m-d-Y H:i:s',MakeTimeStamp($row['ACTIVE_FROM'], "DD.MM.YYYY HH:MI:SS")),
                'end_date' => date('m-d-Y H:i:s',MakeTimeStamp($row['ACTIVE_TO'], "DD.MM.YYYY HH:MI:SS")),
                'car_id' => intval($row['PROPERTY_AUTO_ID_VALUE']) > 0 ? $row['PROPERTY_AUTO_ID_VALUE']: 'requests',
                'type' => $this->_helper->GetStatusXmlIdById($row['PROPERTY_STATUS_ENUM_ID']),
                'pick_location' => $row['PROPERTY_SHIPPING_ADDRESS_VALUE'],
                'drop_location' => $row['PROPERTY_DESTINATION_VALUE'],
                'description' => $row['~PREVIEW_TEXT'],
                'readonly' => true
            );
        }
        $this->responseStatus = 200;
        $this->request['content-type'] = 'json_txt';
        $this->response = json_encode($arResult);

    }
    public function post()
    {
        $request = $this->request['params'];
        $this->responseStatus = 200;
        $el = new CIBlockElement();
        $arUpdateFields = array(
            'ACTIVE_FROM' =>  date('d.m.Y H:i',strtotime($request['start_date'])),
            'ACTIVE_TO' =>  date('d.m.Y H:i',strtotime($request['end_date'])),
            'PREVIEW_TEXT' => $request['description'],
        );
        if($ID = $el->Update($request['Id'],$arUpdateFields,false,false,false,false))
        {
            $arProp = array(
                'DESTINATION' => $request['drop_location'],
                'SHIPPING_ADDRESS' => $request['pick_location'],
                'AUTO_ID' => $request['car_id'] != 'requests' ? $request['car_id'] : '',
                'TARGET' => $request['text']
            );
            if ($request['car_id'] != 'requests')
            {
                $arProp['STATUS'] = $this->_helper->GetStatusEnumIdByXmlId('approve');
                //Отправляем уведомление инициатору заявке
                $templateUrl = $this->_helper->GetTemplateUrl($request['Id']);
                $arUser = $this->_helper->GetUserInfoByRequest($request['Id']);
                if ($arUser['EMAIL']) {
                    $arEventFields = array(
                        'EMAIL_TO' => $arUser['EMAIL'],
                        'MESSAGE' => 'Ваша заявка на автотранспорт одобрена.
                                        <br>
                                        <a href="' . $templateUrl['requests.detail'] . '">Перейти к заявке</a>',
                        'SUBJECT' => 'Ваша заявка на автотранспорт одобрена',
                    );
                    CEvent::Send("MAIL_TEMPLATE", "s1", $arEventFields);
                }
            }
            CIBlockElement::SetPropertyValuesEx($request['Id'],$this->_helper->GetRequestsIBlockId(),$arProp);
            $this->response = json_encode(array('success' => 1));
        }
        else {
            $this->response = json_encode(array('success' => 0));
        }


    }
    public function put()
    {
        $this->responseStatus = 200;
        $request = $this->request['params'];
        $el = new CIBlockElement();
        if($request['mode'] == 'cancel' && $request['Id'] > 0)
        {
            $el->Update($request['Id'],array('ACTIVE' => 'N'),false,false,false);
            $arProp = array('STATUS' => $this->_helper->GetStatusEnumIdByXmlId('cancel'));
            CIBlockElement::SetPropertyValuesEx($request['Id'],$this->_helper->GetRequestsIBlockId(),$arProp);
            //Отправляем уведомление инициатору заявке
            $templateUrl = $this->_helper->GetTemplateUrl($request['Id']);
            $arUser = $this->_helper->GetUserInfoByRequest($request['Id']);
            if ($arUser['EMAIL']) {
                $arEventFields = array(
                    'EMAIL_TO' => $arUser['EMAIL'],
                    'MESSAGE' => 'Ваша заявка на автотранспорт отклонена.
                                        <br>
                                        <a href="' . $templateUrl['requests.detail'] . '">Перейти к заявке</a>',
                    'SUBJECT' => 'Ваша заявка на автотранспорт отклонена.',
                );
                CEvent::Send("MAIL_TEMPLATE", "s1", $arEventFields);
            }
            $this->response = json_encode(array('success' => 1));
            return;
        }
        $this->response = json_encode(array('success' => 1));

    }
    public function delete()
    {
        $this->response = array('TestResponse' => 'I am DELETE response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }
}
?>