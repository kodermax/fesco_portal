<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 2/16/15
 * Time: 12:16 PM
 */
class Controllers_ADAccount extends RestController
{

    public function get()
    {
        // TODO: Implement get() method.
    }

    public function post()
    {
        $this->responseStatus = 200;
        $request = $this->request['params'];
        $request['UserName'] = quotemeta($request['UserName']);
        if ($request['UserName'] && $request['OldPassword'] && $request['NewPassword']) {
            if (($pos = strpos($request['UserName'], "\\\\")) !== false)
            {
                $DOMAIN = substr($request['UserName'], 0, $pos);
                $request['UserName'] = substr($request['UserName'], $pos + 2);
            }

            $client = new \Bitrix\Main\Web\HttpClient();
            $client->setHeader('Content-Type', 'application/json', true);
            $response = $client->post('http://msk-app14/api/Account/ChangePassword', json_encode($request));
            $this->response = $response;
            return;
        }
        $this->response = json_encode(array('Error' => 'Сервис не доступен','Success'=> 0));
   }

    public function put()
    {
        // TODO: Implement put() method.
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }
}
?>