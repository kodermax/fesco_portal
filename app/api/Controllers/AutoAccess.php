<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 1/22/15
 * Time: 12:39 PM
 */
class Controllers_AutoAccess extends RestController
{
    private $_helper = null;
    public function __construct($request)
    {
        parent::__construct($request);	    			// Init parent constructor
        \Bitrix\Main\Loader::includeModule('iblock');
        $this->_helper = new CFESCOIBlockAuto();
    }
    public function get()
    {
        $request = $this->request['params'];
        $this->responseStatus = 200;
        if ($request['entity'] == 'menu') {
            if ($this->_helper->isAdmin() || $GLOBALS['USER']->IsAdmin())
            {
                $this->response = json_encode(array('isAdministrator' => true));

            }
            else {
                $this->response = json_encode(array('isUser' => true));
            }
        }

    }
    public function post()
    {
        $request = $this->request['params'];
        $this->request['content-type'] = 'json';
        $this->responseStatus = 200;
        $this->response = json_encode(array('success'=>0));
    }
    public function put()
    {
        $request = $this->request['params'];
        $this->response = json_encode(array('success' => '0'))  ;
        $this->responseStatus = 200;
    }
    public function delete()
    {
        $this->response = array('TestResponse' => 'I am DELETE response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }

}
?>