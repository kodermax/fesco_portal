<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 1/30/15
 * Time: 12:47 PM
 */

IncludeModuleLangFile(__FILE__);
if (class_exists("fesco"))
    return;

class fesco extends CModule
{
    var $MODULE_ID = "fesco";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;
    var $MODULE_GROUP_RIGHTS = "N";

    function fesco()
    {
        $arModuleVersion = array();
        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path."/version.php");
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = "FESCO";
        $this->MODULE_DESCRIPTION = "Модуль для компании FESCO";
    }

    function GetModuleTasks()
    {
        return array();
    }

    function InstallDB($arParams = array())
    {
        global $DB, $DBType, $APPLICATION;

        RegisterModule("fesco");
        CModule::IncludeModule("fesco");

        return true;
    }

    function UnInstallDB($arParams = array())
    {
        global $DB, $DBType, $APPLICATION;

        UnRegisterModule("fesco");

        return true;
    }

    function InstallEvents()
    {
        return true;
    }

    function UnInstallEvents()
    {
        return true;
    }

    function InstallFiles($arParams = array())
    {
        return true;
    }

    function UnInstallFiles()
    {

        return true;
    }

    function DoInstall()
    {
        global $USER, $APPLICATION;

        if ($USER->IsAdmin())
        {
            if ($this->InstallDB())
            {
                $this->InstallEvents();
                $this->InstallFiles();
            }
            $APPLICATION->IncludeAdminFile('Установка модуля FESCO', $_SERVER["DOCUMENT_ROOT"]."/local/modules/fesco/install/step.php");
        }
    }

    function DoUninstall()
    {
        global $USER, $APPLICATION, $step;
        if ($USER->IsAdmin())
        {
            $step = IntVal($step);
            if ($step < 2)
            {
                $APPLICATION->IncludeAdminFile("Деинсталляция модуля FESCO", $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/fesco/install/unstep1.php");
            }
        }
    }
}
