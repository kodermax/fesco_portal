<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 1/30/15
 * Time: 12:44 PM
 */

CModule::AddAutoloadClasses('fesco', array(
    'FescoAttorneys' => 'lib/attorneys.php',
    'FescoAuto' => 'lib/auto.php',
    'FescoContragents' => 'lib/contragents.php',
    'FescoCountries' => 'lib/countries.php',
    'FescoPayPlan' => 'lib/payplan.php',
    'FescoTCO' => 'lib/tco.php',
    'FescoTrips' => 'lib/trips.php',
    'FESCO\Counters' => 'lib/counters.php',
    'FescoUsers' => 'lib/users.php',
    'FescoUtils' => 'lib/utils.php'
));