<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 2/3/15
 * Time: 3:45 PM
 */

namespace FESCO;

use Bitrix;
use Bitrix\Highloadblock as HL;
class Counters {
    private $_hlblock;
    private $_entity;
    function __construct() {
        $filter = array(
            'select' => array('ID', 'NAME', 'TABLE_NAME', 'FIELDS_COUNT'),
            'filter' => array('=TABLE_NAME' => 'fesco_counters')
        );
        $this->_hlblock = HL\HighloadBlockTable::getList($filter)->fetch();
        $this->_entity = HL\HighloadBlockTable::compileEntity($this->_hlblock);


    }
    public function getCounter($code){
        $hlClass = $this->_entity->getDataClass();
        $row = $hlClass::getList(array(
            'select' => array('*'),
            'filter' => array('UF_CODE' => $code)
        ))->fetch();
        if (!empty($row))
        {
            return $row['UF_CNT'];
        }
        return 0;
    }
    public function incCounter($code){
        $hlClass = $this->_entity->getDataClass();
        $row = $hlClass::getList(array(
            'select' => array('*'),
            'filter' => array('UF_CODE' => $code)
        ))->fetch();
        if (!empty($row))
        {
            $hlClass::update($row['ID'],array("UF_CNT" => $row['UF_CNT'] + 1));
        }

    }
}
