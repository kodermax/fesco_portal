<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 4/14/15
 * Time: 11:22 AM
 */
class FescoUtils{
    function __construct()
    {

    }
    static function getWorkingDays($dateFrom, $dateTo) {
        $holidays = COption::GetOptionString('calendar', 'year_holidays');
        $holidays = explode(',',$holidays);
        $holidays2 = $holidays;
        foreach($holidays as &$holiday)
        {
            $holiday .= ".".date('Y');
        }
        foreach($holidays2 as &$holiday)
        {
            $holiday .= ".".(date('Y') + 1);
        }
        $holidays = array_merge($holidays, $holidays2);
        $dFrom = new DateTime($dateFrom);
        $dTo = new DateTime($dateTo);
        if ($dTo->getTimestamp() < $dFrom->getTimestamp())
            return 0;
        $diff = $dFrom->diff($dTo)->days + 1;
        $wd = date('w',$dFrom->getTimestamp());
        $no_full_weeks = floor($diff / 7);
        $workingDays = $no_full_weeks * 5;
        $no_remaining_days = $diff % 7;
        $the_first_day_of_week = date("N", $dFrom->getTimestamp());
        $the_last_day_of_week = date("N", $dTo->getTimestamp());
        if ($the_first_day_of_week <= $the_last_day_of_week) {
            if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
            if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
        }
        else {
            if ($the_first_day_of_week == 7) {
                $no_remaining_days--;
                if ($the_last_day_of_week == 6) {
                    $no_remaining_days--;
                }
            }
            else {
                $no_remaining_days -= 2;
            }
        }
        if ($no_remaining_days > 0 )
        {
            $workingDays += $no_remaining_days;
        }
        foreach($holidays as $holiday){
            $date = explode('.',$holiday);
            $time_stamp = mktime(0,0,0,$date[1], $date[0],$date[2]);
            //If the holiday doesn't fall in weekend
            if ($dFrom->getTimestamp() <= $time_stamp && $time_stamp <= $dTo->getTimestamp() && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
                $workingDays--;
        }
        return $workingDays;
    }

}