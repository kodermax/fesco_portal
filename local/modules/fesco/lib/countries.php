<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 3/17/15
 * Time: 4:56 PM
 */

use Bitrix\Main\Entity;
use Bitrix\Main\Entity\ExpressionField;
use Bitrix\Highloadblock as HL;
\Bitrix\Main\loader::includeModule('highloadblock');
class FescoCountries
{

    static function getClass()
    {
        $filter = array(
            'select' => array('ID', 'NAME', 'TABLE_NAME', 'FIELDS_COUNT'),
            'filter' => array('=TABLE_NAME' => 'fesco_countries')
        );
        $hlblock = HL\HighloadBlockTable::getList($filter)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $hl = $entity->getDataClass();
        return $hl;
    }
    static function getCountries(){
        $slim = \Slim\Slim::getInstance();
        $arResult = array();

        $hl = self::getClass();
        $result = $hl::getList(array(
            'select' => array('*'),
            'order' => array('UF_NAME' => 'ASC')
        ));
        while ($row = $result->Fetch())
        {
            $arResult[] = array(
                'GUID' => $row["UF_XML_ID"],
                'Id' => $row['ID'],
                'Title' => $row['UF_NAME'],
                'Offshore' => $row['UF_OFFSHORE']
            );
        }
        $slim->response()->write(json_encode($arResult), true);
        return $arResult;
    }
}