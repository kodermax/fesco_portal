<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 3/16/15
 * Time: 3:22 PM
 */
\Bitrix\Main\Loader::includeModule('iblock');
class FescoAttorneys {
    static function getAttorneysIBlockId(){
        $res = CIBlock::GetList(
            Array(),
            Array(
                'ACTIVE' => 'Y',
                "CODE" => 'done_attorneys'
            ), true
        );
        if ($ar_res = $res->Fetch()) {
            return $ar_res['ID'];
        }
        return 0;
    }
    static function getAttorneysHelpers(){
        $arResult = array();
        $iblockId = self::getAttorneysIBlockId();
        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => $iblockId, "CODE" => "STATUS"));
        while ($enum_fields = $property_enums->GetNext()) {
            $arResult['STATUS_XML'][$enum_fields['XML_ID']] = $enum_fields['ID'];
            $arResult['STATUS_ID'][$enum_fields['ID']] = $enum_fields['XML_ID'];
        }
        $properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$iblockId,'CODE'=>'SEQ'));
        if ($prop_fields = $properties->GetNext())
        {
            $arResult['SEQ_ID'] = $prop_fields["ID"];
        }
        return $arResult;
    }
    static function getRequestsIBlockId(){
        \Bitrix\Main\Loader::includeModule('iblock');
        $res = CIBlock::GetList(
            Array(),
            Array(
                'ACTIVE' => 'Y',
                "CODE" => 'req_attorneys'
            ), true
        );
        if ($ar_res = $res->Fetch()) {
            return $ar_res['ID'];
        }
        return 0;
    }
    static function getRequestsHelpers() {
        $arResult = array();
        $iblockId = self::getRequestsIBlockId();
        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$iblockId, "CODE"=>"STATUS"));
        while($enum_fields = $property_enums->GetNext())
        {
            $arResult['STATUS_XML'][$enum_fields['XML_ID']] = $enum_fields['ID'];
            $arResult['STATUS_ID'][$enum_fields['ID']] = $enum_fields['XML_ID'];
        }
        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$iblockId, "CODE"=>"TRANSFERENCE"));
        while($enum_fields = $property_enums->GetNext())
        {
            $arResult['TRANSFERENCE'][$enum_fields['XML_ID']] = $enum_fields['ID'];
        }
        return $arResult;
    }
    static function getTemplateId(){
        \Bitrix\Main\Loader::includeModule('bizproc');
        $iblockId = self::getRequestsIBlockId();
        $dbWorkflowTemplate = CBPWorkflowTemplateLoader::GetList(
            array(),
            array("DOCUMENT_TYPE" => array("iblock", "CIBlockDocument", "iblock_".$iblockId), "ACTIVE"=>"Y"),
            false,
            false,
            array("ID", "NAME", "DESCRIPTION", "MODIFIED", "USER_ID", "AUTO_EXECUTE", "USER_NAME", "USER_LAST_NAME", "USER_LOGIN", "USER_SECOND_NAME")
        );
        while ($arWorkflowTemplate = $dbWorkflowTemplate->GetNext())
        {
            return $arWorkflowTemplate['ID'];
        }
        return 0;
    }
    static function getUserFormat($userId){
        $rsUser = CUser::GetByID($userId);
        $arUser = $rsUser->Fetch();
        $arResult = array('Id' => $arUser['ID'],'Title' => $arUser['NAME']. ' '.$arUser['LAST_NAME']);
        return $arResult;
    }
    function cancelRequest($Id){
        $slim = Slim\Slim::getInstance();
        $iblockId = self::getRequestsIBlockId();
        $arHelpers = FescoAttorneys::getRequestsHelpers();
        CIBlockElement::SetPropertyValuesEx($Id, $iblockId, array('STATUS' => $arHelpers['STATUS_XML']['cancel']));

        //Отправить уведомление
        $db_props = CIBlockElement::GetProperty($iblockId, $Id, array("sort" => "asc"), Array("CODE"=>"USER_ID"));
        if($ar_props = $db_props->Fetch()) {
            $userId = IntVal($ar_props["VALUE"]);
            $rsUser = CUser::GetByID($userId);
            $arUser = $rsUser->Fetch();
            $rsSites = CSite::GetByID("s1");
            $arSite = $rsSites->Fetch();
            $url = "https://".$arSite['SERVER_NAME']."app/dist/#/attorneys/requests";
            if (strlen($arUser['EMAIL']) > 0) {
                $arEventFields = array(
                    "EMAIL_TO" => $arUser['EMAIL'],
                    "SUBJECT" => "Ваша заявка на доверенность отклонена",
                    "MESSAGE" => "
                            Пользователь ".$GLOBALS['USER']->GetFullName()." отклонил Вашу заявку на доверенность.
                            <br>
                            <a href='".$url."'>Перейти к заявке</a>
                            <br><br>"
                );
                CEvent::Send("MAIL_TEMPLATE", "s1", $arEventFields);
            }
        }
        $slim->response()->write(json_encode(array('status' => true)));

    }
    function doneAttorney($Id){
        $slim = \Slim\Slim::getInstance();
        $request = json_decode($slim->request()->getBody(), true);
        $iblockId = self::getAttorneysIBlockId();
        $iblockRequestsId = self::getRequestsIBlockId();
        $arHelpers = self::getAttorneysHelpers();
        $arRequestsHelpers = self::getRequestsHelpers();
        CIBlockElement::SetPropertyValuesEx($Id, $iblockId,array('STATUS' => $arHelpers['STATUS_XML']['active']));
        if ($request['Signatory']['Id'] > 0) {
            CIBlockElement::SetPropertyValuesEx($Id, $iblockId, array('SIGNATORY_ID' => $request['Signatory']['Id']));
        }
        //ID заявки
        $list = CIBlockElement::GetList(array(),array('ID'=> $Id, 'IBLOCK_ID'=>$iblockRequestsId), false, false, array('PROPERTY_REQUEST_ID'));
        if($row = $list->GetNext())
        {
            $linkId = $row['PROPERTY_REQUEST_ID_VALUE'];
            if($linkId > 0)
                CIBlockElement::SetPropertyValuesEx($linkId,$iblockRequestsId,array('STATUS' => $arRequestsHelpers['STATUS_XML']['done']));
        }
        $slim->response()->write(json_encode(array('status' => true)), true);

        //Отправить уведомление
        $db_props = CIBlockElement::GetProperty($iblockId, $Id, array("sort" => "asc"), Array("CODE"=>"USER_ID"));
        if($ar_props = $db_props->Fetch()) {
            $userId = IntVal($ar_props["VALUE"]);
            $rsUser = CUser::GetByID($userId);
            $arUser = $rsUser->Fetch();
            $rsSites = CSite::GetByID("s1");
            $arSite = $rsSites->Fetch();
            $url = "https://".$arSite['SERVER_NAME']."/app/dist/#/attorneys";
            if (strlen($arUser['EMAIL']) > 0) {
                $arEventFields = array(
                    "EMAIL_TO" => $arUser['EMAIL'],
                    "SUBJECT" => "Доверенность готова",
                    "MESSAGE" => "
                Ваша доверенность готова.<br>
                <a href='" . $url . "'>Перейти к доверенности</a>
                "
                );
                CEvent::Send("MAIL_TEMPLATE", "s1", $arEventFields);
            }
        }


    }
    function getAccess() {
        $arResult = array();
        $userId = $GLOBALS['USER']->GetID();
        $slim = Slim\Slim::getInstance();
        $request = $slim->request()->params();
        if ($request['entity'] == 'menu') {
            $rsGroups = CGroup::GetList($by = "c_sort", $order = "asc", array("STRING_ID" => 'admin_attorneys'));
            if ($arGroups = $rsGroups->Fetch()) {
                $groupId = $arGroups['ID'];
                if (in_array($groupId, $GLOBALS['USER']->GetUserGroupArray())) {
                    echo json_encode(array('status' => true, 'admin' => true));
                    return;
                }
            }
        }
        echo json_encode(array('status' => true, 'admin' => false));
    }
    function getAttorney($Id){
        $params = array('Id' => $Id);
        self::getAttorneys($params);
    }
    function getAttorneys($params) {
        $arResult = array();
        $iblockId = FescoAttorneys::getAttorneysIBlockId();
        $iblockRequestsId = FescoAttorneys::getRequestsIBlockId();
        $arHelpers = FescoAttorneys::getAttorneysHelpers();
        $slim = Slim\Slim::getInstance();
        $request = $slim->request()->params();
        $userId = $GLOBALS['USER']->GetID();
        $arFilter = array('IBLOCK_ID' => $iblockId);
        if ($params['userId'] > 0) {
            $arFilter['PROPERTY_USER_ID'] = $params['userId'];
        }
        if ($params["Id"] > 0){
            $arFilter['ID'] = $params['Id'];
        }
        $arSort = array('ID' => 'DESC');
        $arNavParams = array('nPageSize' => 10);
        if ($request['page'] > 0)
            $arNavParams['iNumPage'] = $request['page'];
        $list = CIBlockElement::GetList($arSort, $arFilter, false, $arNavParams,
            array('ID', 'IBLOCK_ID', 'DATE_CREATE', 'NAME', 'PROPERTY_*', 'ACTIVE_TO', 'ACTIVE_FROM', 'PREVIEW_TEXT'));
        $total = $list->NavRecordCount;
        $NavFirstRecordShow = ($list->NavPageNomer - 1) * $list->NavPageSize + 1;

        if ($list->NavPageNomer != $list->NavPageCount)
            $NavLastRecordShow = $list->NavPageNomer * $list->NavPageSize;
        else
            $NavLastRecordShow = $list->NavRecordCount;

        while ($row = $list->GetNextElement()) {
            $arFields = $row->GetFields();
            $arProps = $row->GetProperties();
            $ar = array();
            if ($arProps['USER_ID']['VALUE'] > 0) {
                $ar = FescoAttorneys::getUserFormat($arProps['USER_ID']['VALUE']);
            } else {
                $ar = array('Id' => 0, 'Title' => $arProps['USER_NAME']['VALUE']);
            }
            if ($arProps['SIGNATORY_ID']['VALUE'] > 0) {
                $arSignatory = FescoAttorneys::getUserFormat($arProps['SIGNATORY_ID']['VALUE']);
            }
            $arRequest = array();
            if ($arProps['REQUEST_ID']['VALUE'] > 0) {
                $rsProp = CIBlockElement::GetProperty($iblockRequestsId, $arProps['REQUEST_ID']['VALUE'], "sort", "asc", array('CODE' => 'SEQ_ID'));
                if ($arProp = $rsProp->GetNext()) {
                    $arRequest = array('Id' => $arProps['REQUEST_ID']['VALUE'], 'SeqId' => $arProp['VALUE']);
                }
                if ($arRequest['SeqId'] <= 0) {
                    $rsProp = CIBlockElement::GetProperty($iblockRequestsId, $arProps['REQUEST_ID']['VALUE'], "sort", "asc", array('CODE' => 'ID'));
                    if ($arProp = $rsProp->GetNext()) {
                        $arRequest['SeqId'] = $arProp['VALUE'];
                    }
                }
            }

            switch ($arProps['STATUS']['VALUE_XML_ID']) {
                case 'draft':
                    $cssClass = "label-warning";
                    break;
                case 'active':
                    $cssClass = "label-info";
                    break;
                case 'deactive':
                case 'expired':
                case 'revoked':
                    $cssClass = "label-important";
                    break;
                default:
                    $cssClass = "";
                    break;
            }
            $arResult[] = array(
                'Id' => $arFields['ID'],
                'CreateDate' => $arFields['DATE_CREATE'],
                'Num' => $arProps['ATTORNEY_ID']['VALUE'],
                'Title' => $arFields['NAME'],
                'Contragent' => array('GUID' => $arProps['CONTRAGENT_ID']['VALUE']),
                'Description' => $arFields['~PREVIEW_TEXT'],
                'StartDate' => $arFields['ACTIVE_FROM'],
                'FinishDate' => $arFields['ACTIVE_TO'],
                'Scan' => array('Path' => CFile::GetPath($arProps['SCAN']['VALUE'])),
                'WordFile' => array('Path' => CFile::GetPath($arProps['DOC_FILE']['VALUE'])),
                'User' => $ar,
                'Signatory' => !empty($arSignatory) ? $arSignatory : "",
                'Request' => $arRequest,
                'Status' => array(
                    'Code' => $arProps['STATUS']['VALUE_XML_ID'],
                    'Title' => $arProps['STATUS']['VALUE'],
                    'CssClass' => $cssClass
                )
            );
        }

        foreach ($arResult as &$arItem) {
            if (strlen($arItem['Contragent']['GUID']) > 0) {
                $arTemp = FescoContragents::getContragentByGUID($arItem['Contragent']['GUID']);
                $arItem['Contragent']['Title'] = $arTemp['Title'];
            }
        }
        $arData = array(
            'Items' => $arResult,
            'Count' => (int)$total,
            'NavFirstRecordShow' => (int)$NavFirstRecordShow,
            'NavLastRecordShow' => (int)$NavLastRecordShow
        );
        if ($params['Id'] > 0 && count($arResult) > 0){
            $slim->response()->write(json_encode(array('status' => true, 'data' => $arResult[0])), true);
        }
        else {
            $slim->response()->write(json_encode(array('status' => true, 'data' => $arData)), true);
        }

    }
    function getMyAttorneys(){
        $userId = $GLOBALS['USER']->GetID();
        $params = array('userId' => $userId);
        self::getAttorneys($params);
    }
    function getRequest($Id){
        $params = array('Id' => $Id);
        self::getRequests($params);
    }
    function getMyRequests(){
        $userId = $GLOBALS['USER']->GetID();
        $params = array('userId' => $userId);
        self::getRequests($params);
    }
    function getRequests($params){
        $arResult = array();
        $iblockId = FescoAttorneys::getRequestsIBlockId();
        $arHelpers = FescoAttorneys::getRequestsHelpers();
        $slim = Slim\Slim::getInstance();
        $request = $slim->request()->params();
        $status = $request['status'];
        $arFilter = array(
            'IBLOCK_ID' => $iblockId
        );
        if ($params['Id'] > 0) {
            $arFilter['ID'] = $params['Id'];
        }
        if ($status == 'approval')
        {
            $arFilter['PROPERTY_STATUS'] = $arHelpers['STATUS_XML']['approve'];
        }
        if ($params['userId'] > 0) {
            $arFilter[] = array(
                'LOGIC' => 'OR',
                'PROPERTY_USER_ID' => $params['userId'],
                'CREATED_BY' => $params['userId']
            );
        }

        $arSelect = array('ID','IBLOCK_ID','NAME','PREVIEW_TEXT', 'PROPERTY_*');
        $arNavParams = array('nPageSize' => 10);
        if ($request['page'] > 0)
            $arNavParams['iNumPage'] = $request['page'];
        $arSort = array('ID' => "DESC");
        $list = CIBlockElement::Getlist($arSort, $arFilter, false, $arNavParams, $arSelect);
        $total = $list->NavRecordCount;
        $NavFirstRecordShow = ($list->NavPageNomer-1)*$list->NavPageSize+1;

        if ($list->NavPageNomer != $list->NavPageCount)
            $NavLastRecordShow = $list->NavPageNomer * $list->NavPageSize;
        else
            $NavLastRecordShow = $list->NavRecordCount;

        while($oRow = $list->GetNextElement()) {
            $arFields = $oRow->GetFields();
            $arProps = $oRow->GetProperties();
            switch($arProps['STATUS']['VALUE_XML_ID']) {
                case 'draft':
                case 'approving':
                case 'approve':
                case 'wait':
                    $cssClass = "warning";
                    break;
                case 'done':
                    $cssClass = "positive";
                    break;
                case 'cancel':
                case 'not_approve':
                    $cssClass = "negative";
                    break;
                default:
                    $cssClass = "";
                    break;
            }
            $arEmployee = array();
            if($arProps['USER_ID']['VALUE'] > 0) {
                $arEmployee = FescoAttorneys::getUserFormat($arProps['USER_ID']['VALUE']);
            }
            $arApprover = array();
            if($arProps['APPROVER']['VALUE'] > 0) {
                $arApprover = FescoAttorneys::getUserFormat($arProps['APPROVER']['VALUE']);
            }
            $arResult[] = array(
                'Id' => $arFields['ID'],
                'SeqId' => $arProps['SEQ_ID']['VALUE'] > 0 ? $arProps['SEQ_ID']['VALUE'] : $arProps['ID']['VALUE'],
                'Title' => $arFields['NAME'],
                'Contragent' => array('GUID' => $arProps['CONTRAGENT_ID']["VALUE"]),
                'Description' => $arFields['~PREVIEW_TEXT'],
                'StartDate' => $arProps['START_DATE']['VALUE'],
                'FinishDate' => $arProps['FINISH_DATE']['VALUE'],
                'Passport' => array(
                    'Id' => $arProps['PASSPORT_ID']['VALUE'],
                    'Date' => $arProps['PASSPORT_DATE']['VALUE'],
                    'Whom' => $arProps['PASSPORT_WHOM']['~VALUE'],
                    'Dep' => $arProps['PASSPORT_DEP']['~VALUE']
                ),
                'Comment' => $arProps['BOSS_COMMENT']['~VALUE'],
                'Status' => array(
                    'Title' => $arProps['STATUS']['VALUE'],
                    'CssClass' => $cssClass,
                    'Code' => $arProps['STATUS']['VALUE_XML_ID']
                ),
                'User' => $arEmployee,
                'Approver' => $arApprover,
                'Transference' => $arProps['TRANSFERENCE']['VALUE'],
                'TransferenceText' => $arProps['TRANSFERENCE']['VALUE'] === "Y" ? "Да" : "Нет"
            );
        }
        foreach($arResult as &$arItem) {
            if (strlen($arItem['Contragent']['GUID']) > 0) {
                // Get route
                $arTemp = FescoContragents::getContragentByGUID($arItem['Contragent']['GUID']);
                $arItem['Contragent']['Title'] = $arTemp['Title'];
            }
        }
        $arData = array(
            'Items' => $arResult,
            'Count' => (int)$total,
            'NavFirstRecordShow' => (int)$NavFirstRecordShow,
            'NavLastRecordShow' => (int)$NavLastRecordShow
        );
        if ($params['Id'] > 0 && count($arResult) > 0) {
           $slim->response()->write(json_encode(array('status' => true, 'data' => $arResult[0])), true);
        }
        else {
            $slim->response()->write(json_encode(array('status' => true, 'data' => $arData)), true);
        }
    }
    function postAttorney(){
        $slim = \Slim\Slim::getInstance();
        $iblockRequestsId = self::getRequestsIBlockId();
        $iblockId = self::getAttorneysIBlockId();
        $arHelpers = self::getAttorneysHelpers();
        $arRequestsHelpers = self::getRequestsHelpers();
        $request = json_decode($slim->request()->getBody(), true);
        $requestId = $request['requestId'];

        //Массив заявки
        $list = CIBlockElement::GetList(array(),array('ID' => $requestId),false,false,array('ID','PREVIEW_TEXT','IBLOCK_ID','NAME','PROPERTY_*'));
        if ($row = $list->GetNextElement()){
            
            $arFields = $row->GetFields();
            $arProps = $row->GetProperties();
            //Получить префикс компании
            $prefix = 'ДВМП';
            if ($arProps['CONTRAGENT_ID']['VALUE'] > 0) {
                $arContragent  = FescoContragents::getContragentByGUID($arProps['CONTRAGENT_ID']['VALUE']);
                $prefix = $arContragent['Prefix'];
            }
            $seq = new CIBlockSequence($iblockId, $arHelpers['SEQ_ID']);
            $seqValue = $seq->GetNext();
            $arNewFields = array(
                'IBLOCK_ID' => $iblockId,
                'NAME' => $arFields['NAME'],
                'ACTIVE_FROM' => $arProps['START_DATE']['VALUE'],
                'ACTIVE_TO' => $arProps['FINISH_DATE']['VALUE'],
                'PREVIEW_TEXT' => $arFields['~PREVIEW_TEXT'],
                'PROPERTY_VALUES' => array(
                    'CONTRAGENT_ID' => $arProps['CONTRAGENT_ID']['VALUE'],
                    'USER_ID' => $arProps['USER_ID']['VALUE'],
                    'STATUS' => $arHelpers['STATUS_XML']['draft'],
                    'REQUEST_ID' => $requestId,
                    'ATTORNEY_ID' => $prefix."-".$seqValue."-".date('y'),
                    'SEQ' => $seqValue

                )
            );

            $el = new CIBlockElement();
            if($ID = $el->Add($arNewFields,false,false,false))
            {
                $slim->response()->write(json_encode(array('status' => true, 'id' => $ID)), true);
                //Меняем статус заявки
                CIBlockElement::SetPropertyValuesEx($requestId, false, array('STATUS' => $arRequestsHelpers['STATUS_XML']['wait']));
            }
            else {
                $error = $el->LAST_ERROR;
                $slim->response()->write(json_encode(array('status' => false)), true);

            }
            return;

        }
        $slim->response()->write(json_encode(array('status' => true)), true);

    }
    function postRequest(){
        $arHelpers = self::getRequestsHelpers();
        $iblockId = self::getRequestsIBlockId();
        $slim = Slim\Slim::getInstance();
        $request = json_decode($slim->request()->getBody(), true);
        $properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$iblockId,'CODE' => 'SEQ_ID'));
        if ($prop_fields = $properties->GetNext())
        {
            $propId = $prop_fields['ID'];
            $seq = new CIBlockSequence($iblockId, $propId);
            $nextSeq = $seq->GetNext();
        }
        $arProp = array(
            'CONTRAGENT_ID' => $request['Contragent']["GUID"],
            'USER_ID' => $request['User']['Id'],
            'PASSPORT_ID' => $request['Passport']["Id"],
            'PASSPORT_DATE' => date('d.m.Y',strtotime($request['Passport']["Date"])),
            'PASSPORT_WHOM' => $request['Passport']["Whom"],
            'PASSPORT_DEP' => $request['Passport']["Dep"],
            'START_DATE' => strlen($request['Date']['StartDate']) > 0 ? $request['Date']['StartDate'] : date('d.m.Y'),
            'FINISH_DATE' => strlen($request['Date']['EndDate']) > 0 ? $request['Date']['EndDate'] : date('d.m.Y'),
            'TRANSFERENCE' => $arHelpers['TRANSFERENCE'][$request['Transference']],
            'STATUS' => $arHelpers['STATUS_XML']['draft'],
            'APPROVER' => $request['Approver']['Id'],
            "SEQ_ID" => $nextSeq
        );
        //Согласование президентом
        if ($request['Approver']['Id'] == 8081)
        {
            $arProp['STATUS'] = $arHelpers['STATUS_XML']['approve'];
            $arProp['BOSS_COMMENT'] = 'Шаг электронного согласования не применим*';
        }
        $rsUser = CUser::GetByID($request['User']['Id']);
        $arUser = $rsUser->Fetch();
        $arFields = array(
            "NAME" => "Доверенность для ".$arUser['NAME']." ".$arUser['LAST_NAME'],
            'PROPERTY_VALUES' => $arProp,
            "IBLOCK_SECTION_ID" => false,
            'IBLOCK_ID' => $iblockId,
            'PREVIEW_TEXT' => $request['Description']
        );
        $el = new CIBlockElement;
        if($ID = $el->Add($arFields,false,false))
        {
            $arErrorsTmp = array();
            if ($request['Approver']['Id'] != 8081) {
                $templateId = FescoAttorneys::GetTemplateId($ID);
                $wfId = CBPDocument::StartWorkflow(
                    $templateId,
                    array("iblock", "CIBlockDocument", $ID),
                    array("Approvers" => array("user_" . $request['Approver']['Id'])), $arErrorsTmp);
                if (count($arErrorsTmp) <= 0) {
                    CIBlockElement::SetPropertyValuesEx($ID, $iblockId, array('STATUS' => $arHelpers['STATUS_XML']['approving']));
                    echo json_encode(array('id' => $ID,'status'=> true));
                }
            }

        }
        else {
            echo json_encode(array('status'=>false,'message' => $el->LAST_ERROR));
        }
    }
    function putAttorney($Id){
        $slim = \Slim\Slim::getInstance();
        $iblockId = self::getAttorneysIBlockId();
        $request = json_decode($slim->request()->getBody(), true);
        $arFields = array(
            'PREVIEW_TEXT' => $request['Description']
        );
        $el = new CIBlockElement();
        if($ID = $el->Update($Id, $arFields, false, false, false))
        {
            if ($request['Signatory']['Id'] > 0) {
                CIBlockElement::SetPropertyValuesEx($Id, $iblockId, array('SIGNATORY_ID' => $request['Signatory']['Id']));
            }
            $slim->response()->write(json_encode(array('status' => true, 'id' => $request['Id'])), true);
        }
        else {
            $slim->response()->write(json_encode(array('status' => false,'message' => $el->LAST_ERROR, 'id' => $request['Id'])), true);
        }
    }
    function revokeAttorney($Id){
        $slim = \Slim\Slim::getInstance();
        $iblockId = self::getAttorneysIBlockId();
        $arHelpers = self::getAttorneysHelpers();
        CIBlockElement::SetPropertyValuesEx($Id,$iblockId,array('STATUS' => $arHelpers['STATUS_XML']['revoked']));
        $slim->response()->write(json_encode(array('status' => true)), true);
    }
    function uploadFileToAttorney($Id){
        $slim = \Slim\Slim::getInstance();
        CIBlockElement::SetPropertyValuesEx($Id, false, array('SCAN' => $_FILES['file']));
        $slim->response()->write(json_encode(array('status' => true, 'id' => $Id)), true);
    }
    function uploadDocFileToAttorney($Id){
        $slim = \Slim\Slim::getInstance();
        CIBlockElement::SetPropertyValuesEx($Id, false, array('DOC_FILE' => $_FILES['file']));
        $slim->response()->write(json_encode(array('status' => true, 'id' => $Id)), true);
    }
}