<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 3/18/15
 * Time: 9:30 AM
 */
\Bitrix\Main\loader::includeModule('iblock');

class FescoAuto {
    private function getRequestsIblockId(){
        $list = CIBlock::GetList(Array(), Array('ACTIVE'=>'Y',"CODE"=>'auto_requests'), true);
        if($row = $list->Fetch())
            return $row['ID'];
        return 0;
    }
    private function getHelpers(){
        $arResult = array();
        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>self::getRequestsIblockId(), "CODE"=>"STATUS"));
        while($enum_fields = $property_enums->GetNext())
        {
            $arResult['STATUS_XML'][$enum_fields['XML_ID']] = $enum_fields['ID'];
            $arResult['STATUS_ID'][$enum_fields['ID']] = $enum_fields['XML_ID'];
        }
        return $arResult;
    }
    private function adminGroupId(){
        $rsGroups = CGroup::GetList($by = "c_sort", $order = "asc", array("STRING_ID" => 'admin_auto'));
        if ($arGroups = $rsGroups->Fetch()) {
           return $arGroups['ID'];
        }
        return 0;
    }
    private function driversGroupId(){
        $rsGroups = CGroup::GetList($by = "c_sort", $order = "asc", array("STRING_ID" => 'drivers'));
        if ($arGroups = $rsGroups->Fetch()) {
            return $arGroups['ID'];
        }
        return 0;
    }
    private function isAdmin(){
        return in_array(self::adminGroupId(),$GLOBALS['USER']->GetUserGroupArray());
    }
    private function isDriver(){
        return in_array(self::driversGroupId(),$GLOBALS['USER']->GetUserGroupArray());
    }
    function getAccess() {
        $slim = \Slim\Slim::getInstance();

        if (self::isAdmin() || $GLOBALS['USER']->IsAdmin() || self::isDriver())
        {
            $slim->response()->write(json_encode(array('isAdministrator' => true)), true);

        }
        else {
            $slim->response()->write(json_encode(array('isUser' => true)), true);
        }
    }
    function getCars(){
        $slim = \Slim\Slim::getInstance();
        $arResult = array();
        $userId = $GLOBALS['USER']->GetID();
        $carTaxi = 0;
        $request = $slim->request()->params();


        $arFilter = array(
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => self::getCarsIblockId(),
            'CHECK_PERMISSIONS' => 'N'
        );
        if (self::isDriver()) {
            $arFilter['PROPERTY_CONTRAGENT_ID'] = self::getCompanyIdByUser($userId);
        }
        $list = CIBlockElement::GetList(array("SORT"=>"ASC"),$arFilter, false,false,array('ID','IBLOCK_ID','NAME','PROPERTY_IS_TAXI','PREVIEW_PICTURE'));
        while($row = $list->GetNext())
        {
            if ($row['PROPERTY_IS_TAXI_VALUE'] == 'Y')
                $carTaxi = $row['ID'];
            $arResult[] = array(
                'key' => $row['ID'],
                'label' => $row['NAME'],
                'link' => CFile::GetPath($row['PREVIEW_PICTURE'])
            );
        }
        $arResult[] = array(
            'key' => 'requests',
            'label' => 'Не обработано',
            'link' => '/images/requests/question.jpg'
        );
        $arData = array();
        $arData['data']['Items'] = $arResult;
        $arData['data']['Taxi'] = $carTaxi;
        $slim->response()->write(json_encode($arData), true);
    }
    function getEvents(){
        $slim = \Slim\Slim::getInstance();
        $arResult = array();
        $userId = $GLOBALS['USER']->GetID();
        $request = $slim->request()->params();
        $date = new DateTime();
        $date->sub(new DateInterval('P1M'));

        $arFilter = array(
            'IBLOCK_ID' => self::getRequestsIblockId(),
            'CHECK_PERMISSIONS' => 'N',
            'ACTIVE'=>'Y',
            '>=DATE_CREATE' => $date->format('d.m.Y')

        );
        if (self::isDriver()) {
            $arFilter['PROPERTY_CONTRAGENT_ID'] = self::getCompanyIdByUser($userId);
        }
        $arSelect = array('ID','IBLOCK_ID','ACTIVE_TO','ACTIVE_FROM','NAME','PREVIEW_TEXT', 'PROPERTY_*');
        $list = CIBlockElement::Getlist(array('ID' => "DESC"),$arFilter,false,false, $arSelect);
        while($row = $list->GetNextElement())
        {

            $arFields = $row->GetFields();
            $arProps = $row->GetProperties();
            $arUser = self::GetUserInfo($arProps['USER_ID']['VALUE']);
            if (strlen($arProps['CONTRAGENT_ID']['VALUE']) > 0) {
                $arCompany = \FescoContragents::getContragentByGUID($arProps['CONTRAGENT_ID']['VALUE']);
            }
            $arResult[] = array(
                'id' => $arFields['ID'],
                'text' => $arProps['TARGET']['VALUE'],
                'company' => is_array($arCompany) ? $arCompany['Title'] : '',
                'author' => $arUser['Title'],
                'start_date' => ConvertDateTime($arFields['ACTIVE_FROM'], "MM-DD-YYYY HH:MI:SS"),
                'end_date' => ConvertDateTime($arFields['ACTIVE_TO'], "MM-DD-YYYY HH:MI:SS"),
                'car_id' => intval($arProps['AUTO_ID']['VALUE']) > 0 ? $arProps['AUTO_ID']['VALUE']: 'requests',
                'type' => $arProps['STATUS']['VALUE_XML_ID'],
                'pick_location' => $arProps['SHIPPING_ADDRESS']['VALUE'],
                'drop_location' => $arProps['DESTINATION']['VALUE'],
                'description' => $arFields['~PREVIEW_TEXT'],
                'readonly' => true
            );
        }
        $slim->response()->write(json_encode(array('status' => true, 'data' => $arResult)), true);
    }
    /*
     * Функция возвращает ID компании пользователя
     */
    private function getCompanyIdByUser($userId){
        $companyId = 0;
        if ($userId > 0) {
            $rsUser = CUser::GetList($by, $order, array('ID' => $userId), array('SELECT' => array('UF_CONTRAGENT')));
            if ($arUser = $rsUser->GetNext()) {
                $arContragent = FescoContragents::getContragents(array('Id' => intval($arUser['UF_CONTRAGENT'])));
                return $arContragent['GUID'];
            }
        }
        return $companyId;
    }
    private function getCarsIblockId(){
        $list = CIBlock::GetList(Array(), Array('ACTIVE'=>'Y',"CODE"=>'cars'), true);
        if($row = $list->Fetch())
            return $row['ID'];
        return 0;
    }
    /*
     * Возвращает массив информации о пользователе
     */
    private function getUserInfo($userId){
        $arUser = array();
        if ($userId > 0) {
            $rsUser = CUser::GetByID($userId);
            $arUser = $rsUser->Fetch();
            $arUser = array(
                'Id' => $arUser['ID'],
                'Email' => $arUser['EMAIL'],
                'Title' => CUser::FormatName('#LAST_NAME# #NAME_SHORT# #SECOND_NAME_SHORT#', $arUser)
            );
        }
        return $arUser;
    }
    /*
     * Функция возвращает массив информации о машине
     */
    public function getCarInfo($carId){
        $arResult = array();
        if($carId > 0) {
            $list = CIBlockElement::GetList(array(), array('ID' => $carId, 'IBLOCK_ID' => self::getCarsIblockId()), false, false, array('ID','PREVIEW_PICTURE','NAME'));
            if ($row = $list->GetNext()) {
                $arResult = array(
                    'Id' => $row['ID'],
                    'key' => $row['ID'],
                    'Title' => $row['~NAME'],
                    'label' => $row['~NAME'],
                    'Image' => CFile::GetPath($row['PREVIEW_PICTURE'])
                );
            }
        }
        return $arResult;
    }

    function getMyRequests(){
        self::getRequests(array('filter' => 'my'));
    }
    function getRequest($Id){
        self::getRequests(array('Id' => $Id));
    }
    function getRequests($params){
        $arHelpers = self::getHelpers();
        $arResult = array();
        $slim = \Slim\Slim::getInstance();
        $userId = $GLOBALS['USER']->GetID();
        $request = $slim->request()->params();
        $status = $request['status'];

        $arFilter = array(
            'IBLOCK_ID' => self::getRequestsIblockId()
        );
        if ($params['filter'] == 'my') {
            $arFilter[] = array(
                'LOGIC' => 'OR',
                'PROPERTY_USER_ID' => $userId,
                'CREATED_BY' => $userId
            );
        }
        if ($params['Id'] > 0)
            $arFilter['ID'] = $params['Id'];
        if(self::isDriver()) {
            $arFilter['PROPERTY_CONTRAGENT_ID'] = self::getCompanyIdByUser($userId);
        }

        $arNavParams = array('nPageSize' => 10);
        if ($request['page'] > 0)
            $arNavParams['iNumPage'] = $request['page'];
        $arSelect = array('ID','IBLOCK_ID', 'ACTIVE_TO', 'ACTIVE_FROM', 'NAME', 'PREVIEW_TEXT', 'PROPERTY_*');
        $arOrder = array('ACTIVE_TO' => 'DESC');
        if ($request['sortBy'] && $request['sortOrder'])
        {
            switch ($request['sortBy'])
            {
                case 'date':
                    $sortId = 'ACTIVE_TO';
                    break;
                case 'status':
                    $sortId = 'PROPERTY_STATUS';
                    break;
                default:
                    $sortId = 'ID';
                    break;
            }
            $arOrder = array($sortId => strtoupper($request['sortOrder']));

        }
        $list = CIBlockElement::Getlist($arOrder, $arFilter, false, $arNavParams, $arSelect);
        $total = $list->NavRecordCount;
        $NavFirstRecordShow = ($list->NavPageNomer-1)*$list->NavPageSize+1;

        if ($list->NavPageNomer != $list->NavPageCount)
            $NavLastRecordShow = $list->NavPageNomer * $list->NavPageSize;
        else
            $NavLastRecordShow = $list->NavRecordCount;

        while ($row = $list->GetNextElement()) {
            $arFields = $row->GetFields();
            $arProps = $row->GetProperties();
            $arUser = self::getUserInfo($arProps['USER_ID']['VALUE']);
            $arCar = self::getCarInfo($arProps['AUTO_ID']['VALUE']);
            $arContragent = array();
            if (strlen($arProps['CONTRAGENT_ID']['VALUE']) > 0) {
                $arContragent = FescoContragents::getContragents(array('Id' => strval($arProps['CONTRAGENT_ID']['VALUE'])));
            }
            switch ($arProps['STATUS']['VALUE_XML_ID']) {
                case 'draft':
                    $cssClass = 'label-warning';
                    break;
                case 'approve':
                    $cssClass = 'label-info';
                    break;
                case 'cancel':
                    $cssClass = 'label-important';
                    break;
                case 'stop':
                    $cssClass = 'label-inverse';
                    break;
                default:
                    $cssClass = '';
                    break;
            }

            $arResult[] = array(
                'Id' => $arFields['ID'],
                "Contragent" => $arContragent,
                'User' => $arUser,
                'Comment' => $arFields['~PREVIEW_TEXT'],
                'Car' => $arCar,
                'Title' => $arFields['NAME'],
                'ShippingAddress' => $arProps['SHIPPING_ADDRESS']['~VALUE'],
                'Destination' => $arProps['DESTINATION']['~VALUE'],
                'Target' => $arProps['TARGET']['~VALUE'],
                'Status' => array('Title' => $arProps['STATUS']['~VALUE'], 'CssClass' => $cssClass),
                'StartDate' => ConvertDateTime($arFields['ACTIVE_FROM'], "DD.MM.YYYY HH:MI"),
                'FinishDate' => ConvertDateTime($arFields['ACTIVE_TO'], "DD.MM.YYYY HH:MI")
            );
        }
        if (count($arResult) == 1 && $params['Id'] > 0) {
            $arData = $arResult[0];
            $slim->response()->write(json_encode(array('status' => true, 'data' => $arData)), true);
        } else {
            $arData = array();
            $arData = array(
                'Items' => $arResult,
                'Count' => (int)$total,
                'NavFirstRecordShow' => (int)$NavFirstRecordShow,
                'NavLastRecordShow' => (int)$NavLastRecordShow
            );
            $slim->response()->write(json_encode(array('status' => true, 'data' => $arData)), true);
        }
        return $arData;
    }

    function getRequestUserInfo($Id){
        $db_props = CIBlockElement::GetProperty(self::getRequestsIblockId(), $Id, array("sort" => "asc"), Array("CODE"=>"USER_ID"));
        if($ar_props = $db_props->Fetch()) {
            $userId = IntVal($ar_props["VALUE"]);
            $arUser = self::getUserInfo($userId);
            return $arUser;
        }
        return array();

    }

    function cancelEvent($Id){
        $arHelpers = self::getHelpers();
        $slim = \Slim\Slim::getInstance();
        $el = new CIBlockElement();
        $el->Update($Id,array('ACTIVE' => 'N'),false,false,false);
        $arProp = array('STATUS' => $arHelpers['STATUS_XML']['cancel']);
        CIBlockElement::SetPropertyValuesEx($Id, self::getRequestsIBlockId(),$arProp);
        $arRequest = self::getRequests(array('Id' => $Id));
            //Отправляем уведомление инициатору заявке
        $arUser = self::getRequestUserInfo($Id);
        if ($arUser['EMAIL']) {
            $arEventFields = array(
                'EMAIL_TO' => $arUser['EMAIL'],
                'MESSAGE' => 'Ваша заявка на автотранспорт отклонена.
                                    <br>
                     <b>Период:</b> ' . $arRequest['StartDate']. ' - '. $arRequest['FinishDate'] .
                    '<br/><b>Машина:</b> ' .$arRequest['Car']['Title'].
                    '<br/><b>Место назначения:</b> '.$arRequest['Destination'],
                'SUBJECT' => 'Ваша заявка на автотранспорт отклонена.',
            );
            CEvent::Send("MAIL_TEMPLATE", "s1", $arEventFields);
        }
        $slim->response()->write(json_encode(array('status' => true)), true);
    }
    function postEvents(){
        $slim = \Slim\Slim::getInstance();
        $arHelpers = self::getHelpers();
        $request = json_decode($slim->request()->getBody(), true);
        $el = new CIBlockElement();
        $start_date =  date('d.m.Y H:i',strtotime($request['start_date']));
        $end_date = date('d.m.Y H:i',strtotime($request['end_date']));
        $arUpdateFields = array(
            'ACTIVE_FROM' => $start_date,
            'ACTIVE_TO' =>  $end_date,
            'PREVIEW_TEXT' => $request['description'],
        );
        if($ID = $el->Update($request['Id'],$arUpdateFields,false,false,false,false))
        {
            $arCar = self::getCarInfo($request['car_id']);
            $arProp = array(
                'DESTINATION' => $request['drop_location'],
                'SHIPPING_ADDRESS' => $request['pick_location'],
                'AUTO_ID' => $request['car_id'] != 'requests' ? $request['car_id'] : '',
                'TARGET' => $request['text']
            );
            if ($request['car_id'] != 'requests')
            {
                $arProp['STATUS'] = $arHelpers['STATUS_XML']['approve'];
                //Отправляем уведомление инициатору заявке
                $arUser = self::getRequestUserInfo($request['Id']);
                if ($arUser['EMAIL']) {
                    $arEventFields = array(
                        'EMAIL_TO' => $arUser['EMAIL'],
                        'MESSAGE' => 'Ваша заявка на автотранспорт одобрена.
                                        <br>
                                        <b>Период:</b> ' . $start_date. ' - '. $end_date .
                                       '<br/><b>Машина:</b> ' .$arCar['Title'].
                                        '<br/><b>Место назначения:</b> '.$request['pick_location']
                    ,
                        'SUBJECT' => 'Ваша заявка на автотранспорт одобрена',
                    );
                    CEvent::Send("MAIL_TEMPLATE", "s1", $arEventFields);
                }
            }
            CIBlockElement::SetPropertyValuesEx($request['Id'],self::getRequestsIblockId(),$arProp);
            $slim->response()->write(json_encode(array('status' => true)), true);
        }
        else {
            $slim->response()->write(json_encode(array('status' => false)), true);
        }
    }
    /*
         * Возвращает пользователя в формате
         * Иванов И.И.
         */
    private function GetUserShortName($userId){
        $result = '';
        if ($userId > 0) {
            $rsUser = CUser::GetByID($userId);
            $arUser = $rsUser->Fetch();
            return CUser::FormatName("#LAST_NAME# #NAME_SHORT# #SECOND_NAME_SHORT#", $arUser);
        }
        return $result;
    }

    public function getAdmins(){
        $arResult = array();
            $groupId = self::adminGroupId();
            $rsUser = CUser::GetList(
                $by,
                $order,
                array("GROUPS_ID" => array($groupId),'ACTIVE'=>'Y'), array("SELECT" => array("ID",'EMAIL')));
            while ($arUser = $rsUser->GetNext())
            {
                $arResult[] = $arUser;
            }


        return $arResult;

    }
    function postRequest($Id){
        self::postRequests(array('Id' => $Id));
    }
    function postRequests($params){
        $slim = \Slim\Slim::getInstance();
        $arHelpers = self::getHelpers();
        $request = json_decode($slim->request()->getBody(), true);
        $startDate = new DateTime($request['StartDate']);
        $startDate = $startDate->format('d.m.Y H:i:00');
        $endDate = new DateTime($request['FinishDate']);
        $endDate = $endDate->format('d.m.Y H:i:00');
        $userId = intval($request['User']['Id']) > 0 ? $request['User']['Id'] : $GLOBALS['USER']->GetID();
        if (strlen($request['Contragent']['GUID']) > 0)
            $contragentId = $request['Contragent']['GUID'];
        else
            $contragentId = self::getCompanyIdByUser($GLOBALS['USER']->GetID());
        $arProp = array(
            'USER_ID' => $userId,
            'TARGET' => $request['Target'],
            'SHIPPING_ADDRESS' => $request['ShippingAddress'],
            'DESTINATION' => $request['Destination'],
            'CONTRAGENT_ID' => $contragentId,
            'AUTO_ID' => $request['Car']['key']
        );
        $arProp['STATUS'] = self::isAdmin() || self::isDriver()? $arHelpers['STATUS_XML']['approve'] : $arHelpers['STATUS_XML']['draft'];
        $userName = self::GetUserShortName($userId);
        $arFields = array(
            'NAME' => 'Заявка от '.$userName,
            'IBLOCK_ID' => self::getRequestsIblockId(),
            'ACTIVE_FROM' => $startDate,
            'ACTIVE_TO' => $endDate,
            'PROPERTY_VALUES' => $arProp,
            'PREVIEW_TEXT' => $request['Comment']
        );
        $el = new CIBlockElement();
        if ($params['Id'] > 0) {
            $ID = $el->Update($params['Id'], $arFields, false, false, false);
        }
        else {
            $ID = $el->Add($arFields,false,false,false);
        }

        if ($ID > 0)
        {
            //Уведомление администратора о новой заявке
            $templateUrl = "https://btx.fesco.com/app/dist/#/auto/requests_manage/calendar";
            $arAdmins = self::getAdmins();
            $arEmails = array();
            foreach($arAdmins as $item)
            {
                $arEmails[] = $item['EMAIL'];
            }
            $strEmails = implode(";",$arEmails);
            $arEventFields = array(
                'EMAIL_TO' => $strEmails,
                'MESSAGE' => 'У вас есть новая заявка на автотранспорт
                        <br>
                        <a href="' . $templateUrl.'">Перейти к списку заявок</a>',
                'SUBJECT' => 'Новая заявка на автотранспорт',
            );
            CEvent::Send("MAIL_TEMPLATE", "s1", $arEventFields);
            $slim->response()->write(json_encode(array('status' => true)));
        }

    }
}