<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 3/18/15
 * Time: 4:21 PM
 */
\Bitrix\Main\loader::includeModule('iblock');
class FescoTrips {

    function addFile($Id){
        $slim = \Slim\Slim::getInstance();
        $iblockId = self::getIblockId();
        $request = $slim->request()->params();
        $arHelpers = self::getHelpers();
        CIBlockElement::SetPropertyValuesEx($Id, $iblockId, array('FILE' => $_FILES['file']));
        $templateId = self::getTemplateId($Id);
        $wfId = CBPDocument::StartWorkflow(
            $templateId,
            array("iblock", "CIBlockDocument", $Id),
            array(), $arErrorsTmp);
        if (count($arErrorsTmp) <= 0) {
            CIBlockElement::SetPropertyValuesEx($Id, $iblockId, array('STATUS' => $arHelpers['STATUS_XML']['approve']));
            $slim->response()->write(json_encode(array('status' => true, 'Id'=> $Id)), true);
        }
        else {
            $slim->response()->write(json_encode(array('status' => true, 'message'=> $arErrorsTmp)), true);
        }

    }
    function doneRequest($Id){
        $slim = \Slim\Slim::getInstance();
        $arHelpers = self::getHelpers();
        CIBlockElement::SetPropertyValuesEx($Id, self::getIblockId(),array('STATUS' => $arHelpers['STATUS_XML']['done']));
        $slim->response()->write(json_encode(array('status' => true, 'Id' => $Id)));
    }
    function getIblockId(){
        $list = CIBlock::GetList(Array(), Array('CHECK_PERMISSIONS' => 'N','ACTIVE'=>'Y',"CODE"=>'trips'), false);
        if($row = $list->Fetch())
            return $row['ID'];
        return 0;
    }
    function getHelpers(){
        $arResult = array();
        $iblockId = self::getIblockId();
        $list = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$iblockId, "CODE"=>"VISA"));
        while($row = $list->GetNext())
        {
            $arResult['VISA_ID'] = $row['ID'];
        }
        $list = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$iblockId, "CODE"=>"HOTEL"));
        while($row = $list->GetNext())
        {
            $arResult['HOTEL_ID'] = $row['ID'];
        }
        $list = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$iblockId, "CODE"=>"TRANSFER"));
        while($row = $list->GetNext())
        {
            $arResult['TRANSFER_ID'] = $row['ID'];
        }
        $list = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$iblockId, "CODE"=>"STATUS"));
        while($row = $list->GetNext())
        {
            $arResult['STATUS_XML'][$row['XML_ID']] = $row['ID'];
        }
        return $arResult;

    }
    function getRequest($Id){
        self::getRequests(array('Id' => $Id));
    }
    function getMyRequests(){
        self::getRequests(array('filter' => 'my'));
    }
    /*
     * Возвращает пользователя в формате
     * Иванов И.И.
     */
    private function getUserShortName($userId){
        $result = '';
        if ($userId > 0) {
            $rsUser = CUser::GetByID($userId);
            $arUser = $rsUser->Fetch();
            return CUser::FormatName("#LAST_NAME# #NAME_SHORT# #SECOND_NAME_SHORT#", $arUser);
        }
        return $result;
    }
    function getRequests($params){
        $slim = \Slim\Slim::getInstance();
        $request = $slim->request()->params();
        $arResult = array();
        $userId = $GLOBALS['USER']->GetID();
        $arFilter = array('IBLOCK_ID' => self::getIblockId());
        if ($params['filter'] == 'my') {
            $arFilter[] = array(
                'LOGIC' => 'OR',
                'PROPERTY_USER_ID' => $userId,
                'CREATED_BY' => $userId
            );
        }

        if ($params['Id'] > 0)
            $arFilter['ID'] = $params['Id'];
        $arNavParams = array('nPageSize' => 10);
        if ($request['page'] > 0)
            $arNavParams['iNumPage'] = $request['page'];
        $arSelect = array('ID','IBLOCK_ID', 'ACTIVE_TO', 'ACTIVE_FROM', 'NAME','DETAIL_TEXT', 'PREVIEW_TEXT','PROPERTY_*');
        $arOrder = array();
        if ($request['sortBy'] && $request['sortOrder'])
        {
            switch ($request['sortBy'])
            {
                case 'num':
                    $sortId = 'ID';
                    break;
                case 'date':
                    $sortId = 'ACTIVE_FROM';
                    break;
                case 'transport':
                    $sortId = 'PROPERTY_TRANSPORT';
                    break;
                case 'status':
                    $sortId = 'PROPERTY_STATUS';
                    break;
                case 'company':
                    $sortId = 'PROPERTY_COMPANY';
                    break;
                case 'user':
                    $sortId = 'PROPERTY_USER_NAME';
                    break;
                default:
                    $sortId = 'ID';
                    break;
            }
            $arOrder = array($sortId => strtoupper($request['sortOrder']));

        }
        $list = CIBlockElement::Getlist($arOrder, $arFilter, false, $arNavParams, $arSelect);
        $total = $list->NavRecordCount;
        $NavFirstRecordShow = ($list->NavPageNomer-1)*$list->NavPageSize+1;
        if ($list->NavPageNomer != $list->NavPageCount)
            $NavLastRecordShow = $list->NavPageNomer * $list->NavPageSize;
        else
            $NavLastRecordShow = $list->NavRecordCount;

        while ($row = $list->GetNextElement()) {
            $arFields = $row->GetFields();
            $arProp = $row->GetProperties();
            $arUser = array('Id' =>$arProp['USER_ID']['VALUE'],'Title' => self::GetUserShortName($arProp['USER_ID']['VALUE']));
            $arResult[] = array(
                'Id' => $arFields['ID'],
                'Title' => $arFields['NAME'],
                'Company' => $arProp['COMPANY']['~VALUE'],
                'Transport' => array('Id' => $arProp['TRANSPORT']['VALUE_ENUM_ID'],'Title'=> $arProp['TRANSPORT']['VALUE'], 'Code' => $arProp['TRANSPORT']['VALUE_XML_ID']),
                'Transfer' => strlen($arProp['TRANSFER']['VALUE']) > 0 ? true : false,
                'Visa' => strlen($arProp['VISA']['VALUE']) > 0 ? true : false,
                'Hotel' => strlen($arProp['HOTEL']['VALUE']) > 0 ? true : false,
                'Target' => $arFields['~DETAIL_TEXT'],
                'Date' => array(
                    'StartDate' =>ConvertDateTime($arFields['ACTIVE_FROM'], "DD.MM.YYYY"),
                    'EndDate' =>  ConvertDateTime($arFields['ACTIVE_TO'], "DD.MM.YYYY"), 'Text' => ConvertDateTime($arFields['ACTIVE_FROM'], "DD.MM.YYYY") . " - " .ConvertDateTime($arFields['ACTIVE_TO'], "DD.MM.YYYY")
                ),
                'DateText' => '',
                'TicketComment' => $arProp['TICKET_COMMENT']['~VALUE'],
                'ApproverComments' => $arProp['APPROVER_COMMENTS']['~VALUE']['TEXT'],
                'Status' => array('Title' => $arProp['STATUS']['VALUE'], 'Code' => $arProp['STATUS']['VALUE_XML_ID']),
                'Comment' => $arFields['~PREVIEW_TEXT'],
                'File' => array('Url' => CFile::GetPath($arProp['FILE']['VALUE'])),
                'User' => $arUser,
                'UserId' => $arUser['Id'],
                'Places' => $arProp['PLACES']['~VALUE']
            );
        }
        if (count($arResult) == 1 && $params['Id'] > 0) {
            $arData = $arResult[0];
            $slim->response()->write(json_encode(array('status' => true, 'data' => $arData), true));
        } else {
            $arData = array(
                'Items' => $arResult,
                'Count' => $total,
                'NavFirstRecordShow' => $NavFirstRecordShow,
                'NavLastRecordShow' => $NavLastRecordShow
            );
            $slim->response()->write(json_encode(array('status' => true, 'data' => $arData)), true);
        }
    }
    /*
         * Возвращает ENUM_ID по XML_ILD
         */
    function getTransportId($xmlId)
    {
        $list = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>self::getIblockId(), "CODE"=>"TRANSPORT", "XML_ID" => $xmlId));
        while($row = $list->GetNext())
        {
            return $row['ID'];
        }
        return 0;
    }

    /*
    * Возвращает ID шаблона БП
    */
    public function getTemplateId($docId){
        \Bitrix\Main\Loader::includeModule('bizproc');
        $iblockId = self::getIblockId();
        $dbWorkflowTemplate = CBPWorkflowTemplateLoader::GetList(
            array(),
            array("DOCUMENT_TYPE" => array("iblock", "CIBlockDocument", "iblock_".$iblockId), "ACTIVE"=>"Y"),
            false,
            false,
            array("ID", "NAME", "DESCRIPTION", "MODIFIED", "USER_ID", "AUTO_EXECUTE", "USER_NAME", "USER_LAST_NAME", "USER_LOGIN", "USER_SECOND_NAME")
        );
        while ($arWorkflowTemplate = $dbWorkflowTemplate->GetNext())
        {
            return $arWorkflowTemplate['ID'];
        }
        return 0;
    }

    function postRequest($Id){
        $slim = \Slim\Slim::getInstance();
        $request = json_decode($slim->request()->getBody(), true);
        $iblockId = self::getIblockId();
        $arHelpers = self::getHelpers();
        $userId = $request['User']['Id'] > 0 ? $request['User']['Id'] : $GLOBALS['USER']->GetID();
        $userName = self::getUserShortName($userId);

        $arFields = array(
            'ACTIVE_FROM' => $request['Date']['StartDate'],
            'ACTIVE_TO' => $request['Date']['EndDate'],
            'PREVIEW_TEXT' => $request['Comment'],
            'DETAIL_TEXT' => $request['Target'],
            'IBLOCK_ID' => $iblockId
        );
        $arProp = array(
            'USER_NAME' => $userName,
            'USER_ID' => $userId,
            'PLACES' => $request['Places'],
            'COMPANY' => $request['Company'],
            'TICKET_COMMENT' => $request['TicketComment'],
            'VISA' => $request['Visa'] ? $arHelpers['VISA_ID'] : "",
            'HOTEL' => $request['Hotel'] ? $arHelpers['HOTEL_ID'] : "",
            'TRANSFER' => $request['Transfer'] ? $arHelpers['TRANSFER_ID'] : "",
            'TRANSPORT' => self::getTransportId($request['Transport']['Code'])
        );
        $el = new CIBlockElement();
        $result = $el->Update($Id, $arFields, false, false, false);
        CIBlockElement::SetPropertyValuesEx($request['Id'], $iblockId, $arProp);

        $slim->response()->write(json_encode(array('status' => true, 'Id'=> $Id)), true);
    }
    function postRequests(){
        $iblockId = self::getIblockId();
        $slim = \Slim\Slim::getInstance();
        $request = json_decode($slim->request()->getBody(), true);
        $arHelpers = self::getHelpers();
        $userId = $request['User']['Id'] > 0 ? $request['User']['Id'] : $GLOBALS['USER']->GetID();
        $userName = self::getUserShortName($userId);
        $el = new CIBlockElement();
        $arProp = array(
            'USER_NAME' => $userName,
            'USER_ID' => $userId,
            'PLACES' => $request['Places'],
            'COMPANY' => $request['Company'],
            'TICKET_COMMENT' => $request['TicketComment'],
            'VISA' => $request['Visa'] ? $arHelpers['VISA_ID'] : "",
            'HOTEL' => $request['Hotel'] ? $arHelpers['HOTEL_ID'] : "",
            'TRANSFER' => $request['Transfer'] ? $arHelpers['TRANSFER_ID'] : "",
            'TRANSPORT' => self::getTransportId($request['Transport']['Code'])
        );
        $arFields = array(
            'NAME' => 'Заявка от '.$userName,
            'IBLOCK_ID' => $iblockId,
            'ACTIVE_FROM' => strlen($request['Date']['StartDate']) > 0 ? $request['Date']['StartDate'] : date('d.m.Y'),
            'ACTIVE_TO' => strlen($request['Date']['EndDate']) > 0 ? $request['Date']['EndDate'] : date('d.m.Y'),
            'PROPERTY_VALUES' => $arProp,
            'PREVIEW_TEXT' => $request['Comment'],
            'DETAIL_TEXT' => $request['Target']
        );
        $ID = $el->Add($arFields, false, false, false);
        if ($ID > 0)
        {
            $slim->response()->write(json_encode(array('status' => true, 'Id'=> $ID)), true);
        }
        else {
            $slim->response()->write(json_encode(array('status' => false, 'message'=> $el->LAST_ERROR)), true);
        }

    }
}