<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 2/2/15
 * Time: 11:20 AM
 */
use Bitrix\Main;
class FescoUsers {
    function __construct() {

    }
    static function getById($Id){
      return  self::getUsers(array('Id' => $Id));
    }
    function getMyUser(){
        global $USER;
        $slim = \Slim\Slim::getInstance();
        $userId = $USER->GetID();
        if ($userId <= 0){
             $slim->response()->write(json_encode(array('status' => false, 'message' => 'Ошибка')), true);
            return;
        }
        $arData = array();

        $arParams["NAME_TEMPLATE"] = CSite::GetNameFormat();
        $arFilter['!UF_DEPARTMENT'] = false;
        $arFilter['ACTIVE'] = 'Y';
        $arFilter['ID'] = $userId;
        $dbRes = CUser::GetList($by = 'last_name', $order = 'asc', $arFilter, array('SELECT' => array('UF_DEPARTMENT')));
        if ($arRes = $dbRes->GetNext())
        {
            $arData = array(
                'Id' => $arRes['ID'],
                'Title' => CUser::FormatName($arParams["NAME_TEMPLATE"], $arRes, true, false),
                'WorkPosition' => $arRes['WORK_POSITION'] ? $arRes['WORK_POSITION'] : $arRes['PERSONAL_PROFESSION']
            );
        }
        $slim->response()->write(json_encode(array('status' => true, 'data' => $arData)), true);
    }
    function getUsers($params){
        $arResult = array();
        $slim = \Slim\Slim::getInstance();
        $request = $slim->request()->params();
        $arParams["NAME_TEMPLATE"] = CSite::GetNameFormat();
        $arFilter['!UF_DEPARTMENT'] = false;
        $arFilter['ACTIVE'] = 'Y';
        $q = trim($request['q']);
        if (strlen($q) > 0)
            $arFilter['NAME'] = "%".$q."%";
        if($params['Id'] > 0){
            $arFilter['ID'] = $params['Id'];
        }
        $dbRes = CUser::GetList($by = 'last_name', $order = 'asc', $arFilter, array(
            'SELECT' => array('UF_DEPARTMENT'),
            'NAV_PARAMS'=>array("nPageSize"=>"20")));
        while ($arRes = $dbRes->GetNext())
        {
            $arResult[] = array(
                'Id' => $arRes['ID'],
                'Title' => CUser::FormatName($arParams["NAME_TEMPLATE"], $arRes, true, false),
                'WorkPosition' => $arRes['WORK_POSITION'] ? $arRes['WORK_POSITION'] : $arRes['PERSONAL_PROFESSION'],
            );
        }
        if(count($arResult) == 1 && $params['Id'] > 0) {
            $arData = $arResult[0];
        }
        else {
            $arData = $arResult;
        }
        $slim->response()->write(json_encode(array('status' => true, 'data' => $arData)), true);
        return $arData;
    }
 }