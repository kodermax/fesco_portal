<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 3/17/15
 * Time: 5:22 PM
 */

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
\Bitrix\Main\loader::includeModule('iblock');
\Bitrix\Main\loader::includeModule('highloadblock');

class FescoPayPlan{

    private function getBindings(){
        $arResult = array();
        $iblockId = self::getDepartmentsIblockId();
        $list = CIBlockElement::GetList(array(),array('CHECK_PERMISSIONS' => 'N','IBLOCK_ID' => $iblockId), false,false,array('ID','ACTIVE'));
        while($row = $list->GetNext()){
            $arResult[$row['ID']]['ACTIVE'] = $row['ACTIVE'];
            $dbGroups = CIBlockElement::GetElementGroups($row['ID'], true);
            while($arGroup = $dbGroups->Fetch()){
                $arResult[$row['ID']]['SECTIONS'][] = $arGroup['ID'];
            }
        }
        return $arResult;
    }
    private function getClass(){
        $filter = array(
            'select' => array('ID', 'NAME', 'TABLE_NAME', 'FIELDS_COUNT'),
            'filter' => array('=TABLE_NAME' => 'fesco_payplan')
        );
        $hlblock = HL\HighloadBlockTable::getList($filter)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        return $entity->getDataClass();
    }
    private function getDepartmentsIblockId(){
        $arFilter = array('TYPE' => 'helps','CODE' => 'dvmp_payplan');
        $list = CIBlock::GetList(array(),$arFilter);
        if($row = $list->GetNext()){
            return $row['ID'];
        }
        return 0;
    }
    private function getElementIdByCode($code){
        $iblockId = self::getDepartmentsIblockId();
        $list = CIBlockElement::GetList(array(),array('CODE' => $code, 'IBLOCK_ID' => $iblockId),false,false,array('ID'));
        if($row = $list->GetNext()){
            return $row['ID'];
        }
        return 0;
    }
    private function getEntityCode($id){
        $list = CIBlockSection::GetList(array(),array('ID' => $id),false,array('CODE'));
        if($row = $list->GetNext()){
            return $row['CODE'];
        }
        return '';
    }
    private function getExpenseCode($Id){

        $list = CIBlockElement::GetList(array(),array('ID' => $Id),false,false,array('CODE'));
        if($row = $list->GetNext()){
            return $row['CODE'];
        }
        return '';
    }
    private function getPaymentsForXml($date){
        if (strlen($date) <= 0){
            return "";
        }
        $arDate = explode('_',$date);
        $date = new DateTime($date);
        $startDate = $date->format('d.m.Y');
        $date->add(new DateInterval('P4M'));
        $endDate = $date->format('d.m.Y');
        $hl = self::getClass();
        $filter = array(
            'LOGIC' => 'AND',
            array('>=UF_START_DATE' => $startDate),
            array('<UF_END_DATE' => $endDate),
            array('!UF_PAYMENT' => 0)
        );
        $arEntries = array();
        $list = $hl::getList(array('filter' => $filter,'select' => array('*')));
        $payments = array();
        $s = '';
        $arBindings = self::getBindings();
        while($row = $list->fetch()){
            //check active entity
            if ($arBindings[$row['UF_EXPENSE']]['ACTIVE'] == 'Y'){
                if (in_array($row['UF_DEPARTMENT'],$arBindings[$row['UF_EXPENSE']]['SECTIONS'])){
                    $account = (string)self::getExpenseCode($row['UF_EXPENSE']);
                    $entityCode = self::getEntityCode($row['UF_DEPARTMENT']);
                    if ($row['UF_CONTRAGENT'] > 0) {
                        $contragent = FescoContragents::getGUID($row['UF_CONTRAGENT']);
                    }
                    else {
                        $contragent = '3dPartiesTotal';
                    }
                    $arPeriod = json_decode($row['UF_PERIOD'], true);
                    $s  .= '<m:Payments><Year>'.$arPeriod['Year'].'</Year>';
                    $s .= '<Period>'.$arPeriod['Month'].'</Period>';
                    $s .= '<Entity>'.$entityCode.'</Entity>';
                    $s .= '<Account>'.$account.'</Account>';
                    $s .= '<Counterparty>'.$contragent.'</Counterparty>';
                    $s .= '<Custom2>Week' . $arPeriod['Week'].'</Custom2>';
                    $s .= '<Custom3>In'. $row['UF_CURRENCY'].'</Custom3>';
                    $s .= '<Amount>'. $row['UF_PAYMENT'] .'</Amount></m:Payments>';
                }
            }

        }
        return $s;
    }
    private function getPeriodWeekByMonth($year, $month){
        $date = new DateTime($year.'-'.$month.'-01');
        $lastDay = $date->format('t');
        $days = 0;
        $day = 0;
        $bool = true;
        $arResult = array();
        while ($bool) {
            $one = $date->format('d.m.Y');
            $unix  = $date->format('U');
            $currentDay = $date->format('d');
            $numOfWeek = $date->format('N');
            $days = intval($day) + 7;
            if ($days >= $lastDay) {
                $subDay = $lastDay - $currentDay;
                $bool = false;
            } else {
                $subDay = 7 - $numOfWeek;
            }
            $date->add(new DateInterval('P' . $subDay . 'D'));

            $arResult[] =  array(
                'Period' => intval($unix) + intval($date->format('U')),
                'StartDate' => $one,
                'EndDate' => $date->format('d.m.Y'),
                'Title' => $one . ' - ' . $date->format('d.m.Y'),
                'Year' => $date->format('Y')
            );
            if (!$bool) break;
            $date->add(new DateInterval('P1D'));
            $day = $date->format('d');
        }
        return $arResult;
    }
    private function getSectionIdByCode($code){
        $iblockId = self::getDepartmentsIblockId();
        $list = CIBlockElement::GetList(array(),array('CODE' => $code, 'IBLOCK_ID' => $iblockId),false,false,array('ID'));
        if($row = $list->GetNext()){
            return $row['ID'];
        }
        return 0;
    }
    private function adminGroupId(){
        $rsGroups = CGroup::GetList($by = "c_sort", $order = "asc", array("STRING_ID" => 'admin_payplan'));
        if ($arGroups = $rsGroups->Fetch()) {
            return $arGroups['ID'];
        }
        return 0;
    }
    private function changeActiveAllCFO($value){
        $slim = \Slim\Slim::getInstance();
        $bs = new CIBlockSection();
        $list = CIBlockSection::GetList(array(),array('IBLOCK_Id' => self::getDepartmentsIblockId()),false,array('ID'));
        while($row = $list->GetNext()){
            $success = $bs->Update($row['ID'], array('UF_ACTIVE' => $value));
        }
        $slim->response()->write(json_encode(array('status' => true)));
    }
    function activateAllCFO(){
        self::changeActiveAllCFO(1);
    }
    static public function isAccess(){
        global $USER;
        if ($USER->isAdmin())
            return true;

        $iblockId = self::getDepartmentsIblockId();
        $arFilter = array('ACTIVE' => 'Y','IBLOCK_TYPE' => 'helps','IBLOCK_ID' => $iblockId, 'UF_USERS' => $USER->GetID());
        $arResult = array();
        $list = CIBlockSection::GetList(
            array('NAME' => 'ASC'),
            $arFilter
            ,
            false,
            array('ID')
        );
        if($row = $list->GetNext())
        {
          return true;
        }
        return false;
    }
    function getAccess(){
        $slim = \Slim\Slim::getInstance();
        if(in_array(self::adminGroupId(), $GLOBALS['USER']->GetUserGroupArray())){
            $slim->response()->write(json_encode(array('status' => true, 'access' => true)), true);
      }
        else {
            $slim->response()->write(json_encode(array('status' => true, 'access' => false)), true);
        }
    }
    function deActivateAllCFO(){
        self::changeActiveAllCFO(0);
    }
    function changeActiveCFO($Id){
        $slim = \Slim\Slim::getInstance();
        $request = json_decode($slim->request()->getBody(), true);
        $bs = new CIBlockSection();
        $success = $bs->Update($Id, array('UF_ACTIVE' => $request['value']));
        if ($success) {
            $slim->response()->write(json_encode(array('status' => true)));
        }
        else {
            $slim->response()->write(json_encode(array('status' => false)));
        }
    }
    function getMyCFO(){
        $userId = $GLOBALS['USER']->GetID();
        self::getListCFO(array('userId' => $userId));
    }
    function getListCFO($params){
        $slim = \Slim\Slim::getInstance();
        $iblockId = self::getDepartmentsIblockId();
        $arFilter = array('ACTIVE' => 'Y','IBLOCK_TYPE' => 'helps','IBLOCK_ID' => $iblockId);
        if ($params['userId'] > 0){
            $arFilter['UF_USERS'] = array($params['userId']);
        }
        $arResult = array();
        $list = CIBlockSection::GetList(
            array('NAME' => 'ASC'),
            $arFilter
            ,
            false,
            array('ID','NAME','UF_ACTIVE')
        );
        while($row = $list->GetNext())
        {
            $arResult[] = array(
                'Id' => $row['ID'],
                'Title' => $row['NAME'],
                'ActiveYes' => $row['UF_ACTIVE'] == 1  ? 1 : "",
                'ActiveNo' => $row['UF_ACTIVE'] == 0 ? 0 : "",
                'Active' => $row['UF_ACTIVE']
            );
        }
        $slim->response()->headers('Access-Control-Expose-Headers','true');
        $slim->response()->write(json_encode(array('status' => true, 'Items' => $arResult)), true);
    }
    function getPayments($depId){

        $months = array(
            1 => 'Январь', 2 => 'Февраль', 3 => 'Март', 4 => 'Апрель',
            5 => 'Май', 6 => 'Июнь', 7 => 'Июль', 8 => 'Август',
            9 => 'Сентябрь', 10 => 'Октябрь', 11 => 'Ноябрь', 12 => 'Декабрь'
        );
        $slim = \Slim\Slim::getInstance();
        $CFOActive = 0;
        $iblockId = self::getDepartmentsIblockId();
        $list = CIBlockSection::GetList(array(),array('ID' => $depId,'IBLOCK_ID' => $iblockId), false, array('UF_ACTIVE'));
        if($row = $list->GetNext()){
            $CFOActive = boolval($row['UF_ACTIVE']);
        }
        $Items = array();
        //get entries
        $startMonth = new DateTime(date('Y-m-01'));
        $startMonth->sub(new DateInterval('P1M'));
        $endMonth = clone $startMonth;
        $endMonth->add(new DateInterval('P5M'));
        $startDate = $startMonth->format('d.m.Y');
        $endDate = $endMonth->format('d.m.Y');
        $hl = self::getClass();
        $filter = array(
            'LOGIC' => 'AND',
            array('>=UF_START_DATE' => $startDate),
            array('<=UF_END_DATE' => $endDate),
            array('=UF_DEPARTMENT' => $depId),
            array('!UF_PAYMENT' => 0)
        );
        $arEntries = array();
        $list = $hl::getList(array('filter' => $filter,'select' => array('*')));
        while($row = $list->fetch()){
            $arEntries[] = $row;
        }
        $count = count($arEntries);
        for($i = 0; $i < $count; $i++){
            $month = new DateTime(ConvertDateTime($arEntries[$i]['UF_START_DATE'], "YYYY-MM-DD", "ru"));
            $arEntries[$i]['Month'] = $month->format('m');
        }
        // Array[Месяц][Статья][Контрагент] = инфа о компании
        $arCompanies = array();
        $arPayments = array();
        foreach($arEntries as $arItem){
            $key = intval($arItem['UF_CONTRAGENT']);
            $title = '';
            if ($key !== 0)
            {
                $arContragent = FescoContragents::getContragents(array('Id' => $key));
                $title = $arContragent['Title'];
                $arCompanies[$arItem['Month']][$arItem['UF_EXPENSE']][$key] = array('Id' => $key,'Title' => $title,'Type' => 'row');
            }
            //Array[Статья][Контрагент][Период] = платёж
            $arPeriod = json_decode($arItem['UF_PERIOD'],true);
            $arPayments[$arItem['UF_EXPENSE']][$key][$arPeriod['Period']][$arItem['UF_CURRENCY']] = array(
                'Id' => (int)$arItem['ID'],
                'Currency' => $arItem['UF_CURRENCY'],
                'Value' => $arItem['UF_PAYMENT']);
        }

        $date = new DateTime(date('Y-m-01'));
        $date->sub(new DateInterval('P1M'));
        for($i = 0; $i < 5; $i++){
            //get categories
            $month = $date->format('m');
            $arIntervals = self::getPeriodWeekByMonth($date->format('Y'),$date->format('m'));
            $arCategories = array();
            $list = CIBlockElement::GetList(array('SORT' =>'ASC'),array('CHECK_PERMISSIONS' => 'N','IBLOCK_TYPE' => 'helps','ACTIVE' => 'Y','IBLOCK_ID' => $iblockId,'SECTION_ID' => $depId),false,false,array('ID','NAME','PROPERTY_ICP'));
            while($row = $list->GetNext()){
                $arCategories[] = array(
                    'Id' => $row['ID'],
                    'ICP' => $row['PROPERTY_ICP_VALUE'],
                    'Title' => $row['NAME'],
                    'Type' => 'group',

                );
                $arCategories[] = array(
                    'Id' => 0,
                    'CategoryId' => $row['ID'],
                    'Title' => 'Третьи лица',
                    'Type' => 'row');
                foreach($arCompanies[$month][$row['ID']] as $item){
                    $item['CategoryId'] = $row['ID'];
                    $arCategories[] = $item;
                }
            }
            $Items[$month] = array(
                'Categories' => $arCategories,
                'Intervals' => $arIntervals,
                'Title' => $months[$date->format('n')]. ' '.$date->format('Y'),
                'Id' => $month,
                'Active' => $i == 1 ? true : false,
                'Access' => $i == 0 ? false : true

            );
            $date = $date->add(new DateInterval('P1M'));
        }
        $slim->response()->write(
            json_encode(
                array(
                    'status' => true,
                    'data' => array(
                        'CFO' => array(
                            'Active' => $CFOActive
                        ),
                        'Items' => $Items,
                        'Payments' => $arPayments)
                )
            ), true);
        }
    function getPaymentsXML(){
        $slim = \Slim\Slim::getInstance();
        $slim->contentType('application/soap+xml; charset=utf-8');
        $xmlString = file_get_contents('php://input');
        $xml = simplexml_load_string($xmlString);
        $date = $xml->xpath('//hyp:pDate')[0];
        $s = '<?xml version="1.0" encoding="UTF-8"?>
            <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope">
                <soap:Header/>
                    <soap:Body>
                        <m:GetPaymentsResponse xmlns:m="http://hyperion-exchange.org">
                            <m:return Comment="test" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                            ';
        $s .= self::getPaymentsForXml($date);
        $s .= '
                </m:return>
            </m:GetPaymentsResponse>
        </soap:Body>
    </soap:Envelope>';
        $slim->response()->write($s, true);
    }
    function getPaymentsWSDL(){
        $serverName = $_SERVER['SERVER_NAME'];
        $env = getenv('APPLICATION_ENV');
        if($env == 'development'){
            $serverName = 'btx3.fesco.com';
        }
        else if($env == 'production'){
            $serverName = 'btx.fesco.com';
        }
        $slim = \Slim\Slim::getInstance();
        $slim->contentType('text/xml; charset=utf-8');
        $request = $slim->request()->params();
        $slim->response()->write('<definitions xmlns="http://schemas.xmlsoap.org/wsdl/" xmlns:soap12bind="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:soapbind="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tns="http://hyperion-exchange.org" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsd1="http://hyperion-exchange.org" name="GetPaymentsWS" targetNamespace="http://hyperion-exchange.org">
            <types>
            <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xs1="http://hyperion-exchange.org" targetNamespace="http://hyperion-exchange.org" attributeFormDefault="unqualified" elementFormDefault="qualified">
            <xs:complexType name="tPayments">
                <xs:sequence>
                    <xs:element name="Year" type="xs:integer"/>
                    <xs:element name="Period" type="xs:string"/>
                    <xs:element name="Entity" type="xs:string"/>
                    <xs:element name="Account" type="xs:string"/>
                    <xs:element name="Counterparty" type="xs:string"/>
                    <xs:element name="Custom2" type="xs:string"/>
                    <xs:element name="Custom3" type="xs:string"/>
                    <xs:element name="Amount" type="xs:integer"/>
                </xs:sequence>
            </xs:complexType>
            <xs:complexType name="tPaymentsData">
            <xs:sequence>
            <xs:element name="Payments" type="tns:tPayments" maxOccurs="unbounded"/>
            </xs:sequence>
            <xs:attribute name="Comment" type="xs:string" use="required"/>
            </xs:complexType>
            <xs:element name="GetPayments">
            <xs:complexType>
            <xs:sequence>
            <xs:element name="pDate" type="xs:date" nillable="true"/>
            </xs:sequence>
            </xs:complexType>
            </xs:element>
            <xs:element name="GetPaymentsResponse">
            <xs:complexType>
            <xs:sequence>
            <xs:element name="return" type="tns:tPaymentsData" nillable="true"/>
            </xs:sequence>
            </xs:complexType>
            </xs:element>
            </xs:schema>
            </types>
            <message name="GetPaymentsRequestMessage">
            <part name="parameters" element="tns:GetPayments"/>
            </message>
            <message name="GetPaymentsResponseMessage">
            <part name="parameters" element="tns:GetPaymentsResponse"/>
            </message>
            <portType name="GetPaymentsWSPortType">
            <operation name="GetPayments">
            <input message="tns:GetPaymentsRequestMessage"/>
            <output message="tns:GetPaymentsResponseMessage"/>
            </operation>
            </portType>
            <binding name="GetPaymentsWSSoapBinding" type="tns:GetPaymentsWSPortType">
            <soapbind:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>
            <operation name="GetPayments">
            <soapbind:operation style="document" soapAction="http://hyperion-exchange.org#GetPaymentsWS:GetPayments"/>
            <input>
            <soapbind:body use="literal"/>
            </input>
            <output>
            <soapbind:body use="literal"/>
            </output>
            </operation>
            </binding>
            <binding name="GetPaymentsWSSoap12Binding" type="tns:GetPaymentsWSPortType">
            <soap12bind:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>
            <operation name="GetPayments">
            <soap12bind:operation style="document" soapAction="http://hyperion-exchange.org#GetPaymentsWS:GetPayments"/>
            <input>
            <soap12bind:body use="literal"/>
            </input>
            <output>
            <soap12bind:body use="literal"/>
            </output>
            </operation>
            </binding>
            <service name="GetPaymentsWS">
            <port name="GetPaymentsWSSoap" binding="tns:GetPaymentsWSSoapBinding">
            <documentation>
            <wsi:Claim xmlns:wsi="http://ws-i.org/schemas/conformanceClaim/" conformsTo="http://ws-i.org/profiles/basic/1.1"/>
            </documentation>
            <soapbind:address location="https://' . $serverName.'/app/public/api/payplan/payments/xml"/>
            </port>
            <port name="GetPaymentsWSSoap12" binding="tns:GetPaymentsWSSoap12Binding">
            <soap12bind:address location="https://'.$serverName.'/app/public/api/payplan/payments/xml"/>
            </port>
            </service>
            </definitions>', true);
    }
    function postPayment($Id){
        $slim = \Slim\Slim::getInstance();
        $request = json_decode($slim->request()->getBody(), true);
        $hl = self::getClass();
        if (empty($request['Value']['Value'])) {
            if ($Id > 0) {
                $result = $hl::delete($Id);
                if ($result->isSuccess()) {
                    $slim->response()->write(json_encode(array('status' => true,'id' => $Id)), true);
                }
                else {
                    $slim->response()->write(json_encode(array('status' => false,'message'=> $result->getErrorMessages())), true);
                }
            } else {
                $slim->response()->write(json_encode(array('status' => true)), true);
            }
        }
        else {
            $days = FescoUtils::getWorkingDays($request['StartDate'], $request['EndDate']);
            if ($days == 0) {
                $slim->response()->write(json_encode(array('status' => false, 'message' => 'В указанном периоде нет рабочих дней!')), true);
                return;
            }
            $arData = array(
                'UF_CONTRAGENT' => $request['ContragentId'],
                'UF_DEPARTMENT' => $request['DepartmentId'],
                'UF_EXPENSE' => $request['ExpenseId'],
                'UF_PAYMENT' => $request['Value']['Value'],
                'UF_CURRENCY' => $request['Value']['Currency'],
                'UF_PERIOD' => json_encode(array('Period' => strval($request['Period']), 'Month' => $request['Month'], 'Week' => $request['Week'] + 1, 'Year' => $request['Year'])),
                'UF_START_DATE' => $request['StartDate'],
                'UF_END_DATE' => $request['EndDate'],
                'UF_USER' => $GLOBALS['USER']->GetID(),
                'UF_MODIFY_BY_ID' => $GLOBALS['USER']->GetID(),
                'UF_DATE_MODIFY' => date('d.m.Y H:i:s')
            );
            if ($Id > 0) {
                $result = $hl::update($Id, array('UF_PAYMENT' => $request['Value']['Value']));
            } else {
                $arData['UF_DATE_CREATE'] = date('d.m.Y H:i:s');
                $arData['UF_CREATED_BY_ID'] = $GLOBALS['USER']->GetID();
                $result = $hl::add($arData);
            }
            if ($result->isSuccess()) {
                $slim->response()->write(json_encode(array('status' => true, 'Id' => $result->getId())), true);
            } else {
                $slim->response()->write(json_encode(array('status' => false, 'message' => $result->getErrors())), true);
            }
        }
    }
    static public function postHelps(){
        try {
            $status = true;
            #region Get Xml
            $xmlString = file_get_contents('http://msk-phws01.hq.fesco.com/SharedData/MC_STCF_Binding.xml');
            $xml = new SimpleXMLElement($xmlString);
            $arAccounts = array();
            $arBindings = array();
            $arEntities = array();
            $arContragents = array();
            foreach ($xml->Accounts as $account) {
                $code = strval($account->AccountCode);
                $arAccounts[$code] = array(
                    'Code' => $code,
                    'Name' => strval($account->AccountName),
                    'ICP' => strval($account->IsICP),
                    'Sort' => strval($account->SortOrder)
                );
            }
            foreach ($xml->Binding as $binding) {
                $arBindings[] = array(
                    'AccountCode' => strval($binding->AccountCode),
                    'EntityCode' => strval($binding->EntityCode)
                );
            }
            foreach ($xml->Entities as $entity) {
                $code = strval($entity->EntityCode);
                $arEntities[$code] = array(
                    'Code' => $code,
                    'Name' => strval($entity->EntityName)
                );
            }
            foreach($xml->Counterparties as $contragent){
                $guid = strval($contragent->CounterpartyGUID);
                $arContragents[] = $guid;
            }
            #endregion
            $iblockId = self::getDepartmentsIblockId();
            #region update contragents
            $arOldContragents = array();
            $arBinContragents = array();
            $hl = FescoContragents::getClass();
            $list = $hl::getList(array('filter' => array(),'select'=>array('ID','UF_FIN','UF_XML_ID')));
            while($row = $list->fetch()){
                if ($row['UF_FIN'] == 1) {
                    $arOldContragents[] = $row['UF_XML_ID'];
                }
                $arBinContragents[$row['UF_XML_ID']] = $row['ID'];
            }
            $arNewContragents = array_diff($arContragents, $arOldContragents);
            $arDelContragents = array_diff($arOldContragents, $arContragents);

            foreach($arNewContragents as $item){
                if (array_key_exists($item, $arBinContragents))
                    $hl::update($arBinContragents[$item],array('UF_FIN' => 1));
            }
            foreach($arDelContragents as $item){
                if (array_key_exists($item, $arBinContragents))
                    $hl::update($arBinContragents[$item],array('UF_FIN' => 0));
            }
            #region check entities
            $allEntities = array();
            $listEntities = array();
            $list = CIBlockSection::GetList(array(), array('IBLOCK_ID' => $iblockId), false, array('CODE'));
            while ($row = $list->GetNext()) {
                $allEntities[] = $row['CODE'];
                $listEntities[$row['CODE']] = $row['ID'];
            }
            $hypEntities = array_keys($arEntities);
            $newEntities = array_diff($hypEntities, $allEntities);
            $delEntities = array_diff($allEntities, $hypEntities);
            $bs = new CIBlockSection();
            foreach ($newEntities as $entity) {
                $arFields = array(
                    'ACTIVE' => 'Y',
                    'IBLOCK_ID' => $iblockId,
                    'NAME' => $arEntities[$entity]['Name'],
                    'CODE' => $arEntities[$entity]['Code'],
                    'UF_ACTIVE' => 1
                );
                $ID = $bs->Add($arFields);
            }
            //Деактивация ЦФО
            foreach ($delEntities as $item) {
                $bs->Update($listEntities[$item], array('ACTIVE' => 'N'), false, false, false);
            }
            #endregion
            #region check accounts
            $allAccounts = array();
            $listAccounts = array();
            $list = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $iblockId,'CHECK_PERMISSIONS' => 'N'), false, false, array('ID','ACTIVE', 'CODE','NAME'));
            while ($row = $list->GetNext()) {
                if ($row['ACTIVE'] == 'Y') {
                    $allAccounts[] = $row['CODE'];
                }
                $listAccounts[$row['CODE']] = array('ID' => $row['ID'], 'NAME' => $row['NAME']);
            }

            $hypAccounts = array_keys($arAccounts);
            $newAccounts = array_diff($hypAccounts, $allAccounts);
            $delAccounts = array_diff($allAccounts, $hypAccounts);

            //Создание счетов
            $icpId = 0;
            $db_enum_list = CIBlockProperty::GetPropertyEnum("ICP", Array(), Array("IBLOCK_ID"=>$iblockId, "VALUE"=>"Y"));
            if($ar_enum_list = $db_enum_list->GetNext())
            {
                $icpId = $ar_enum_list['ID'];

            }
            $el = new CIBlockElement();
            foreach ($newAccounts as $item) {
                if (array_key_exists($item, $listAccounts)){
                    $el->Update($listAccounts[$item]['ID'],array('ACTIVE' => 'Y'));
                }
                else {
                    $arFields = array(
                        'IBLOCK_ID' => $iblockId,
                        'NAME' => $arAccounts[$item]['Name'],
                        'ACTIVE' => 'Y',
                        'CODE' => $arAccounts[$item]['Code'],
                        'SORT' => $arAccounts[$item]['Sort'],
                        'IBLOCK_SECTION_ID' => false
                    );
                    $ID = $el->Add($arFields);
                    $listAccounts[$arAccounts[$item]['Code']] = array('ID' => $ID, 'NAME' => $arAccounts[$item]['Name']);
                }

            }
            //Деактивация счетов
            foreach ($delAccounts as $item) {
                $ID = $el->Update($listAccounts[$item]['ID'], array('ACTIVE' => 'N'), false, false, false, false);
            }
            foreach($arAccounts as $item) {
                $el->Update($listAccounts[$item['Code']]['ID'], array('SORT' => $item['Sort']), false,false,false,false);
                if ($item['ICP'] == 'Y') {
                    CIBlockElement::SetPropertyValuesEx($listAccounts[$item['Code']]['ID'], $iblockId, array('ICP' => $icpId));
                }
                else {
                    CIBlockElement::SetPropertyValuesEx($listAccounts[$item['Code']]['ID'], $iblockId, array('ICP' => ""));
                }
                if ($listAccounts[$item['Code']]['NAME'] != $item['Name']){
                    $el->Update($listAccounts[$item['Code']]['ID'], array('NAME' => $item['Name']), false,false,false,false);
                }
            }
            #endregion
            #region check bindings
            $arInitSections = array();
            $arSections = array();
            $list = CIBlockSection::GetList(array(), array('IBLOCK_ID' => $iblockId), false, array('ID', 'CODE'));
            while ($row = $list->GetNext()) {
                $arInitSections[$row['CODE']] = $row['ID'];
            }

            foreach ($arBindings as $item) {
                $arSections[$item['AccountCode']][] = $arInitSections[$item['EntityCode']];
            }
            $listAccounts = array();
            $list = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $iblockId, 'CHECK_PERMISSIONS' => 'N'), false, false, array('ID', 'CODE'));
            while ($row = $list->GetNext()) {
                $listAccounts[$row['CODE']] = $row['ID'];
            }
            foreach ($arSections as $key => $item) {
                CIBlockElement::SetElementSection($listAccounts[$key], $item);
            }
            #endregion
        }
        catch(Exception $e){
            $status = false;
        }
        echo '<?xml version="1.0" encoding="UTF-8" ?>
            <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope">
                <soap:Header/>
                    <soap:Body>
                        <m:GetResultResponse xmlns:m="http://hyperion-exchange.org">
                            <m:return Comment="test" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                                <m:Result>
                                    <Result>'.$status.'</Result>
                                </m:Result>
                            </m:return>
                        </m:GetResultResponse>
                    </soap:Body>
            </soap:Envelope>';
        $slim = \Slim\Slim::getInstance();
        $slim->response()->header('content-type','application/soap+xml');
    }
    function updateHelpsWsdl(){
            header("Content-Type:text/xml; charset=utf-8");
        $env = getenv('APPLICATION_ENV');
        $serverName = '';
        if($env == 'development'){
            $serverName = 'btx.fesco.com';
        }
        else if($env == 'production'){
            $serverName = 'btx3.fesco.com';
        }
            echo '<definitions xmlns="http://schemas.xmlsoap.org/wsdl/" xmlns:soap12bind="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:soapbind="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tns="http://hyperion-exchange.org" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsd1="http://hyperion-exchange.org" name="GetPaymentsWS" targetNamespace="http://hyperion-exchange.org">
                    <types>
                    <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xs1="http://hyperion-exchange.org" targetNamespace="http://hyperion-exchange.org" attributeFormDefault="unqualified" elementFormDefault="qualified">
                    <xs:complexType name="tResult">
                        <xs:sequence>
                            <xs:element name="Result" type="xs:integer"/>
                        </xs:sequence>
                    </xs:complexType>
                    <xs:complexType name="tResultData">
                    <xs:sequence>
                    <xs:element name="Result" type="xs1:tResult" maxOccurs="unbounded"/>
                    </xs:sequence>
                    </xs:complexType>
                    <xs:element name="GetResult">
                    <xs:complexType>
                    <xs:sequence>
                    <xs:element name="pDate" type="xs:string" nillable="true"/>
                    </xs:sequence>
                    </xs:complexType>
                    </xs:element>
                    <xs:element name="GetResultResponse">
                    <xs:complexType>
                    <xs:sequence>
                    <xs:element name="return" type="xs1:tResultData" nillable="true"/>
                    </xs:sequence>
                    </xs:complexType>
                    </xs:element>
                    </xs:schema>
                    </types>
                    <message name="GetResultRequestMessage">
                    <part name="parameters" element="tns:GetResult"/>
                    </message>
                    <message name="GetResultResponseMessage">
                    <part name="parameters" element="tns:GetResultResponse"/>
                    </message>
                    <portType name="GetResultWSPortType">
                    <operation name="GetResult">
                    <input message="tns:GetResultRequestMessage"/>
                    <output message="tns:GetResultResponseMessage"/>
                    </operation>
                    </portType>
                    <binding name="GetResultWSSoapBinding" type="tns:GetResultWSPortType">
                    <soapbind:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>
                    <operation name="GetResult">
                    <soapbind:operation style="document" soapAction="http://hyperion-exchange.org#GetResultWS:GetResult"/>
                    <input>
                    <soapbind:body use="literal"/>
                    </input>
                    <output>
                    <soapbind:body use="literal"/>
                    </output>
                    </operation>
                    </binding>
                    <binding name="GetResultWSSoap12Binding" type="tns:GetResultWSPortType">
                    <soap12bind:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>
                    <operation name="GetResult">
                    <soap12bind:operation style="document" soapAction="http://hyperion-exchange.org#GetResultWS:GetResult"/>
                    <input>
                    <soap12bind:body use="literal"/>
                    </input>
                    <output>
                    <soap12bind:body use="literal"/>
                    </output>
                    </operation>
                    </binding>
                    <service name="GetResultWS">
                    <port name="GetResultWSSoap" binding="tns:GetResultWSSoapBinding">
                    <soapbind:address location="https://'.$serverName.'/app/public/api/payplan/helps"/>
                    </port>
                    <port name="GetResultWSSoap12" binding="tns:GetResultWSSoap12Binding">
                    <soap12bind:address location="https://'.$serverName.'/app/public/api/payplan/helps"/>
                    </port>
                    </service>
                    </definitions>';

    }
    function removePayment($Id){
        $slim = \Slim\Slim::getInstance();
        $hl = self::getClass();
        $success = $hl::delete($Id);
        if ($success->isSuccess()){
            $slim->response()->write(json_encode(array('status' => true,'Id' => $Id)));
        }
        else {
            $slim->response()->write(json_encode(array('status' => false,'Id' => $Id)));
        }
    }
}



