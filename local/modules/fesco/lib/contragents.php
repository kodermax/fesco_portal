<?php

/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 1/30/15
 * Time: 12:32 PM
 */

use Bitrix\Main\Entity;
use Bitrix\Main\Entity\ExpressionField;
use Bitrix\Highloadblock as HL;
\Bitrix\Main\loader::includeModule('highloadblock');

class FescoContragents {
    static function getClass(){
        $filter = array(
            'select' => array('ID', 'NAME', 'TABLE_NAME', 'FIELDS_COUNT'),
            'filter' => array('=TABLE_NAME' => 'fesco_contragents')
        );
        $hlblock = HL\HighloadBlockTable::getList($filter)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $hl = $entity->getDataClass();
        return $hl;
    }
    function addFile($Id){
        $slim = \Slim\Slim::getInstance();
        $hl = self::getClass();
        $data = array();
        $list = $hl::getList(array(
            'filter' => array('ID' => $Id),
            'select' => array('UF_FILES')
        ));
        $arFiles = array();
        if($row = $list->fetch())
        {
            foreach ($row['UF_FILES'] as $file)
            {
                $arFile = array('name' => '','type' => '','tmp_name'=>'','error'=>4,'size'=>0,'del'=>false,'old_id' => $file);
                $arFiles[] = $arFile;

            }
        }

        $i = 0;
        $arFiles['n'.$i++] = $_FILES['file'];
        $result = $hl::update($Id, array('UF_FILES' => $arFiles));
        if ($result->isSuccess())
            $slim->response()->write(json_encode(array('status' => true)), true);
        else
            $slim->response()->write(json_encode(array('status' => false)), true);
    }
    static function getContragent($Id){
        $params = array('Id' => intval($Id));
        return self::getContragents($params);
    }
    static function getContragentByGUID($guid){
        return self::getContragents(array('Id' => strval($guid)));
    }
    function getMyContragents(){
        $arData = array();
        $slim = \Slim\Slim::getInstance();
        $userId = $GLOBALS['USER']->GetID();
        $rsUsers =CUser::GetList($by, $order, array('ID' => $userId),array('SELECT'=>array('UF_CONTRAGENT')));
        if($arUser = $rsUsers->GetNext()) {
            if (strlen($arUser['UF_CONTRAGENT']) > 0) {
                $hl = FescoContragents::getClass();
                $result = $hl::getList(array(
                        'filter' => array('ID' => $arUser['UF_CONTRAGENT']),
                        'select' => array('*')
                    )
                );
                $arData = array();
                if ($row = $result->Fetch()) {
                    $arData = array('Id' => $row['ID'], 'GUID' => $row['UF_XML_ID'],'Title' => $row['UF_NAME']);
                }
            }
        }
        $slim->response()->write(json_encode(array('status' => true, 'data' => $arData)), true);
    }
    static function getContragents($params){
        $slim = \Slim\Slim::getInstance();
        $request = $slim->request()->params();

        //Страны
        $arCountries = array();
        $arTemp = FescoCountries::getCountries();
        foreach($arTemp as $arItem)
        {
            $arCountries[$arItem['GUID']] = $arItem;
        }
        $arResult = array();
        $hl = FescoContragents::getClass();
        $filter = array();
        if ($params['Id']) {
            if (is_int($params['Id'])) {
                $filter = array('=ID' => $params['Id']);
            } else if (is_string($params['Id'])) {
                $filter = array('=UF_XML_ID' => $params['Id']);
            }
        }
        $request['q'] = trim($request['q']);
        if (strlen($request['q']) > 0) {
            $filter[] = array(
                'LOGIC' => 'OR',
                array(
                    '%=UF_NAME' => '%'.$request['q'].'%'
                ),
                array(
                    '%=UF_SHORT_NAME' => '%'.$request['q'].'%'
                ),
                array(
                    '%=UF_INN' => '%'.$request['q'].'%'
                ),
                array(
                    '%=UF_KPP' => '%'.$request['q'].'%'
                ),
                array(
                    '%=UF_OGRN' => '%'.$request['q'].'%'
                ),
            );
        }
        if ($request['fesco'] == 1) {
            $filter[] = array(
                'LOGIC' => 'AND',
                '=UF_FESCO' => 1
            );
        }
        if($request['fin'] == 1){
            $filter[] = array(
                'LOGIC' => 'AND',
                '=UF_FIN' => 1
            );
        }

        $limit = array(
            'nPageSize' => 10,
            'iNumPage' => $request['page'] ? $request['page'] : 1,
            'bShowAll' => false
        );
        if ($request['allShow'] == 'Y')
        {
             $limit = array();
        }
        $list = $hl::getList(array(
            'filter' => array($filter),
            'select' => array('*'),
            'order' => array('UF_NAME' => 'ASC'),
            'limit' => $limit['nPageSize'],
            'offset' => ($limit['iNumPage']-1) * $limit['nPageSize']
        ));
        while ($row = $list->Fetch())
        {
            $arFiles = array();
            foreach($row['UF_FILES'] as $file)
            {
                $rsFile = CFile::GetById($file);
                $arFile = $rsFile->Fetch();
                $arFiles[] = array('Name' => $arFile['FILE_NAME'],'Path' => CFile::GetPath($file),'Size' => $arFile['FILE_SIZE']);
            }
            $arResult[] = array(
                'GUID' => $row["UF_XML_ID"],
                'Id' => (int)$row["ID"],
                'Title' => $row['UF_NAME'],
                'Name' => $row['UF_NAME'],
                'Prefix' => $row['UF_PREFIX'],
                'Fesco' => $row['UF_FESCO'] == 1 ? true: false,
                'FullName' => $row['UF_FULL_NAME'],
                'NameEn' => $row['UF_NAME_EN'],
                'OrgForm' => $row['UF_ORG_FORM'],
                'Offshore' => $row['UF_LEGAL_COUNTRY'] > 0 ? $arCountries[$row['UF_LEGAL_COUNTRY']]['Offshore'] : 0,
                'LegalCountry' => array('GUID' => $row['UF_LEGAL_COUNTRY'], 'Title' => $arCountries[$row['UF_LEGAL_COUNTRY']]['Title']),
                'LegalAddress' => $row['UF_LEGAL_ADDRESS'],
                'FullLegalAddress' => $row['UF_LEGAL_COUNTRY'] > 0 ? $arCountries[$row['UF_LEGAL_COUNTRY']]['Title'].', '.$row['UF_LEGAL_ADDRESS'] : $row['UF_LEGAL_ADDRESS'],
                'ActualCountry' => array('GUID' => $row['UF_ACTUAL_COUNTRY'], 'Title' => $arCountries[$row['UF_ACTUAL_COUNTRY']]['Title']),
                'ActualAddress' => $row['UF_ACTUAL_ADDRESS'],
                'FullActualAddress' => $row['UF_ACTUAL_COUNTRY'] > 0 ? $arCountries[$row['UF_ACTUAL_COUNTRY']]['Title'].', '.$row['UF_ACTUAL_ADDRESS'] : $row['UF_ACTUAL_ADDRESS'],
                'Resident' => $row['UF_RESIDENT'] == 1 ? true: false,
                'Agcy' => $row['UF_AGCY'] == 1 ? true : false,
                'Phone' => $row['UF_PHONE'],
                'Fax' => $row['UF_FAX'],
                'WWW' => $row['UF_WWW'],
                'OGRN' => $row['UF_OGRN'],
                'INN' => $row['UF_INN'],
                'KPP' => $row['UF_KPP'],
                'Files' => $arFiles
            );
        }
        $arData = array();
        $countQuery = new Entity\Query($hl::getEntity());
        $countQuery->addSelect(new ExpressionField('CNT', 'COUNT(1)'));
        $countQuery->setFilter($filter);
        $totalCount = $countQuery->setLimit(null)->setOffset(null)->exec()->fetch();
        $totalCount = (int)$totalCount['CNT'];
        $totalPages = ceil($totalCount/$limit['nPageSize']);
        $NavFirstRecordShow = ($limit['iNumPage']-1)*$limit['nPageSize']+1;
        if ($limit['iNumPage'] != $totalPages)
            $NavLastRecordShow = $limit['iNumPage'] * $limit['nPageSize'];
        else
            $NavLastRecordShow = $totalCount;
        unset($countQuery);
        $arData = array(
            'Items' => $arResult,
            'Count' => $totalCount,
            'NavFirstRecordShow' => $NavFirstRecordShow,
            'NavLastRecordShow' => $NavLastRecordShow
        );
        if (strlen($params['Id']) > 0) {
            $arData = array_key_exists(0, $arResult) ? $arResult[0] : array();
            $slim->response()->write(json_encode(array('status' => true,'data' => $arData)), true);
        }
        else{
            $slim->response()->write(json_encode(array('status' => true,'data' => $arData)), true);
        }
        return $arData;

    }
    static function getGUID($Id){
        $hl = FescoContragents::getClass();
        $list = $hl::getList(array('filter' => array('ID' => $Id),'select' => array('UF_XML_ID')));
        if($row = $list->fetch()){
            return $row['UF_XML_ID'];
        }
        return '';
    }
    function postContragents(){
        $slim = \Slim\Slim::getInstance();
        $request = json_decode($slim->request()->getBody(), true);
        // get entity
        $hl = FescoContragents::getClass();
        $data = array(
            'UF_FULL_NAME' => $request['FullName'],
            'UF_NAME' => $request['Name'],
            'UF_SHORT_NAME' => $request['Name'],
            'UF_NAME_EN' => $request['NameEn'],
            'UF_LEGAL_COUNTRY' => $request['LegalCountry']['GUID'] ,
            'UF_LEGAL_ADDRESS' => $request['LegalAddress'],
            'UF_ACTUAL_COUNTRY' => $request['ActualCountry']['GUID'],
            'UF_ACTUAL_ADDRESS' => $request['ActualAddress'],
            'UF_PHONE' => $request['Phone'],
            'UF_FAX' => $request['Fax'],
            'UF_WWW' => $request['WWW'],
            'UF_OGRN' => $request['OGRN'],
            'UF_INN' => $request['INN'],
            'UF_KPP' => $request['KPP'],
            'UF_RESIDENT' => $request['Resident'],
            'UF_AGCY' => strpos($request['INN'],'9909') !== false ? 1 : 0,
            'UF_FESCO' => $request['Fesco'],
            'UF_XML_ID' => CFESCOTools::GUID()
        );
        $result = $hl::add($data);
        if($result->isSuccess())
            $slim->response()->write(json_encode(array('status' => true,'Id' => $result->getId())), true);
        else
            $slim->response()->write(json_encode(array('status' => false,'message' => $result->getErrors())), true);
    }
    function postContragent(){
        $slim = \Slim\Slim::getInstance();
        $hl = self::getClass();
        $request = json_decode($slim->request()->getBody(), true);
        $data = array(
            'UF_FULL_NAME' => $request['FullName'],
            'UF_NAME' => $request['Name'],
            'UF_NAME_EN' => $request['NameEn'],
            'UF_LEGAL_COUNTRY' => $request['LegalCountry']['GUID'] ,
            'UF_LEGAL_ADDRESS' => $request['LegalAddress'],
            'UF_ACTUAL_COUNTRY' => $request['ActualCountry']['GUID'],
            'UF_ACTUAL_ADDRESS' => $request['ActualAddress'],
            'UF_PHONE' => $request['Phone'],
            'UF_FAX' => $request['Fax'],
            'UF_WWW' => $request['WWW'],
            'UF_OGRN' => $request['OGRN'],
            'UF_INN' => $request['INN'],
            'UF_KPP' => $request['KPP']
        );
        $result = $hl::update($request['Id'],$data);
        if($result->isSuccess())
            $slim->response()->write(json_encode(array('status' => true, 'Id' => $request['Id'])), true);
        else
             $slim->response()->write(json_encode(array('status' => false)), true);
    }
}