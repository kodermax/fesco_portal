<?php
/**
 * Created by PhpStorm.
 * User: bitrix
 * Date: 2/2/15
 * Time: 9:55 AM
 */

use Bitrix\Main;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use Bitrix\Main\Entity\ExpressionField;
\Bitrix\Main\loader::includeModule('highloadblock');
class FescoTCO {
    function __construct(){

    }
    private function getRole() {
        //Группа администраторов
        $adminGroupId = self::getAdminGroupId();
        $usersGroupId = self::getUsersGroupId();
        $arGroups = $GLOBALS['USER']->GetUserGroupArray();
        if (in_array($adminGroupId, $arGroups))
        {
            return "admin";
        } else if (in_array($usersGroupId, $arGroups)) {
            return "user";
        }
        else {
            return "public";
        }
    }
    function getAccess(){
        $slim = \Slim\Slim::getInstance();
        $role = self::getRole();
        $slim->response()->write(json_encode(array('status' => true, 'role' => $role)), true);
    }
    function getMyLibrary(){
        self::getLibrary();
    }
    function getDocsLibrary(){
        $params = array('type' => 'doc');
        self::getLibrary($params);
    }
    function getMethodsLibrary(){
        $params = array('type' => 'method');
        self::getLibrary($params);
    }
    function getLibrary($params=array()){

        $slim = \Slim\Slim::getInstance();

        $request = $slim->request()->params();
        $arResult = array();
        $entity =  self::getEntity();
        $arHelps = self::getHelps();
        $filter = array();

        $limit = array(
            'nPageSize' => 10,
            'iNumPage' => $request['page'] ? $request['page'] : 1,
            'bShowAll' => false
        );
        if ($params['methodId'] > 0){
            $filter['=UF_METHOD_ID'] = $params['methodId'];
        }
        if ($params['type'] == 'method'){
            $filter['UF_TYPE'] = 'METHOD';
        }
        else if($params['type'] == 'doc'){
            $filter['UF_TYPE'] = 'DOC';
        }
        $request['q'] = trim($request['q']);
        if (strlen($request['q']) > 0) {
            $filter[] = array(
                'LOGIC' => 'OR',
                array(
                    '%=UF_NAME' => '%'.$request['q'].'%'
                ),
                array(
                    '%=UF_NUMBER' => '%'.$request['q'].'%'
                ),
            );

        }
        if (strlen($request['number']) > 0){
            $filter[] = array(
                array(
                    '%=UF_NUMBER' => '%'.$request['number'].'%'
                )

            );
        }
        if (strlen($request['title']) > 0){
            $filter[] = array(
                array(
                    '%=UF_NAME' => '%'.$request['title'].'%'
                )

            );
        }
        if ($request['dealType'] > 0){
            $filter[] = array(
                array(
                    '=UF_DEAL_TYPE' => $request['dealType']
                )

            );
        }
        if ($request['tcoMethod'] > 0){
            $filter[] = array(
                array(
                    '=UF_METHOD_TCO' => $request['tcoMethod']
                )

            );
        }
        if (strlen($request['status']) > 0 && (int)$request['status'] >= 0) {
            $filter[]  =array(
                '=UF_STATUS' => $request['status']
            );
        }

        $main_query = new Entity\Query($entity);
        $main_query->setSelect(array('*'));
        $main_query->setOrder(array('UF_NAME' => 'ASC'));
        if (strlen($request['contragent']) > 0){

            $main_query->registerRuntimeField('contragents',array(
                "data_type" => FescoContragents::getClass(),
                'reference' => array('=this.UF_CONTRAGENT' => 'ref.ID')

            ));
            $filter[] = array(
                'contragents.UF_NAME' => '%'.$request['contragent'].'%'
            );
        }
        if (strlen($request['taxPayer']) > 0){

            $main_query->registerRuntimeField('contragents',array(
                "data_type" => FescoContragents::getClass(),
                'reference' => array('=this.UF_TAXPAYER' => 'ref.ID')

            ));
            $filter[] = array(
                'contragents.UF_NAME' => '%'.$request['taxPayer'].'%'
            );
        }
        $main_query->setFilter(array($filter));
        $main_query->setLimit($limit['nPageSize']);
        $main_query->setOffset(($limit['iNumPage']-1) * $limit['nPageSize']);

        $result = $main_query->exec();
        $result = new CDBResult($result);
        while ($row = $result->Fetch())
        {

            $createdDate = new DateTime($row['UF_CREATE_DATE']);
            $changedDate = new DateTime($row['UF_CHANGE_DATE']);
            switch($row['UF_STATUS']){
                case 0:
                    $statusTitle = 'Черновик';
                    break;
                case 1:
                    $statusTitle = "На согласовании";
                    break;
                case 2:
                    $statusTitle = "Согласовано";
                    break;
                case 3:
                    $statusTitle = "На доработке";
                    break;



            }
            $arResult[] = array(
                'Id' => $row['ID'],
                'Number' => $row['UF_NUMBER'],
                'Contragent' => FescoContragents::getContragent($row['UF_CONTRAGENT']),
                'TaxPayer' => FescoContragents::getContragent($row['UF_TAXPAYER']),
                'GUID' => $row["UF_XML_ID"],
                'Title' => $row['UF_NAME'],
                'Type' => $row['UF_TYPE'],
                'TCOMethod' => $arHelps['TCOMethods'][$row['UF_METHOD_TCO']],
                'DealType' => $arHelps['DealTypes'][$row['UF_DEAL_TYPE']],
                'Author' => FescoUsers::getById($row['UF_CREATED_BY']),
                'Date' => array('Change' => $changedDate->format('d.m.Y'),'Create' => $createdDate->format('d.mm.Y')),
                'Status' => array('Value' => $row['UF_STATUS'], 'Title' => $statusTitle)
            );


        }
            $arData = array();
            $countQuery = new Entity\Query($entity);
            $countQuery->addSelect(new ExpressionField('CNT', 'COUNT(1)'));
            if (strlen($request['contragent']) > 0){

                $countQuery->registerRuntimeField('contragents',array(
                    "data_type" => FescoContragents::getClass(),
                    'reference' => array('=this.UF_CONTRAGENT' => 'ref.ID')

                ));
            }
        if (strlen($request['taxPayer']) > 0){

            $countQuery->registerRuntimeField('contragents',array(
                "data_type" => FescoContragents::getClass(),
                'reference' => array('=this.UF_TAXPAYER' => 'ref.ID')

            ));
        }
            $countQuery->setFilter($filter);
            $totalCount = $countQuery->setLimit(null)->setOffset(null)->exec()->fetch();
            $totalCount = (int)$totalCount['CNT'];
            $totalPages = ceil($totalCount/$limit['nPageSize']);
            $NavFirstRecordShow = ($limit['iNumPage']-1)*$limit['nPageSize']+1;
            if ($limit['iNumPage'] != $totalPages)
                $NavLastRecordShow = $limit['iNumPage'] * $limit['nPageSize'];
            else
                $NavLastRecordShow = $totalCount;
            unset($countQuery);
            $arData = array(
                'Items' => $arResult,
                'Count' => $totalCount,
                'NavFirstRecordShow' => $NavFirstRecordShow,
                'NavLastRecordShow' => $NavLastRecordShow
            );
           $slim->response->headers->set('Content-Type', 'application/json');
           $slim->response()->write(json_encode(array('status' => true, 'data' => $arData)), true);

    }
    function getDoc($Id){
        self::getDocs(array('Id' => $Id));
    }
    function getDocs($params){
        $slim = \Slim\Slim::getInstance();
        $slim->response->headers->set('Content-Type', 'application/json');
        $request = $slim->request()->params();
        $arHelps = self::getHelps();
        $entity = self::getEntity();
        $hl = $entity->getDataClass();
        $userId = $GLOBALS['USER']->GetID();
        $filter = array();
        if ($params['Id'] > 0){
            $filter['=ID'] = $params['Id'];
        }

        $row = $hl::getList(
            array(
                'select' => array('*'),
                'filter' => $filter
            ))->fetch();
        if (!empty($row)){
            $rsUser = CUser::GetByID($row['UF_CREATED_BY']);
            $arUser = $rsUser->Fetch();
            $arFiles = self::getFiles($row['UF_FILES']);
            $contractDate = new DateTime($row['UF_CONTRACT_DATE']);
            $arData = array(
                'Access' => array(
                    'admin' => self::getRole() == 'admin' ? true : false,
                    'author' => $row['UF_CREATED_BY'] == $userId ? true : false
                ),
                'Author' => array('Id' => $row['UF_CREATED_BY'], 'Name' => $arUser['NAME'].' '.$arUser['LAST_NAME']),
                'Contragent' => FescoContragents::getContragent($row['UF_CONTRAGENT']),
                'ContractDate' => $contractDate->format('d.m.Y'),
                'ContractNumber' => $row['UF_CONTRACT_NUMBER'],
                'DealType' => $arHelps['DealTypes'][$row['UF_DEAL_TYPE']],
                'Division' => $arHelps['Divisions'][$row['UF_DIVISION']],
                'Id' => $row['ID'],
                'IncomeSide' => $arHelps['IncomeSide'][$row['UF_INCOME_SIDE']],
                'InfoSource' => $arHelps['InfoSources'][$row['UF_INFO_SOURCE']],
                'Files' => $arFiles,
                'Name' => $row['UF_NAME'],
                'Number' => $row['UF_NUMBER'],
                'Materiality' => $arHelps['Materiality'][$row['UF_MATERIALITY']],
                'Method' => self::getMethod($row['UF_METHOD_ID']),
                'Sections' => json_decode($row['UF_SECTIONS'], true),
                'Status' => array('Code' => (int)$row['UF_STATUS']),
                'TaxPayer' => FescoContragents::getContragent($row['UF_TAXPAYER']),
                'TestContragent' => FescoContragents::getContragent($row['UF_TEST_CONTRAGENT']),
                'TCOMethod' => $arHelps['TCOMethods'][$row['UF_METHOD_TCO']],
                'Year' => $row['UF_YEAR']
            );
            switch (intval($row['UF_STATUS'])){
                case 0:
                    $title = 'Черновик';
                    break;
                case 1:
                    $title = 'На согласовании';
                    break;
                case 2:
                    $title = 'Согласовано';
                    break;
                default:
                    $title = 'Черновик';
                    break;
            }
            $arData['Status']['Title'] = $title;
            $slim->response()->write(json_encode(array('status' => true, 'data' => $arData)), true);
        }
        else {
            $slim->response()->write(json_encode(array('status' => true, 'data' => array())), true);
        }
    }

    function getDocWordFile($Id, $Section){
        $slim = \Slim\Slim::getInstance();
        $entity = FescoTCO::getEntity();
        $request = $slim->request()->params();
        $hl = $entity->getDataClass();
        $row = $hl::getList(
            array(
                'select' => array('UF_SECTIONS','UF_XML_ID'),
                'filter' => array('ID' => $Id)
            ))->fetch();
        if (!empty($row)){
            $arSections = json_decode($row['UF_SECTIONS'], true);
            $sourcePath = $_SERVER['DOCUMENT_ROOT'].'/upload/dav/tco/';
            $dirCode = md5($row['UF_XML_ID']);
            $dirPath = $_SERVER['DOCUMENT_ROOT'].'/upload/dav/tco/'.$dirCode;
            if (empty($arSections[$Section]['File']))
            {

                //Создаём ворд файл
                //Проверяем директорию
                if (!is_dir($dirPath)){
                    mkdir($dirPath, 0775);
                }
                $sourceFile = $sourcePath.$Section.'.docx';
                $destFile = $dirPath."/".$Section.'.docx';
                if (!copy($sourceFile,$destFile))
                    echo json_encode(array('status' => false, 'message' => 'Не удалось записать файл!'));

                $arSections[$Section]['File'] = '/tco/'.$dirCode."/".$Section.'.docx';
                $arData = array('UF_SECTIONS' => json_encode($arSections));
                $result = $hl::update($Id, $arData);
                if ($result->isSuccess())
                    $slim->response()->write(json_encode(array('status' => true, 'file' => "http://".$_SERVER['SERVER_NAME'].":8887".$arSections[$Section]['File'])), true);
                else
                    $slim->response()->write(json_encode(array('status' => false, 'message' => $result->getErrors())), true);
            }
            else {
                $slim->response()->write(json_encode(array('status' => true,'file' => "http://".$_SERVER['SERVER_NAME'].":8887".$arSections[$Section]['File'])), true);
            }
        }
    }

    function getHelps(){
        $slim = \Slim\Slim::getInstance();
        $arResult = array();
        $arHelps = array();
        $rs = CUserFieldEnum::GetList(array(), array(
            "USER_FIELD_NAME" => 'UF_DEAL_TYPE',
        ));
        while($ar = $rs->GetNext()) {
            $arResult['DealTypes'][] = array('Id' => $ar['ID'], 'Title' => $ar['VALUE']);
            $arHelps['DealTypes'][$ar['ID']] = array('Id' => $ar['ID'], 'Title' => $ar['VALUE']);
        }
        $rs = CUserFieldEnum::GetList(array(), array(
            "USER_FIELD_NAME" => 'UF_INFO_SOURCE',
        ));
        while($ar = $rs->GetNext()) {
            $arResult['InfoSources'][] = array('Id' => $ar['ID'], 'Title' => $ar['VALUE']);
            $arHelps['InfoSources'][$ar['ID']] = array('Id' => $ar['ID'], 'Title' => $ar['VALUE']);
        }
        $rs = CUserFieldEnum::GetList(array(), array(
            "USER_FIELD_NAME" => 'UF_METHOD_TCO',
        ));
        while($ar = $rs->GetNext()) {
            $arResult['TCOMethods'][] = array('Id' => $ar['ID'], 'Title' => $ar['VALUE']);
            $arHelps['TCOMethods'][$ar['ID']] = array('Id' => $ar['ID'], 'Title' => $ar['VALUE']);
        }
        $rs = CUserFieldEnum::GetList(array(), array(
            "USER_FIELD_NAME" => 'UF_DIVISION',
        ));
        while($ar = $rs->GetNext()) {
            $arResult['Divisions'][] = array('Id' => $ar['ID'], 'Title' => $ar['VALUE']);
            $arHelps['Divisions'][$ar['ID']] = array('Id' => $ar['ID'], 'Title' => $ar['VALUE']);
        }
        $rs = CUserFieldEnum::GetList(array(), array(
            "USER_FIELD_NAME" => 'UF_MATERIALITY',
        ));
        while($ar = $rs->GetNext()) {
            $arResult['Materiality'][] = array('Id' => $ar['ID'], 'Title' => $ar['VALUE']);
            $arHelps['Materiality'][$ar['ID']] = array('Id' => $ar['ID'], 'Title' => $ar['VALUE']);
        }
        $arResult['IncomeSide'] = array(array('Id' => 0,'Title' => 'Расходная сторона'),array('Id' => 1,'Title' => 'Доходная сторона'));
        $arHelps['IncomeSide'] = array(0 => array('Id' => 0,'Title' => 'Расходная сторона'), 1 => array('Id' => 1,'Title' => 'Доходная сторона'));

        $slim->response()->write(json_encode(array('status' => true, 'data' => $arResult)), true);
        return $arHelps;
    }

    function getMethod($Id){
        $arData = array();

        if ($Id == 0)
        {
            $slim = \Slim\Slim::getInstance();
            $arData = array('Id' => 0, 'Title' => 'Методика не известна');
            $slim->response()->write(json_encode(array('status' => true, 'data' => $arData)),true);
        }
        else {
            $arData = self::getMethods(array('Id' => $Id));
        }
        return $arData;

    }
    function getDocsFromMethod($methodId){
        self::getLibrary(array('type'=>'doc','methodId' => $methodId));
    }
    function getMethods($params){
        $slim = \Slim\Slim::getInstance();
        $entity = self::getEntity();
        $arHelps = self::getHelps();
        $arResult = array();
        $request = $slim->request()->params();
        $hl = $entity->getDataClass();
        $userId = $GLOBALS['USER']->GetID();
        $filter = array('UF_TYPE'=> 'METHOD');
        if (strlen($request['q']) > 0) {
            $filter[] = array(
                'LOGIC' => 'OR',
                array(
                    '%=UF_NAME' => '%' . $request['q'] . '%'
                ),
                array(
                    '%=UF_SHORT_NAME' => '%' . $request['q'] . '%'
                ),
                array(
                    '%=UF_INN' => '%' . $request['q'] . '%'
                ),
                array(
                    '%=UF_KPP' => '%' . $request['q'] . '%'
                ),
                array(
                    '%=UF_OGRN' => '%' . $request['q'] . '%'
                ),
            );
        } else {
            if ($params['Id'] > 0) {
                $filter = array('ID' => $params['Id']);
            }
        }
        $list = $hl::getList(
            array(
                'select' => array('*'),
                'filter' => $filter
            ));
        while ($row = $list->fetch()) {
            switch (intval($row['UF_STATUS'])){
                case 0:
                    $title = 'Черновик';
                    break;
                case 1:
                    $title = 'На согласовании';
                    break;
                case 2:
                    $title = 'Согласовано';
                    break;
                default:
                    $title = 'Черновик';
                    break;
            }
            $rsUser = CUser::GetByID($row['UF_CREATED_BY']);
            $arUser = $rsUser->Fetch();
            $arData['Status']['Title'] = $title;
                $arResult[] = array(
                    'ActiveDate' => strlen($row['UF_ACTIVE_FROM']) > 0 && strlen($row['UF_ACTIVE_TO']) > 0 ? array(
                        'StartDate' => strval($row['UF_ACTIVE_FROM']),
                        'EndDate' => strval($row['UF_ACTIVE_TO']),
                        'Text' => $row['UF_ACTIVE_FROM'] . ' - '. $row['UF_ACTIVE_TO']): '',
                    'Access' => array(
                        'admin' => self::getRole() == 'admin' ? true : false,
                        'author' => $row['UF_CREATED_BY'] == $userId ? true : false
                    ),
                    'Author' => array('Id' => $row['UF_CREATED_BY'], 'Name' => $arUser['NAME'].' '.$arUser['LAST_NAME']),
                    'Id' => $row['ID'],
                    'id' => $row['ID'],
                    'Number' => $row['UF_NUMBER'],
                    'Contragent' => $row['UF_CONTRAGENT'] > 0 ? FescoContragents::getContragent($row['UF_CONTRAGENT']):"",
                    'DealType' => $arHelps['DealTypes'][$row['UF_DEAL_TYPE']],
                    'Division' => $arHelps['Divisions'][$row['UF_DIVISION']],
                    'DocFile' => array('Url' => CFile::GetPath($row['UF_DOC_FILE'])),
                    'Name' => $row['UF_NAME'],
                    'Status' => array('Code' => (int)$row['UF_STATUS'],'Title' => $title),
                    'Title' => $row['UF_NAME'],
                    'Type' => $row['UF_TYPE'],
                    'text' => $row['UF_NAME'],
                    'IncomeSide' => $arHelps['IncomeSide'][$row['UF_INCOME_SIDE']],
                    'InfoSource' => $arHelps['InfoSources'][$row['UF_INFO_SOURCE']],
                    'TaxPayer' => $row['UF_TAXPAYER'] > 0 ? FescoContragents::getContragent($row['UF_TAXPAYER']): "",
                    'TestContragent' => $row['UF_TEST_CONTRAGENT'] > 0 ? FescoContragents::getContragent($row['UF_TEST_CONTRAGENT']):"",
                    'TCOMethod' => $arHelps['TCOMethods'][$row['UF_METHOD_TCO']]
                );
        }
        if (count($arResult) == 1 && $params['Id'] > 0) {
            $arData = $arResult[0];
        }
        else {
            $arData = array(
                'Items' => $arResult
            );
        }
        $slim->response->headers->set('Content-Type', 'application/json');
        $slim->response()->write(json_encode(array('status' => true, 'data' => $arData)), true);
        return $arData;
    }
    function getMethodsForSelect(){
        $slim = \Slim\Slim::getInstance();
        $entity = self::getEntity();
        $date = new DateTime();
        $currentDate = $date->format('d.m.Y');

        $arResult = array();

        $request = $slim->request()->params();
        $hl = $entity->getDataClass();
        $filter = array(
            'LOGIC' => 'AND',
            array('<=UF_ACTIVE_FROM' => $currentDate),
            array('>=UF_ACTIVE_TO' => $currentDate),
            array('=UF_STATUS' => 2),
            array('=UF_TYPE' => 'METHOD')
        );
        $list = $hl::getList(
            array(
                'select' => array('*'),
                'filter' => $filter
            ));
        while ($row = $list->fetch()) {
            $arResult[] = array(
                'Id' => $row['ID'],
                'Title' => $row['UF_NAME']
            );
        }
        if (count($arResult) > 0 ) {
            $slim->response()->write(json_encode(array('status' => true, 'data' => $arResult)), true);
        }
        else {
            $slim->response()->write(json_encode(array('status' => true, 'data' => array(array('Id' => 0, 'Title' => 'Нет методик')))), true);
        }
    }
    private function getAdminGroupId(){
        $rsGroups = CGroup::GetList ($by = "c_sort", $order = "asc", Array ("STRING_ID" => 'admin_tco'));
        if($arGroup = $rsGroups->Fetch()) {
            return $arGroup['ID'];
        }
        return 0;
    }
    private function getUsersGroupId(){
        $rsGroups = CGroup::GetList ($by = "c_sort", $order = "asc", Array ("STRING_ID" => 'dvmp_users_tco'));
        if($arGroup = $rsGroups->Fetch()) {
            return $arGroup['ID'];
        }
        return 0;
    }
    static function getEntity (){
        $filter = array(
            'select' => array('ID', 'NAME', 'TABLE_NAME', 'FIELDS_COUNT'),
            'filter' => array('=TABLE_NAME' => 'fesco_tcolibrary')
        );
        $hlblock = HL\HighloadBlockTable::getList($filter)->fetch();

        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        return $entity;
    }
    private function getDealTypes()
    {
        $arResult = array();

        return $arResult;
    }
    static function getUser($Id){

    }
    private function getFiles($arFiles){
        $arResult = array();
        foreach($arFiles as $fileId){
            $rsFile = CFile::GetByID($fileId);
            $arFile = $rsFile->Fetch();
            $ext = substr(strrchr($arFile['FILE_NAME'], '.'), 1);
            switch($ext){
                case "docx":
                case "doc":
                    $icon = "fa-file-word-o";
                    break;
                case "pdf":
                    $icon = "fa-file-pdf-o";
                    break;
                case "xlsx":
                case "xls":
                    $icon = "fa-file-excel-o";
                    break;
                case "zip":
                case "rar":
                    $icon = "fa-file-archive-o";
                    break;
                default:
                    $icon = "";
                    break;
            }
            $arResult[] = array(
                'Icon' => $icon,
                'Id' => $fileId,
                'Name' => $arFile['FILE_NAME'],
                'Size' => CFile::FormatSize($arFile['FILE_SIZE']),
                'Path' => CFile::GetPath($fileId),
            );
        }
        return $arResult;
    }
    function postDoc($Id){
        $slim = \Slim\Slim::getInstance();
        $entity = FescoTCO::getEntity();
        $request = json_decode($slim->request()->getBody(), true);

        $arHelps = FescoTCO::getHelps();
        $arData = array(
            'UF_NAME' => $request['Name'],
            'UF_TAXPAYER' => $request['TaxPayer']['Id'],
            'UF_CONTRAGENT' => $request['Contragent']['Id'],
            'UF_CONTRACT_DATE' => $request['ContractDate'],
            'UF_CONTRACT_NUMBER' => $request['ContractNumber'],
            'UF_DEAL_TYPE' => $request['DealType']['Id'],
            'UF_CREATED_BY' => $GLOBALS['USER']->GetID(),
            'UF_CREATE_DATE' => date('d.m.Y h:i:s'),
            'UF_CHANGE_DATE' => date('d.m.Y h:i:s'),
            'UF_INFO_SOURCE' => $request['InfoSource']['Id'],
            'UF_MATERIALITY' => $request['Materiality']['Id'],
            'UF_METHOD_ID' => $request['Method']['Id'],
            'UF_METHOD_TCO' => $request['TCOMethod']['Id'],
            'UF_TEST_CONTRAGENT' => $request['TestContragent']['Id'],
            'UF_INCOME_SIDE' => $request['IncomeSide']['Id'],
            'UF_DIVISION' => $request['Division']['Id'],
            'UF_TYPE' => 'DOC',
            'UF_YEAR' => intval(trim($request['Year']))

        );
        $hl = $entity->getDataClass();
        $result = $hl::update($Id, $arData);
        if ($result->isSuccess()) {
            $slim->response()->write(json_encode(array('status' => true,'id' => $result->getId())), true);
        }
        else {
            $slim->response()->write(json_encode(array('status' => false, 'message' => $result->getErrors())), true);
        }
    }
    function postDocs(){
        $slim = \Slim\Slim::getInstance();
        $entity = FescoTCO::getEntity();
        $request = json_decode($slim->request()->getBody(), true);
        $arHelps = FescoTCO::getHelps();
        $arData = array(
            'UF_NAME' => $request['Name'],
            'UF_TAXPAYER' => $request['TaxPayer']['Id'],
            'UF_CONTRAGENT' => $request['Contragent']['Id'],
            'UF_CONTRACT_DATE' => $request['ContractDate'],
            'UF_CONTRACT_NUMBER' => $request['ContractNumber'],
            'UF_DEAL_TYPE' => $request['DealType']['Id'],
            'UF_CREATED_BY' => $GLOBALS['USER']->GetID(),
            'UF_CREATE_DATE' => date('d.m.Y h:i:s'),
            'UF_CHANGE_DATE' => date('d.m.Y h:i:s'),
            'UF_INFO_SOURCE' => $request['InfoSource']['Id'],
            'UF_MATERIALITY' => $request['Materiality']['Id'],
            'UF_METHOD_ID' => $request['Method']['Id'],
            'UF_METHOD_TCO' => $request['TCOMethod']['Id'],
            'UF_TEST_CONTRAGENT' => $request['TestContragent']['Id'],
            'UF_INCOME_SIDE' => $request['IncomeSide']['Id'],
            'UF_DIVISION' => $request['Division']['Id'],
            'UF_TYPE' => 'DOC',
            'UF_YEAR' => intval(trim($request['Year'])),
            'UF_SECTIONS' => json_encode(array(
                'Section1' => array('Status' => 0),
                'Section2' => array('Status' => 0),
                'Section3' => array('Status' => 0),
                'Section4' => array('Status' => 0),
                'Section5' => array('Status' => 0),
                'Section6' => array('Status' => 0),
                'Section7' => array('Status' => 0),
                'Section8' => array('Status' => 0),
                'Section9' => array('Status' => 0),
                'Section10' => array('Status' => 0)))

        );
        $oCounter = new FESCO\Counters();
        $cnt = $oCounter->getCounter('tco_docs');
        $number = sprintf('%1$04d_', $cnt + 1);
        $number .= 'Д1_';
        if ($request['IncomeSide']['Id'])
            $number .= 'Д3_';
        else
            $number .= 'Р3_';
        $number .= $request['Division']['Title'];
        $arData['UF_NUMBER'] = $number."_".date('Y');

        $hl = $entity->getDataClass();
        $result = $hl::add($arData);
        if ($result->isSuccess()) {
            $oCounter->incCounter('tco_docs');
            $slim->response()->write(json_encode(array('status' => true,'id' => $result->getId())), true);
        }
        else {
            $slim->response()->write(json_encode(array('status' => false, 'message' => $result->getErrors())), true);
        }
    }

    function addFileToDoc($Id){
        $slim = \Slim\Slim::getInstance();
        $entity = self::getEntity();
        $hl = $entity->getDataClass();
        $data = array();
        $list = $hl::getList(array(
            'filter' => array('ID' => $Id),
            'select' => array('UF_FILES')
        ));
        $arFiles = array();
        if($row = $list->fetch())
        {
            foreach ($row['UF_FILES'] as $file)
            {
                $arFile = array('name' => '','type' => '','tmp_name'=>'','error'=>4,'size'=>0,'del'=>false,'old_id' => $file);
                $arFiles[] = $arFile;
            }
        }
        $i = 0;
        $arFiles['n'.$i++] = $_FILES['file'];
        $result = $hl::update($Id, array('UF_FILES' => $arFiles));
        if ($result->isSuccess()) {
            $arData = $result->getData();
            $arFiles = unserialize($arData['UF_FILES']);
            $arResult = self::getFiles($arFiles);;
            $slim->response()->write(json_encode(array('status' => true, 'files' => $arResult)), true);
        }
        else
            $slim->response()->write(json_encode(array('status' => false, 'message' => $result->getErrors())), true);
    }

    function approveTraffic($Id){
        $entity = FescoTCO::getEntity();
        $hl = $entity->getDataClass();
        $row = $hl::getList(
            array(
                'select' => array('UF_SECTIONS'),
                'filter' => array('ID' => $Id)
            ))->fetch();
        if (!empty($row)){
            $arSections = json_decode($row['UF_SECTIONS'], true);
        }
        foreach($arSections as $key => $arSection) {
            $arSections[$key]['Status'] = 2;
        }
        $arData = array('UF_SECTIONS' => json_encode($arSections));
        $result = $hl::update($Id, $arData);
    }
    function changeStatusTraffic($Id){
        $slim = \Slim\Slim::getInstance();
        $entity = FescoTCO::getEntity();
        $request = json_decode($slim->request()->getBody(), true);
        $hl = $entity->getDataClass();
        $row = $hl::getList(
            array(
                'select' => array('UF_SECTIONS'),
                'filter' => array('ID' => $Id)
            ))->fetch();
        if (!empty($row)){
            $arSections = json_decode($row['UF_SECTIONS'], true);
        }
        $arSections[$request['Section']]['Status'] = intval($request['Value']);
        $arData = array('UF_SECTIONS' => json_encode($arSections));
        $result = $hl::update($Id, $arData);
        if ($result->isSuccess())
            echo json_encode(array('status' => true));
        else
            echo json_encode(array('status' => false, 'message' => $result->getErrors()));
    }
    function removeFileFromDoc($docId, $fileId){
        $slim = \Slim\Slim::getInstance();
        $entity = FescoTCO::getEntity();
        $hl = $entity->getDataClass();
        $data = array();
        $list = $hl::getList(array(
            'filter' => array('ID' => $docId),
            'select' => array('UF_FILES')
        ));
        $arFiles = array();
        if($row = $list->fetch())
        {
            foreach ($row['UF_FILES'] as $file)
            {
                if ($file == $fileId)
                    $arFile = array('name' => '','type' => '','tmp_name'=>'','error'=>4,'size'=>0,'del'=>true,'old_id' => $file);
                else
                    $arFile = array('name' => '','type' => '','tmp_name'=>'','error'=>4,'size'=>0,'del'=>false,'old_id' => $file);

                $arFiles[] = $arFile;
            }
        }
        $result = $hl::update($docId, array('UF_FILES' => $arFiles));
        if ($result->isSuccess())
            $slim->response()->write(json_encode(array('status' => true)), true);
        else
            $slim->response()->write(json_encode(array('status' => false, 'message'=>$result->getErrors())), true);
    }
    function startDocApproval($docId){
       self::changeStatus($docId, 1);
    }
    function approveDoc($Id){
       self::approveTraffic($Id);
       self::changeStatus($Id, 2);
    }

    function changeStatus($Id, $status){
        $slim = \Slim\Slim::getInstance();
        $entity = FescoTCO::getEntity();
        $hl = $entity->getDataClass();
        $arData = array(
            'UF_STATUS' => $status
        );
        $result = $hl::update($Id, $arData);
        if ($result->isSuccess()){
            switch($status){
                case 0:
                    $title = 'Черновик';
                    break;
                case 1:
                    $title = 'На согласовании';
                    break;
                case 2:
                    $title = 'Согласовано';
                    break;
                case 3:
                    $title = 'На доработке';
                    break;
                default:
                    $title = 'Черновик';
                    break;
            }
            $slim->response()->write(json_encode(array('status' => true, 'result' => array('Code' => $status,'Title' => $title))), true);
        }
        else {
            $slim->response()->write(json_encode(array('status' => false, 'message' => $result->getErrors())), true);
        }
    }
    function revisionDoc($Id){;
        self::changeStatus($Id, 3);
    }
    function revisionMethod($Id){
        self::changeStatus($Id, 3);
    }
    function approveMethod($Id){
        self::changeStatus($Id, 2);
    }
    function startMethodApproval($Id){
      self::changeStatus($Id, 1);
    }

    function addFileToMethod(){
        $slim = \Slim\Slim::getInstance();
        $request = $slim->request()->params();
        $entity = FescoTCO::getEntity();
        $hl = $entity->getDataClass();
        $result = $hl::update($request['Id'], array('UF_DOC_FILE' => $_FILES['file']));
        if ($result->isSuccess())
            $slim->response()->write(json_encode(array('status' => true, 'id' => $request['Id'])), true);
        else
            $slim->response()->write(json_encode(array('status' => false)), true);
    }

    function postMethod($Id){
        $slim = \Slim\Slim::getInstance();
        $entity = self::getEntity();
        $hl = $entity->getDataClass();
        $request = json_decode($slim->request()->getBody(), true);
        $arHelps = self::getHelps();
        $arData = array(
            'UF_ACTIVE_FROM' => $request['ActiveDate']['StartDate'],
            'UF_ACTIVE_TO' => $request['ActiveDate']['EndDate'],
            'UF_NAME' => $request['Name'],
            'UF_TAXPAYER' => $request['TaxPayer']['Id'],
            'UF_CONTRAGENT' => $request['Contragent']['Id'],
            'UF_DEAL_TYPE' => $request['DealType']['Id'],
            'UF_CREATED_BY' => $GLOBALS['USER']->GetID(),
            'UF_CREATE_DATE' => date('d.m.Y h:i:s'),
            'UF_CHANGE_DATE' => date('d.m.Y h:i:s'),
            'UF_INFO_SOURCE' => $request['InfoSource']['Id'],
            'UF_METHOD_TCO' => $request['TCOMethod']['Id'],
            'UF_TEST_CONTRAGENT' => $request['TestContragent']['Id'],
            'UF_INCOME_SIDE' => $request['IncomeSide']['Id'],
            'UF_DIVISION' => $request['Division']['Id'],
            'UF_TYPE' => 'METHOD'
        );
        $result = $hl::update($Id, $arData);
        if ($result->isSuccess()) {
            $slim->response()->write(json_encode(array('status' => true,'id' => $result->getId())), true);
        }
        else {
            $slim->response()->write(json_encode(array('status' => false, 'message' => $result->getErrors())), true);
        }
    }

    function postMethods(){
        $slim = \Slim\Slim::getInstance();
        $entity = FescoTCO::getEntity();
        $hl = $entity->getDataClass();
        $request = json_decode($slim->request()->getBody(), true);
        $arHelps = FescoTCO::getHelps();
        $arData = array(
            'UF_ACTIVE_FROM' => $request['ActiveDate']['StartDate'],
            'UF_ACTIVE_TO' => $request['ActiveDate']['EndDate'],
            'UF_NAME' => $request['Name'],
            'UF_TAXPAYER' => $request['TaxPayer']['Id'],
            'UF_CONTRAGENT' => $request['Contragent']['Id'],
            'UF_DEAL_TYPE' => $request['DealType']['Id'],
            'UF_CREATED_BY' => $GLOBALS['USER']->GetID(),
            'UF_CREATE_DATE' => date('d.m.Y h:i:s'),
            'UF_CHANGE_DATE' => date('d.m.Y h:i:s'),
            'UF_INFO_SOURCE' => $request['InfoSource']['Id'],
            'UF_METHOD_TCO' => $request['TCOMethod']['Id'],
            'UF_TEST_CONTRAGENT' => $request['TestContragent']['Id'],
            'UF_INCOME_SIDE' => $request['IncomeSide']['Id'],
            'UF_DIVISION' => $request['Division']['Id'],
            'UF_TYPE' => 'METHOD'

        );
        $oCounter = new FESCO\Counters();
        $cnt = $oCounter->getCounter('tco_methods');
        $number = sprintf('%1$04d_', $cnt + 1);
        $number .= 'М1_';
        if ($request['IncomeSide']['Id'])
            $number .= 'Д3_';
        else
            $number .= 'Р3_';
        $number .= $arHelps['Divisions'][$request['Division']['Id']]['Title'];
        $arData['UF_NUMBER'] = $number."_".date('Y');
        $result = $hl::add($arData);
        if ($result->isSuccess()) {
            $oCounter->incCounter('tco_methods');
            $slim->response()->write(json_encode(array('status' => true,'id' => $result->getId())), true);
        }
        else {
            $slim->response()->write(json_encode(array('status' => false, 'errors' => $result->getErrors())), true);
        }
    }
}
?>