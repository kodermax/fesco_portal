<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$GLOBALS['APPLICATION']->RestartBuffer();
?>

<?require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php');
?>

<?
$file = trim(preg_replace("'[\\\\/]+'", "/", (dirname(__FILE__)."/lang/".LANGUAGE_ID."/template.php")));
__IncludeLang($file);

if (isset($_REQUEST['CHECK_NAME']))
{
	$fileName = CUtil::ConvertToLangCharset(urldecode($_REQUEST['CHECK_NAME']));
	$ob = $arParams['OBJECT'];
	$fileName = $ob->CorrectName($fileName);
	$result = array();
	$result['name'] = $fileName;
	$result['section'] = $arParams['SECTION_ID'];
	if (!check_bitrix_sessid())
	{
		$result['permission'] = false;
		$result['errormsg'] = GetMessage("WD_ERROR_BAD_SESSID");
	}
	elseif (!$ob->CheckWebRights("PUT", array('arElement' => $fileName)))
	{
		$result['permission'] = false;
		$oError = $GLOBALS["APPLICATION"]->GetException();
		$result['errormsg'] = ($oError ? $oError->GetString() : GetMessage("WD_ERROR_UPLOAD_BAD_FILE"));
	}
	elseif (COption::GetOptionString('webdav', 'hide_system_files', 'Y') == 'Y' && substr($fileName, 0, 1) == '.')
	{
		$result['permission'] = true;
		$result['okmsg'] = GetMessage("WD_WARNING_FIRST_DOT");
	}
	elseif (
		is_numeric($arParams['ELEMENT_ID'])
		&& ($arParams['ELEMENT_ID'] == 0)
		&& (!$ob->CheckUniqueName($fileName, $arParams["SECTION_ID"], $res))
	)
	{
		if ($ob->Type == "folder")
		{
			$result['permission'] = false;
			$result['errormsg'] = GetMessage("WD_ERROR_SAME_NAME");
		}
		else
		{
			$result['permission'] = false;
			$result['okmsg'] = GetMessage("WD_WARNING_SAME_NAME", array("#LINK#"=>'class="ajax" onclick="WDUploadExpand();"'));
		}
	}
	elseif (($arParams['ELEMENT_ID'] !== 0) && ($arResult['ELEMENT']['FILE_EXTENTION'] != strToLower(strrchr($fileName, "."))))  
	{
		$result['permission'] = false;
		$result['errormsg'] = GetMessage("WD_WARNING_EXTENSIONS_DONT_MATCH");
	}
	else 
	{
		$result['permission'] = true;
		$result['okmsg'] = '';
	}
	echo CUtil::PhpToJSObject($result);
	die();
}
?>
<style>
    .k-multiselect-wrap>.k-input {
        color: #aaa;
        font-family: "Helvetica Neue",Helvetica,Arial;;
    }
    .k-multiselect {
        background-color: #fff;
        border: 1px solid rgba(0, 0, 0, 0.15);
        border-radius: 0.3125em;
        box-shadow: 0 0 rgba(0, 0, 0, 0.3) inset;
        box-sizing: border-box;
        color: rgba(0, 0, 0, 0.7);
        font-size: 1em;
        margin: 0;
        outline: 0 none;
        padding: 0.65em 1em;
        transition: background-color 0.3s ease-out 0s, box-shadow 0.2s ease 0s, border-color 0.2s ease 0s;
    }
    .k-state-hover {
        border-color: #rgba(0, 0, 0, 0.15)!important;
        background-color: #ffffff!important;
        background-image: none!important;
    }
    .k-state-focused {
        border-color: #ffffff!important;
        background-color: #ffffff!important;
        background-image: none!important;
    }
    .k-icon {
        font-size:0!important;
    }
    .k-multiselect ul li {
        padding-right: 3px;
    }
</style>
<?
$popupWindow = new CJSPopup('', '');
if ($arParams['ELEMENT_ID'] != 0) 
{
	$popupWindow->ShowTitlebar(TruncateText(GetMessage("WD_UPLOAD_VERSION_TITLE", array("#NAME#" => $arResult["ELEMENT"]["NAME"])), 75));
} else {
	$popupWindow->ShowTitlebar(GetMessage("WD_UPLOAD_TITLE"));
}
$popupWindow->StartContent();
?>
</form>

<iframe id="upload_iframe" name="upload_iframe" style="display:none;"> </iframe>
<script type="text/javascript" src="/shared/kendo-ui/js/kendo.custom.min.js"></script>
<link href="/shared/kendo-ui/styles/kendo.common.core.min.css" type="text/css" rel="stylesheet"/>
<link href="/shared/kendo-ui/styles/kendo.default.min.css" type="text/css" rel="stylesheet"/>
<form method="post" name='wd_upload_form' id="wd_upload_form" target="upload_iframe" action="<?=POST_FORM_ACTION_URI?>" enctype="multipart/form-data">
<?=bitrix_sessid_post()?>
<input type="hidden" name="SECTION_ID" value="<?=htmlspecialcharsbx($arParams["SECTION_ID"])?>" />
<input type="hidden" name="ELEMENT_ID" value="<?=htmlspecialcharsbx($arParams["ELEMENT_ID"])?>" />
<input type="hidden" name="IBLOCK_SECTION_ID" value="<?=intval($arResult["SECTION"]["IBLOCK_SECTION_ID"])?>" />
<input type="hidden" name="edit_section" value="Y" />
<input type="hidden" name="save_upload" value="Y" />
<input type="hidden" name="ACTIVE" value="Y" />
<input type="hidden" name="overview" value="Y" />
<input type="hidden" name="AJAX_CALL" value="Y" />
<input type="hidden" name="SIMPLE_UPLOAD" value="Y" />
<input type="hidden" name="MAX_FILE_SIZE" value="<?=htmlspecialcharsbx($arParams["UPLOAD_MAX_FILESIZE_BYTE"])?>" />
<? if ($arParams['ELEMENT_ID'] != 0): ?>
<input type="hidden" name="overview" value="Y" />
<? endif; ?>


<div class="ui green form segment">
    <?if($arParams['ELEMENT_ID'] == 0):?>
        <div class="field">
            <label>Наименование договора <span class="required starrequired">*</span></label>
            <input placeholder="Наименование договора" id="PROPERTY_DOC_NAME" name="PROPERTY_DOC_NAME" type="text" style="height:35px">
			<div class="ui red pointing prompt label transition" style="display:none;">Необходимо заполнить поле "Наименование договора"</div>
        </div>
		<div class="ui two fields">
			<div class="field">
				<label>Номер договора <span class="required starrequired">*</span></label>
				<input placeholder="Номер договора" id="PROPERTY_DOC_ID" name="PROPERTY_DOC_ID" type="text" style="height:35px">
			</div>
			<div class="field">
				<label>Дата договора <span class="required starrequired">*</span></label>
				<input style="width:90%;height:35px;" placeholder="Дата договора" id="PROPERTY_DOC_DATE" name="PROPERTY_DOC_DATE" type="text">
				<?ob_start();
				$GLOBALS['APPLICATION']->IncludeComponent(
					"bitrix:main.calendar",
					"",
					array(
						"SHOW_INPUT" => "N",
						"FORM_NAME" => $arParams["form_name"],
						"INPUT_NAME" => "PROPERTY_DOC_DATE",
						"SHOW_TIME" => 'N',
					),
					$component,
					array("HIDE_ICONS" => "Y"));
				$html = ob_get_clean();
				echo $html
				?>
			</div>
		</div>
		<div class="ui three fields">
			<div class="field">
				<label>Цена договора <span class="required starrequired">*</span></label>
				<input placeholder="Цена договора" id="PROPERTY_PRICE" name="PROPERTY_PRICE" type="text" style="height:35px">
			</div>
			<div class="field">
				<label>Валюта договора <span class="required starrequired">*</span></label>
				<select class="form-control" id="PROPERTY_CURRENCY" name="PROPERTY_CURRENCY">
						<option value="0">Выберите валюту</option>
						<?foreach($arResult['CURRENCY_LIST'] as $key => $item) :?>
							<option value="<?=$key?>"><?=$item?></option>
						<?endforeach;?>
				</select>
			</div>
			<div class="field">
				<label>Курс валюты <span class="required starrequired">*</span></label>
				<input placeholder="Курс валюты" id="PROPERTY_RATE" name="PROPERTY_RATE" type="text" style="height:35px">
			</div>
		</div>
		<div class="field">
			<label>Сумма в рублях (авто)</label>
			<span class="ui label totalSum"></span>
		</div>
		<div class="field">
			<label>Родительский договор</label>
			<input type="hidden" id="PROPERTY_PARENT_ID" name="PROPERTY_PARENT_ID">
			<input type="text" id="PARENT_ID" name="PARENT_ID" style="width:100%;height: 35px;"/>
		</div>
		<div class="field">
			<label>Стороны <span class="required starrequired">*</span></label>
			<select multiple name="CONTRACTORS[]" id="CONTRACTORS"></select>
			<div class="ui red pointing prompt label transition" style="display:none;">Необходимо заполнить поле "Стороны"</div>
		</div>
		<div class="field">
			<label>Предмет договора <span class="required starrequired">*</span></label>
			<textarea id="Description_1" name="Description_1"></textarea>
			<div class="ui red pointing prompt label transition" style="display:none;">Необходимо заполнить поле "Предмет договора"</div>
		</div>
		<div class="field">
			<label>Пролонгация <span class="required starrequired">*</span></label>
			<input placeholder="Пролонгация" id="PROPERTY_PROLONGATION" name="PROPERTY_PROLONGATION" type="text" style="height:35px">
			<div class="ui red pointing prompt label transition" style="display:none;">Необходимо заполнить поле "Пролонгация"</div>
		</div>
		<div class="inline field">
			<label>Дополнительные сопровождающие документы <span class="required starrequired">*</span>
				<i class="info square icon big link" title="Полномочия подписанта, справка контрагента ДЭБ, протокол тендерной комиссии"></i></label>
			<table id="tblSIGNATORY">
				<tr>
					<td><input type="file" size="20" name="SIGNATORY[n0][VALUE]"/> </td>
				</tr>
			</table>
			<div class="ui tiny icon button" onclick="addNewTableRow('tblSIGNATORY', 1, /SIGNATORY\[(n)([0-9]*)\]/g, 2)">
				<i class="add icon"></i>Добавить
			</div>
		</div>
    <?endif;?>
    <div class="field">
        <label><b>Договор</b> <span class="required starrequired">*</span></label>
        <input type="file" id="SourceFile_1" name="SourceFile_1"/>
    </div>
</div>
<script>
	function updateSum()
	{
		var rate = parseInt($("#PROPERTY_RATE").val());
		var sum = Number($("#PROPERTY_PRICE").val().replace(/[,]+/g,".").replace(/[^0-9\.]+/g,""));
		var curr = $("#PROPERTY_CURRENCY option:selected" ).text();
		if (!isNaN(rate) && !isNaN(sum))
		{
			var total = 0;
			switch (curr)
			{
				case "USD":
					total = sum*rate;
					break;
				case "EUR":
					total = sum*rate;
					break;
				case "RUB":
					total = sum;
					break;
				default:
					curr = "";
			}

			$('.totalSum').text(accounting.formatMoney(total, "руб.", 0, " ", ",", "%v %s"));
		}

	}
	$(document).ready(function() {
		$("#PROPERTY_RATE").change(function() {
			updateSum();
		});
		$("#PROPERTY_PRICE").change(function() {
			updateSum();
		});
		$("#PROPERTY_CURRENCY").change(function() {
			var curr = $("#PROPERTY_CURRENCY option:selected").text();
			if (curr == "RUB")
				$("#PROPERTY_RATE").val(1);
			updateSum();
		});
		$("#PARENT_ID").kendoAutoComplete({
			placeholder: "Введите № родительского договора",
			dataTextField: "Name",
			filter: "contains",
			autoBind: false,
			highlightFirst: true,
			select: function(e) {
				var dataItem = this.dataItem(e.item.index());
				$("#PROPERTY_PARENT_ID").val(dataItem.Id);
			},
			template: '<span class="k-state-default">#:data.Name#</span>',
			dataSource: {
				serverFiltering: true,
				transport: {
					read: {
						url: "/local/json/contracts.json.php",
						dataType: 'json'
					}
				}
			}
		});
		$("#CONTRACTORS").kendoMultiSelect({
			placeholder: "Выберите компанию",
			filter: "contains",
			dataTextField: "Name",
			dataValueField: "Id",
			autoBind: false,
			dataSource: {
				serverFiltering: true,
				transport: {
					read: {
						url: "/local/json/contragents.json.php",
						dataType: 'json'
					}
				}
			}
		});
	});
</script>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<? if (($arParams['ELEMENT_ID'] != 0) && ($arResult["ELEMENT"]["LOCK_STATUS"] == 'yellow')): ?>
	<tr>
		<td width="40%">
			<?=GetMessage("WD_UPLOAD_UNLOCK")?>:
		</td>
		<td width="60%">
			<input type="checkbox" id="wd_upload_unlock" name="UploadUnlock" style="width:90%;" />
		</td>
	</tr>
<? endif; ?>
</table>

<table id="wd_upload_props" style="display:none;" cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td width="40%">
			<?=GetMessage("Title")?>:
		</td>
		<td width="60%">
<?if ($arParams["OBJECT"]->Type=='iblock'):?>
			<input type="text" id="Title_1" name="Title_1" style="width:90%;" value="<?=(isset($arResult['ELEMENT']['NAME']) ? htmlspecialcharsbx($arResult['ELEMENT']['NAME']) : '')?>" />
<?else:?>
			<input type="text" id="Title_1" name="Title_1" style="width:90%;" value="<?=(isset($arParams['SECTION_ID']) ? htmlspecialcharsbx($arParams['OBJECT']->arParams['base_name']) : '')?>" />
<?endif;?>
		</td>
	</tr>
	<tr>
		<td width="40%">
			<?=GetMessage("Tags")?>:
		</td>
		<td width="60%">
<?
	if (IsModuleInstalled("search"))
	{
?>
		<script>
		BX( function() {
			setTimeout( function() {
				TCJsUtils._show = TCJsUtils.show;
				TCJsUtils.show = function(oDiv, iLeft, iTop) {
					oDiv.style.zIndex = 3000;
					return TCJsUtils._show(oDiv, iLeft, iTop);
				}
			}, 800); });
		</script>
<?
		$arTagParams = array(
			"VALUE" => (isset($arResult['ELEMENT']['TAGS']) ? $arResult['ELEMENT']['TAGS'] : ''),
			"NAME" => "Tag_1",
			"ID" => "Tag_1"
		);
		if (isset($ob->attributes['group_id']))
		{
			$groupID = intval($ob->attributes['group_id']);
			if ($groupID > 0)
			{
				$arTagParams['arrFILTER'] = 'socialnetwork';
				$arTagParams['arrFILTER_socialnetwork'] = $groupID;
			}
		}
		$APPLICATION->IncludeComponent(
			"bitrix:search.tags.input",
			"",
			$arTagParams,
			null,
			array("HIDE_ICONS" => "Y"));
	}
	else
	{
?>
		<input type="text" id="Tag_1" name="Tag_1" style="width:90%;" value="<?=(isset($arResult['ELEMENT']['TAGS']) ? $arResult['ELEMENT']['TAGS'] : '')?>" />
<?
	}
?>
		</td>
	</tr>
	<tr>
		<td width="40%">
			<?=GetMessage("Description")?>:
		</td>
	</tr>
</table>
</form>
<table id="wd_messages" style="display:none;" cellpadding="0" cellspacing="0" width="100%"> <tr><td>
			<div id="wd_upload_error_message" style="color:#dd0000;"></div>
			<div id="wd_upload_ok_message" style="color:#009900;"></div>
		</td></tr></table>
<? include trim(preg_replace("'[\\\\/]+'", "/", (dirname(__FILE__)."/footer.php"))); ?>

<script>

$(document).ready(function(){
    $('.adm-workarea').removeClass('adm-workarea');
    $('.bx-core-window').removeClass('bx-core-window');
});
function WDUploadExpand(link)
{
	var wdExtTable = BX('wd_upload_props');
	if (wdExtTable.style.display != 'table')
	{
		wdExtTable.style.display = 'table';
		if (link)
			link.parentNode.innerHTML = '<?=GetMessage('WD_UPLOAD_COLLAPSE_PROPS', array("#LINK#" => 'id="wd_upload_expand" onclick="WDUploadExpand(this);"'))?>';
	}
	else
	{
		wdExtTable.style.display = 'none';
		if (link)
			link.parentNode.innerHTML = '<?=GetMessage('WD_UPLOAD_EXPAND_PROPS', array("#LINK#" => 'id="wd_upload_expand" onclick="WDUploadExpand(this);"'))?>';
	}
}

function WDUploadInit(dialog)
{
	dialog.WDUploadInit({
		'msg':{
			'Submit':"<?=CUtil::JSEscape(GetMessage((($arParams['ELEMENT_ID'] == 0)?'Send':'Send_Version')));?>",
			'Close':"<?=CUtil::JSEscape(GetMessage('WD_CLOSE'));?>",
			'SendVersion':"<?=CUtil::JSEscape(GetMessage("Send_Version"))?>",
			'SendDocument':"<?=CUtil::JSEscape(GetMessage("Send_Document"))?>",
			'UploadSuccess':"<?=CUtil::JSEscape(GetMessage('WD_UPLOAD_SUCCESS'));?>",
			'UploadInterrupt':"<?=CUtil::JSEscape(GetMessage('WD_UPLOAD_INTERRUPT_BEWARE'))?>",
			'UploadInterruptConfirm':"<?=CUtil::JSEscape(GetMessage('WD_UPLOAD_INTERRUPT_CONFIRM'))?>",
			'UploadNotDone':"<?=CUtil::JSEscape(GetMessage('WD_UPLOAD_NOT_DONE'))?>",
			'UploadNotDoneAsk':"<?=CUtil::JSEscape(GetMessage('WD_UPLOAD_NOT_DONE_ASK'))?>"
		},
		'fileUpdate':<?=(($arParams['ELEMENT_ID'] != 0) ? "true" : "false" )?>,
		'closeAfterUpload': <?=((isset($_REQUEST["close_after_upload"]) && $_REQUEST["close_after_upload"] == "Y") ? "true" : "false")?>,
		'dropAutoUpload': <?=((isset($_REQUEST["bp_param_required"])) ? "false" : "true")?>,
		'checkFileUrl': "<?=CUtil::JSEscape(POST_FORM_ACTION_URI);?>",
		'uploadFileUrl': "<?=CUtil::JSEscape(WDAddPageParams(POST_FORM_ACTION_URI, array('use_light_view'=>'Y')));?>",
		'targetUrl': "<?=CUtil::JSEscape($url)?>",
		'updateDocument': <?=(isset($_REQUEST["update_document"]) ? "true" : "false" )?>,
		'sessid':"<?=bitrix_sessid()?>",
		'sectionID':"<?=intval($arParams['SECTION_ID'])?>",
		'elementID':"<?=CUtil::JSEscape(urlencode($arParams['ELEMENT_ID']))?>"
	});
	BX.onCustomEvent(dialog,  'onUploadPopupReady');
}

BX(function() {
	var dialog = BX.WindowManager.Get();

	BX.loadScript("<?=CUtil::GetAdditionalFileURL($this->__folder . '/script_deferred.js')?>", function(){
		WDUploadInit(dialog);
	});
});

</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_js.php");?>


