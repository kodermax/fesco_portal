<?
\Bitrix\Main\Loader::includeModule('lists');
$property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "CODE" => "CURRENCY"));
while ($enum_fields = $property_enums->GetNext()) {
    if(!empty($enum_fields['VALUE']))
        $arResult['CURRENCY_LIST'][$enum_fields['ID']] = $enum_fields['VALUE'];
}
?>