<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 02.10.2014
 * Time: 14:14
 */
 if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "IBLOCK_ID" => array(
            "PARENT" => "BASE",
            "NAME" => "ID инфоблока",
            "TYPE" => "INT",
            "MULTIPLE" => "N",
            "DEFAULT" => 0
        ),
        "ELEMENT_ID" => array(
            "PARENT" => "BASE",
            "NAME" => "ID элемента",
            "TYPE" => "INT",
            "MULTIPLE" => "N",
            "DEFAULT" => 0
        ),
    ),
    );
?>