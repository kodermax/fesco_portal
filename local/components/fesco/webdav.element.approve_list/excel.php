<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 06.10.2014
 * Time: 11:52
 */
$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../../../..");
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
global $APPLICATION;
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

if ($_REQUEST['mode'] == 'download') {

    $elementId = intval($_REQUEST['id']);
    $bid = $_REQUEST['bid'];
    $iblockId = $_REQUEST['iblockId'];
    switch ($iblockId)
    {
        case 66:
            $type = "документа";
            break;
        case 67:
            $type = "договора";
            break;
        default:
            $type = "документа";
            break;
    }
    $bp = new CBPApproveDocs($iblockId, $elementId);
    $arResult = $bp->GetApproveListByBid($_REQUEST['bid']);
    date_default_timezone_set('Europe/Moscow');
    /** Error reporting */
    //error_reporting(E_ALL);
    //ini_set('display_errors', TRUE);
    //ini_set('display_startup_errors', TRUE);
    ini_set("mbstring.func_overload", 0);
    require_once $_SERVER["DOCUMENT_ROOT"] . '/local/classes/PHPExcel.php';


// Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

// Set document properties
    $objPHPExcel->getProperties()->setTitle("Лист согласования");

    $objPHPExcel->setActiveSheetIndex(0);
    $objWorkSheet = $objPHPExcel->getActiveSheet();
    $objWorkSheet->mergeCells('A3:E3')
        ->setCellValue('A3', 'Лист согласования')
        ->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('A3')->getFont()->setSize(16);
    $objWorkSheet->getStyle('A3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
    $objWorkSheet->getRowDimension('3')->setRowHeight(30);
    $title = html_entity_decode($bp->GetTitle());

    $objWorkSheet->mergeCells('A4:B4')->setCellValue('A4','Номер '.$type.':');
    $objWorkSheet->getStyle('A4:B4')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
    $objWorkSheet->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $objWorkSheet->getRowDimension('4')->setRowHeight(20);
    $objWorkSheet->mergeCells('C4:D4')->setCellValue('C4',$bp->GetDocNum());
    $objWorkSheet->getStyle('C4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
    $objWorkSheet->mergeCells('A5:B5')->setCellValue('A5','Наименование '.$type.':');
    $objWorkSheet->getStyle('A5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
    $objWorkSheet->getStyle('A5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $objWorkSheet->getStyle('A5:B5')->getFont()->setBold(true);
    $objWorkSheet->mergeCells('C5:E5')->setCellValue('C5', $title);
    $objWorkSheet->getStyle('C5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
    $objWorkSheet->getStyle('C5')->getAlignment()->setWrapText(true)->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
    $h = ceil(strlen($title) / 87);
    $h = $h == 0 ? 1 : $h;
    $objWorkSheet->getRowDimension('5')->setRowHeight(15*$h);
    $objWorkSheet
        ->setCellValueByColumnAndRow(0, 7, '№ п/п')
        ->setCellValueByColumnAndRow(1, 7, 'ФИО визирующего лица')
        ->setCellValueByColumnAndRow(2, 7, 'Дата комментария')
        ->setCellValueByColumnAndRow(3, 7, 'Комментарий')
        ->setCellValueByColumnAndRow(4, 7, 'Подпись');
    $objWorkSheet->getStyle('A7:E7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('BFBFBF');
    $objWorkSheet->getStyle('A7:E7')->getFont()->setBold(true);
//Autosize
    $objWorkSheet->getColumnDimension('B')->setAutoSize(true);
    $objWorkSheet->getColumnDimension('C')->setWidth(19);
// Поле комментарий
    $objWorkSheet->getColumnDimension('D')->setWidth(45);
    //Подпись
    $objWorkSheet->getColumnDimension('E')->setWidth(20);

//Данные
    if (count($arResult['ITEMS']) > 0) {
        $row = 8;
        $statusTemplate = "#STATUS# #COMMENT#";
        for($i = 0; $i < count($arResult['ITEMS']);$i++){
            $status = str_replace(
                array("#STATUS#","#COMMENT#"),
                array(
                    strlen(trim($arResult['ITEMS'][$i]['status_text'])) > 0 ? $arResult['ITEMS'][$i]['status_text'].".": '',
                    strlen(trim($arResult['ITEMS'][$i]['comment'])) > 0 ? "Комментарий: ".$arResult['ITEMS'][$i]['comment'] : ''),
                $statusTemplate);
            $status = html_entity_decode($status);
            $objWorkSheet->setCellValueByColumnAndRow(0, $row, $i + 1)
                ->setCellValueByColumnAndRow(1, $row, $arResult['ITEMS'][$i]['fio'])
                ->setCellValueByColumnAndRow(2, $row, $arResult['ITEMS'][$i]['date'])
                ->setCellValueByColumnAndRow(3, $row, $status)
                ->setCellValueByColumnAndRow(4, $row, '');
            if (strlen($status) < 44)
                $objWorkSheet->getRowDimension($row)->setRowHeight(30);

            $row++;
        }
    }

    $highestRow = $objWorkSheet->getHighestRow();
    //Border Style
    $styleArray = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => '0000'),
            ),
        ),
    );
    $objPHPExcel->getActiveSheet()->getStyle('A7:E'.$highestRow)->applyFromArray($styleArray);
    $highestRow += 2;
    $objWorkSheet->mergeCells('D'.$highestRow.':E'.$highestRow)->setCellValue('D'.$highestRow, 'Подпись инициатора   __________________    '.$bp->GetTargetUserName($bid));
    $objWorkSheet->getStyle('D'.$highestRow.':E'.$highestRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $objWorkSheet->getStyle('D6:D'.$highestRow)->getAlignment()->setWrapText(true);


// Set page orientation, size, Print Area and Fit To Pages

    $objPageSetup = new PHPExcel_Worksheet_PageSetup();
    $objPageSetup->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_LETTER);
    $objPageSetup->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
    $objPageSetup->setPrintArea("A1:E".$highestRow);
    $objPageSetup->setFitToWidth(1);
    $objWorkSheet->setPageSetup($objPageSetup);

// Rename worksheet
    $objWorkSheet->setTitle('Simple');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="approve_list.xlsx"');
    header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    //$APPLICATION->RestartBuffer();
    $objWriter->save('php://output');
}