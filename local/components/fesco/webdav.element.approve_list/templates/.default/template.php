<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 02.10.2014
 * Time: 14:13
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(count($arResult) <=0):?>
    <div class="ui warning message">
        Данные отсутствуют.
    </div>
<?else:?>
    <?foreach($arResult as $bid => $arItem):?>
    <div class="ui <?=$arResult[$bid]['INFO'][$bid]['cssClass']?> segment"
         xmlns="http://www.w3.org/1999/html">
        <h4 class="ui small header">Бизнес-процесс: <?=$arResult[$bid]['INFO']['TEMPLATE_NAME']?> <?=$arResult[$bid]['INFO']['ID']?></h4>
        <div class="ui form">
            <div class="inline field">
                <label>Дата начала: </label> <span><?=$arResult[$bid]['INFO']['STARTED']?></span>
            </div>
            <div class="inline field">
                <label>Статус:</label> <span><?=$arResult[$bid]['INFO']['STATE_TITLE']?></span>
            </div>
        </div>

        <table class="ui five column celled table segment">
            <thead>
            <tr>
                <th class="one wide">№ п/п</th>
                <th class="three wide">Ф.И.О.</th>
                <th class="two wide">Дата</th>
                <th class="three wide">Статус</th>
                <th>Комментарий</th>
                <th>Файл с правками</th>
            </tr>
            </thead>
            <tbody>
            <?$i=0;foreach($arItem['ITEMS'] as $item):?>
                <tr class="<?=$item['cssClass']?>">
                    <td><?=++$i?></td>
                    <td><?=$item['fio']?></td>
                    <td><?=$item['date']?></td>
                    <td>
                        <?if($item['status'] == 1 || $item['status'] == 5 || $item['status'] == 6):?><i class="icon checkmark"></i><?endif;?>
                        <?if($item['status'] == 2):?><i class="icon close"></i><?endif;?>
                        <?if($item['status'] == 4):?><i class="attention icon"></i><?endif;?>
                        <?=$item['status_text']?></td>
                    <td><? echo $item['comment']?></td>
                    <td>
                        <div class="ui list">
                            <?foreach($item['files'] as $file):?>
                              <a class="item" href="<?=$file['FILE']['PATH']?>"><?=$file['FILE']['NAME']?></a>
                            <?endforeach;?>
                        </div>
                    </td>
                </tr>
            <?endforeach;?>
            <tr>
                <td colspan="6">
                    <div class="ui blue labeled icon small button" data-bid="<?=$bid?>"><i class="save icon"></i>Скачать</div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
<?endforeach;?>
    <script type="text/javascript">
    $(document).ready(function(){
        $(".icon.button").click(function(){
            var elementId = '<?=$this->__component->arParams['ELEMENT_ID']?>';
            var iblockId = '<?=$this->__component->arParams['IBLOCK_ID']?>';

            var bid = $(this).attr('data-bid');
            window.location.href = "/local/components/fesco/webdav.element.approve_list/excel.php?mode=download&bid=" + bid +"&id=" + elementId + "&iblockId="+iblockId;
        });


    });
</script>
<?endif;