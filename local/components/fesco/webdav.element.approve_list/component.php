<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 02.10.2014
 * Time: 14:12
 */
 if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
\Bitrix\Main\Loader::includeModule('iblock');

$bp = new CBPApproveDocs($arParams['IBLOCK_ID'], $arParams['ELEMENT_ID']);
$arResult = $bp->GetApproveList();
$templateUrl = $this->__parent->arResult['URL_TEMPLATES']['webdav_bizproc_history_get'];
foreach($arResult as $key => $ITEMS)
{
  foreach($ITEMS['ITEMS'] as $id => $arItem)
  {
      $arResult[$key]['ITEMS'][$id]['files'] = $bp->GetHistoryDocuments($key, $arItem['user']);
  }
}
$this->IncludeComponentTemplate();
?>