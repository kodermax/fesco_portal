<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 15.10.2014
 * Time: 9:44
 */
$GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/panel/main/admin-public.css');
$this->SetViewTarget("sidebar", 10);
?>
<div class="sidebar-widget sidebar-widget-bizproc">
    <div class="sidebar-widget-top">
        <div class="sidebar-widget-top-title">Бизнес-процессы</div>
    </div>
    <div class="sidebar-widget-item-content">
        <div class="ui small icon red message" style="width:222px;!important;">
            <i class="tiny settings icon"></i>
            <div class="content">
                <div class="header">
                    Есть задания.
                </div>
                <p>Заданий к исполнению: <?=$arResult['TASKS_COUNT']?></p>
                <div class="sidebar-widget-item-footer">
                    <a href="/company/personal/bizproc/">Перейти к списку заданий</a>
                </div>
            </div>

        </div>

    </div>
</div>