<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 15.10.2014
 * Time: 9:44
 */
global $USER;
$tasksCount = CUserCounter::GetValue($USER->GetID(), 'bp_tasks');

$arResult["TASKS_COUNT"] = $tasksCount;
if ($arResult["TASKS_COUNT"] > 0)
    $this->IncludeComponentTemplate();