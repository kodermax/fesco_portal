<?

$arStatuses = CBPApproveDocs::GetApproveStatuses($arParams['IBLOCK_ID']);
$arResult['IS_ADMIN'] = $isAdmin = $GLOBALS['USER']->IsAdmin();
$arResult['IS_EDITOR'] = $bEditor = CIBlockSectionRights::UserHasRightTo($arParams['IBLOCK_ID'],$arResult['SECTION']['ID'],"section_delete");
if (in_array('PROPERTY_CONTRACTORS',$arParams['COLUMNS']))
{
    $bp = new CBPApproveDocs($arParams['IBLOCK_ID'], 0);
    foreach($arResult['DATA'] as $key => $arItem)
    {
        $bp->SetElementId($arItem['ID']);
        $arResult['DATA'][$key]['CONTRACTORS'] = $bp->GetContractorsText();
    }

}
foreach($arResult['GRID_DATA'] as $key => $arItem) {
    $index = 5;
    foreach($arItem['actions'] as $i => $item) {
       if($item['ICONCLASS'] == 'element_view') {
           $index = $i;
           break;
       }
    }
    if ($arItem['data']['TYPE'] == 'E') {
        $arResult['GRID_DATA'][$key]['columns']['NAME'] = "<div onclick=\"".$arItem["actions"][$index]['ONCLICK']."\"><a href=\"javascript:void(0);\">".$arItem["data"]["PROPERTY_DOC_NAME_VALUE"]."</a></div>";

        if (!$isAdmin && !$bEditor)
            unset($arResult['GRID_DATA'][$key]['actions']);

        if($arStatuses[$arItem['data']['PROPERTY_STATUS_ENUM_ID']] == 1)
        {
            $cssClass = 'positive';
        }
        else if($arStatuses[$arItem['data']['PROPERTY_STATUS_ENUM_ID']] == 2)
        {
            $cssClass = 'negative';
        }
        else if($arStatuses[$arItem['data']['PROPERTY_STATUS_ENUM_ID']] == 3) {
            $cssClass = 'info';
        }
        else if($arStatuses[$arItem['data']['PROPERTY_STATUS_ENUM_ID']] == 4) {
            $cssClass = 'warning';
        }
        $arResult['GRID_DATA'][$key]['columns']['PROPERTY_STATUS'] = "<div class='ui ".$cssClass." message'>".$arItem['data']['PROPERTY_STATUS_VALUE']."</div>";
        $arResult['GRID_DATA'][$key]['columns']['PROPERTY_CONTRACTORS'] = $arResult['DATA'][$arItem['data']['ID']]['CONTRACTORS'];
        $userId = $arResult['GRID_DATA'][$key]['data']['PROPERTY_RESPONSIBLE_USER'];
        $dbUser = CUser::GetById($userId);
        if ($arUser = $dbUser->Fetch()) {
            $fio = CUser::FormatName(COption::GetOptionString("bizproc", "name_template", CSite::GetNameFormat(false), SITE_ID), $arUser);
            $arResult['GRID_DATA'][$key]['columns']['PROPERTY_RESPONSIBLE_USER'] = $fio;
        }
    }
}

