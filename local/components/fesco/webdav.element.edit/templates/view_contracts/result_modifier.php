<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 27.10.2014
 * Time: 13:39
 */
\Bitrix\Main\Loader::includeModule('lists');
//$arResult["WRITEABLE"] = 'Y';
$arResult['BP_STARTED'] = false;
$arDocumentStates = CBPDocument::GetDocumentStates($arParams["DOCUMENT_TYPE"], $arParams["DOCUMENT_ID"]);
foreach ($arDocumentStates as $arState)
{
    if ($arState['WORKFLOW_STATUS'] == 3 || $arState['WORKFLOW_STATUS'] == 1)
    {
        $arResult['WORKFLOW_ID'] = $arState['ID'];
        $arState["IS_TARGET_USER"] = $arState['STARTED_BY'] == $GLOBALS['USER']->GetID() ? true : false;
        $arResult['BP_STARTED'] = true;
        if ($arState['STATE_TITLE'] == 'Согласование правок с КА')
        {
            $arState['URL_YES'] = $APPLICATION->GetCurPageParam(bitrix_sessid_get()."&edit=Y&workflowId=".$arState['ID']."&eventId=".$arState['STATE_PARAMETERS'][0]['NAME'], array());
            $arState['URL_NO'] = $APPLICATION->GetCurPageParam(bitrix_sessid_get()."&edit=Y&workflowId=".$arState['ID']."&eventId=".$arState['STATE_PARAMETERS'][1]['NAME'], array());
            $arState['IS_STATE_BUTTONS'] = true;
            $arResult["WRITEABLE"] = 'N';
        }
        elseif ($arState['STATE_TITLE'] == 'Подписание договора с КА'){
            $arState['URL_YES'] = $APPLICATION->GetCurPageParam(bitrix_sessid_get()."&edit=Y&workflowId=".$arState['ID']."&eventId=".$arState['STATE_PARAMETERS'][0]['NAME'], array());
            $arState['URL_NO'] = $APPLICATION->GetCurPageParam(bitrix_sessid_get()."&edit=Y&workflowId=".$arState['ID']."&eventId=".$arState['STATE_PARAMETERS'][1]['NAME'], array());
            $arState['IS_STATE_BUTTONS'] = true;
            $arResult["WRITEABLE"] = 'N';
        }
        elseif ($arState['STATE_TITLE'] == 'Регистрация договора'){
            $arState['URL_YES'] = $APPLICATION->GetCurPageParam(bitrix_sessid_get()."&edit=Y&workflowId=".$arState['ID']."&eventId=".$arState['STATE_PARAMETERS'][0]['NAME'], array());
            $arState['IS_STATE_BUTTONS'] = true;
        }
        else {
            $arResult["WRITEABLE"] = 'N';
        }
        $arResult["BP_STATE"] = $arState;
        break;
    }
}
if ($arResult["TASK_ID"] <= 0) {
    $arParams["USER_GROUPS"] = $GLOBALS["USER"]->GetUserGroupArray();
    $dbWorkflowTemplate = CBPWorkflowTemplateLoader::GetList(
        array(),
        array("DOCUMENT_TYPE" => $arParams["DOCUMENT_TYPE"], "ACTIVE" => "Y"),
        false,
        false,
        array("ID", "NAME", "DESCRIPTION", "MODIFIED", "USER_ID", "PARAMETERS")
    );
    while ($arWorkflowTemplate = $dbWorkflowTemplate->GetNext()) {

        if (!CBPDocument::CanUserOperateDocument(
            CBPCanUserOperateOperation::StartWorkflow,
            $GLOBALS["USER"]->GetID(),
            $arParams["DOCUMENT_ID"],
            array(
                "UserGroups" => $arParams["USER_GROUPS"],
                "DocumentStates" => $arDocumentStates,
                "WorkflowTemplateId" => $arWorkflowTemplate["ID"]))
        ):
            continue;
        endif;
        $arResult["TEMPLATES"][$arWorkflowTemplate["ID"]] = $arWorkflowTemplate;
        $arResult["TEMPLATES"][$arWorkflowTemplate["ID"]]["URL"] = $arResult['URL']['WEBDAV_START_BIZPROC'] .=
            "&workflow_template_id=" . $arWorkflowTemplate["ID"] . "&" . bitrix_sessid_get();
    }
    foreach ($arResult['TEMPLATES'] as $key => $arItem) {
        $arResult['BP_URL'] = $arItem['URL'];
        break;
    }
}

$actionUrl = CHTTP::urlAddParams(
    CHTTP::urlDeleteParams(
        $APPLICATION->GetCurPage(), array("id", "action", "sessid", "back_url")),
    array("sessid" => bitrix_sessid())
);
$arResult['URL']['BP_STOP'] = CHTTP::urlAddParams($actionUrl, array("id" => urlencode($arResult["WORKFLOW_ID"]), "action" => "stop_bizproc"));

//Родительский договор
if(intval($arResult['ELEMENT']['PROPERTY_PARENT_ID_VALUE']) > 0) {
    $rsElement = CIBlockElement::GetList(array(), array('ID' => $arResult['ELEMENT']['PROPERTY_PARENT_ID_VALUE'], 'IBLOCK_ID' => $linkIblockId), false, false, array('ID', 'NAME'));
    if ($arElement = $rsElement->Fetch()) {
        $arResult['ELEMENT']['PARENT'] = array('ID' => $arElement['ID'], 'NAME' => $arElement['NAME'], 'URL' => '/contracts/element/view/' . $arElement['ID'] . '/');
    }
}




#region Полномочия подписантов
if (in_array('PROPERTY_SIGNATORY', $arParams['COLUMNS'])) {

    $VALUES = array();
    $rsProperty = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $arResult['ELEMENT']['ID'], "sort", "asc", array("CODE" => "SIGNATORY"));
    while ($obProperty = $rsProperty->GetNext()) {
        if (!empty($obProperty['VALUE'])) {
            $arInfo = CFile::MakeFileArray($obProperty['VALUE']);
            $path = CFile::GetPath($obProperty['VALUE']);
            $arResult['ELEMENT']['SIGNATORY'][$obProperty['PROPERTY_VALUE_ID']] = array('ID' =>$obProperty['PROPERTY_VALUE_ID'], 'VALUE' => $obProperty['VALUE'], 'PATH' => $path,'INFO'=> $arInfo);
        }
    }
    $arResult['ELEMENT']['SIGNATORY']['n0'] = array('VALUE' => '');
}
#endregion
#region Скан копия
if (in_array('PROPERTY_DOC_SCAN', $arParams['COLUMNS'])) {
    if ($arResult['ELEMENT']['PROPERTY_DOC_SCAN_VALUE'] > 0) {
        $arInfo = CFile::MakeFileArray($arResult['ELEMENT']['PROPERTY_DOC_SCAN_VALUE']);
        $arResult['ELEMENT']['DOC_SCAN'][$arResult['ELEMENT']['PROPERTY_DOC_SCAN_VALUE_ID']] = array(
            'PATH' => CFile::GetPath($arResult['ELEMENT']['PROPERTY_DOC_SCAN_VALUE']),
            'VALUE' => $arResult['ELEMENT']['PROPERTY_DOC_SCAN_VALUE'],
            'INFO' => $arInfo
        );
    }
    else {
        $arResult['ELEMENT']['DOC_SCAN']['n0'] = array('VALUE' => '');
    }
}
#endregion
#region Список валют
if (in_array('PROPERTY_CURRENCY', $arParams['COLUMNS'])) {
    $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "CODE" => "CURRENCY"));
    while ($enum_fields = $property_enums->GetNext()) {
        if(!empty($enum_fields['VALUE']))
            $arResult['CURRENCY_LIST'][$enum_fields['ID']] = $enum_fields['VALUE'];
    }
}
#endregion
if(in_array('PROPERTY_RESPONSIBLE_USER',$arParams['COLUMNS'])) {
//Автор документа
    $arResponsible = array();
    $cUser = new CUser;
    $sort_by = "ID";
    $sort_ord = "ASC";
    $arFilter = array(
        "ID" => $arResult['ELEMENT']['PROPERTY_RESPONSIBLE_USER_VALUE']
    );
    $dbUsers = $cUser->GetList($sort_by, $sort_ord, $arFilter);
    if ($arUser = $dbUsers->Fetch()) {
        $arResult['ELEMENT']['RESPONSIBLE_USER'] = array(
            'NAME' => $arUser['NAME'],
            'LAST_NAME' => $arUser['LAST_NAME'],
            'FULL_NAME' => $arUser['NAME'] . ' ' . $arUser['LAST_NAME']
        );

    }
}

\Bitrix\Main\Loader::includeModule('currency');
$total = $arResult['ELEMENT']['PROPERTY_RATE_VALUE'] * $arResult['ELEMENT']['PROPERTY_PRICE_VALUE'];
$arResult['ELEMENT']['TOTAL_SUM'] = CCurrencyLang::CurrencyFormat($total, "RUB", true);

//Тип договора
$bp = new CBPApproveDocs($arResult['ELEMENT']['IBLOCK_ID'], $arResult['ELEMENT']['ID']);
$arResult['DOC_TYPE'] = $bp->GetTypeContract();
?>