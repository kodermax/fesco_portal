<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 20.10.2014
 * Time: 10:01
 */
$arResult['BP_STARTED'] = false;
$arDocumentStates = CBPDocument::GetDocumentStates($arParams["DOCUMENT_TYPE"], $arParams["DOCUMENT_ID"]);
foreach ($arDocumentStates as $arState)
{
    if ($arState['WORKFLOW_STATUS'] == 3 || $arState['WORKFLOW_STATUS'] == 1)
    {
        $arResult['WORKFLOW_ID'] = $arState['ID'];
        $arResult['BP_STARTED'] = true;
        break;
    }
}
if ($arResult["TASK_ID"] <= 0) {
    $arParams["USER_GROUPS"] = $GLOBALS["USER"]->GetUserGroupArray();
    $dbWorkflowTemplate = CBPWorkflowTemplateLoader::GetList(
        array(),
        array("DOCUMENT_TYPE" => $arParams["DOCUMENT_TYPE"], "ACTIVE" => "Y"),
        false,
        false,
        array("ID", "NAME", "DESCRIPTION", "MODIFIED", "USER_ID", "PARAMETERS")
    );
    while ($arWorkflowTemplate = $dbWorkflowTemplate->GetNext()) {

        if (!CBPDocument::CanUserOperateDocument(
            CBPCanUserOperateOperation::StartWorkflow,
            $GLOBALS["USER"]->GetID(),
            $arParams["DOCUMENT_ID"],
            array(
                "UserGroups" => $arParams["USER_GROUPS"],
                "DocumentStates" => $arDocumentStates,
                "WorkflowTemplateId" => $arWorkflowTemplate["ID"]))
        ):
            continue;
        endif;
        $arResult["TEMPLATES"][$arWorkflowTemplate["ID"]] = $arWorkflowTemplate;
        $arResult["TEMPLATES"][$arWorkflowTemplate["ID"]]["URL"] = $arResult['URL']['WEBDAV_START_BIZPROC'] .=
            "&workflow_template_id=" . $arWorkflowTemplate["ID"] . "&" . bitrix_sessid_get();
    }
    foreach ($arResult['TEMPLATES'] as $key => $arItem) {
        $arResult['BP_URL'] = $arItem['URL'];
        break;
    }
}
if ($arResult['BP_STARTED'])
    $arResult["WRITEABLE"] = 'N';
$actionUrl = CHTTP::urlAddParams(
    CHTTP::urlDeleteParams(
        $APPLICATION->GetCurPage(), array("id", "action", "sessid", "back_url")),
    array("sessid" => bitrix_sessid())
);
$arResult['URL']['BP_STOP'] = CHTTP::urlAddParams($actionUrl, array("id" => urlencode($arResult["WORKFLOW_ID"]), "action" => "stop_bizproc"));
$arResult['STATUSES'] = CBPApproveDocs::GetApproveStatuses($arParams['IBLOCK_ID']);

if(in_array('PROPERTY_RESPONSIBLE_USER',$arParams['COLUMNS'])) {
//Автор документа
    $arResponsible = array();
    $dbUsers = CUser::GetList(($by = "id"), ($order = "desc"), array("ID" => $arResult['ELEMENT']['PROPERTY_RESPONSIBLE_USER_VALUE']), array('SELECT' => array('NAME','LAST_NAME')));
    if ($arUser = $dbUsers->Fetch()) {
        $arResult['ELEMENT']['RESPONSIBLE_USER'] = array(
            'NAME' => $arUser['NAME'],
            'LAST_NAME' => $arUser['LAST_NAME'],
            'FULL_NAME' => $arUser['NAME'] . ' ' . $arUser['LAST_NAME']
        );
    }
}