<?
\Bitrix\Main\Loader::includeModule('iblock');
$list = CIBlockElement::GetList(array(),array('SHOW_NEW'=>'Y','CHECK_PERMISSIONS'=>'N','IBLOCK_ID' => $arParams['IBLOCK_ID'],'ID' => $arParams['DOC_ID']),false,false,array('PREVIEW_TEXT'));
if($row = $list->GetNext())
{
    $arResult['PREVIEW_TEXT']  = $row['~PREVIEW_TEXT'];
}

$list = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $arParams['DOC_ID'], "sort", "asc", array("CODE" => "DOC_NAME"));
if ($row = $list->GetNext()) {
    $arResult['DOCUMENT_NAME'] = $row['VALUE'];
}
$list = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $arParams['DOC_ID'], "sort", "asc", array("CODE" => "DOC_ID"));
if ($row = $list->GetNext()) {
    $arResult['DOC_ID'] = $row['VALUE'];
}
$list = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $arParams['DOC_ID'], "sort", "asc", array("CODE" => "DOC_DATE"));
if ($row = $list->GetNext()) {
    $arResult['DOC_DATE'] = $row['VALUE'];
}
$list = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $arParams['DOC_ID'], "sort", "asc", array("CODE" => "PRICE"));
if ($row = $list->GetNext()) {
    $arResult['PRICE'] = $row['VALUE'];
}
$arCurrency = array();
$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], "CODE"=>"CURRENCY"));
while($enum_fields = $property_enums->GetNext())
{
    $arCurrency[$enum_fields["ID"]] = $enum_fields["VALUE"];
}
$list = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $arParams['DOC_ID'], "sort", "asc", array("CODE" => "CURRENCY"));
if ($row = $list->GetNext()) {
    $arResult['CURRENCY'] = $arCurrency[$row['VALUE']];
}
$list = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $arParams['DOC_ID'], "sort", "asc", array("CODE" => "RATE"));
if ($row = $list->GetNext()) {
    $arResult['RATE'] = $row['VALUE'];
}
$list = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $arParams['DOC_ID'], "sort", "asc", array("CODE" => "PROLONGATION"));
if ($row = $list->GetNext()) {
    $arResult['PROLONGATION'] = $row['VALUE'];
}
//Полномочия
$VALUES = array();
$rsProperty = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $arParams['DOC_ID'], "sort", "asc", array("CODE" => "SIGNATORY"));
while ($obProperty = $rsProperty->GetNext()) {
    if (!empty($obProperty['VALUE'])) {
        $arInfo = CFile::MakeFileArray($obProperty['VALUE']);
        $path = CFile::GetPath($obProperty['VALUE']);
        $arResult['SIGNATORY'][$obProperty['PROPERTY_VALUE_ID']] = array('ID' =>$obProperty['PROPERTY_VALUE_ID'], 'VALUE' => $obProperty['VALUE'], 'PATH' => $path,'INFO'=> $arInfo);
    }
}
$arContractors = array();
$rsProperty = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $arParams['DOC_ID'], "sort", "asc", array("CODE" => "CONTRAGENTS"));
while ($obProperty = $rsProperty->GetNext()) {
    $linkIblockId = $obProperty['LINK_IBLOCK_ID'];
    if (!empty($obProperty['VALUE']))
        $arContractors[] = $obProperty['VALUE'];
}
$client = new \Bitrix\Main\Web\HttpClient();
foreach ($arContractors as $item) {
    $result = $client->get('https://'.$_SERVER['SERVER_NAME'].'/app/api/Contragents?xmlId='. $item);
    $response = json_decode($result, true);
    $arResult['CONTRAGENTS'][] = array('ID' => $item, 'NAME' => $response['Title'], 'URL' => '');
}

//Родительский договор
$list = CIBlockElement::GetProperty($arParams['IBLOCK_ID'], $arParams['DOC_ID'], "sort", "asc", array("CODE" => "PARENT_ID"));
if ($row = $list->GetNext()) {
    $arResult['PARENT_ID'] = $row['VALUE'];
}
if ($arResult['PARENT_ID'] > 0) {
    $rsElement = CIBlockElement::GetList(array(), array('ID' => $arResult['PARENT_ID'], 'IBLOCK_ID' => $arParams['IBLOCK_ID']), false, false, array('ID', 'NAME'));
    if ($arElement = $rsElement->Fetch()) {
        $arResult['PARENT'] = array('ID' => $arElement['ID'], 'NAME' => $arElement['NAME'], 'URL' => '/contracts/element/view/' . $arElement['ID'] . '/');
    }
}

\Bitrix\Main\Loader::includeModule('currency');
$arResult['TOTAL_SUM'] = CCurrencyLang::CurrencyFormat($arResult['RATE'] * $arResult['PRICE'], "RUB", true);


//Тип договора
$bp = new CBPApproveDocs($arParams['IBLOCK_ID'], $arParams['DOC_ID']);
$arResult['DOC_TYPE'] = $bp->GetTypeContract();
$templateUrl = $arParams['webdav_bizproc_history_get'];;
$arResult["URL"] = array("DOWNLOAD" => CComponentEngine::MakePathFromTemplate($templateUrl,
    array("ELEMENT_ID" => $arParams['DOC_ID'], "ID" => $arParams['DOC_ID'])));
?>