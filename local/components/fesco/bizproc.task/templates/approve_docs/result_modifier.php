<?
if ($arParams['DOC_ID'] > 0) {
    \Bitrix\Main\Loader::includeModule('iblock');

    $iblockId = $arParams['IBLOCK_ID'];
    $list = CIBlockElement::GetProperty($iblockId, $arParams['DOC_ID'], "sort", "asc", array("CODE" => "DOC_NAME"));
    if ($row = $list->GetNext()) {
        $arResult['DOCUMENT_NAME'] = $row['VALUE'];
    }
    $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => $iblockId, "CODE" => "DOC_TYPE"));
    while ($enum_fields = $property_enums->GetNext()) {
        if (!empty($enum_fields['VALUE']))
            $arResult['TYPE_LIST'][$enum_fields['ID']] = $enum_fields['VALUE'];
    }
    $list = CIBlockElement::GetProperty($iblockId, $arParams['DOC_ID'], "sort", "asc", array("CODE" => "DOC_TYPE"));
    if ($row = $list->GetNext()) {
        $arResult['DOC_TYPE'] = $arResult['TYPE_LIST'][$row['VALUE']];
    }
    $arContractors = array();
    $rsProperty = CIBlockElement::GetProperty($iblockId, $arParams['DOC_ID'], "sort", "asc", array("CODE" => "CONTRACTORS"));
    while ($obProperty = $rsProperty->GetNext()) {
        $linkIblockId = $obProperty['LINK_IBLOCK_ID'];
        if (!empty($obProperty['VALUE']))
            $arContractors[] = $obProperty['VALUE'];
    }
    foreach ($arContractors as $item) {
        $rsElement = CIBlockElement::GetList(array(), array('ID' => $item, 'IBLOCK_ID' => $linkIblockId), false, false, array('NAME'));
        if ($arElement = $rsElement->Fetch()) {
            $arResult['CONTRACTORS'][] = array('ID' => $item, 'NAME' => $arElement['NAME']);
        }
    }
    $rsProperty = CIBlockElement::GetProperty($iblockId, $arParams['DOC_ID'], "sort", "asc", array("CODE" => "RESPONSIBLE_USER"));
    if ($obProperty = $rsProperty->GetNext()) {
        $dbUser = CUser::GetById($obProperty['VALUE']);
        if ($arUser = $dbUser->Fetch()) {
            $arResult['AUTHOR']['ID'] = $arUser['ID'];
            $arResult['AUTHOR']['NAME'] = CUser::FormatName(COption::GetOptionString("bizproc", "name_template", CSite::GetNameFormat(false), SITE_ID), $arUser);
            $arResult['AUTHOR']['URL'] = "/company/personal/user/" . $arUser['ID'] . "/";
        }
    }
    $templateUrl = $this->__component->__parent->arResult['URL_TEMPLATES']['webdav_bizproc_history_get'];;
    $arResult["URL"] = array("DOWNLOAD" => CComponentEngine::MakePathFromTemplate($templateUrl,
        array("ELEMENT_ID" => $arParams['DOC_ID'], "ID" => $arParams['DOC_ID'])));
}
?>

