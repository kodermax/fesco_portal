<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

?>
<div class="ui form segment">
<div class="ui form basic segment">
    <div class="inline field">
        <b>Наименование документа:</b>
        <span><?=$arResult['DOCUMENT_NAME']?></span>
    </div>
    <div class="inline field">
        <b>Тип документа:</b>
        <span><?=$arResult['DOC_TYPE']?></span>
    </div>
    <div class="inline field">
        <b>Область применения:</b>
        <span>
            <ul>
            <?foreach($arResult['CONTRACTORS'] as $item):?>
                <li><a href=""><?=$item['NAME']?></a></li>
            <?endforeach;?>
            </ul>
        </span>
    </div>
   <div class="inline field">
        <b>Автор документа:</b>
       <?php $anchor_id = RandString(16); ?>
        <span id="anchor_created_<?php echo $anchor_id; ?>"><a target="_blank" href="<?=$arResult["AUTHOR"]['URL']?>"><?=$arResult['AUTHOR']['NAME']?></a></span>
       <script>
           BX.tooltip(<?php echo $arResult['AUTHOR']["ID"]; ?>, "anchor_created_<?php echo $anchor_id; ?>", "", 'intrantet-user-selector-tooltip');
       </script>
    </div>
    <div class="inline field">
        <a target="_blank" class="ui mini button" href="<?=$arResult['URL']['DOWNLOAD']?>">Открыть документ</a>
    </div>
</div>


    <?
    if (!empty($arResult["ERROR_MESSAGE"])):
        ShowError($arResult["ERROR_MESSAGE"]);
    endif;

    if ($arResult["ShowMode"] == "Success"):
        ?>
            <div class="bizproc-item-text bizproc-task-success">
                <div class="ui compact positive message">
                    <p><?=GetMessage("BPATL_SUCCESS")?></p>
                </div>

            </div>
    <?
    else:
        ?>
        <form method="post" name="task_form1" action="<?=POST_FORM_ACTION_URI?>" enctype="multipart/form-data">
            <input type="hidden" name="action" value="doTask" />
            <input type="hidden" name="id" value="<?= intval($arResult["TASK"]["ID"]) ?>" />
            <input type="hidden" name="workflow_id" value="<?= htmlspecialcharsbx($arResult["TASK"]["WORKFLOW_ID"]) ?>" />
            <input type="hidden" name="back_url" value="<?= htmlspecialcharsbx($arParams["REDIRECT_URL"]) ?>" />
            <?= bitrix_sessid_post() ?>
        <div class="ui left floated form basic segment">
            <div class="field" style="margin-bottom: 10px;">
                <a id="wd_end_edit" class="ui mini button" href="javascript:void(0);" style="text-transform: none"><i class="upload disk icon"></i> Загрузить файл с правками </a>
                <?if(count($arResult['HISTORY_FILES']) > 0):?>
                    <table class="ui celled table segment">
                        <thead>
                            <tr>
                                <th>Наименование</th>
                                <th>Дата</th>
                                <th>Размер</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?foreach($arResult["HISTORY_FILES"] as $arItem):?>
                                <tr>
                                    <td><a href="<?=$arItem['FILE']['PATH']?>"><?=$arItem['FILE']['NAME']?></a></td>
                                    <td><?=$arItem['MODIFIED']?></td>
                                    <td><?=$arItem['FILE']['FILE_SIZE']?></td>
                                </tr>
                            <?endforeach;?>
                    </tbody>
                    </table>
                <?endif;?>
            </div>
            <div class="field">
                <label>Комментарий:</label>
                <textarea style="display:inline-block;" rows="3" cols="50" id="task_comment" name="task_comment"></textarea>
                <div class="ui red pointing prompt label transition commentError" style="display: none">Укажите комментарий</div>
            </div>
            <div class="inline field">
                <label>Делегировать пользователю:</label>
                <script type="text/javascript">
                    function onChangeUser(arUser){
                        arUser.forEach(function (element, index, array) {
                            $("#USER_ID").val(index);
                        });
                    }
                    $(document).ready(function() {
                        $("#USER_INPUT").click(function(){
                            $("#USER_selector_content").show();
                        });
                    });
                    $(document).on('click', '.ui.form.segment', function(e) {
                        if (e.target !== $("#USER_INPUT").get(0)
                            && $(e.target).closest('#USER_selector_content').size() <= 0) {
                            $("#USER_selector_content").hide();
                        }
                    });
                </script>
                <div class="ui left icon input">
                    <input id="USER_INPUT" name="USER_INPUT" placeholder="Выберите пользователя" type="text" style="width: 250px;">
                    <input type="hidden" id="USER_ID" name="USER_ID" value="">
                    <i class="user icon"></i>
                </div>
                <input type="submit" value="Делегировать" name="delegate" class="delegate ui mini button">
                <div class="ui red pointing prompt label transition delegateError" style="display: none">Укажите пользователя</div>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:intranet.user.selector.new",
                    "",
                    array(
                        'NAME' => "USER",
                        "MULTIPLE" => "N",
                        'POPUP' => 'Y',
                        'ON_CHANGE' => 'onChangeUser',
                        'INPUT_NAME' => "USER_INPUT",
                        'INPUT_NAME_STRING' => "USER_STRING",
                        'INPUT_NAME_SUSPICIOUS' => "USER_SUSPICIOUS",
                        "SITE_ID" => SITE_ID,
                    ),
                    null,
                    array());?>

            </div>

            <div class="field">
                <div class="ui text menu" style="padding-top:30px;">
                    <?=$arResult["TaskFormButtons"]?>
                </div>
            </div>
        </div>

        </form>
        <script type="text/javascript">
            var editMode = true;
            var WDFileUpload_my = function()
            {

                fileDownloadDone = false;
                if (!editMode)
                {
                    fileDownloadDone = true;
                }

                var wait = BX.showWait();
                var uploadDialog = null;
                BX.ajax.get("<?=CUtil::JSEscape(WDAddPageParams($arResult["ELEMENT"]["URL"]["UPLOAD"], array("use_light_view" => "Y", "close_after_upload" => "Y", "update_document" => $arParams["DOC_ID"]), false))?>", null, function(data) {
                    BX.closeWait(null, wait);
                    uploadDialog = new BX.CDialog({"content": data || '&nbsp', "width":650 , "height":150 });
                    // disable window events
                    uploadDialog.WDUploaded = false;
                    uploadDialog.WDUpdate = true;
                    editMode = false;
                    // reenable if required
                    BX.addCustomEvent(uploadDialog, 'onBeforeWindowClose', function() {
                        if (!(uploadDialog.WDUploaded))
                        {
                            editMode = true;
                        }
                    });
                    //show dialog
                    uploadDialog.Show();
                });
            };
            BX.bind(BX('wd_end_edit'), 'click', function() { WDFileUpload_my()});
            $(document).ready(function(){
                $("#tab_approve_edit_table").find(".bx-field-value").removeClass('bx-field-value');
                $(".commentCheck").click(function(){
                   var text = $("#task_comment").val();
                   var commentError = $(".commentError");
                    if (!text || text.length <= 0)
                    {
                        commentError.parent().addClass('error');
                        commentError.addClass('visible');
                        return false;
                    }
                    else {
                        commentError.parent().removeClass('error');
                        commentError.removeClass('visible');
                        return true;
                    }
                });
               $(".delegate").click(function(){
                   var id = parseInt($("#USER_ID").val());
                   if(id <= 0 || isNaN(id)){
                       var delegateError = $(".delegateError");
                       delegateError.parent().addClass('error');
                       delegateError.addClass('visible');
                       return false;
                   }
               });
            });
        </script>
    <?
    endif;
    ?>
</div>