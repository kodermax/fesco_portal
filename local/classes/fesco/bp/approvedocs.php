<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 01.10.2014
 * Time: 11:58
 */

/**
 * Class CBPApproveDocs
 * Класс помогающей в работе с бизнес-процессом Согласование документов
 */
class CBPApproveDocs
{
    private $arStatusMessage = array(
        1 => 'согласовал #DOCUMENT_NAME#',
        2 => 'отклонил #DOCUMENT_NAME#',
        3 => 'делегировал задание "Согласование #DOCUMENT_NAME# пользователю #DELEGATE_USER#',
        4 => 'отправил на доработку #DOCUMENT_NAME#',
        5 => 'автоматически согласовал #DOCUMENT_NAME#'
    );
    private $_iblockId = 0;
    private $_elementId = 0;
    private $_typeText = "";

    /**
     * @param $iblockId
     * @param $elementId
     */
    function __construct($iblockId, $elementId, $typeId = 1)
    {
        if ($typeId == 2)
            $this->_typeText = "договор";
        else
            $this->_typeText = "документ";

        $this->_iblockId = $iblockId;
        $this->_elementId = $elementId;
        \Bitrix\Main\Loader::IncludeModule('iblock');
        \Bitrix\Main\Loader::IncludeModule('webdav');
        \Bitrix\Main\Loader::IncludeModule('bizproc');
    }
    public function SetElementId($elementId){
        if (intval($elementId) > 0)
            $this->_elementId = intval($elementId);
    }

    static function AddWorkingDays($count) {
        $date              = date( 'd-m-Y' );
        $day_week          = date( 'N', strtotime( $date ) );
        $day_count         = $count + $day_week;
        $week_count        = floor($day_count/5);
        $holiday_count     = ( $day_count % 5 > 0 ) ? 0 : 2;
        $week_day          = $week_count * 7 - $day_week + ( $day_count % 5 ) - $holiday_count;
        $date_end          = date( "d-m-Y", strtotime( $date . " + $week_day day " ) );
        $date_end_count    = date( 'N', strtotime( $date_end ) );
        $holiday_shift     = $date_end_count > 5 ? 7 - $date_end_count + 1 : 0;
        return strtotime($date_end . " + $holiday_shift day ");
    }
    public function GetDocApprovers()
    {
        $arUsers = array();
        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$this->_iblockId, "CODE"=>"DOC_TYPE"));
        while($enum_fields = $property_enums->GetNext())
        {
            $arStatuses[$enum_fields["ID"]] = $enum_fields["XML_ID"];
        }

        $list = CIBlockElement::GetProperty($this->_iblockId,$this->_elementId,"sort","asc",array('CODE' => 'DOC_TYPE'));
        if($row = $list->GetNext())
        {
            if ($row['VALUE_XML_ID'] == 'orders') {
                //$arUsers[] = "user_4787";
            }
            if ($row['VALUE_XML_ID'] == 'regulations')
                $arUsers[] = 'user_4814';
        }
        $arUsers[] = "user_4787";
        $arUsers[] = "user_6963";
        return $arUsers;
    }
    public function GetDocApproversName()
    {
        $arUsers = $this->GetDocApprovers();
        $arResult = array();
        foreach($arUsers as $user)
        {
            $userId = substr($user, 5, strlen($user));
            $rsUser = CUser::GetByID($userId);
            $arUser = $rsUser->Fetch();
            if (is_array($arUser))
                $arResult[] = array('NAME' => $arUser['NAME']. ' '. $arUser['LAST_NAME']);
        }
        return $arResult;
    }
    /*
     * Получить список согласующих для договоров
     */
    public function GetContractApprovers($bBp = true)
    {
        $arUsers = array();
        $rsGroups = CGroup::GetList ($by = "c_sort", $order = "asc", Array ("STRING_ID" => 'APPROVERS_CONTRACTS'));
        $arGroup = $rsGroups->Fetch();
        $groupId = $arGroup['ID'];
        if ($groupId > 0) {
            $arFilter = array("ACTIVE" => "Y");
            $arFilter["GROUPS_ID"] = $groupId;
            $by = 'ID';
            $order = 'ASC';
            $dbUsersList = CUser::GetList($by, $order, $arFilter);
            while ($arUser = $dbUsersList->Fetch())
                $arUsers[] = $bBp ? "user_".$arUser["ID"] : $arUser['ID'];
        }
        if ($this->GetTypeContract() == 2) {
            $rsGroups = CGroup::GetList($by = "c_sort", $order = "asc", Array("STRING_ID" => 'TCO'));
            $arGroup = $rsGroups->Fetch();
            $groupId = $arGroup['ID'];
            if ($groupId > 0) {
                $arFilter = array("ACTIVE" => "Y");
                $arFilter["GROUPS_ID"] = $groupId;
                $by = 'ID';
                $order = 'ASC';
                $dbUsersList = CUser::GetList($by, $order, $arFilter);
                while ($arUser = $dbUsersList->Fetch())
                    $arUsers[] = $bBp ? "user_".$arUser["ID"] : $arUser['ID'];
            }
        }
        return $arUsers;
    }

    static function GetApproveStatuses($iblockId)
    {
        \Bitrix\Main\Loader::includeModule('iblock');
        $arStatuses = array();
        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$iblockId, "CODE"=>"STATUS"));
        while($enum_fields = $property_enums->GetNext())
        {
            $arStatuses[$enum_fields["ID"]] = $enum_fields["XML_ID"];
        }
        return $arStatuses;
    }
    static function GetApproveXmlStatuses($iblockId)
    {
        \Bitrix\Main\Loader::includeModule('iblock');
        $arStatuses = array();
        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$iblockId, "CODE"=>"STATUS"));
        while($enum_fields = $property_enums->GetNext())
        {
            $arStatuses[$enum_fields["XML_ID"]] = $enum_fields["ID"];
        }
        return $arStatuses;
    }
    public function GetApproversBD($bBp = true)
    {
        $arUsers = array();
        $rsGroups = CGroup::GetList($by = "c_sort", $order = "asc", Array("STRING_ID" => 'APPROVERS_BD'));
        $arGroup = $rsGroups->Fetch();
        $groupId = $arGroup['ID'];
        if ($groupId > 0) {
            $arFilter = array("ACTIVE" => "Y");
            $arFilter["GROUPS_ID"] = $groupId;
            $by = 'ID';
            $order = 'ASC';
            $dbUsersList = CUser::GetList($by, $order, $arFilter);
            while ($arUser = $dbUsersList->Fetch()) {
                $arUsers[] = $bBp ? "user_".$arUser['ID'] : $arUser['ID'];
            }
        }
        return $arUsers;
    }
    public function GetApproversLD($bBp = true)
    {
        $arUsers = array();
        $rsGroups = CGroup::GetList($by = "c_sort", $order = "asc", Array("STRING_ID" => 'APPROVERS_LD'));
        $arGroup = $rsGroups->Fetch();
        $groupId = $arGroup['ID'];
        if ($groupId > 0) {
            $arFilter = array("ACTIVE" => "Y");
            $arFilter["GROUPS_ID"] = $groupId;
            $by = 'ID';
            $order = 'ASC';
            $dbUsersList = CUser::GetList($by, $order, $arFilter);
            while ($arUser = $dbUsersList->Fetch()) {
                $arUsers[] = $bBp ? "user_".$arUser['ID'] : $arUser['ID'];
            }
        }
        return $arUsers;
    }
    public function AddApproversToContract($bid)
    {
        $arValues = array();
        $res = CIBlockElement::GetProperty($this->_iblockId, $this->_elementId, "sort", "asc", array("CODE" => "APPROVE_LIST"));
        while ($ob = $res->GetNext()) {
            if (count($ob['VALUE']) > 0)
                $arValues[] = $ob['VALUE'];
        }
        //Разбиваем по бизнес-процессам
        $arResult = array();
        if (count($arValues) > 0) {
            foreach ($arValues as $item) {
                $arResult[$item['bid']][] = $item;
            }
        }
        $arUsers = $this->GetContractApprovers(false);
        foreach ($arUsers as $userId) {
            $dbUser = CUser::GetById($userId);
            if ($arUser = $dbUser->Fetch()) {
                $fio = CUser::FormatName(COption::GetOptionString("bizproc", "name_template", CSite::GetNameFormat(false), SITE_ID), $arUser);
                $arResult[$bid][] = array(
                    'bid' => $bid,
                    'fio' => $fio,
                    'date' => '',
                    'status' => 0,
                    'comment' => '',
                    'user' => $userId,
                    'delegate_user' => 0,
                    'stage' => 'approving'
                );
            }
        }
        /*
        $arUsers = $this->GetApproversBD(false);
        foreach ($arUsers as $userId) {
            $dbUser = CUser::GetById($userId);
            if ($arUser = $dbUser->Fetch()) {
                $fio = CUser::FormatName(COption::GetOptionString("bizproc", "name_template", CSite::GetNameFormat(false), SITE_ID), $arUser);
                $arResult[$bid][] = array(
                    'bid' => $bid,
                    'fio' => $fio,
                    'date' => '',
                    'status' => 0,
                    'comment' => '',
                    'user' => $userId,
                    'delegate_user' => 0,
                    'stage' => 'approving_bd'
                );
            }
        }
        $arUsers = $this->GetApproversLD(false);
        foreach ($arUsers as $userId) {
            $dbUser = CUser::GetById($userId);
            if ($arUser = $dbUser->Fetch()) {
                $fio = CUser::FormatName(COption::GetOptionString("bizproc", "name_template", CSite::GetNameFormat(false), SITE_ID), $arUser);
                $arResult[$bid][] = array(
                    'bid' => $bid,
                    'fio' => $fio,
                    'date' => '',
                    'status' => 0,
                    'comment' => '',
                    'user' => $userId,
                    'delegate_user' => 0,
                    'stage' => 'approving_ld'
                );
            }
        }
            */
        $arFields = array();
        foreach ($arResult as $bid => $arItems) {
            foreach ($arItems as $item) {
                $arFields[] = array('VALUE' => $item);
            }
        }
        CIBlockElement::SetPropertyValuesEx($this->_elementId, $this->_iblockId, array('APPROVE_LIST' => $arFields));
    }
    public function AddAllApproversToList($arApprovers, $bid, $stage = '')
    {
        $arUsers = array();
        foreach ($arApprovers as $item) {
            if (substr($item, 0, strlen("user_")) == "user_") {
                $userId = intval(substr($item, strlen("user_")));
                if ($userId > 0)
                    $arUsers[] = $userId;
            }
        }
        $arValues = array();
        $res = CIBlockElement::GetProperty($this->_iblockId, $this->_elementId, "sort", "asc", array("CODE" => "APPROVE_LIST"));
        while ($ob = $res->GetNext()) {
            if (count($ob['VALUE']) > 0)
                $arValues[] = $ob['VALUE'];
        }
        //Разбиваем по бизнес-процессам
        $arResult = array();
        if (count($arValues) > 0) {

            foreach ($arValues as $item) {
                $arResult[$item['bid']][] = $item;
            }
        }
        foreach ($arUsers as $userId) {
            $dbUser = CUser::GetById($userId);
            if ($arUser = $dbUser->Fetch()) {
                $fio = CUser::FormatName(COption::GetOptionString("bizproc", "name_template", CSite::GetNameFormat(false), SITE_ID), $arUser);
                $arResult[$bid][] = array(
                    'bid' => $bid,
                    'fio' => $fio,
                    'date' => '',
                    'status' => 0,
                    'comment' => '',
                    'user' => $userId,
                    'delegate_user' => 0,
                    'stage' => $stage
                );
            }
        }

        $arFields = array();
        foreach ($arResult as $bid => $arItems) {
            foreach ($arItems as $item) {
                $arFields[] = array('VALUE' => $item);
            }
        }
        CIBlockElement::SetPropertyValuesEx($this->_elementId, $this->_iblockId, array('APPROVE_LIST' => $arFields));
    }
    static public function GetTaskIdByDocId($docId)
    {

        $dbTask = CBPTaskService::GetList(
            array(),
            array("USER_ID" => $GLOBALS['USER']->GetID(),'USER_STATUS' => CBPTaskUserStatus::Waiting),
            false,
            false,
            array("ID", "WORKFLOW_ID", "ACTIVITY", "ACTIVITY_NAME", "MODIFIED", "OVERDUE_DATE", "NAME", "DESCRIPTION", "PARAMETERS")
        );
        while ($arTask = $dbTask->GetNext()) {
            if (intval($arTask['PARAMETERS']['DOCUMENT_ID'][2]) == $docId) {
                return $arTask['ID'];
            }
        }
        return 0;
    }
    public function GetContractorsText()
    {
        $rsProperty = CIBlockElement::GetProperty($this->_iblockId, $this->_elementId, "sort", "asc", array("CODE" => "CONTRACTORS"));
        while ($arProperty = $rsProperty->GetNext()) {
            if (!empty($arProperty['VALUE']))
                $arContractors[] = $arProperty["VALUE"];
        }
        $result = '';
        foreach ($arContractors as $item) {

            $rs = CIBlockElement::GetList(array(), array('ID' => $item), false, false, array('NAME'));
            if ($ar = $rs->GetNext())
                $result .= html_entity_decode($ar['NAME']) . " ";
        }
        return $result;
    }
    static public function GetHistoryGetUrl($elementId, $iblockId, $templateUrl)
    {
        $list = CIBlockElement::GetList(array(), array('ID' => $elementId, 'IBLOCK_ID' => $iblockId, 'SHOW_NEW' => 'Y'), false, false, array('NAME'));
        if ($row = $list->GetNext()) {
            $name = $row['NAME'];
            return CComponentEngine::MakePathFromTemplate($templateUrl,
                array("ELEMENT_ID" => $elementId, "ID" => $elementId, "ELEMENT_NAME" => $name));
        }
        return "";

    }
    static public function GetFullName($user)
    {
        $userId = intval($user);
        if (substr($user, 0, strlen("user_")) == "user_")
            $userId = intval(substr($user, strlen("user_")));

        $dbUser = CUser::GetById($userId);
        if ($arUser = $dbUser->Fetch())
            return CUser::FormatName(COption::GetOptionString("bizproc", "name_template", CSite::GetNameFormat(false), SITE_ID), $arUser);
        return '';
    }

    public function GetTypeContract()
    {
        $offshore = $this->IsOffshore();
        $aff = $this->IsContractFirstType();
        if ($aff == 1)
           return 1;
        if ($offshore == 1)
            return 2;
        return 3;
    }
    public function IsOffshore()
    {
        $arContractors = array();
        $rsProperty = CIBlockElement::GetProperty($this->_iblockId, $this->_elementId, "sort", "asc", array("CODE" => "CONTRAGENTS"));
        while ($arProperty = $rsProperty->GetNext()) {
            $arContractors[] = $arProperty['VALUE'];
        }
        $cnt = 0;
        $client = new \Bitrix\Main\Web\HttpClient();
        foreach($arContractors as $item)
        {
            $response = $client->get('https://' . $_SERVER['SERVER_NAME'] .'/app/api/Contragents?guid='.$item);
            $response = json_decode($response, true);
            if ($response['Offshore'])
                $cnt++;
        }
        return ($cnt > 1) ? 1 : 0;
    }
    public function IsContractFirstType()
    {
        $arContractors = array();
        $rsProperty = CIBlockElement::GetProperty($this->_iblockId, $this->_elementId, "sort", "asc", array("CODE" => "CONTRAGENTS"));
        while ($arProperty = $rsProperty->GetNext()) {
            $arContractors[] = $arProperty['VALUE'];
        }

        $cnt = 0;
        $client = new \Bitrix\Main\Web\HttpClient();
        foreach($arContractors as $item)
        {
            $response = $client->get('https://' . $_SERVER['SERVER_NAME'] .'/app/api/Contragents?guid='.$item);
            $response = json_decode($response, true);
            if ($response['Fesco'])
                $cnt++;
        }
        return ($cnt > 1) ? 1 : 0;

    }
    public function GetTargetUserName($bid)
    {
        $arState = CBPStateService::GetWorkflowState($bid);
        if($arState['STARTED_BY'] > 0)
        {
            $dbUser = CUser::GetById($arState['STARTED_BY']);
            if ($arUser = $dbUser->Fetch()) {
                return CUser::FormatName("#LAST_NAME# #NAME_SHORT# #SECOND_NAME_SHORT#", $arUser);
            }
        }
        return "";
    }
    public function GetDocNum()
    {
        $res = CIBlockElement::GetProperty($this->_iblockId, $this->_elementId, "sort", "asc", array("CODE" => "DOC_ID"));
        if ($ob = $res->GetNext()) {
            return $ob['VALUE'];
        }
        return '';
    }
    public function GetTitle()
    {
        $res = CIBlockElement::GetProperty($this->_iblockId, $this->_elementId, "sort", "asc", array("CODE" => "DOC_NAME"));
        if ($ob = $res->GetNext()) {
            return $ob['VALUE'];
        }
        return '';
    }
    public function GetApproveList()
    {
        $arValues = array();
        $res = CIBlockElement::GetProperty($this->_iblockId, $this->_elementId, "sort", "asc", array("CODE" => "APPROVE_LIST"));
        while ($ob = $res->GetNext()) {
            if (is_array($ob['VALUE']))
                $arValues[] = $ob['VALUE'];
        }

        //Разбиваем по бизнес-процессам
        $arResult = array();
        foreach ($arValues as &$item) {
            if ($item['delegate_user'] > 0) {
                $dbUser = CUser::GetById($item['delegate_user']);
                if ($arUser = $dbUser->Fetch()) {
                    $item['delegateUser'] = CUser::FormatName(COption::GetOptionString("bizproc", "name_template", CSite::GetNameFormat(false), SITE_ID), $arUser);
                }
            }
            //$item['comment'] = $item['comment'];
            switch ($item['status']) {
                case 1:
                    $item['cssClass'] = 'positive';
                    $item['status_text'] = 'Согласовано';
                    break;
                case 2:
                    $item['cssClass'] = 'negative';
                    $item['status_text'] = 'Отклонил';
                    break;
                case 3:
                    $item['status_text'] = 'Делегировал пользователю ' . $item['delegateUser'];
                    break;
                case 4:
                    $item['cssClass'] = 'warning';
                    $item['status_text'] = 'Отправил на доработку';
                    break;
                case 5:
                    $item['status_text'] = 'Согласование просрочено';
                    $item['cssClass'] = 'warning';
                    break;
                case 6:
                    $item['cssClass'] = 'positive';
                    $item['status_text'] = 'Шаг не применим';
                    break;
                case 0:
                    $item['status_text'] = '';
                    break;

            }
            $arResult[$item['bid']]['ITEMS'][] = $item;
        }
        foreach ($arResult as $bid => $arItems) {
            $documentId = array(
                0 => 'webdav',
                1 => 'CIBlockDocumentWebdav',
                2 => $this->_elementId);
            $arInfo = CBPDocument::GetDocumentState($documentId, $bid);
            if (empty($arInfo)) {
              //  unset($arResult[$bid]);
                continue;
            }

            switch (trim($arInfo[$bid]['STATE_TITLE'])) {
                case 'Согласован':
                    $arInfo[$bid]['cssClass'] = 'green';
                    break;
                case 'Отклонен':
                    $arInfo[$bid]['cssClass'] = 'red';
                    break;
            }
            $arResult[$bid]['INFO'] = $arInfo[$bid];
        }
        //Сортировка бизнес-процессов
        //Сортировка массива
        function cmp_bp($a, $b)
        {

            $a = MakeTimeStamp($a['INFO']['STARTED'], "YYYY-MM-DD HH:MI:SS");
            $b = MakeTimeStamp($b['INFO']['STARTED'], "YYYY-MM-DD HH:MI:SS");
            if ($a == $b) {
                return 0;
            }
            return ($a > $b) ? -1 : 1;
        }
        uasort($arResult, 'cmp_bp');
        return $arResult;
    }
    public function GetApproveListByBid($bid)
    {
        $arResult = $this->GetApproveList();
        return $arResult[$bid];
    }
    public function SendEvent($arParameters, $status)
    {
        $documentName = '';
        $starterUserEmail = "mkarpychev@fesco.com";
        $starterUserName = "";
        $arFilter = array(
            "ACTIVE" => "",
            'IBLOCK_ID' => $arParameters['IBLOCK_ID'],
            'ID' => $arParameters['DOC_ID'],
            'CHECK_PERMISSIONS' => 'N',
            "SHOW_HISTORY" => 'N',
            'SHOW_NEW' => 'Y'
        );
        $res = CIBlockElement::GetList(array(), $arFilter, false, false, array('NAME', 'PROPERTY_DOC_NAME'));
        if ($arRes = $res->GetNext()) {
            if (strlen($arRes['PROPERTY_DOC_NAME_VALUE']) > 0)
                $documentName = $arRes['PROPERTY_DOC_NAME_VALUE'];
            else
                $documentName = $arRes['NAME'];
        }

        $dbUser = CUser::GetById($arParameters['STARTED_BY']);
        if ($arUser = $dbUser->Fetch()) {
            $starterUserEmail = $arUser['EMAIL'];
            $starterUserName = CUser::FormatName(COption::GetOptionString("bizproc", "name_template", CSite::GetNameFormat(false), SITE_ID), $arUser);
        }
        $dbUser = CUser::GetById($arParameters['USER_ID']);
        if ($arUser = $dbUser->Fetch()) {
            $targetUserName = CUser::FormatName(COption::GetOptionString("bizproc", "name_template", CSite::GetNameFormat(false), SITE_ID), $arUser);
        }
        if ($arParameters['DELEGATE_USER_ID'] > 0)
        {
            $dbUser = CUser::GetById($arParameters['DELEGATE_USER_ID']);
            if ($arUser = $dbUser->Fetch()) {
                $delegateUserEmail = $arUser['EMAIL'];
            }
        }

        $rsSites = CSite::GetByID(SITE_ID);
        $arSite = $rsSites->Fetch();
        $siteName = $arSite['SITE_NAME'];
        $statusTitle = str_replace(
            array('#DOCUMENT_NAME#', '#DELEGATE_USER#'),
            array($this->_typeText . ' "' . $documentName . '"', $arParameters['DELEGATE_USER']),
            $this->arStatusMessage[$status]);
        if ($status != 3) {
            $message = '
                Информационное сообщение портала #SITE_NAME#<br>
                ----------------------------------------------------------------------
                <br><br>
                Пользователь <b>#USER# #STATUS_TITLE#</b><br>
                #COMMENT#
                <br><br>
                <a href="#URL#">Перейти к документу</a>
                                <br><br>
                Это письмо сформировано автоматически.';
            $message = str_replace(
                array("#SITE_NAME#", "#USER#", "#STATUS_TITLE#", "#COMMENT#", "#URL#"),
                array($siteName,
                    $targetUserName,
                    $statusTitle,
                    strlen($arParameters['COMMENT']) > 0 ? "Комментарий: " . $arParameters['COMMENT'] : "",
                    "http://" . SITE_SERVER_NAME . $arParameters['URL']),
                $message
            );
            $receiver = $starterUserEmail;
        } else {
            $message = '
                        Информационное сообщение портала #SITE_NAME#<br>
                        ----------------------------------------------------------------------
                        <br><br>
                        Пользователь <b>#USER#</b> делегировал вам задание: "Согласование #DOCUMENT_NAME#".<br>
                        <b>Инициатор задания:</b> #TARGET_USER#<br>
                        <span style="color:#FF0000"><b>Срок исполнения задания 3 дня.</b></span>
                        <br>
                        <br>
                        <a href="#URL#">Перейти к списку заданий</a>
                        <br><br>
                        Это письмо сформировано автоматически.';
            $message = str_replace(
                array("#SITE_NAME#",
                    "#USER#",
                    "#DOCUMENT_NAME#",
                    "#TARGET_USER#",
                    "#URL#"),
                array($siteName,
                    $targetUserName,
                    $this->_typeText . 'а "' . $documentName . '"',
                    $starterUserName,
                    "http://" . SITE_SERVER_NAME . "/company/personal/bizproc/"),
                $message
            );
            $receiver = $delegateUserEmail;
        }


        $arEventFields = array(
            "SENDER" => "btx@fesco.com",
            "RECEIVER" => $receiver,
            "TITLE" => "Согласование документа: Изменение документа",
            "MESSAGE" => $message
        );
        CEvent::Send("BIZPROC_MAIL_TEMPLATE", SITE_ID, $arEventFields);
    }
    private function array_insert(&$array, $position, $insert_array)
    {
        $first_array = array_splice($array, 0, $position);
        $array = array_merge($first_array, $insert_array, $array);
    }
       //Сортировка массива
    private function cmp($a, $b)
    {

        $a = MakeTimeStamp($a['date'], "DD.MM.YYYY HH:MI:SS");
        $b = MakeTimeStamp($b['date'], "DD.MM.YYYY HH:MI:SS");
        if ($a == $b) {
            return 0;
        }
        return ($a < $b) ? -1 : 1;
    }
    /**
     * Функция добавляет запись в таблицу согласования
     * @param $bid - id БП
     * @param $fio - ФИО
     * @param $status - Статуст согласованности
     * @param $comment - Комментарий
     */
    public function AddToLog($bid, $status, $comment, $userId = 0, $delegateUser = 0, $stage)
    {
        $arValues = array();
        $res = CIBlockElement::GetProperty($this->_iblockId, $this->_elementId, "sort", "asc", array("CODE" => "APPROVE_LIST"));
        while ($ob = $res->GetNext()) {
            if (count($ob['VALUE']) > 0) {
                $ob['~VALUE']['comment'] = htmlspecialchars_decode($ob['~VALUE']['comment']);
                $arValues[] = $ob['~VALUE'];
            }
        }
        //Разбиваем по бизнес-процессам
        $arResult = array();
        if (count($arValues) > 0) {
            foreach ($arValues as $item) {
                $arResult[$item['bid']][] = $item;
            }
        }
        foreach ($arResult[$bid] as $key => $arItem) {
            if ($arItem['user'] == $userId && $stage == $arItem['stage']) {
                $arResult[$bid][$key]['date'] = date('d.m.Y H:i:s',time());
                $arResult[$bid][$key]['status'] = $status;
                $arResult[$bid][$key]['comment'] = $comment;
                $arResult[$bid][$key]['delegate_user'] = $delegateUser;
                if ($delegateUser > 0) {
                    $dbUser = CUser::GetById($delegateUser);
                    if ($arUser = $dbUser->Fetch()) {
                        $fio = CUser::FormatName(COption::GetOptionString("bizproc", "name_template", CSite::GetNameFormat(false), SITE_ID), $arUser);
                        array_push($arResult[$bid], array(
                            'bid' => $bid,
                            'fio' => $fio,
                            'date' => '',
                            'status' => 0,
                            'comment' => '',
                            'user' => $delegateUser,
                            'delegate_user' => 0,
                            'stage' => $stage
                        ));
                    }
                }
            }
        }

        foreach ($arResult as $key => &$arItem) {
            uasort($arItem,  array($this, 'cmp'));
        }
        $arFields = array();
        foreach ($arResult as $bid => $arItems) {
            foreach ($arItems as $item) {
                $arFields[] = array('VALUE' => $item);
            }
        }

        CIBlockElement::SetPropertyValuesEx($this->_elementId, $this->_iblockId, array('APPROVE_LIST' => $arFields));
    }

    public function GetHistoryDocuments($bid, $userId)
    {
        $arResult = array();
        $arParams["NAME_TEMPLATE"] = COption::GetOptionString("bizproc", "name_template", CSite::GetNameFormat(false), SITE_ID);
        //Файл смотрим в элементе сначала
        $arSelect = Array("IBLOCK_ID","TIMESTAMP_X", "PROPERTY_FILE","PROPERTY_WORKFLOW_ID", "MODIFIED_BY");
        $arFilter = Array("=PROPERTY_WORKFLOW_ID" => $bid, "=MODIFIED_BY" => $userId, "IBLOCK_ID"=>$this->_iblockId,"=ID" => $this->_elementId, "SHOW_NEW" => "Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

        if($arFields = $res->GetNext())
        {
            $FILE_ID = IntVal($arFields["PROPERTY_FILE_VALUE"]);
            if ($FILE_ID > 0) {
                $rsFile = CFile::GetByID($FILE_ID);
                $arFile = $rsFile->Fetch();
                $res = array();
                $res['FILE'] = array(
                    'NAME' => $arFile['FILE_NAME'],
                    'PATH' => CFile::GetPath($FILE_ID),
                    'FILE_SIZE' => $arFile['FILE_SIZE']
                );
                $res['MODIFIED'] = $arFields['TIMESTAMP_X'];
                $arResult[] = $res;
            }
        }

        $history = new CBPHistoryService();
        $db_res = $history->GetHistoryList(
            array(strtoupper("modified") => strtoupper("desc")),
            array("DOCUMENT_ID" =>  array(
                                    0 => 'webdav',
                                    1 => 'CIBlockDocumentWebdav',
                                    2 => $this->_elementId)),
            false,
            false,
            array("ID", "DOCUMENT_ID", "NAME", "MODIFIED", "USER_ID", "USER_NAME", "USER_LAST_NAME", "USER_LOGIN", "DOCUMENT", "USER_SECOND_NAME")
        );
        if ($db_res) {
            while ($res = $db_res->GetNext())
            {
                if ($res['DOCUMENT']['FIELDS']['MODIFIED_BY'] == $userId && $res['DOCUMENT']['PROPERTIES']['WORKFLOW_ID']['VALUE'] == $bid) {
                    $arFile = CFile::MakeFileArray($res['DOCUMENT']['PROPERTIES']['FILE']["VALUE"]);
                    $res['FILE'] = array(
                        'NAME' => $arFile['name'],
                        'FILE_SIZE' => $arFile['size'],
                        'PATH' => $res['DOCUMENT']['PROPERTIES']['FILE']["VALUE"]
                    );
                    $arResult[] = $res;
                }
            }
        }
        return $arResult;
    }
}