<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 21.11.2014
 * Time: 11:28
 */

/*
 * Класс помогает работать с инфоблоком Авто
 */
class CFESCOIBlockAuto
{
    private $_requestsIbId = 0;
    private $_carsIbId = 0;
    private $_arStatus = array();
    private $_arStatusIds = array();
    private $_groupId = 0;
    function __construct()
    {
        \Bitrix\Main\loader::includeModule('iblock');
        $list = CIBlock::GetList(Array(), Array('ACTIVE'=>'Y',"CODE"=>'auto_requests'), true);
        if($row = $list->Fetch())
            $this->_requestsIbId = $row['ID'];
        $list = CIBlock::GetList(Array(), Array('ACTIVE'=>'Y',"CODE"=>'cars'), true);
        if($row = $list->Fetch())
            $this->_carsIbId = $row['ID'];
        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$this->_requestsIbId, "CODE"=>"STATUS"));
        while($enum_fields = $property_enums->GetNext())
        {
            $this->_arStatus[$enum_fields['XML_ID']] = $enum_fields['ID'];
            $this->_arStatusIds[$enum_fields['ID']] = $enum_fields['XML_ID'];
        }
        $rsGroups = CGroup::GetList($by = "c_sort", $order = "asc", array("STRING_ID" => 'admin_auto'));
        if ($arGroups = $rsGroups->Fetch()) {
            $this->_groupId = $arGroups['ID'];
        }
    }

    /*
     * Возвращаем массив статусов
     * [XML_ID] = ID
     */
    public function GetStatusEnumIdByXmlId($xmlId)
    {
        return $this->_arStatus[$xmlId];
    }
    /*
     * Возвращаем массив статусов
     * [ID] = XML_ID
     */
    public function GetStatusXmlIdById($id)
    {
        return $this->_arStatusIds[$id];
    }

    /*
     * Возвращает ID группы администраторов автомашин
     */
    public function GetAdminGroupId()
    {
        return $this->_groupId;
    }
    /*
     * Функция проверяет является ли текущий пользователь администратором Авто
     */
    public function isAdmin()
    {
        return in_array($this->_groupId,$GLOBALS['USER']->GetUserGroupArray());
    }
    /*
     * Функция возвращает ID инфоблока с заявками
     */
    public function GetRequestsIBlockId()
    {
       return $this->_requestsIbId;
    }
    /*
     *  Функция возвращает ID инфоблока справочника машин
     */
    public function GetCarsIBlockId()
    {
        return $this->_carsIbId;
    }
    /*
     * Возвращает массив информации о пользователе
     */
    public function GetUserInfoArray($userId)
    {
        $arUser = array();
        if ($userId > 0) {
            $rsUser = CUser::GetByID($userId);
            $arUser = $rsUser->Fetch();
            $arUser = array(
                'Id' => $arUser['ID'],
                'id' => $arUser['ID'],
                'text' => CUser::FormatName('#LAST_NAME# #NAME_SHORT# #SECOND_NAME_SHORT#', $arUser),
                'Title' => CUser::FormatName('#LAST_NAME# #NAME_SHORT# #SECOND_NAME_SHORT#', $arUser)
            );
        }
        return $arUser;
    }
    /*
     * Функция возвращает массив информации о машине
     */
    public function GetCarInfoArray($carId)
    {
        $arResult = array();
        if($carId > 0) {
            $list = CIBlockElement::GetList(array(), array('ID' => $carId, 'IBLOCK_ID' => $this->_carsIbId), false, false, array('ID','PREVIEW_PICTURE','NAME'));
            if ($row = $list->GetNext()) {
                $arResult = array(
                    'Id' => $row['ID'],
                    'Title' => $row['~NAME'],
                    'Image' => CFile::GetPath($row['PREVIEW_PICTURE'])
                );
            }
        }
        return $arResult;
    }
    /*
     * Функция возвращает массив данных о компании
     */
    public function GetCompanyInfoArray($companyId)
    {
        if (strlen($companyId) > 0) {
            $client = new Bitrix\Main\Web\HttpClient();
            $response = $client->get("https://".$_SERVER['SERVER_NAME']."/app/api/Contragents?guid=".$companyId);
            $response = json_decode($response, true);
            return $response;
        }
        return array();
    }
    /*
     * Функция возвращает ID компании пользователя
     */
    public function GetCompanyIdByUser($userId)
    {
        $companyId = 0;
        if ($userId > 0) {
            $rsUser = CUser::GetList($by, $order, array('ID' => $userId), array('SELECT' => array('UF_CONTRAGENT')));
            if ($arUser = $rsUser->GetNext()) {
                $client = new \Bitrix\Main\Web\HttpClient();
                $result = $client->get('https://'.$_SERVER['SERVER_NAME'].'/app/api/Contragents?id=' . $arUser['UF_CONTRAGENT']);
                $result = json_decode($result, true);
                return $result['Id'];
            }
        }
        return $companyId;
    }

    /*
     * Возвращает пользователя в формате
     * Иванов И.И.
     */
    public function GetUserShortName($userId)
    {
        $result = '';
        if ($userId > 0) {
            $rsUser = CUser::GetByID($userId);
            $arUser = $rsUser->Fetch();
            return CUser::FormatName("#LAST_NAME# #NAME_SHORT# #SECOND_NAME_SHORT#", $arUser);
        }
        return $result;
    }
    /*
     * Возвращает админов компании
     */
    public function GetAdminByCompanyId($companyId)
    {
        $arResult = array();
        if ($companyId > 0) {
            $groupId = $this->GetAdminGroupId();
            $rsUser = CUser::GetList(
                $by,
                $order,
                array("GROUPS_ID" => array($groupId),'ACTIVE'=>'Y', 'UF_CONTRAGENT' => $companyId), array("SELECT" => array("ID",'EMAIL')));
            while ($arUser = $rsUser->GetNext())
            {
                $arResult[] = $arUser;
            }

        }
        return $arResult;

    }
    /*
     * Получить информацию о пользователя заявки
     */
    public function GetUserInfoByRequest($requestId)
    {
        $list = CIBlockElement::GetList(array(), array('ID' => $requestId),false,false,array('IBLOCK_ID',"PROPERTY_USER_ID"));
        if($row = $list->GetNext())
        {
            $userId = $row['PROPERTY_USER_ID_VALUE'];
            if ($userId > 0)
            {
                $rsUser = CUser::GetList($by, $order, array('ID' => $userId), array('SELECT' => array('ID','EMAIL')));
                if ($arUser = $rsUser->GetNext()) {
                    return $arUser;
                }
            }
        }
        return array();
    }
    /*
     * Возвращает ссылки
     */
    public function GetTemplateUrl($Id = 0)
    {
        $rsSites = CSite::GetByID("s1");
        $arSite = $rsSites->Fetch();
        return array(
            'requests_manage.list' => "http://".$arSite['SERVER_NAME']."/app/dist/#/auto/requests_manage/calendar"
        );
    }
}