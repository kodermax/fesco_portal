<?
/*
 * Класс помогает работать с инфоблоком Командировки
 */
class CFESCOIBlockTrips
{
    public $iblockId = 0;
    function __construct(){
        \Bitrix\Main\loader::includeModule('iblock');
        $list = CIBlock::GetList(Array(), Array('ACTIVE'=>'Y',"CODE"=>'trips'), true);
        if($row = $list->Fetch())
            $this->iblockId = $row['ID'];
    }
    /*
     * Возвращает пользователя в формате
     * Иванов И.И.
     */
    public function GetUserShortName($userId){
        $result = '';
        if ($userId > 0) {
            $rsUser = CUser::GetByID($userId);
            $arUser = $rsUser->Fetch();
            return CUser::FormatName("#LAST_NAME# #NAME_SHORT# #SECOND_NAME_SHORT#", $arUser);
        }
        return $result;
    }
    /*
     * Возвращает массив типов транспорта
     */
    function GetTransportType(){
        $arResult = array();
        $list = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$this->iblockId, "CODE"=>"TRANSPORT"));
        while($row = $list->GetNext())
        {
            $arResult[$row['ID']] = array(
                'id' => $row['ID'],
                'label' => $row['VALUE']
            );
        }
        return $arResult;
    }
    /*
     * Возвращает ID утвердительного значения VISA
     */
    function GetVisaId(){
        $list = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$this->iblockId, "CODE"=>"VISA"));
        while($row = $list->GetNext())
        {
            return $row['ID'];
        }
        return 0;
    }
    /*
     * Возвращает ID утвердительного значения HOTEL
     */
    function GetHotelId()
    {
        $list = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$this->iblockId, "CODE"=>"HOTEL"));
        while($row = $list->GetNext())
        {
            return $row['ID'];
        }
        return 0;
    }
    /*
     * Возвращает ID утвердительного значения HOTEL
     */
    function GetTransferId()
    {
        $list = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$this->iblockId, "CODE"=>"TRANSFER"));
        while($row = $list->GetNext())
        {
            return $row['ID'];
        }
        return 0;
    }
    /*
     * Возвращает ENUM_ID по XML_ILD
     */
    function GetTransportId($xmlId)
    {
        $list = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$this->iblockId, "CODE"=>"TRANSPORT", "XML_ID" => $xmlId));
        while($row = $list->GetNext())
        {
            return $row['ID'];
        }
        return 0;
    }
    /*
     * Возвращает статусы в формате Array('XML_ID' => 'ID')
     */
    function GetStatuses()
    {
        $arResult = array();
        $list = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$this->iblockId, "CODE"=>"STATUS"));
        while($row = $list->GetNext())
        {
            $arResult[$row['XML_ID']] = $row['ID'];
        }
        return $arResult;
    }
    /*
     * Возвращает статусы в формате Array('ID' => 'XML_ID')
     */
    function GetStatusesXml()
    {
        $arResult = array();
        $list = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$this->iblockId, "CODE"=>"STATUS"));
        while($row = $list->GetNext())
        {
            $arResult[$row['ID']] = $row['XML_ID'];
        }
        return $arResult;
    }
    /*
     * Возвращает ID шаблона БП
     */
    public function GetTemplateId($docId)
    {
        \Bitrix\Main\Loader::includeModule('bizproc');
        $dbWorkflowTemplate = CBPWorkflowTemplateLoader::GetList(
            array(),
            array("DOCUMENT_TYPE" => array("iblock", "CIBlockDocument", "iblock_".$this->iblockId), "ACTIVE"=>"Y"),
            false,
            false,
            array("ID", "NAME", "DESCRIPTION", "MODIFIED", "USER_ID", "AUTO_EXECUTE", "USER_NAME", "USER_LAST_NAME", "USER_LOGIN", "USER_SECOND_NAME")
        );
        while ($arWorkflowTemplate = $dbWorkflowTemplate->GetNext())
        {
            return $arWorkflowTemplate['ID'];
        }
        return 0;
    }
    public function isAdmin()
    {
        if ($GLOBALS['USER']->IsAdmin())
            return true;
        $rsGroups = CGroup::GetList(($by="c_sort"), ($order="desc"), array('STRING_ID' => 'admin_trips'));
        if($arGroup = $rsGroups->Fetch())
        {
            $groupId = $arGroup['ID'];
            if($groupId > 0)
            {
                $arGroups = $GLOBALS['USER']->GetUserGroupArray();
                if (in_array($groupId, $arGroups))
                    return true;
            }
        }
        return false;
    }
}
?>