<?php
/**
 * Created by PhpStorm.
 * User: Mkarpychev
 * Date: 28.11.2014
 * Time: 15:08
 */

class CFESCOTools {
    function __construct()
    {

    }
    static function GUID()
    {
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }
    static function AddWorkingDays($date, $count) {
        $date              = date( 'd-m-Y', $date );
        $day_week          = date( 'N', strtotime( $date ) );
        $day_count         = $count + $day_week;
        $week_count        = floor($day_count/5);
        $holiday_count     = ( $day_count % 5 > 0 ) ? 0 : 2;
        $week_day          = $week_count * 7 - $day_week + ( $day_count % 5 ) - $holiday_count;
        $date_end          = date( "d-m-Y", strtotime( $date . " + $week_day day " ) );
        $date_end_count    = date( 'N', strtotime( $date_end ) );
        $holiday_shift     = $date_end_count > 5 ? 7 - $date_end_count + 1 : 0;
        $result = strtotime($date_end . " + $holiday_shift day ");
        $result = date('Y-m-d', $result);
        return $result;
    }
    static function getWorkingDays($dateFrom, $dateTo) {
        $holidays = COption::GetOptionString('calendar', 'year_holidays');
        $holidays = explode(',',$holidays);
        $holidays2 = $holidays;
        foreach($holidays as &$holiday)
        {
            $holiday .= ".".date('Y');
        }
        foreach($holidays2 as &$holiday)
        {
            $holiday .= ".".(date('Y') + 1);
        }
        $holidays = array_merge($holidays, $holidays2);
        $dFrom = new DateTime($dateFrom);
        $dTo = new DateTime($dateTo);
        if ($dTo->getTimestamp() < $dFrom->getTimestamp())
            return 0;
        $diff = $dFrom->diff($dTo)->days;
        $wd = date('w',$dFrom->getTimestamp());
        $no_full_weeks = floor($diff / 7);
        $workingDays = $no_full_weeks * 5;
        $no_remaining_days = $diff % 7;
        $the_first_day_of_week = date("N", $dFrom->getTimestamp());
        $the_last_day_of_week = date("N", $dTo->getTimestamp());
        if ($the_first_day_of_week <= $the_last_day_of_week) {
            if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
            if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
        }
        else {
            if ($the_first_day_of_week == 7) {
                $no_remaining_days--;
                if ($the_last_day_of_week == 6) {
                    $no_remaining_days--;
                }
            }
            else {
                $no_remaining_days -= 2;
            }
        }
        if ($no_remaining_days > 0 )
        {
            $workingDays += $no_remaining_days;
        }
        foreach($holidays as $holiday){
            $date = explode('.',$holiday);
            $time_stamp = mktime(0,0,0,$date[1], $date[0],$date[2]);
            //If the holiday doesn't fall in weekend
            if ($dFrom->getTimestamp() <= $time_stamp && $time_stamp <= $dTo->getTimestamp() && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
                $workingDays--;
        }
        return $workingDays;
    }
}