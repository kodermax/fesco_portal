<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="ui list">
	<?if(is_array($arResult["item"])):
	foreach($arResult["item"] as $arItem):?>
		<div class="item">
			<?if(strlen($arItem["enclosure"]["url"])>0):?>
				<img class="preview_img" src="<?=$arItem["enclosure"]["url"]?>" alt="<?=$arItem["enclosure"]["url"]?>" /><br />
			<?endif;?>
			<?if(strlen($arItem["pubDate"])>0):?>
				<b><span style="font-size: 12px;color:gray"><?=CIBlockRSS::XMLDate2Dec($arItem["pubDate"], FORMAT_DATE)?></span></b>
			<?endif;?>
			<br/>
			<?if(strlen($arItem["link"])>0):?>
			<b><a style="font-size:13px;" target="_blank" href="<?=$arItem["link"]?>"><?=$arItem["title"]?></a></b>
			<?else:?>
				<?=$arItem["title"]?>
			<?endif;?>
			<p style="font-size:12px;">
				<?echo $arItem["description"];?>
			</p>
	</div>
	<?endforeach;
	endif;?>
</div>
