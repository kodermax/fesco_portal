<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 15.10.2014
 * Time: 17:38
 */
$sTabName =  'tab_approve';
$taskId = $this->__component->arResult['TASK_ID'];
if ($taskId > 0) {
    ob_start();
    $APPLICATION->IncludeComponent(
        "fesco:bizproc.task",
        "approve_docs",
        Array(
            "TASK_ID" => $taskId,
            "IBLOCK_ID" => $this->__component->arParams['IBLOCK_ID'],
            "DOC_ID" => $this->__component->arResult['VARIABLES']['ELEMENT_ID']
    ),
        $this->__component
    );
    $html = ob_get_clean();
    $this->__component->arResult['TABS'][] =
        array("id" => $sTabName,
            "name" => "Cогласование",
            "title" => "Согласование",
            "fields" => array(
                array(
                    "id" => "IBLIST_BP",
                    "name" => "Согласование",
                    "colspan" => true,
                    "type" => "custom",
                    "value" => $html
                )
            )
        );
}