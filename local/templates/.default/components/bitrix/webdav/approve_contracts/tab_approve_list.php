<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 01.10.2014
 * Time: 17:52
 */

$sTabName =  'tab_approve_list';
ob_start();
 $APPLICATION->IncludeComponent(
    "fesco:webdav.element.approve_list",
    ".default",
    Array(
        "IBLOCK_ID" => $this->__component->arParams['IBLOCK_ID'],
        "ELEMENT_ID" => $this->__component->arResult['VARIABLES']['ELEMENT_ID']
    ),
    false
);
$html = ob_get_clean();
$this->__component->arResult['TABS'][] =
    array( "id" => $sTabName,
        "name" => "Лист согласования",
        "title" => "Лист согласования",
        "fields" => array(
            array(
                "id" => "IBLIST_BP",
                "name" => "Лист согласования",
                "colspan" => true,
                "type" => "custom",
                "value" => $html
            )
        )
    );
?>