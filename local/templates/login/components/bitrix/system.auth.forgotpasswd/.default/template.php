<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?

$forgetLogin = isset($_REQUEST["forgot_login"]) && $_REQUEST["forgot_login"] == "yes" ? true : false;

if ($forgetLogin)
	$APPLICATION->IncludeComponent("bitrix:bitrix24.auth.forgotlogin", "", array());
else
{
?>
	<form name="form_auth" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
		<div class="log-popup-header"><?=$APPLICATION->GetTitle();?></div>
		<hr class="b_line_gray">
		<?ShowMessage($arParams["~AUTH_RESULT"]);?>
            <ul class="errors" style="color:red;list-style-type: none"></ul>
            <div class="success" style="color:green;font-weight:bold;"></div>
             <?if (strlen($arResult["BACKURL"]) > 0):?>
			<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
		<?endif?>
		<input type="hidden" name="AUTH_FORM" value="Y">
		<input type="hidden" name="TYPE" value="RESET_PWD">

		<div class="">
			<div class="login-item">
				<span class="login-item-alignment"></span><span class="login-label"><?=GetMessage("AUTH_LOGIN")?></span>
				<input class="login-inp" id="userName" type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["LAST_LOGIN"]?>"/>
			</div>
    		<div class="login-item">
				<span class="login-item-alignment"></span><span class="login-label"><?=GetMessage("AUTH_OLD_PASSWORD")?></span>
				<input type="password" class="login-inp" id="oldPassword" type="text" name="OLD_PASSWORD" maxlength="255" />
			</div>
            <div class="login-item">
                <span class="login-item-alignment"></span><span class="login-label"><?=GetMessage("AUTH_NEW_PASSWORD")?></span>
                <input type="password" class="login-inp" id="newPassword" type="text" name="NEW_PASSWORD" maxlength="255" />
            </div>
		</div>
		<div class="login-text login-item">
			<?=GetMessage("AUTH_FORGOT_PASSWORD_1")?>
			<div class="login-links"><a href="<?=$arResult["AUTH_AUTH_URL"]?>"><?=GetMessage("AUTH_AUTH")?></a></div>
		</div>

		<div class="log-popup-footer">
			<button class="login-btn" value="<?=GetMessage("AUTH_GET_CHECK_STRING")?>" onclick="BX.addClass(this, 'wait');"><?=GetMessage("AUTH_GET_CHECK_STRING")?></button>
		</div>
	</form>

	<script type="text/javascript">
		BX.ready(function() {
			BX.focus(document.forms["form_auth"]["USER_LOGIN"]);
		});
        $('.login-btn').click(function(){
            var userName;
            userName = $('#userName').val().replace('\\','\\\\');
            var errors = [];
            if (userName.length <= 0)
              errors.push("<?=GetMessage("AUTH_LOGIN_REQUIRED")?>");
            var oldPassword = $('#oldPassword').val();
            if (oldPassword.length <= 0)
                errors.push('<?=GetMessage("AUTH_OLD_PASSWORD_REQUIRED")?>');
            var newPassword = $('#newPassword').val();
            if (newPassword.length <= 0)
                errors.push('<?=GetMessage("AUTH_NEW_PASSWORD_REQUIRED")?>');
            var errors_el = $('.errors');
            errors_el.html('');
            $('.success').html('');
            errors.forEach(function(item){
                $('.errors').append("<li>" +item + "</li>");
            });
            if (errors.length <= 0) {
                errors_el.html('');
                $.ajax({
                    type: 'POST',
                    url: '/app/api/ADAccount',
                    dataType: 'json',
                    success: function (data) {
                        if (data.Success === 0){
                            errors_el.append("<li>" + data.Error + "</li>");
                        }
                        else {
                            $('.success').html('<?=GetMessage("AUTH_SUCCESS")?>');
                            document.forms["form_auth"].reset();
                        }
                    },
                    data: '{"UserName": "' + userName + '", "OldPassword" : "' + oldPassword + '","NewPassword": "' + newPassword + '"}'
                });
            }
            $(this).removeClass('wait');
            return false;
        })
	</script>
<?
}
?>
