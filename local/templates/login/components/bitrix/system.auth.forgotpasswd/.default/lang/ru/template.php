<?
$MESS ['AUTH_FORGOT_PASSWORD_1'] = "Если вы забыли пароль, введите логин, старой пароль и новый пароль.<br />";
$MESS ['AUTH_GET_CHECK_STRING'] = "Сбросить пароль";
$MESS ['AUTH_SEND'] = "Выслать";
$MESS ['AUTH_AUTH'] = "Авторизация";
$MESS ['AUTH_LOGIN'] = "Логин:";
$MESS ['AUTH_OR'] = "или";
$MESS ['AUTH_EMAIL'] = "E-Mail:";
$MESS ['AUTH_OLD_PASSWORD'] = "Старый пароль:";
$MESS ['AUTH_NEW_PASSWORD'] = "Новый пароль:";
$MESS ['AUTH_FORGOT_EMAIL_LINK'] = "Не помню E-mail";
$MESS ['AUTH_FORGOT_EMAIL_TITLE'] = "Запрос E-Mail";
$MESS ['AUTH_LOGIN_REQUIRED'] = 'Логин должен быть указан!';
$MESS ['AUTH_OLD_PASSWORD_REQUIRED'] = 'Старый пароль должен быть указан!';
$MESS ['AUTH_NEW_PASSWORD_REQUIRED'] = 'Новый пароль должен быть указан!';
$MESS ['AUTH_SUCCESS'] = 'Пароль успешно изменен!';
?>