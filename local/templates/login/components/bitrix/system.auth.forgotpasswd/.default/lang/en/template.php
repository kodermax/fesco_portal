<?
$MESS["AUTH_FORGOT_PASSWORD_1"] = "If you reset your password, enter your login, old password and new password.";
$MESS["AUTH_GET_CHECK_STRING"] = "Reset password";
$MESS["AUTH_SEND"] = "Send";
$MESS["AUTH_AUTH"] = "Authorization";
$MESS["AUTH_LOGIN"] = "Login:";
$MESS["AUTH_OR"] = "or";
$MESS["AUTH_EMAIL"] = "E-Mail:";
$MESS["AUTH_FORGOT_EMAIL_LINK"] = "I don't remember my e-mail address";
$MESS["AUTH_FORGOT_EMAIL_TITLE"] = "Recover E-mail Address";
$MESS ['AUTH_OLD_PASSWORD'] = "Old password:";
$MESS ['AUTH_NEW_PASSWORD'] = "New Password:";
$MESS ['AUTH_LOGIN_REQUIRED'] = 'Login must be specified!';
$MESS ['AUTH_OLD_PASSWORD_REQUIRED'] = 'Old password must be specified!';
$MESS ['AUTH_NEW_PASSWORD_REQUIRED'] = 'New password must be specified!';
$MESS ['AUTH_SUCCESS'] = 'Your password has been reset successfully!';
?>