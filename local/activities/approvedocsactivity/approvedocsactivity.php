<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

class CBPApproveDocsActivity
	extends CBPCompositeActivity
	implements IBPEventActivity, IBPActivityExternalEventListener
{
	private $taskId = 0;
	private $subscriptionId = 0;

	private $isInEventActivityMode = false;

	private $arReviewResults = array();

	public function __construct($name)
	{
		parent::__construct($name);
		$this->arProperties = array(
			"Title" => "",
			"Users" => null,
            "ActivityType" => '1',
			"ApproveType" => "all",
            "RevisionButton" => "1",
            "DelegateButton" => "1",
			"OverdueDate" => null,
			"Name" => null,
			"Description" => null,
			"Parameters" => null,
            "RevisionStatus" => false,
            "CancelStatus" => false,
            "ApproveStatus" => false,
            "ActivityStatus" => 0,
			"ReviewedCount" => 0,
			"TotalCount" => 0,
			"StatusMessage" => "",
			"SetStatusMessage" => "Y",
			"TaskButtonMessage" => "",
			"TimeoutDuration" => 0,
			"TimeoutDurationType" => "s",
			"IsTimeout" => 0,
			"Comments" => "",
			"CommentLabelMessage" => "",
			"ShowComment" => "Y",
		);
	}

	protected function ReInitialize()
	{
		parent::ReInitialize();

		$this->arReviewResults = array();
        $this->ActivityStatus = 0;
		$this->ReviewedCount = 0;
		$this->Comments = '';
		$this->IsTimeout = 0;
	}

	public function Execute()
	{
		if ($this->isInEventActivityMode)
			return CBPActivityExecutionStatus::Closed;

		$this->Subscribe($this);
		$this->isInEventActivityMode = false;
		return CBPActivityExecutionStatus::Executing;
	}

	public function Subscribe(IBPActivityExternalEventListener $eventHandler)
	{
		if ($eventHandler == null)
			throw new Exception("eventHandler");

		$this->isInEventActivityMode = true;

		$rootActivity = $this->GetRootActivity();
		$documentId = $rootActivity->GetDocumentId();

		$runtime = CBPRuntime::GetRuntime();
		$documentService = $runtime->GetService("DocumentService");

		$arUsersTmp = $this->Users;
		if (!is_array($arUsersTmp))
			$arUsersTmp = array($arUsersTmp);

		$this->WriteToTrackingService(str_replace("#VAL#", "{=user:".implode("}, {=user:", $arUsersTmp)."}", GetMessage("BPAR_ACT_TRACK2")));

		$arUsers = CBPHelper::ExtractUsers($arUsersTmp, $documentId, false);

		$arParameters = $this->Parameters;
		if (!is_array($arParameters))
			$arParameters = array($arParameters);
		$arParameters["DOCUMENT_ID"] = $documentId;
		$arParameters["DOCUMENT_URL"] = $documentService->GetDocumentAdminPage($documentId);
		$arParameters["TaskButtonMessage"] = $this->IsPropertyExists("TaskButtonMessage") ? $this->TaskButtonMessage : GetMessage("BPAR_ACT_BUTTON2");
		if (strlen($arParameters["TaskButtonMessage"]) <= 0)
			$arParameters["TaskButtonMessage"] = GetMessage("BPAR_ACT_BUTTON2");
		$arParameters["CommentLabelMessage"] = $this->IsPropertyExists("CommentLabelMessage") ? $this->CommentLabelMessage : GetMessage("BPAR_ACT_COMMENT");
        $arParameters["RevisionButton"] = $this->IsPropertyExists("RevisionButton") ? $this->RevisionButton : 1;
        $arParameters["DelegateButton"] = $this->IsPropertyExists("DelegateButton") ? $this->DelegateButton : 1;
		if (strlen($arParameters["CommentLabelMessage"]) <= 0)
			$arParameters["CommentLabelMessage"] = GetMessage("BPAR_ACT_COMMENT");
		$arParameters["ShowComment"] = $this->IsPropertyExists("ShowComment") ? $this->ShowComment : "Y";
		if ($arParameters["ShowComment"] != "Y" && $arParameters["ShowComment"] != "N")
			$arParameters["ShowComment"] = "Y";

		$taskService = $this->workflow->GetService("TaskService");

		$this->taskId = $taskService->CreateTask(
			array(
				"USERS" => $arUsers,
				"WORKFLOW_ID" => $this->GetWorkflowInstanceId(),
				"ACTIVITY" => "ApproveDocsActivity",
				"ACTIVITY_NAME" => $this->name,
				"OVERDUE_DATE" => $this->OverdueDate,
				"NAME" => $this->Name,
				"DESCRIPTION" => $this->Description,
				"PARAMETERS" => $arParameters,
			)
		);

		$this->TotalCount = count($arUsers);
		if (!$this->IsPropertyExists("SetStatusMessage") || $this->SetStatusMessage == "Y")
		{
			$totalCount = $this->TotalCount;
			$message = ($this->IsPropertyExists("StatusMessage") && strlen($this->StatusMessage) > 0) ? $this->StatusMessage : GetMessage("BPAR_ACT_INFO");
			$this->SetStatusTitle(str_replace(
				array("#PERC#", "#PERCENT#", "#REV#", "#REVIEWED#", "#TOT#", "#TOTAL#", "#REVIEWERS#"),
				array(0, 0, 0, 0, $totalCount, $totalCount, ""),
				$message
			));
		}

		$timeoutDuration = $this->CalculateTimeoutDuration();
		if ($timeoutDuration > 0)
		{
			$schedulerService = $this->workflow->GetService("SchedulerService");
			$this->subscriptionId = $schedulerService->SubscribeOnTime($this->workflow->GetInstanceId(), $this->name, CBPApproveDocs::AddWorkingDays($this->TimeoutDuration));
		}

		$this->workflow->AddEventHandler($this->name, $eventHandler);

	}

	private function ReplaceTemplate($str, $ar)
	{
		$str = str_replace("%", "%2", $str);
		foreach ($ar as $key => $val)
		{
			$val = str_replace("%", "%2", $val);
			$val = str_replace("#", "%1", $val);
			$str = str_replace("#".$key."#", $val, $str);
		}
		$str = str_replace("%1", "#", $str);
		$str = str_replace("%2", "%", $str);

		return $str;
	}

	public function Unsubscribe(IBPActivityExternalEventListener $eventHandler)
	{
		if ($eventHandler == null)
			throw new Exception("eventHandler");

		$taskService = $this->workflow->GetService("TaskService");
		$taskService->DeleteTask($this->taskId);

		$timeoutDuration = $this->CalculateTimeoutDuration();
		if ($timeoutDuration > 0)
		{
			$schedulerService = $this->workflow->GetService("SchedulerService");
			$schedulerService->UnSubscribeOnTime($this->subscriptionId);
		}

		$this->workflow->RemoveEventHandler($this->name, $eventHandler);

		$this->taskId = 0;
		$this->subscriptionId = 0;
	}

	public function HandleFault(Exception $exception)
	{
		if ($exception == null)
			throw new Exception("exception");

		$status = $this->Cancel();
		if ($status == CBPActivityExecutionStatus::Canceling)
			return CBPActivityExecutionStatus::Faulting;

		return $status;
	}

	public function Cancel()
	{
		if (!$this->isInEventActivityMode && $this->taskId > 0)
			$this->Unsubscribe($this);

		return CBPActivityExecutionStatus::Closed;
	}

	public function OnExternalEvent($arEventParameters = array())
    {
        if ($this->executionStatus == CBPActivityExecutionStatus::Closed)
            return;
        $documentService = $this->workflow->GetService("DocumentService");
        $taskService = $this->workflow->GetService("TaskService");
        $rootActivity = $this->GetRootActivity();
        $documentId = $rootActivity->GetDocumentId();
        $documentType = $rootActivity->GetDocumentType();
        $elementId = $documentId[2];
        $iblockId = intval(str_replace('iblock_','',$documentType[2]));
        $typeId = $this->ActivityType;
        $cbp = new CBPApproveDocs($iblockId, $elementId, $typeId);
        $bid = $rootActivity->GetWorkflowInstanceId();
        $arEventParameters['URL'] =  $documentService->GetDocumentAdminPage($documentId);
        $arInfo = CBPDocument::GetDocumentState($documentId, $bid);
		$stage = 0;
		$stateTitle = $arInfo[$bid]['STATE_TITLE'];
		if (strpos($stateTitle, 'Согласование руководителя') !== false)
			$stage = "boss";
		else if(strpos($stateTitle, 'Веерное согласование') !== false )
			$stage = 'approving';
		else if(strpos($stateTitle,'Принятие решения подписантом') !== false)
			$stage = 'signatory';
		else if(strpos($stateTitle, 'Согласование с КС') !== false)
			$stage = 'secretary';
        $arEventParameters['STARTED_BY'] = $arInfo[$bid]['STARTED_BY'];
        $arEventParameters['DOC_ID'] = intval($elementId);
        $arEventParameters['IBLOCK_ID'] = $iblockId;
        $timeoutDuration = $this->CalculateTimeoutDuration();

        if ($timeoutDuration > 0) {
            if (array_key_exists("SchedulerService", $arEventParameters) && $arEventParameters["SchedulerService"] == "OnAgent") {

				$arUsers = CBPHelper::ExtractUsers($this->Users, $documentId, false);
				$arUsersNotApprove = array_diff($arUsers, $this->arReviewResults); // вычисляем кто еще не голосовал
                //Автоматически завершаем задачу у пользователей
                foreach($arUsersNotApprove as $userId) {
                    $arEventParameters['USER_ID'] = $userId;
                   // $taskService->MarkCompleted($this->taskId, $userId);
                    $messageTemplate = "Пользователь #PERSON# автоматически согласовал документ";
                    $this->WriteToTrackingService(str_replace(array('#PERSON#'),array("{=user:user_" . $userId. "}"), $messageTemplate));
                    $cbp->AddToLog($bid, 5, "", $userId,0,$stage);
                    $cbp->SendEvent($arEventParameters, 5);
                }
                $messageTemplate = ($this->IsPropertyExists("StatusMessage") && strlen($this->StatusMessage) > 0) ? $this->StatusMessage : GetMessage("BPAR_ACT_INFO");
                $votedPercent = intval($this->TotalCount / $this->TotalCount * 100);
                $votedCount = $this->TotalCount;
                $totalCount = $this->TotalCount;
                $reviewers = "";
                if (strpos($messageTemplate, "#REVIEWERS#") !== false)
                    $reviewers = $this->GetReviewersNames();
                if ($reviewers == "")
                    $reviewers = GetMessage("BPAA_ACT_APPROVERS_NONE");

                $this->SetStatusTitle(str_replace(
                    array("#PERC#", "#PERCENT#", "#REV#", "#REVIEWED#", "#TOT#", "#TOTAL#", "#REVIEWERS#"),
                    array($votedPercent, $votedPercent, $votedCount, $votedCount, $totalCount, $totalCount, $reviewers),
                    $messageTemplate
                ));
                $this->WriteToTrackingService(GetMessage("BPAR_ACT_REVIEWED"));

                if ($this->RevisionStatus)
                    $this->ActivityStatus = 4;
                elseif ($this->CancelStatus)
                    $this->ActivityStatus = 2;
                else
                    $this->ActivityStatus = 1;

                $this->IsTimeout = 1;
                $this->Unsubscribe($this);
                $this->workflow->CloseActivity($this);
				return;
            }
        }


        $dbUser = CUser::GetById($arEventParameters["USER_ID"]);
        if ($arUser = $dbUser->Fetch())
            $this->Comments = $this->Comments .
                CUser::FormatName(COption::GetOptionString("bizproc", "name_template", CSite::GetNameFormat(false), SITE_ID), $arUser) . " (" . $arUser["LOGIN"] . ")" .
                ((strlen($arEventParameters["COMMENT"]) > 0) ? ": " : "") . $arEventParameters["COMMENT"] . "\n";
        $delegateUserFormat = '';
        if ($arEventParameters["DELEGATE_USER_ID"] > 0) {
            $dbUser = CUser::GetById($arEventParameters["DELEGATE_USER_ID"]);
            if ($arUser = $dbUser->Fetch()) {
                $delegateUserFormat = CUser::FormatName(COption::GetOptionString("bizproc", "name_template", CSite::GetNameFormat(false), SITE_ID), $arUser);
            }
			$arEventParameters['DELEGATE_USER'] = $delegateUserFormat;
        }

		$arUsers = CBPHelper::ExtractUsers($this->Users, $documentId, false);
        $arEventParameters["USER_ID"] = intval($arEventParameters["USER_ID"]);
        if (!in_array($arEventParameters["USER_ID"], $arUsers))
            return;
        if ($arEventParameters['DELEGATE_USER_ID'] <= 0)
            $taskService->MarkCompleted($this->taskId, $arEventParameters["USER_ID"]);


        switch($arEventParameters['APPROVE'])
        {
            case 'ok':
                $this->WriteToTrackingService(
                    str_replace(
                        array("#PERSON#", "#COMMENT#"),
                        array("{=user:user_" . $arEventParameters["USER_ID"] . "}", (strlen($arEventParameters["COMMENT"]) > 0 ? ": " . $arEventParameters["COMMENT"] : "")),
                        GetMessage("BPAR_ACT_REVIEW_TRACK")
                    ),
                    $arEventParameters["USER_ID"]
                );
                $cbp->AddToLog($bid, 1, $arEventParameters['COMMENT'], $arEventParameters["USER_ID"],0, $stage);
                $cbp->SendEvent($arEventParameters, 1);

                break;
            case 'cancel':
                $this->WriteToTrackingService(
                    str_replace(
                        array("#PERSON#", "#COMMENT#"),
                        array("{=user:user_" . $arEventParameters["USER_ID"] . "}", (strlen($arEventParameters["COMMENT"]) > 0 ? ": " . $arEventParameters["COMMENT"] : "")),
                        GetMessage("BPAR_ACT_CANCEL_TRACK")
                    ),
                    $arEventParameters["USER_ID"]
                );
                $cbp->AddToLog($bid, 2, $arEventParameters['COMMENT'], $arEventParameters["USER_ID"],0, $stage);
                $cbp->SendEvent($arEventParameters, 2);
                break;
            case 'delegate':
                $this->WriteToTrackingService(
                    str_replace(
                        array("#PERSON#", "#COMMENT#", "#DELEGATE_USER#"),
                        array(
                            "{=user:user_" . $arEventParameters["USER_ID"] . "}",
                            (strlen($arEventParameters["COMMENT"]) > 0 ? ": " . $arEventParameters["COMMENT"] : ""),
                            $delegateUserFormat
                        ),
                        GetMessage("BPAR_ACT_DELEGATE_TRACK")
                    ),
                    $arEventParameters["USER_ID"]
                );
                foreach ($arUsers as $key => $userId)
                {
                    if ($userId == $arEventParameters["USER_ID"])
                        unset($arUsers[$key]);
                }
                $arUsers[] = $arEventParameters['DELEGATE_USER_ID'];

                //Удаляем из списка согласующих, того кто делегировал задачу
				$arApprovers = array();
				$arApprovers = $this->Users;
				foreach ($arApprovers as $key => $item) {
					if ($item == 'user_' . $arEventParameters["USER_ID"]) {
						unset($arApprovers[$key]);
					}
				}

				//Добавляем в список согласующих делеганта
                $arApprovers[] = 'user_'.$arEventParameters['DELEGATE_USER_ID'];
				$this->Users = $arApprovers;

                $arFields = array("USERS" => $arUsers, "DESCRIPTION" => "Задание делегировано");
				//Удаляем ползователя из списка задач
				CUserCounter::Decrement($arEventParameters["USER_ID"], 'bp_tasks', '**');
				global $DB;
				$strSql =
					"UPDATE b_bp_task_user SET ".
					" USER_ID = ".intval($arEventParameters['DELEGATE_USER_ID']).
					" WHERE USER_ID = ".intval($arEventParameters["USER_ID"])." AND TASK_ID = ".intval($this->taskId);
				$DB->Query($strSql, False, "File: ".__FILE__."<br>Line: ".__LINE__);
				CUserCounter::Increment($arEventParameters['DELEGATE_USER_ID'], 'bp_tasks', '**');

                $cbp->AddToLog($bid, 3, $arEventParameters['COMMENT'], $arEventParameters["USER_ID"], $arEventParameters['DELEGATE_USER_ID'], $stage);
                $cbp->SendEvent($arEventParameters, 3);
                break;
            case 'revision':
                $this->WriteToTrackingService(
                    str_replace(
                        array("#PERSON#", "#COMMENT#"),
                        array("{=user:user_" . $arEventParameters["USER_ID"] . "}", (strlen($arEventParameters["COMMENT"]) > 0 ? ": " . $arEventParameters["COMMENT"] : "")),
                        GetMessage("BPAR_ACT_REVISION_TRACK")
                    ),
                    $arEventParameters["USER_ID"]
                );
                $cbp->AddToLog($bid, 4, $arEventParameters['COMMENT'], $arEventParameters["USER_ID"], 0, $stage);
                $cbp->SendEvent($arEventParameters, 4);
                break;
			case 'president':
				$messageTemplate = "Пользователь #PERSON# автоматически согласовал документ";
				$this->WriteToTrackingService(str_replace(array('#PERSON#'),array("{=user:user_" . $arEventParameters["USER_ID"]. "}"), $messageTemplate));
				$cbp->AddToLog($bid, 6, '', $arEventParameters["USER_ID"], 0, $stage);
				$cbp->SendEvent($arEventParameters, 1);
				break;
        }

		$result = "Continue";
        if ($arEventParameters['DELEGATE_USER_ID'] <= 0)
    		$this->arReviewResults[] = $arEventParameters["USER_ID"];
		$this->ReviewedCount = count($this->arReviewResults);

		if ($this->IsPropertyExists("ApproveType") && $this->ApproveType == "any")
		{
			$result = "Finish";
		}
		else
		{
			$allAproved = true;
			foreach ($arUsers as $userId)
			{
				if (!in_array($userId, $this->arReviewResults))
					$allAproved = false;
			}

			if ($allAproved)
				$result = "Finish";
		}

		if (!$this->IsPropertyExists("SetStatusMessage") || $this->SetStatusMessage == "Y")
		{
			$messageTemplate = ($this->IsPropertyExists("StatusMessage") && strlen($this->StatusMessage) > 0) ? $this->StatusMessage : GetMessage("BPAR_ACT_INFO");
			$votedPercent = intval($this->ReviewedCount / $this->TotalCount * 100);
			$votedCount = $this->ReviewedCount;
			$totalCount = $this->TotalCount;

			$reviewers = "";
			if (strpos($messageTemplate, "#REVIEWERS#") !== false)
				$reviewers = $this->GetReviewersNames();
			if ($reviewers == "")
				$reviewers = GetMessage("BPAA_ACT_APPROVERS_NONE");

			$this->SetStatusTitle(str_replace(
				array("#PERC#", "#PERCENT#", "#REV#", "#REVIEWED#", "#TOT#", "#TOTAL#", "#REVIEWERS#"),
				array($votedPercent, $votedPercent, $votedCount, $votedCount, $totalCount, $totalCount, $reviewers),
				$messageTemplate
			));
		}
        if ($arEventParameters['APPROVE'] == 'cancel')
            $this->CancelStatus = true;
        elseif ($arEventParameters['APPROVE'] == 'revision')
            $this->RevisionStatus = true;

		if ($result != "Continue")
		{
            if ($this->RevisionStatus)
                $this->ActivityStatus = 4;
            elseif ($this->CancelStatus)
                $this->ActivityStatus = 2;
            else
                $this->ActivityStatus = 1;

            $this->WriteToTrackingService(GetMessage("BPAR_ACT_REVIEWED"));

 			$this->Unsubscribe($this);
			$this->workflow->CloseActivity($this);
		}
	}

	private function GetReviewersNames()
	{
		$result = "";

		foreach ($this->arReviewResults as $k)
		{
			$dbUsers = CUser::GetByID($k);
			if ($arUser = $dbUsers->Fetch())
			{
				if ($result != "")
					$result .= ", ";
				$result .= CUser::FormatName(COption::GetOptionString("bizproc", "name_template", CSite::GetNameFormat(false), SITE_ID), $arUser)." (".$arUser["LOGIN"].")";
			}
		}

		return $result;
	}

	protected function OnEvent(CBPActivity $sender)
	{

		$sender->RemoveStatusChangeHandler(self::ClosedEvent, $this);
		$this->workflow->CloseActivity($this);
	}

	public static function ShowTaskForm($arTask, $userId, $userName = "")
	{
		$form = '';

		if (!array_key_exists("ShowComment", $arTask["PARAMETERS"]) || ($arTask["PARAMETERS"]["ShowComment"] != "N"))
		{
			$form .=
				'<tr><td valign="top" width="40%" align="right" class="bizproc-field-name">'.(strlen($arTask["PARAMETERS"]["CommentLabelMessage"]) > 0 ? $arTask["PARAMETERS"]["CommentLabelMessage"] : GetMessage("BPAR_ACT_COMMENT")).':</td>'.
				'<td valign="top" width="60%" class="bizproc-field-value">'.
				'<textarea rows="3" cols="50" name="task_comment"></textarea>'.
				'</td></tr>';
		}
		$buttons = '<div class="item"><input class="positive ui small button" type="submit" name="review" value="Согласовать"/></div>';
        $buttons .= '<div class="item"><input class="commentCheck negative ui small button" type="submit" name="cancel" value="Отклонить"/></div>';
        if (intval($arTask['PARAMETERS']['RevisionButton']) != 0)
            $buttons .= '<div class="item"><input class="commentCheck revision ui small button" type="submit" name="revision" value="Отправить на доработку"/></div>';
        //$buttons .= '<div class="item"><input type="submit" value="Делегировать" name="delegate" class="delegate ui small button"></div>';


		return array($form, $buttons);
	}

	public static function PostTaskForm($arTask, $userId, $arRequest, &$arErrors, $userName = "")
	{
		$arErrors = array();

		try
		{
			$userId = intval($userId);
			if ($userId <= 0)
				throw new CBPArgumentNullException("userId");

			$arEventParameters = array(
				"USER_ID" => $GLOBALS['USER']->GetID(),
				"USER_NAME" => $userName,
				"COMMENT" => $arRequest["task_comment"],
			);
            if (strlen($arRequest["review"]) > 0)
                $arEventParameters["APPROVE"] = 'ok';
            elseif (strlen($arRequest["cancel"]) > 0)
                $arEventParameters["APPROVE"] = 'cancel';
            elseif(strlen($arRequest["delegate"]) > 0) {
                $arEventParameters["APPROVE"] = 'delegate';
                $arEventParameters['DELEGATE_USER_ID'] = $arRequest['USER'];
            }
            elseif(strlen($arRequest["revision"]) > 0)
                $arEventParameters["APPROVE"] = 'revision';

			CBPRuntime::SendExternalEvent($arTask["WORKFLOW_ID"], $arTask["ACTIVITY_NAME"], $arEventParameters);

			return true;
		}
		catch (Exception $e)
		{
			$arErrors[] = array(
				"code" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile()." [".$e->getLine()."]",
			);
		}

		return false;
	}

	public static function ValidateProperties($arTestProperties = array(), CBPWorkflowTemplateUser $user = null)
	{
		$arErrors = array();

		if (!array_key_exists("Users", $arTestProperties))
		{
			$bUsersFieldEmpty = true;
		}
		else
		{
			if (!is_array($arTestProperties["Users"]))
				$arTestProperties["Users"] = array($arTestProperties["Users"]);

			$bUsersFieldEmpty = true;
			foreach ($arTestProperties["Users"] as $userId)
			{
				if (!is_array($userId) && (strlen(trim($userId)) > 0) || is_array($userId) && (count($userId) > 0))
				{
					$bUsersFieldEmpty = false;
					break;
				}
			}
		}

		if ($bUsersFieldEmpty)
			$arErrors[] = array("code" => "NotExist", "parameter" => "Users", "message" => GetMessage("BPAR_ACT_PROP_EMPTY1"));

		if (!array_key_exists("Name", $arTestProperties) || strlen($arTestProperties["Name"]) <= 0)
		{
			$arErrors[] = array("code" => "NotExist", "parameter" => "Name", "message" => GetMessage("BPAR_ACT_PROP_EMPTY4"));
		}

		return array_merge($arErrors, parent::ValidateProperties($arTestProperties, $user));
	}

	private function CalculateTimeoutDuration()
	{
		$timeoutDuration = ($this->IsPropertyExists("TimeoutDuration") ? $this->TimeoutDuration : 0);

		$timeoutDurationType = ($this->IsPropertyExists("TimeoutDurationType") ? $this->TimeoutDurationType : "s");
		$timeoutDurationType = strtolower($timeoutDurationType);
		if (!in_array($timeoutDurationType, array("s", "d", "h", "m")))
			$timeoutDurationType = "s";

		$timeoutDuration = intval($timeoutDuration);
		switch ($timeoutDurationType)
		{
			case 'd':
				$timeoutDuration *= 3600 * 24;
				break;
			case 'h':
				$timeoutDuration *= 3600;
				break;
			case 'm':
				$timeoutDuration *= 60;
				break;
			default:
				break;
		}

		return $timeoutDuration;
	}

	public static function GetPropertiesDialog($documentType, $activityName, $arWorkflowTemplate, $arWorkflowParameters, $arWorkflowVariables, $arCurrentValues = null, $formName = "")
	{
		$runtime = CBPRuntime::GetRuntime();

		$arMap = array(
			"Users" => "review_users",
            "RevisionButton" => "revision_button",
            "DelegateButton" => "delegate_button",
			"ApproveType" => "approve_type",
			"ActivityType" => "activity_type",
			"OverdueDate" => "review_overdue_date",
			"Name" => "review_name",
			"Description" => "review_description",
			"Parameters" => "review_parameters",
			"StatusMessage" => "status_message",
			"SetStatusMessage" => "set_status_message",
			"TaskButtonMessage" => "task_button_message",
			"CommentLabelMessage" => "comment_label_message",
			"ShowComment" => "show_comment",
			"TimeoutDuration" => "timeout_duration",
			"TimeoutDurationType" => "timeout_duration_type",
		);

		if (!is_array($arWorkflowParameters))
			$arWorkflowParameters = array();
		if (!is_array($arWorkflowVariables))
			$arWorkflowVariables = array();

		if (!is_array($arCurrentValues))
		{
			$arCurrentActivity = &CBPWorkflowTemplateLoader::FindActivityByName($arWorkflowTemplate, $activityName);
			if (is_array($arCurrentActivity["Properties"]))
			{
				foreach ($arMap as $k => $v)
				{
					if (array_key_exists($k, $arCurrentActivity["Properties"]))
					{
						if ($k == "Users")
						{
							$arCurrentValues[$arMap[$k]] = CBPHelper::UsersArrayToString($arCurrentActivity["Properties"][$k], $arWorkflowTemplate, $documentType);
						}
						elseif ($k == "TimeoutDuration")
						{
							$arCurrentValues["timeout_duration"] = $arCurrentActivity["Properties"]["TimeoutDuration"];
							if (!preg_match('#^{=[A-Za-z0-9_]+:[A-Za-z0-9_]+}$#i', $arCurrentValues["timeout_duration"])
								&& !array_key_exists("TimeoutDurationType", $arCurrentActivity["Properties"]))
							{
								$arCurrentValues["timeout_duration"] = intval($arCurrentValues["timeout_duration"]);
								$arCurrentValues["timeout_duration_type"] = "s";
								if ($arCurrentValues["timeout_duration"] % (3600 * 24) == 0)
								{
									$arCurrentValues["timeout_duration"] = $arCurrentValues["timeout_duration"] / (3600 * 24);
									$arCurrentValues["timeout_duration_type"] = "d";
								}
								elseif ($arCurrentValues["timeout_duration"] % 3600 == 0)
								{
									$arCurrentValues["timeout_duration"] = $arCurrentValues["timeout_duration"] / 3600;
									$arCurrentValues["timeout_duration_type"] = "h";
								}
								elseif ($arCurrentValues["timeout_duration"] % 60 == 0)
								{
									$arCurrentValues["timeout_duration"] = $arCurrentValues["timeout_duration"] / 60;
									$arCurrentValues["timeout_duration_type"] = "m";
								}
							}
						}
						else
						{
							$arCurrentValues[$arMap[$k]] = $arCurrentActivity["Properties"][$k];
						}
					}
					else
					{
						if (!is_array($arCurrentValues) || !array_key_exists($arMap[$k], $arCurrentValues))
							$arCurrentValues[$arMap[$k]] = "";
					}
				}
			}
			else
			{
				foreach ($arMap as $k => $v)
					$arCurrentValues[$arMap[$k]] = "";
			}
		}

		if (strlen($arCurrentValues['status_message']) <= 0)
			$arCurrentValues['status_message'] = GetMessage("BPAR_ACT_INFO");
		if (strlen($arCurrentValues['comment_label_message']) <= 0)
			$arCurrentValues['comment_label_message'] = GetMessage("BPAR_ACT_COMMENT");
		if (strlen($arCurrentValues['task_button_message']) <= 0)
			$arCurrentValues['task_button_message'] = GetMessage("BPAR_ACT_BUTTON2");
		if (strlen($arCurrentValues["timeout_duration_type"]) <= 0)
			 $arCurrentValues["timeout_duration_type"] = "s";

		$documentService = $runtime->GetService("DocumentService");
		$arDocumentFields = $documentService->GetDocumentFields($documentType);

		return $runtime->ExecuteResourceFile(
			__FILE__,
			"properties_dialog.php",
			array(
				"arCurrentValues" => $arCurrentValues,
				"arDocumentFields" => $arDocumentFields,
				"formName" => $formName,
			)
		);
	}

	public static function GetPropertiesDialogValues($documentType, $activityName, &$arWorkflowTemplate, &$arWorkflowParameters, &$arWorkflowVariables, $arCurrentValues, &$arErrors)
	{
		$arErrors = array();

		$runtime = CBPRuntime::GetRuntime();

		$arMap = array(
			"review_users" => "Users",
            "revision_button" => "RevisionButton",
            "delegate_button" => "DelegateButton",
			"approve_type" => "ApproveType",
			"activity_type" => "ActivityType",
			"review_overdue_date" => "OverdueDate",
			"review_name" => "Name",
			"review_description" => "Description",
			"review_parameters" => "Parameters",
			"status_message" => "StatusMessage",
			"set_status_message" => "SetStatusMessage",
			"task_button_message" => "TaskButtonMessage",
			"comment_label_message" => "CommentLabelMessage",
			"show_comment" => "ShowComment",
			"timeout_duration" => "TimeoutDuration",
			"timeout_duration_type" => "TimeoutDurationType",
		);

		$arProperties = array();
		foreach ($arMap as $key => $value)
		{
			if ($key == "review_users")
				continue;
			$arProperties[$value] = $arCurrentValues[$key];
		}

		$arProperties["Users"] = CBPHelper::UsersStringToArray($arCurrentValues["review_users"], $documentType, $arErrors);
		if (count($arErrors) > 0)
			return false;

		$arErrors = self::ValidateProperties($arProperties, new CBPWorkflowTemplateUser(CBPWorkflowTemplateUser::CurrentUser));
		if (count($arErrors) > 0)
			return false;

		$arCurrentActivity = &CBPWorkflowTemplateLoader::FindActivityByName($arWorkflowTemplate, $activityName);
		$arCurrentActivity["Properties"] = $arProperties;

		return true;
	}
}
?>
