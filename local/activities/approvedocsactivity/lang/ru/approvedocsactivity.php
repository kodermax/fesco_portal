<?
$MESS ['BPAR_ACT_COMMENT'] = "Комментарий";
$MESS ['BPAR_ACT_INFO'] = "Согласовано #PERCENT#% (#REVIEWED# из #TOTAL#)";
$MESS ['BPAR_ACT_BUTTON2'] = "Согласовать";
$MESS ['BPAR_ACT_REVIEWED'] = "Согласование документа завершено.";
$MESS ['BPAR_ACT_TRACK2'] = "Согласование документа пользователями из списка: #VAL#";
$MESS ['BPAR_ACT_REVIEW_TRACK'] = "Пользователь #PERSON# согласовал документ #COMMENT#";
$MESS ['BPAR_ACT_CANCEL_TRACK'] = "Пользователь #PERSON# отклонил документ #COMMENT#";
$MESS ['BPAR_ACT_DELEGATE_TRACK'] = "Пользователь #PERSON# делегировал документ пользователю #DELEGATE_USER# #COMMENT#";
$MESS ['BPAR_ACT_REVISION_TRACK'] = "Пользователь #PERSON# отправил документ на доработку  #COMMENT#";
$MESS ['BPAR_ACT_PROP_EMPTY1'] = "Свойство 'Пользователи' не указано.";
$MESS ['BPAR_ACT_PROP_EMPTY4'] = "Свойство 'Название' не указано.";
$MESS ['BPAA_ACT_APPROVERS_NONE'] = "нет";
?>