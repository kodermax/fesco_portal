<?define('STOP_STATISTICS',    true);
define('NO_AGENT_CHECK',     true);
define('DisableEventsCheck', true);
define('BX_SECURITY_SHOW_MESSAGE', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('highloadblock');
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
$arResult = array();
/*$arFilter = array('IBLOCK_ID' => '46');
if (array_key_exists('filter',$_REQUEST)) {
    $value = $_REQUEST['filter']['filters'][0]['value'];
    if (strlen($value) > 0)
    {
        $arFilter['NAME'] = "%".$value."%";
    }
}
$list = CIBlockElement::GetList(array("NAME" => "ASC"),$arFilter,false,false,array('ID','NAME'));
while($row = $list->GetNext())
{
    $arResult[] = array('ID' => $row['ID'],'Name' => html_entity_decode($row['NAME']));
}*/
$hlblock = HL\HighloadBlockTable::getById(3)->fetch();
$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();
$value = $_REQUEST['filter']['filters'][0]['value'];
if (strlen($value) > 0) {
    $filter = array(
        'LOGIC' => 'OR',
        array(
            '%=UF_NAME' => '%'.$value.'%'
        ),
        array(
            '%=UF_SHORT_NAME' => '%'.$value.'%'
        ),
        array(
            '%=UF_INN' => '%'.$value.'%'
        ),
        array(
            '%=UF_KPP' => '%'.$value.'%'
        ),
        array(
            '%=UF_OGRN' => '%'.$value.'%'
        ),
    );
}
$main_query = new Entity\Query($entity);
$main_query->setFilter($filter);
$main_query->setSelect(array('*'));
$main_query->setOrder(array('UF_NAME' => 'ASC'));
$limit = array(
    'nPageSize' => 10,
);
$main_query->setLimit($limit['nPageSize']);
//$main_query->setOffset(($limit['iNumPage']-1) * $limit['nPageSize']);
$result = $main_query->exec();
$result = new CDBResult($result);
while ($row = $result->Fetch())
{

    $arResult[] = array(
        'Id' => $row["UF_XML_ID"],
        'Name' => $row['UF_NAME'],
    );
}

$APPLICATION->RestartBuffer();
header('Content-Type: application/json');
echo json_encode($arResult,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);