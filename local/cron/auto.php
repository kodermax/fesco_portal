<?php

$_SERVER["DOCUMENT_ROOT"] = "/home/bitrix/www/sites/msk/dvmp";
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
set_time_limit(0);
define("LANG", "ru");
define("SITE_ID", "s1");
define("BX_CAT_CRON", true);
define('NO_AGENT_CHECK', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
\Bitrix\Main\loader::includeModule('iblock');

$helper = new CFESCOIBlockAuto();
$startCurrentDate = date('d.m.Y 00:00:00');
$endCurrentDate = date('d.m.Y H:i:s');

//Делаем автовыезд
$arFilter = array(
    'ACTIVE' => 'Y',
    'PROPERTY_STATUS' => $helper->GetStatusEnumIdByXmlId('approve'),
    'IBLOCK_ID' => $helper->GetRequestsIBlockId(),
    "<=DATE_ACTIVE_FROM" => $endCurrentDate,
    ">=DATE_ACTIVE_FROM" => $startCurrentDate,
);

$list = CIBlockElement::GetList(array(),$arFilter, false, false,array('DATE_ACTIVE_FROM','ID','IBLOCK_ID','PROPERTY_STATUS'));
while($row = $list->GetNext())
{
    CIBlockElement::SetPropertyValuesEx($row['ID'], $row['IBLOCK_ID'], array('STATUS' => $helper->GetStatusEnumIdByXmlId('run')));
}

//Делаем автоприезд
$arFilter = array(
    'ACTIVE' => 'Y',
    'PROPERTY_STATUS' => $helper->GetStatusEnumIdByXmlId('run'),
    'IBLOCK_ID' => $helper->GetRequestsIBlockId(),
    "<=DATE_ACTIVE_TO" => $endCurrentDate,
    ">=DATE_ACTIVE_TO" => $startCurrentDate,
);

$list = CIBlockElement::GetList(array(),$arFilter, false, false,array('DATE_ACTIVE_FROM','ID','IBLOCK_ID','PROPERTY_STATUS'));
while($row = $list->GetNext())
{
    CIBlockElement::SetPropertyValuesEx($row['ID'], $row['IBLOCK_ID'], array('STATUS' => $helper->GetStatusEnumIdByXmlId('stop')));
}
print "Done!";
?>