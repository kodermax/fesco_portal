<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 06.11.2014
 * Time: 18:32
 */

$_SERVER["DOCUMENT_ROOT"] = "/home/bitrix/www/sites/msk/dvmp";
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
set_time_limit(0);
define("LANG", "ru");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\loader::includeModule('bizproc');
$arResult = array();
$list = CBPTaskService::GetList(array(),array(),false,false,array('NAME','USER_ID'));
while($row = $list->GetNext())
{
    $arResult[$row['USER_ID']][] = $row;
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>