<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 06.11.2014
 * Time: 18:32
 */

$_SERVER["DOCUMENT_ROOT"] = "/home/bitrix/www/sites/msk/dvmp";
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
set_time_limit(0);
define("LANG", "ru");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\loader::includeModule('bizproc');
$arResult = array(); 
$list = CBPTaskService::GetList(array(),array('USER_STATUS' => CBPTaskUserStatus::Waiting) ,false,false,array('NAME','USER_ID','USER_STATUS'));
while($row = $list->GetNext())
{
    $arResult[$row['USER_ID']][] = $row;
}
$rsSites = CSite::GetByID("s1");
$arSite = $rsSites->Fetch();

$URL = 'http://'.$arSite['SERVER_NAME']."/company/personal/bizproc/";
foreach($arResult as $key => $arItems)
{
    $rsUser = CUser::GetByID($key);
    $arUser = $rsUser->Fetch();
    $email = $arUser['EMAIL'];
    $list = '';
    foreach($arItems as $arItem) {
        $list .= html_entity_decode($arItem['NAME'])."\n";
    }
    $arEventFields = array(
        "EMAIL_TO"    => $email,
        "LIST"        => $list,
        "URL"         => $URL,
    );
   CEvent::Send("BP_REMINDER", "s1", $arEventFields);
}



require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>