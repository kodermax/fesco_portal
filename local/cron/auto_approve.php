<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 21.11.2014
 * Time: 13:50
 */
$_SERVER["DOCUMENT_ROOT"] = "/home/bitrix/www/sites/msk/dvmp";
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define("BX_CRONTAB", true);
define('BX_NO_ACCELERATOR_RESET', true);
set_time_limit(0);
//define("LANG", "");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
\Bitrix\Main\loader::includeModule('bizproc');

//Авто согласование для президента
$userId = 8081;
$list = CBPTaskService::GetList(array('ID' => 'DESC'),array('USER_ID' => $userId));
while($row = $list->GetNext())
{

    if($row['ACTIVITY'] == 'ApproveDocsActivity')
    {
        CBPRuntime::SendExternalEvent($row['WORKFLOW_ID'], $row['ACTIVITY_NAME'], array('USER_ID' => $userId, 'APPROVE' => 'president'));
    }
    else if($row['ACTIVITY'] == 'ApproveActivity') {
     //   CBPRuntime::SendExternalEvent($row['WORKFLOW_ID'], $row['ACTIVITY_NAME'], array('USER_ID'=>$userId,'APPROVE'=>true));
    }

}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");

