<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 06.11.2014
 * Time: 18:32
 */

$_SERVER["DOCUMENT_ROOT"] = "/home/bitrix/www/sites/msk/dvmp";
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
set_time_limit(0);
define("LANG", "ru");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('iblock');
$res = CIBlock::GetList(
    Array(),
    Array(
        'ACTIVE'=>'Y',
        "CODE"=>'req_attorneys'
    ), true
);
if($ar_res = $res->Fetch())
{
    $iblockId = $ar_res['ID'];
}
$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>$iblockId, "CODE"=>"STATUS"));
while($enum_fields = $property_enums->GetNext())
{
    $arStatus[$enum_fields['XML_ID']] = $enum_fields['ID'];
}
$arResult = array();
$list = CIBlockElement::GetList(array(),array('IBLOCK_ID' => $iblockId,'PROEPRTY_STATUS' => $arStatus['approve']),false,false,array('ID'));
if($row = $list->GetNext())
{
    $rsSites = CSite::GetByID("s1");
    $arSite = $rsSites->Fetch();
    $URL = 'http://'.$arSite['SERVER_NAME']."/services/requests/attorneys/#/requests_approve";
    $arEventFields = array(
        "URL"         => $URL,
    );
    CEvent::Send("ATTORNEYS_REMINDER", "s1", $arEventFields);
    return;
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>