<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 01.10.2014
 * Time: 12:11
 */
CJSCore::Init(array("jquery"));
$APPLICATION->SetAdditionalCSS("/shared/fesco/css/style.css", true);
$APPLICATION->SetAdditionalCSS("/shared/semantic/semantic.min.css", true);
$APPLICATION->SetAdditionalCSS("/shared/semantic/grid.min.css", true);
$APPLICATION->AddHeadScript("/shared/semantic/semantic.min.js", true);
require_once($_SERVER["DOCUMENT_ROOT"].'/local/classes/fesco/bp/approvedocs.php');
require_once($_SERVER["DOCUMENT_ROOT"].'/local/classes/fesco/tools.php');

\Bitrix\Main\loader::includeModule('fesco');

?>